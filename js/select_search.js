$(document).ready(function(){
    $('.division_materials').hide();
    $('.division_colors').hide();
    $('.selects select').selectbox();

    $('div.checkboxes').on('change','input', function(){
        var mebel_type_value = $('select.mebel_type option:selected').val();
        var mebel_material_value = $('select.materials option:selected').val();
        var mebel_color_value = $('select.colors option:selected').val();
        var width_before = $('.width_before').val();
        var price_low = $('.price_low').val();
        var price_to = $('.price_to').val();

        var akciya =  $('#akciya').prop('checked');
        var is_new = $('#is_new').prop('checked');
        var is_present = $('#is_present').prop('checked');
        var econom = $('#econom').prop('checked');
        var for_guests = $('#for_guests').prop('checked');
        var in_bedroom = $('#in_bedroom').prop('checked');
        var for_cabin = $('#for_cabin').prop('checked');

        $.get("/search/act/getcount/id_type/"+mebel_type_value+"/id_material/"+mebel_material_value+"/id_color/"+mebel_color_value+"/width_before/"+width_before+"/price_low/"+price_low+"/price_to/"+price_to+"/akciya/"+akciya+"/is_present/"+is_present+"/is_new/"+is_new+"/econom/"+econom+"/for_guests/"+for_guests+"/in_bedroom/"+in_bedroom+"/for_cabin/"+for_cabin, {},
            function(data){
                if(data != ''){
                    //alert(data);
                    var obj = jQuery.parseJSON(data);
                    $('.count_tovars').html('');
                    $('.count_tovars').html('Найдено '+obj.count+' товаров');
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
    });

    $('.mebel_type').change(function(){
	arrow();
        var mebel_type_value = $('select.mebel_type option:selected').val();
        var width_before = $('.width_before').val();
        var price_low = $('.price_low').val();
        var price_to = $('.price_to').val();

        var akciya =  $('#akciya').prop('checked');
        var is_new = $('#is_new').prop('checked');
        var is_present = $('#is_present').prop('checked');
        var econom = $('#econom').prop('checked');
        var for_guests = $('#for_guests').prop('checked');
        var in_bedroom = $('#in_bedroom').prop('checked');
        var for_cabin = $('#for_cabin').prop('checked');


        $.get("/search/act/getcount/id_type/"+mebel_type_value+"/width_before/"+width_before+"/price_low/"+price_low+"/price_to/"+price_to+"/akciya/"+akciya+"/is_present/"+is_present+"/is_new/"+is_new+"/econom/"+econom+"/for_guests/"+for_guests+"/in_bedroom/"+in_bedroom+"/for_cabin/"+for_cabin, {},
            function(data){
                if(data != ''){
		    //alert(data);
                    var obj = jQuery.parseJSON(data);
                    $('.materials').html('');
                    $('.selects span').remove();
                    $('.materials').html(obj.material);
                    $('.count_tovars').html('');
                    $('.count_tovars').html('Найдено '+obj.count+' товаров');
                    //$('.input_search').css({marginTop:-201});
                    $('.selects select').selectbox();
                    $('.select').css({width: 80});
		    $('.division_types').children().children('.select').css({width: 173});
		    $('.division_materials').children().children('.select').css({width: 125});
                    $('.division_materials').show();
                    $('.division_colors').hide();
		    $('.division_colors').children('.colors').html('');
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
    });

    $('.materials').change(function(){
	arrow();
        var mebel_type_value = $('select.mebel_type option:selected').val();
        var mebel_material_value = $('select.materials option:selected').val();
        var width_before = $('.width_before').val();
        var price_low = $('.price_low').val();
        var price_to = $('.price_to').val();

        var akciya =  $('#akciya').prop('checked');
        var is_new = $('#is_new').prop('checked');
        var is_present = $('#is_present').prop('checked');
        var econom = $('#econom').prop('checked');
        var for_guests = $('#for_guests').prop('checked');
        var in_bedroom = $('#in_bedroom').prop('checked');
        var for_cabin = $('#for_cabin').prop('checked');

        $.get("/search/act/getcount/id_type/"+mebel_type_value+"/id_material/"+mebel_material_value+"/akciya/"+akciya+"/is_present/"+is_present+"/is_new/"+is_new+"/econom/"+econom+"/for_guests/"+for_guests+"/in_bedroom/"+in_bedroom+"/for_cabin/"+for_cabin, {},
            function(data){
                if(data != ''){
                    var obj = jQuery.parseJSON(data);
                    $('.selects span').remove();
                    $('.colors').html(obj.color);
                    $('.count_tovars').html('');
                    $('.count_tovars').html('Найдено '+obj.count+' товаров');
                    $('.selects select').selectbox();
                    $('.select').css({width: 80});
		    $('.division_types').children().children('.select').css({width: 173});
		    $('.division_materials').children().children('.select').css({width: 125});
                    $('.division_colors').show();
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
    });

    $('.colors').change(function(){
	arrow();
        var mebel_type_value = $('select.mebel_type option:selected').val();
        var mebel_material_value = $('select.materials option:selected').val();
        var mebel_color_value = $('select.colors option:selected').val();
        var width_before = $('.width_before').val();
        var price_low = $('.price_low').val();
        var price_to = $('.price_to').val();

        var akciya =  $('#akciya').prop('checked');
        var is_new = $('#is_new').prop('checked');
        var is_present = $('#is_present').prop('checked');
        var econom = $('#econom').prop('checked');
        var for_guests = $('#for_guests').prop('checked');
        var in_bedroom = $('#in_bedroom').prop('checked');
        var for_cabin = $('#for_cabin').prop('checked');

        $.get("/search/act/getcount/id_type/"+mebel_type_value+"/id_material/"+mebel_material_value+"/id_color/"+mebel_color_value+"/width_before/"+width_before+"/price_low/"+price_low+"/price_to/"+price_to+"/akciya/"+akciya+"/is_present/"+is_present+"/is_new/"+is_new+"/econom/"+econom+"/for_guests/"+for_guests+"/in_bedroom/"+in_bedroom+"/for_cabin/"+for_cabin, {},
            function(data){
                if(data != ''){
                    var obj = jQuery.parseJSON(data);
                    $('.count_tovars').html('');
                    $('.count_tovars').html('Найдено '+obj.count+' товаров');
                    $('.selects select').selectbox();
                    ('.select').css({width: 80});
		    $('.division_types').children().children('.select').css({width: 173});
		    $('.division_materials').children().children('.select').css({width: 125});
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
    });


    $('.width_before').change(function(){
	arrow();
        var mebel_type_value = $('select.mebel_type option:selected').val();
        var mebel_material_value = $('select.materials option:selected').val();
        var mebel_color_value = $('select.colors option:selected').val();
        var width_before = $('input.width_before').val();
        var price_low = $('input.price_low').val();
        var price_to = $('input.price_to').val();

        var akciya =  $('#akciya').prop('checked');
        var is_new = $('#is_new').prop('checked');
        var is_present = $('#is_present').prop('checked');
        var econom = $('#econom').prop('checked');
        var for_guests = $('#for_guests').prop('checked');
        var in_bedroom = $('#in_bedroom').prop('checked');
        var for_cabin = $('#for_cabin').prop('checked');

        $.get("/search/act/getcount/id_type/"+mebel_type_value+"/id_material/"+mebel_material_value+"/id_color/"+mebel_color_value+"/width_before/"+width_before+"/price_low/"+price_low+"/price_to/"+price_to+"/akciya/"+akciya+"/is_present/"+is_present+"/is_new/"+is_new+"/econom/"+econom+"/for_guests/"+for_guests+"/in_bedroom/"+in_bedroom+"/for_cabin/"+for_cabin, {},
            function(data){
                if(data != ''){
                    //alert(data);
                    var obj = jQuery.parseJSON(data);
                    $('.count_tovars').html('');
                    $('.count_tovars').html('Найдено '+obj.count+' товаров');
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
    });

    $('.price_low').change(function(){
	arrow();
        var mebel_type_value = $('select.mebel_type option:selected').val();
        var mebel_material_value = $('select.materials option:selected').val();
        var mebel_color_value = $('select.colors option:selected').val();
        var width_before = $('input.width_before').val();
        var price_low = $('input.price_low').val();
        var price_to = $('input.price_to').val();

        var akciya =  $('#akciya').prop('checked');
        var is_new = $('#is_new').prop('checked');
        var is_present = $('#is_present').prop('checked');
        var econom = $('#econom').prop('checked');
        var for_guests = $('#for_guests').prop('checked');
        var in_bedroom = $('#in_bedroom').prop('checked');
        var for_cabin = $('#for_cabin').prop('checked');

        $.get("/search/act/getcount/id_type/"+mebel_type_value+"/id_material/"+mebel_material_value+"/id_color/"+mebel_color_value+"/width_before/"+width_before+"/price_low/"+price_low+"/price_to/"+price_to+"/akciya/"+akciya+"/is_present/"+is_present+"/is_new/"+is_new+"/econom/"+econom+"/for_guests/"+for_guests+"/in_bedroom/"+in_bedroom+"/for_cabin/"+for_cabin, {},
            function(data){
                if(data != ''){
                    //alert(data);
                    var obj = jQuery.parseJSON(data);
                    $('.count_tovars').html('');
                    $('.count_tovars').html('Найдено '+obj.count+' товаров');
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
    });

    $('.price_to').change(function(){
	arrow();
        var mebel_type_value = $('select.mebel_type option:selected').val();
        var mebel_material_value = $('select.materials option:selected').val();
        var mebel_color_value = $('select.colors option:selected').val();
        var width_before = $('.width_before').val();
        var price_low = $('.price_low').val();
        var price_to = $('.price_to').val();

        var akciya =  $('#akciya').prop('checked');
        var is_new = $('#is_new').prop('checked');
        var is_present = $('#is_present').prop('checked');
        var econom = $('#econom').prop('checked');
        var for_guests = $('#for_guests').prop('checked');
        var in_bedroom = $('#in_bedroom').prop('checked');
        var for_cabin = $('#for_cabin').prop('checked');

        $.get("/search/act/getcount/id_type/"+mebel_type_value+"/id_material/"+mebel_material_value+"/id_color/"+mebel_color_value+"/width_before/"+width_before+"/price_low/"+price_low+"/price_to/"+price_to+"/akciya/"+akciya+"/is_present/"+is_present+"/is_new/"+is_new+"/econom/"+econom+"/for_guests/"+for_guests+"/in_bedroom/"+in_bedroom+"/for_cabin/"+for_cabin, {},
            function(data){
                if(data != ''){
                    //alert(data);
                    var obj = jQuery.parseJSON(data);
                    $('.count_tovars').html('');
                    $('.count_tovars').html('Найдено '+obj.count+' товаров');
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
    });

   $('input:checkbox').each(function(){
	$(this).change(function(){
		arrow();
	})
   });

   var interval = {};
   function arrow() {
        var show = false;
        clearInterval(interval);
        interval = setInterval(function() {
            if (show == false) {
            $('.arrow_image').fadeIn('500');
            show=true;
            }
            else {
                $('.arrow_image').fadeOut('500');
                show=false;
            }
        }, 2000);
    }

});
