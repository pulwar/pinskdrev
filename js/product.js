function setImg(id)
{
    document.getElementById('img').src = '/thumb.php?img=/pics/catalog/product/img/' + id + '&h=320&w=440';
    document.getElementById('link').href = '/pics/catalog/product/big/' + id +'?image';
}
$(document).ready(function(){
    // если нет подсказок по параметрам товаров, то скрыть иконки подсказок
             //при первонаэальной загрузке страницы
    var materialValue = $('#material :selected').val();
    var colorValue = $('#color :selected').val();
    var clrValue = $('#clr :selected').val();
    var decorValue = $('#decor :selected').val();
    var transformersValue = $('#transformers :selected').val();
    
    if($('.mat_block_'+materialValue).length){
        $('#mat_tip_image').show();
    }else{
        $('#mat_tip_image').hide();
    }
    
    if($('.color_block_'+colorValue).length){
        $('#color_tip').show();
    }else{
        $('#color_tip').hide();
    }
    
    if($('.clr_block_'+clrValue).length){
        $('#clr_tip_image').show();
    }else{
        $('#clr_tip_image').hide();
    }
    
    if($('.dec_block_'+decorValue).length){
        $('#decor_tip_image').show();
    }else{
        $('#decor_tip_image').hide();
    }
    
    if($('.trans_block_'+transformersValue).length){
        $('#transform_tip_image').show();
    }else{
        $('#transform_tip_image').hide();
    }
               // при изменении значений селектов
//    $('#material').change(function(){ 
//        var materialValue = $('#material :selected').val();
//        if($('.mat_block_'+materialValue).length){
//            $('#mat_tip_image').show();
//        }else{
//            $('#mat_tip_image').hide();
//        }
//    });
//    
//    $('#color').change(function(){
//        var colorValue = $('#color :selected').val();console.log(colorValue);
//        if($('.color_block_'+colorValue).length){
//            $('#color_tip').show();
//        }else{
//            $('#color_tip').hide();
//        }
//    });
//    
//    $('#clr').change(function(){
//        var clrValue = $('#clr :selected').val();
//        if($('.clr_block_'+clrValue).length){
//            $('#clr_tip_image').show();
//        }else{
//            $('#clr_tip_image').hide();
//        }
//    });
//    
//    $('#decor').change(function(){
//        if($('.dec_block_'+decorValue).length){
//            $('#decor_tip_image').show();
//        }else{
//            $('#decor_tip_image').hide();
//        }
//    });
//    
//    $('#transformers').change(function(){
//        if($('.trans_block_'+transformersValue).length){
//            $('#transform_tip_image').show();
//        }else{
//            $('#transform_tip_image').hide();
//        }
//    });
    // __________________________________________________
    
    var $id_trans = $('select#transformers option:selected').val();
    $("#zakaz_button").attr("id_transformer", $id_trans);
    $('.dd').css({width: 230});
    $("#link").click(function(){
        $(this).attr('rel','group1');
        $("#link2").attr('rel','');
    });
    $("#link2").click(function(){
        $(this).attr('rel','group1');
        $("#link").attr('rel','');
    });

    var img_big = $('.big_img').attr('unit_image');
    $('.imageitem').each(function(){
        if ($(this).attr('id') == img_big) {$(this).attr('rel','')} else {$(this).attr('rel','group1')}
    });

    $("a[zoom]").fancybox({
        'titleShow' : true,
        'titlePosition' : 'inside',
        'transitionIn'	: 'elastic',
        'transitionOut'	: 'elastic',
        'padding' : 0,
        'margin' : 0,
        'centerOnScroll' : true
    });

    var bed_space = $('select#transformers option:selected').val();
    if (bed_space == 0) {
        $('.sleeping_place').hide();
    }

    $("a[img_lit]").click(function(){

        var img = $(this).attr('img');
        var img_big = $(this).attr('big_img');
        var title = $(this).attr("title");
        var unit = $(this).attr("id_unit");
        $("#zakaz_button").attr("id_unit", unit);
        $("#link").attr("title", title);
        $("#link2").attr("title", title);
        $('#img').attr('src','/thumb.php?img=/pics/catalog/product/img/'+img+'&h=320&w=440');

        $('#link').attr('href','/protect.php?img=/pics/catalog/product/big/'+img_big+'&wt=');
        $('#link2').attr('href','/protect.php?img=/pics/catalog/product/big/'+img_big+'&wt=');
        $('.imageitem').each(function(){
            if ($(this).attr('id') == img_big) {$(this).attr('rel','')} else {$(this).attr('rel','group1')}
        });
        $.get("/productunit/id/"+$(this).attr('id_unit')+"/cat/<?=$this->cat?>", {},
            function(data){
                if(data != '' && data != 'false'){
                    $('.additional_tovar_block').html(data);
                    var $unit = img_big.substr(0,4);
                    var $id_transformers = $('select#transformers option:selected').val();
                    if ($id_transformers == 0) {
                        $('.sleeping_place').hide();
                    }
                    else {
                        $('.sleeping_place').show();
                    }
                    $("#zakaz_button").attr("id_transformer", $id_transformers);


                    $.get("/prices/id/"+$unit+"/transformer/"+$id_transformers, {},
                        function(data){
                            if(data != '' && data != 'false'){
                                $('.tovar_price').html(data);
                            }
                        }
                    );

                    $.get("/pricesold/id/"+$unit+"/transformer/"+$id_transformers, {},
                        function(data){
                            if(data){
                                $('.unit_old_price').html(data);
                            }
                            if (data.charAt(1) != '!') {
                                $('.tovar_price').css('paddingTop', '5px');
                            }
                            else {
                                $('.tovar_price').css('padding', '20px');
                            }
                        }
                    );
                    $.get("/delivery/id/"+$unit+"/transformer/"+$id_transformers, {},
                        function(data){
                            if(data != '' && data != 'false'){
                                $('.tovar_delivery').html(data);
                            }
                        }
                    );
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
    });

//    $(document).on( 'change', '#material', function() {
//       
//        var $id_mat = $('select#material option:selected').val();
//        var $id_transformers = $('select#transformers option:selected').val();
//        var $unit=$('select#transformers option:selected').attr('unit');
//        if ($unit === undefined) {
//            $unit = 0;
//        }
//        if ($id_transformers == 0) {
//            $('.sleeping_place').hide();
//        }
//        else {
//            $('.sleeping_place').show();
//        }
//        $("#zakaz_button").attr("id_transformer", $id_transformers);
//        $.get("/prices/id/"+$unit+"/transformer/"+$id_transformers, {},
//            function(data){
//                if(data != '' && data != 'false'){
//                    $('.tovar_price').html(data);
//                }
//            }
//        );
//        $.get("/pricesold/id/"+$unit+"/transformer/"+$id_transformers, {},
//            function(data){
//                if(data){
//                    $('.unit_old_price').html(data);
//                }
//                if (data.charAt(1) != '!') {
//                    $('.tovar_price').css('paddingTop', '5px');
//                }
//                else {
//                    $('.tovar_price').css('padding', '20px');
//                }
//            }
//        );
//        $.get("/productunit/id/"+$(this).attr('id_unit')+"/cat/<?=$this->cat?>"+"/mat/" + $id_mat + "/pr/" + $(this).attr('id_tovar'), {},
//            function(data){
//                if(data != '' && data != 'false'){
//                    $('.additional_tovar_block').html(data);
//                    var $unit=$('select#material option:selected').attr('unit');
//                    var img = $unit+"_img.jpg";
//                    var img_big = $unit+"_img.jpg";
//                    var title = $(this).attr("title");
//                    $("#zakaz_button").attr("id_unit", $unit);
//
//                    $("#link").attr("title", title);
//                    $("#link2").attr("title", title);
//                    $('#img').attr('src','/thumb.php?img=/pics/catalog/product/img/'+img+'&h=320&w=440');
//
//                    $('#link').attr('href','/protect.php?img=/pics/catalog/product/big/'+img_big+'&wt=');
//                    $('#link2').attr('href','/protect.php?img=/pics/catalog/product/big/'+img_big+'&wt=');
//                    $.get("/delivery/id/"+$unit, {},
//                        function(data){
//                            if(data != '' && data != 'false'){
//                                $('.tovar_delivery').html(data);
//                            }
//                        }
//                    );
//                } else {
//                    showAlertWindow('Ошибка','error');
//                }
//            }
//        );
//    });
//
//    $(document).on( 'change', '#color', function() { 
//        var $id_color = $('select#color option:selected').val();
//        var $id_transformers = $('select#transformers option:selected').val();
//        if ($id_transformers === undefined) {
//            $id_transformers = 0;
//        }
//        var $unit=$('select#material option:selected').attr('unit');
//        if ($unit === undefined) {
//            $unit = 0;
//        }
//        if ($id_transformers == 0) {
//            $('.sleeping_place').hide();
//        }
//        else {
//            $('.sleeping_place').show();
//        }
//        $.get("/productunit/id/"+$(this).attr('id_unit')+"/cat/<?=$this->cat?>"+"/color/" + $id_color + "/pr/" + $(this).attr('id_tovar'), {},
//            function(data){
//                if(data != '' && data != 'false'){
//                    $('.additional_tovar_block').html(data);
//                    var $unit=$('select#color option:selected').attr('unit');
//                    var img = $unit+"_img.jpg";
//                    var img_big = $unit+"_img.jpg";
//                    var title = $(this).attr("title");
//                    $("#zakaz_button").attr("id_unit", $unit);
//
//                    $("#link").attr("title", title);
//                    $("#link2").attr("title", title);
//                    $('#img').attr('src','/thumb.php?img=/pics/catalog/product/img/'+img+'&h=320&w=440');
//                    $('#link').attr('href','/protect.php?img=/pics/catalog/product/big/'+img_big+'&wt=');
//                    $('#link2').attr('href','/protect.php?img=/pics/catalog/product/big/'+img_big+'&wt=');
//                    $.get("/delivery/id/"+$unit, {},
//                        function(data){
//                            if(data != '' && data != 'false'){
//                                $('.tovar_delivery').html(data);
//                            }
//                        }
//                    );
//                    $.get("/prices/id/"+$unit+"/transformer/"+$id_transformers, {},
//                        function(data){
//                            if(data != '' && data != 'false'){
//                                $('.tovar_price').html(data);
//                            }
//                        }
//                    );
//                    $.get("/pricesold/id/"+$unit+"/transformer/"+$id_transformers, {},
//                        function(data){
//                            if(data){
//                                $('.unit_old_price').html(data);
//                            }
//                            if (data.charAt(1) != '!') {
//                                $('.tovar_price').css('paddingTop', '5px');
//                            }
//                            else {
//                                $('.tovar_price').css('padding', '20px');
//                            }
//                        }
//                    );
//                } else {
//                    showAlertWindow('Ошибка','error');
//                }
//            }
//        );
//    });
//
//    $(document).on( 'change', '#decor', function() {
//        var $id_decor = $('select#decor option:selected').val();
//        var $id_transformers = $('select#transformers option:selected').val();
//        var $unit=$('select#transformers option:selected').attr('unit');
//        if ($unit === undefined) {
//            $unit = 0;
//        }
//        if ($id_transformers == 0) {
//            $('.sleeping_place').hide();
//        }
//        else {
//            $('.sleeping_place').show();
//        }
//        $("#zakaz_button").attr("id_transformer", $id_transformers);
//        $.get("/prices/id/"+$unit+"/transformer/"+$id_transformers, {},
//            function(data){
//                if(data != '' && data != 'false'){
//                    $('.tovar_price').html(data);
//                }
//            }
//        );
//        $.get("/pricesold/id/"+$unit+"/transformer/"+$id_transformers, {},
//            function(data){
//                if(data){
//                    $('.unit_old_price').html(data);
//                }
//                if (data.charAt(1) != '!') {
//                    $('.tovar_price').css('paddingTop', '5px');
//                }
//                else {
//                    $('.tovar_price').css('padding', '20px');
//                }
//            }
//        );
//        $.get("/productunit/id/"+$(this).attr('id_unit')+"/cat/<?=$this->cat?>"+"/decor/" + $id_decor + "/pr/" + $(this).attr('id_tovar'), {},
//            function(data){
//                if(data != '' && data != 'false'){
//                    $('.additional_tovar_block').html(data);
//                    var $unit=$('select#decor option:selected').attr('unit');
//                    var img = $unit+"_img.jpg";
//                    var img_big = $unit+"_img.jpg";
//                    var title = $(this).attr("title");
//                    $("#zakaz_button").attr("id_unit", $unit);
//
//                    $("#link").attr("title", title);
//                    $("#link2").attr("title", title);
//                    $('#img').attr('src','/thumb.php?img=/pics/catalog/product/img/'+img+'&h=320&w=440');
//                    $('#link').attr('href','/protect.php?img=/pics/catalog/product/big/'+img_big+'&wt=');
//                    $('#link2').attr('href','/protect.php?img=/pics/catalog/product/big/'+img_big+'&wt=');
//                    $.get("/delivery/id/"+$unit, {},
//                        function(data){
//                            if(data != '' && data != 'false'){
//                                $('.tovar_delivery').html(data);
//                            }
//                        }
//                    );
//                } else {
//                    showAlertWindow('Ошибка','error');
//                }
//            }
//        );
//    });
//
//    $(document).on( 'change', '#transformers', function() {
//        var $id_transformers = $('select#transformers option:selected').val();
//        var $unit=$('select#transformers option').attr('unit_id');
//        $("#zakaz_button").attr("id_transformer", $id_transformers);
//        if ($id_transformers == 0) {
//            $('.sleeping_place').hide();
//        }
//        else {
//            $('.sleeping_place').show();
//        }
//        $.get("/prices/id/"+$unit+"/transformer/"+$id_transformers, {},
//            function(data){
//                if(data != '' && data != 'false'){
//                    $('.tovar_price').html(data);
//                }
//            }
//        );
//        $.get("/pricesold/id/"+$unit+"/transformer/"+$id_transformers, {},
//            function(data){
//                if(data){
//                    $('.unit_old_price').html(data);
//                }
//                if (data.charAt(1) != '!') {
//                    $('.tovar_price').css('paddingTop', '5px');
//                }
//                else {
//                    $('.tovar_price').css('padding', '20px');
//                }
//            }
//        );
//    });
//
//    $(document).on( 'change', '#clr', function() {
//        var $id_clr = $('select#clr option:selected').val();
//        var $type = $('select#clr option:selected').attr('just_type');
//        console.log($(this).attr('id_unit'));
//        $.get("/productunit/id/"+$(this).attr('id_unit')+"/cat/<?=$this->cat?>/clr/" + $id_clr + "/pr/" + $(this).attr('id_tovar')+"/type/"+$type, {},
//            function(data){
//                if(data != '' && data != 'false'){
//                    $('.additional_tovar_block').html(data);
//                    var $unit=$('select#clr option:selected').attr('unit');
//                    var img = $unit+"_img.jpg";
//                    var img_big = $unit+"_img.jpg";
//                    var title = $(this).attr("title");
//                    $("#zakaz_button").attr("id_unit", $unit);
//                    $("#link").attr("title", title);
//                    $("#link2").attr("title", title);
//                    $('#img').attr('src','/thumb.php?img=/pics/catalog/product/img/'+img+'&h=320&w=440');
//                    $('#link').attr('href','/protect.php?img=/pics/catalog/product/big/'+img_big+'&wt=');
//                    $('#link2').attr('href','/protect.php?img=/pics/catalog/product/big/'+img_big+'&wt=');
//                    var $id_transformers = $('select#transformers option:selected').val();
//                    $("#zakaz_button").attr("id_transformer", $id_transformers);
//                    $.get("/prices/id/"+$unit+"/transformer/"+$id_transformers, {},
//                        function(data){
//                            if(data != '' && data != 'false'){
//                                $('.tovar_price').html(data);
//                            }
//                        }
//                    );
//                    $.get("/pricesold/id/"+$unit+"/transformer/"+$id_transformers, {},
//                        function(data){
//                            if(data){
//                                $('.unit_old_price').html(data);
//                            }
//                            if (data.charAt(1) != '!') {
//                                $('.tovar_price').css('paddingTop', '5px');
//                            }
//                            else {
//                                $('.tovar_price').css('padding', '20px');
//                            }
//                        }
//                    );
//                    $.get("/delivery/id/"+$unit, {},
//                        function(data){
//                            if(data != '' && data != 'false'){
//                                $('.tovar_delivery').html(data);
//                            }
//                        }
//                    );
//                } else {
//                    alert('Ошибка','error');
//                }
//            }
//        );
//    });

});

$(".zakaz_button").on('click', function(){
    $('.buy').hide();
    $('.zakaz_button').hide();
    $('.in_basket').show();
});



function getMat() {
    var $id_mat = $('select#material option:selected').val();console.log($id_mat);
    $('.mat_block_'+$id_mat).show();
}

function hideMat() {
    var $id_mat = $('select#material option:selected').val();
    $('.mat_block_'+$id_mat).hide();
}

function getDec() {
    var $id_decor = $('select#decor option:selected').val();
    $('.dec_block_'+$id_decor).show();
}

function hideDec() {
    var $id_decor = $('select#decor option:selected').val();
    $('.dec_block_'+$id_decor).hide();
}

function getTrans() {
    var $id_transformers = $('select#transformers option:selected').val();
    $('.trans_block_'+$id_transformers).show();
}

function hideTrans() {
    var $id_transformers = $('select#transformers option:selected').val();
    $('.trans_block_'+$id_transformers).hide();
}

function getColor() {
    var $id_color = $('select#color option:selected').val();
    $('.color_block_'+$id_color).show();
}

function hideColor() {
    var $id_color = $('select#color option:selected').val();
    $('.color_block_'+$id_color).hide();
}

function getClr() {
    var $id_clr = $('select#clr option:selected').val();
    $('.clr_block_'+ $id_clr).show();
}

function hideClr() {
    var $id_clr = $('select#clr option:selected').val();
    $('.clr_block_'+ $id_clr).hide();
}
