function sortByParameter(data,sort_parameter,order){
    $('.sort_select i').removeClass('icon-up');
    $('.sort_select i').addClass('icon-down');

    if($('#'+sort_parameter).attr('data-order') == 'desc'){
        $('#'+sort_parameter).attr({'data-order':'asc'});
        $('#i-'+sort_parameter).removeClass('icon-down');
        $('#i-'+sort_parameter).addClass('icon-up');

    }
    else{
        $('#'+sort_parameter).attr({'data-order':'desc'});
        $('#i-'+sort_parameter).removeClass('icon-up');
        $('#i-'+sort_parameter).addClass('icon-down');
    }

    var sorted_data = '';

    if(sort_parameter == 'id'){
        var novelties = [];
        var ids = [];
        data.forEach(function(item, i, data) {
            if(item.is_new == 1){
                novelties.push(item);
            }
            else{
                ids.push(item);
            }
        });
        ids = ids.sort(function(obj1, obj2) {
            if(order == 'asc'){
                return obj2.id-obj1.id;
            }
            else{
                return obj1.id-obj2.id;
            }
        });
        novelties = novelties.sort(function(obj1, obj2) {
            if(order == 'asc'){
                return obj2.id-obj1.id;
            }
            else{
                return obj1.id-obj2.id;
            }
        });
        if(order == 'asc'){
            sorted_data = novelties.concat(ids);
        }
        else{
            sorted_data = ids.concat(novelties);
        }

        return sorted_data;
    }
    if(sort_parameter == 'price'){
        sorted_data = data.sort(function(obj1, obj2) {
            if(order == 'asc'){
                return obj2.price-obj1.price;
            }
            else{
                return obj1.price-obj2.price;

            }

        });
    }
    if(sort_parameter == 'name'){
        sorted_data = data.sort(function(obj1, obj2) {

            if(order == 'asc'){
                if (obj2.name < obj1.name) return -1;
                if (obj2.name > obj1.name) return 1;

                return 0;
            }
            else{
                if (obj1.name < obj2.name) return -1;
                if (obj1.name > obj2.name) return 1;

                return 0;
            }

        });
    }

    return sorted_data;
}

function generateProductBlock(division_url,product_url,product_image_src,alt_image_title,title_short,promotion_image,price_block){
    var block =
        '<div class="division_tovar_one">\n\
            <div class="division_tovar_one_photo">\n\
                <a href="/cat/'+division_url+'/'+product_url+'">\n\
                    '+promotion_image+'\n\
                </a>\n\
                <center>\n\
                <img height="232" src="'+product_image_src+'" alt="'+alt_image_title+'" />\n\
                </center>\n\
            </div>\n\
            <div class="division_tovar_one_description">\n\
                <div class="division_tovar_one_name">\n\
                    <a href="/cat/'+division_url+'/'+product_url+'">\n\
                        '+title_short+'\n\
                    </a>\n\
                </div>\n\
                <div class="division_tovar_one_price">\n\
                    '+price_block+'\n\
                </div>\n\
            </div>\n\
        </div>';

    return block;
}

function generateContent(spd, container){
    var content = '';

    spd.forEach(function(item, i, spd) {

        if(item.special_offer == 1){
            var promotionImage = '<img class="action" src="/img/action.png" style="width:auto;margin-top: -10px; margin-left: 3px;" alt="Акция"/>';
        }else{
            var promotionImage = '';
        }
        if(item.discount == 0){
            var priceBlock = 'Цена от: <span>'+item.price_format+'  руб.</span>';
        }else if(item.discount == 1){
            var priceBlock = 'Цена со скидкой: <span>'+item.price_format+'  руб.</span>';
        }
        if(item.price_with_tr == 0 && item.price == 0){
            var priceBlock = '<span>Изучение спроса</span>';
        }

        content +=  generateProductBlock(item.division_url,item.product_url,item.product_image_src,item.alt_image_title,item.title_short,promotionImage,priceBlock);
    });

    container.html(content);
}