function slider(){
    var $widthall = 0;
    $('.slider li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl() {

        $("#gallery").jCarouselLite({
            visible: 1,
            btnNext: ".arrowRight",
            btnPrev: ".arrowLeft",
            speed: 600,
            auto: false,
            circular: false
        });
    }
    $(".slider").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider").css('left')))) > $widthaInfo) {
                    $(".slider").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider").css('left') > 0+"px") {
                    $(".slider").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl,100);
}

function slider1(){
    var $widthall = 0;
    $('.slider1 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider1 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl1() {

        $("#gallery1").jCarouselLite({
            visible: 1,
//            visible: 6,
            btnNext: ".arrowRight1",
            btnPrev: ".arrowLeft1",
            speed: 600,
            auto: false,
            circular: false
//            circular: true
        });
    }
    $(".slider1").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider1").css('left')))) > $widthaInfo) {
                    $(".slider1").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery1").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider1").css('left') > 0+"px") {
                    $(".slider1").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery1").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl1,100);
}
function slider2(){
    var $widthall = 0;
    $('.slider2 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider2 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
//    function Sl2() {
//        $("#gallery2").jCarouselLite({
//            visible: 1,
//            scroll: 1,
//            auto: false,
//            circular: false,
//            btnNext: ".arrowRight2",
//            btnPrev: ".arrowLeft2"
//        });
//    }
//    $(".slider2").draggable({
//        axis: "x",
//        start: function(event) {
//            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
//            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
//            start = {
//                coords: [ data.pageX, data.pageY ]
//            };
//        },
//        stop: function(event) {
//            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
//            stop = {
//                coords: [ data.pageX, data.pageY ]
//            };
//            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
//            function moveLeft() {
//                var countSlides = 1;
//                if (Math.abs(parseInt(($(".slider2").css('left')))) > $widthaInfo) {
//                    $(".slider2").animate({
//                        left: -$widthaInfo+'px'
//                    },300)
//                }
//                $("#gallery2").trigger("go", "+=" + 1);
//            }
//            function moveRight() {
//                if ($(".slider2").css('left') > 0+"px") {
//                    $(".slider2").animate({
//                        left: '0px'
//                    },300)
//                }
//                var countSlides = 1;
//                var slideWidth = $(event.target).find("li:first").width();
//
//                countSlides = Math.round(dragDelta() / slideWidth) + 1;
//
//                $("#gallery2").trigger("go", "-=" + 1);
//            }
//            function dragDelta() {
//                return Math.abs((start.coords[0] - stop.coords[0]));
//            }
//        }
//    });
//    setTimeout(Sl2,100);
}

function slider3(){
    var $widthall = 0;
    $('.slider3 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider3 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl3() {
        $("#gallery3").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight3",
            btnPrev: ".arrowLeft3"
        });
    }
    $(".slider3").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider3").css('left')))) > $widthaInfo) {
                    $(".slider3").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery3").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider3").css('left') > 0+"px") {
                    $(".slider3").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery3").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl3,100);
}

function slider4(){
    var $widthall = 0;
    $('.slider4 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider4 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl4() {
        $("#gallery4").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight4",
            btnPrev: ".arrowLeft4"
        });
    }
    $(".slider4").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider4").css('left')))) > $widthaInfo) {
                    $(".slider4").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery4").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider4").css('left') > 0+"px") {
                    $(".slider4").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery4").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl4,100);
}

function slider5(){
    var $widthall = 0;
    $('.slider5 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider5 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl5() {
        $("#gallery5").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight5",
            btnPrev: ".arrowLeft5"
        });
    }
    $(".slider5").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider5").css('left')))) > $widthaInfo) {
                    $(".slider5").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery5").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider5").css('left') > 0+"px") {
                    $(".slider5").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery5").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl5,100);
}

function slider6(){
    var $widthall = 0;
    $('.slider6 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider6 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl6() {
        $("#gallery6").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight6",
            btnPrev: ".arrowLeft6"
        });
    }
    $(".slider6").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider6").css('left')))) > $widthaInfo) {
                    $(".slider6").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery6").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider6").css('left') > 0+"px") {
                    $(".slider6").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery6").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl6,100);
}

function slider7(){
    var $widthall = 0;
    $('.slider7 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider7 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl7() {
        $("#gallery7").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight7",
            btnPrev: ".arrowLeft7"
        });
    }
    $(".slider7").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider7").css('left')))) > $widthaInfo) {
                    $(".slider7").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery7").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider7").css('left') > 0+"px") {
                    $(".slider7").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery7").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl7,100);
}

function slider8(){
    var $widthall = 0;
    $('.slider8 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider8 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl8() {
        $("#gallery8").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight8",
            btnPrev: ".arrowLeft8"
        });
    }
    $(".slider8").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider8").css('left')))) > $widthaInfo) {
                    $(".slider8").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery8").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider8").css('left') > 0+"px") {
                    $(".slider8").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery8").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl8,100);
}

function slider9(){
    var $widthall = 0;
    $('.slider9 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider9 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl9() {
        $("#gallery9").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight9",
            btnPrev: ".arrowLeft9"
        });
    }
    $(".slider9").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider9").css('left')))) > $widthaInfo) {
                    $(".slider9").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery9").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider9").css('left') > 0+"px") {
                    $(".slider9").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery9").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl9,100);
}


function slider10(){
    var $widthall = 0;
    $('.slider10 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider10 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl10() {
        $("#gallery10").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight10",
            btnPrev: ".arrowLeft10"
        });
    }
    $(".slider10").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider10").css('left')))) > $widthaInfo) {
                    $(".slider10").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery10").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider10").css('left') > 0+"px") {
                    $(".slider10").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery10").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl10,100);
}

function slider11(){
    var $widthall = 0;
    $('.slider11 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider11 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl11() {
        $("#gallery11").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight11",
            btnPrev: ".arrowLeft11"
        });
    }
    $(".slider11").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider11").css('left')))) > $widthaInfo) {
                    $(".slider11").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery11").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider11").css('left') > 0+"px") {
                    $(".slider11").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery11").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl11,100);
}


function slider12(){
    var $widthall = 0;
    $('.slider12 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider12 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl12() {
        $("#gallery12").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight12",
            btnPrev: ".arrowLeft12"
        });
    }
    $(".slider12").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider12").css('left')))) > $widthaInfo) {
                    $(".slider12").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery12").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider12").css('left') > 0+"px") {
                    $(".slider12").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery12").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl12,100);
}

function slider13(){
    var $widthall = 0;
    $('.slider13 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider13 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl13() {
        $("#gallery13").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight13",
            btnPrev: ".arrowLeft13"
        });
    }
    $(".slider13").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider13").css('left')))) > $widthaInfo) {
                    $(".slider13").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery13").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider13").css('left') > 0+"px") {
                    $(".slider13").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery13").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl13,100);
}

function slider14(){
    var $widthall = 0;
    $('.slider14 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider14 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl14() {
        $("#gallery14").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight14",
            btnPrev: ".arrowLeft14"
        });
    }
    $(".slider14").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider14").css('left')))) > $widthaInfo) {
                    $(".slider14").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery14").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider14").css('left') > 0+"px") {
                    $(".slider14").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery14").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl14,100);
}

function slider15(){
    var $widthall = 0;
    $('.slider15 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider15 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl15() {
        $("#gallery15").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight15",
            btnPrev: ".arrowLeft15"
        });
    }
    $(".slider15").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider15").css('left')))) > $widthaInfo) {
                    $(".slider15").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery15").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider15").css('left') > 0+"px") {
                    $(".slider15").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery15").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl15,100);
}

function slider16(){
    var $widthall = 0;
    $('.slider16 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider16 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl16() {
        $("#gallery16").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight16",
            btnPrev: ".arrowLeft16"
        });
    }
    $(".slider16").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider16").css('left')))) > $widthaInfo) {
                    $(".slider16").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery16").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider16").css('left') > 0+"px") {
                    $(".slider16").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery16").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl16,100);
}

function slider17(){
    var $widthall = 0;
    $('.slider17 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider17 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl17() {
        $("#gallery17").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight17",
            btnPrev: ".arrowLeft17"
        });
    }
    $(".slider17").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider17").css('left')))) > $widthaInfo) {
                    $(".slider17").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery17").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider17").css('left') > 0+"px") {
                    $(".slider17").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery17").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl17,100);
}

function slider18(){
    var $widthall = 0;
    $('.slider18 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider18 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl18() {
        $("#gallery18").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight18",
            btnPrev: ".arrowLeft18"
        });
    }
    $(".slider18").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider18").css('left')))) > $widthaInfo) {
                    $(".slider18").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery18").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider18").css('left') > 0+"px") {
                    $(".slider18").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery18").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl18,100);
}

function slider19(){
    var $widthall = 0;
    $('.slider19 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider19 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl19() {
        $("#gallery19").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight19",
            btnPrev: ".arrowLeft19"
        });
    }
    $(".slider19").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider19").css('left')))) > $widthaInfo) {
                    $(".slider19").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery19").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider19").css('left') > 0+"px") {
                    $(".slider19").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery19").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl19,100);
}

function slider20(){
    var $widthall = 0;
    $('.slider20 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider20 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl20() {
        $("#gallery20").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight20",
            btnPrev: ".arrowLeft20"
        });
    }
    $(".slider20").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider20").css('left')))) > $widthaInfo) {
                    $(".slider20").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery20").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider20").css('left') > 0+"px") {
                    $(".slider20").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery20").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl20,100);
}

function slider21(){
    var $widthall = 0;
    $('.slider21 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider21 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;
    function Sl21() {
        $("#gallery21").jCarouselLite({
            visible: 1,
            auto: false,
            circular: false,
            btnNext: ".arrowRight21",
            btnPrev: ".arrowLeft21"
        });
    }
    $(".slider21").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($(".slider21").css('left')))) > $widthaInfo) {
                    $(".slider21").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery21").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($(".slider21").css('left') > 0+"px") {
                    $(".slider21").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery21").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl21,100);
}

/*
function slider22(){
    var $widthall = 0;
    $('.slider22 li').each(function(){$widthall += 1;});
    var $widthaOne = $('.slider2 li').width();
    var $widthaInfo = ($widthall*$widthaOne)-$widthaOne;

    var start, stop;

    function Sl22() {
        $("#gallery_spec").jCarouselLite({
            visible: 1,
            btnNext: ".arrowRight_s",
            btnPrev: ".arrowLeft_s",
            speed: 600,
            auto: false,
            scroll: 1,
            circular: false
        });
    }

    // new

    $("#gallery_spec").draggable({
        axis: "x",
        start: function(event) {
            //здесь мы определяем событие перетаскивания. Либо это пальцем, либо мышью
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            start = {
                coords: [ data.pageX, data.pageY ]
            };
        },
        stop: function(event) {
            var data = event.originalEvent.touches ? event.originalEvent.touches[0] : event;
            stop = {
                coords: [ data.pageX, data.pageY ]
            };
            start.coords[0] > stop.coords[0] ? moveLeft() : moveRight();
            function moveLeft() {
                var countSlides = 1;
                if (Math.abs(parseInt(($("#gallery_spec").css('left')))) > $widthaInfo) {
                    $("#gallery_spec").animate({
                        left: -$widthaInfo+'px'
                    },300)
                }
                $("#gallery_spec").trigger("go", "+=" + 1);
            }
            function moveRight() {
                if ($("#gallery_spec").css('left') > 0+"px") {
                    $("#gallery_spec").animate({
                        left: '0px'
                    },300)
                }
                var countSlides = 1;
                var slideWidth = $(event.target).find("li:first").width();

                countSlides = Math.round(dragDelta() / slideWidth) + 1;

                $("#gallery_spec").trigger("go", "-=" + 1);
            }
            function dragDelta() {
                return Math.abs((start.coords[0] - stop.coords[0]));
            }
        }
    });
    setTimeout(Sl22,100);
}
*/