$(document).ready(function() {
   $("body").on('click', '#btnPrint', function () {

       var contents = "<h3>" + $('.fast_buy_form h1').html() + "</h3>";
       if($('#form_phone').val()!='') {
           contents += "<h3>Номер договора: "+$('#form_phone').val()+"</h3>";
       }
       if($('#form_name').val()!='') {
           contents += "<h3>ФИО: "+$('#form_name').val()+"</h3>";
       }
       if($('#form_email').val()!='') {
           contents += "<h3>email: "+$('#form_email').val()+"</h3>";
       }
       var frame1 = $('<iframe />');
       frame1[0].name = "frame1";
       frame1.css({ "position": "absolute", "top": "-1000000px" });
       $("body").append(frame1);
       var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
       frameDoc.document.open();
       //Create a new HTML document.
       frameDoc.document.write('<html><head><title>Пинскдрев</title>');
       frameDoc.document.write('</head><body>');
       //Append the external CSS file.
       frameDoc.document.write('<link href="css/print.css" rel="stylesheet" type="text/css" />');
       //Append the DIV contents.
       frameDoc.document.write(contents);
       frameDoc.document.write('</body></html>');
       frameDoc.document.close();
       setTimeout(function () {
           window.frames["frame1"].focus();
           window.frames["frame1"].print();
           frame1.remove();
       }, 500);
       return false;
   });
});

