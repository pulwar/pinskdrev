(function($){
	$.fn.customSlider = function(o) {
		return this.each(function() {
			new $.customSlider(this,o);
		});
	};
	
	
	var defaults = {
		optionsClass: 'showarea',		
		loop:false,
		fade_speed: 500	
	};
	
	$.customSlider = function(el,o) {	
		var self = this;		
		this.o = $.extend({}, defaults, o || {});		
		this.$list = $(el);		
		self.init();
	};
	
	$.customSlider.fn = $.customSlider.prototype = {};
	$.customSlider.fn.extend = $.customSlider.extend = $.extend;
	
	
	$.customSlider.fn.extend({
		init: function() {					
			this.$items = this.$list.children("."+this.o.optionsClass);	
			this.$total = $(this.$items).length;
			this.$index = 1;
			
			
			$(this.$items).hide();
			$(this.$items[this.$index-1]).fadeIn(this.o.fade_speed);			
		
					
			var self = this;
			
			this.funcMoveBack = function() { self.moveBack(); };
			this.funcMoveForward = function() { self.moveForward(); };
			this.funcCheckIndex = function() { self.checkIndex(); };
			
			this.$btnForward = $('.rightarrow',this.$list).click(this.funcMoveForward);
			this.$btnBack = $('.leftarrow',this.$list).click(this.funcMoveBack);
			this.$btnBack.addClass('prev_off');
			if(this.$total == this.$index) this.$btnForward.addClass('next_off');
			
			
		},
		moveForward: function() {
			var self = this;			
			this.$index = this.$index+1;
			self.funcCheckIndex();
			if(this.$index<=this.$total){
				
				var current_item = $(this.$items[this.$index-1]);
				this.$items.hide();
				$(current_item).fadeIn(this.o.fade_speed);
					
			}			
		
		},
		moveBack: function() {
		
			var self = this;			
			this.$index = this.$index-1;			
			self.funcCheckIndex();			
			if(this.$index>0 ){				
				var current_item = $(this.$items[this.$index-1]);
				this.$items.hide();
				$(current_item).fadeIn(this.o.fade_speed);					
			}
			
		
		},
		
		checkIndex: function(){		
			
			if(!this.o.loop) {		
				if(this.$index<=1){
					this.$index = 1;
					this.$btnBack.addClass('prev_off');
				} else {
					this.$btnBack.removeClass('prev_off');
				}
				if(this.$index>=this.$total){
					this.$index = this.$total;
					this.$btnForward.addClass('next_off');
					
				} else{
					this.$btnForward.removeClass('next_off');
				}
			} else {
				if(this.$index<=0){
					this.$index = this.$total;					
				}
				if(this.$index>this.$total){
					this.$index = 1;
				} 
			}
		}
	
	
	
	
	
	
	
	});
	
				
})(jQuery);