$(document).ready(function() {

    function parseGetParams() {
        var $_GET = {};
        var __GET = window.location.search.substring(1).split("&");
        for(var i=0; i<__GET.length; i++) {
            var getVar = __GET[i].split("=");
            $_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1];
        }
        return $_GET;
    }

    var AllParams = parseGetParams();
    var type = AllParams.type;
    var materials = AllParams.materials;
    var colors = AllParams.colors;
    var width = AllParams.width;
    var price_from = AllParams.price_from;
    var price_to = AllParams.price_to;

    var akciya =  $('#akciya').prop('checked');
    var is_new = $('#is_new').prop('checked');
    var is_present = $('#is_present').prop('checked');
    var econom = $('#econom').prop('checked');
    var for_guests = $('#for_guests').prop('checked');
    var in_bedroom = $('#in_bedroom').prop('checked');
    var for_cabin = $('#for_cabin').prop('checked');

    if (type != 0) {
        $.get("/search/act/getcount/id_type/"+type+"/akciya/"+akciya+"/is_present/"+is_present+"/is_new/"+is_new+"/econom/"+econom+"/for_guests/"+for_guests+"/in_bedroom/"+in_bedroom+"/for_cabin/"+for_cabin, {},
            function(data){
                //console.log(24);
                if(data != ''){
                    //console.log(data);
                    var obj = jQuery.parseJSON(data);
                    $('.materials').html('');
                    $('.selects span').remove();
                    $('.materials').html(obj.material);
                    $('.materials option[value="'+materials+'"]').attr("selected","selected");
                    if (materials == 0) {
                    $('.count_tovars').html('');
                    $('.count_tovars').html('Найдено '+obj.count+' товаров');
                    }
                    $('select').selectbox();
                    $('.select').css({width: 80});
		    $('.division_types').children().children('.select').css({width: 173});
		    $('.division_materials').children().children('.select').css({width: 125});
                    $('.division_materials').show();
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
    }

    if (materials != 0 && type !=0) {
        var akciya =  $('#akciya').prop('checked');
        var is_new = $('#is_new').prop('checked');
        var is_present = $('#is_present').prop('checked');
        var econom = $('#econom').prop('checked');
        var for_guests = $('#for_guests').prop('checked');
        var in_bedroom = $('#in_bedroom').prop('checked');
        var for_cabin = $('#for_cabin').prop('checked');

        $.get("/search/act/getcount/id_type/"+type+"/id_material/"+materials+"/akciya/"+akciya+"/is_present/"+is_present+"/is_new/"+is_new+"/econom/"+econom+"/for_guests/"+for_guests+"/in_bedroom/"+in_bedroom+"/for_cabin/"+for_cabin, {},
            function(data){
                //console.log(51);
                if(data != ''){
                    //console.log(data);
                    var obj = jQuery.parseJSON(data);
                    $('.selects span').remove();
                    $('.colors').html(obj.color);
                    $('.colors option[value="'+colors+'"]').attr("selected","selected");
                    if (colors == 0) {
                    $('.count_tovars').html('');
                    $('.count_tovars').html('Найдено '+obj.count+' товаров');
                    }
                    $('select').selectbox();
                    $('.select').css({width: 80});
		    $('.division_types').children().children('.select').css({width: 173});
		    $('.division_materials').children().children('.select').css({width: 125});
                    $('.division_colors').show();
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
    }

    if (type !=0 && materials != 0 && colors != 0) {
        var akciya =  $('#akciya').prop('checked');
        var is_new = $('#is_new').prop('checked');
        var is_present = $('#is_present').prop('checked');
        var econom = $('#econom').prop('checked');
        var for_guests = $('#for_guests').prop('checked');
        var in_bedroom = $('#in_bedroom').prop('checked');
        var for_cabin = $('#for_cabin').prop('checked');

        $.get("/search/act/getcount/id_type/"+type+"/id_material/"+materials+"/id_color/"+colors+"/akciya/"+akciya+"/is_present/"+is_present+"/is_new/"+is_new+"/econom/"+econom+"/for_guests/"+for_guests+"/in_bedroom/"+in_bedroom+"/for_cabin/"+for_cabin, {},
            function(data){
                //console.log(77);
                if(data != ''){
                    var obj = jQuery.parseJSON(data);
                    $('.count_tovars').html('');
                    $('.count_tovars').html('Найдено '+obj.count+' товаров');
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
    }

    check_width = width % 1;
    if (check_width == 0) {
        $('.width_before').val(width);
    }
    else {
        $('.width_before').val('Ширина до');
    }
    check_price_from = price_from % 1;
    if (check_price_from == 0) {
        $('.price_low').val(price_from);
    }
    else {
        $('.price_low').val('Цена от');
    }
    check_price_to = price_to % 1;
    if (check_price_to == 0) {
        $('.price_to').val(price_to);
    }
    else {
        $('.price_to').val('Цена до');
    }

        var akciya =  $('#akciya').prop('checked');
        var is_new = $('#is_new').prop('checked');
        var is_present = $('#is_present').prop('checked');
        var econom = $('#econom').prop('checked');
        var for_guests = $('#for_guests').prop('checked');
        var in_bedroom = $('#in_bedroom').prop('checked');
        var for_cabin = $('#for_cabin').prop('checked');

        $.get("/search/act/getcount/id_type/"+type+"/id_material/"+materials+"/id_color/"+colors+"/width_before/"+width+"/price_low/"+price_from+"/price_to/"+price_to+"/akciya/"+akciya+"/is_present/"+is_present+"/is_new/"+is_new+"/econom/"+econom+"/for_guests/"+for_guests+"/in_bedroom/"+in_bedroom+"/for_cabin/"+for_cabin, {},
            function(data){
                //console.log(113);
                if(data != ''){
                    var obj = jQuery.parseJSON(data);
                    $('.count_tovars').html('');
                    $('.count_tovars').html('Найдено '+obj.count+' товаров');
                } else {
                    showAlertWindow('Ошибка','error');
                }
            }
        );
	


});
