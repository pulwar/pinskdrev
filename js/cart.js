$(document).ready(function() {
	// add product to cart
		$("#add_to_cart_flash").click(
				function() {
					var id = $(this).attr("prod_id") * 1;
					if (id > 0) {
						var config = getConfiguration();
						$.post("/catalog/index/saveconfig/tovar/" + id + "/", {
							conf : config
						}, function(data) {
							// alert(data);
								if (data != 'err') {

								} else {
									alert("Ошибка");
								}

							});

						$.get("/catalog/cart/add/", {
							prod_id : id
						}, function(data) {
							if (data == 'ok') {
								$("#product_status_text").html(
										"Товар добавлен в корзину");
							} else {
								$("#product_status_text").html(
										"Товар уже есть в корзине.");
							}
							$("#product_status").show();
						}

						);
					}

					return false;
				});

		$("#add_to_cart").click(
				function() {
					var id = $(this).attr("prod_id") * 1;
					if (id > 0) {
						$.get("/catalog/cart/add/", {
							prod_id : id
						}, function(data) {
							if (data == 'ok') {
								$("#product_status_text").html(
										"Товар добавлен в корзину");
							} else {
								$("#product_status_text").html(
										"Товар уже есть в корзине.");
							}
							$("#product_status").show();
						}

						);
					}

					return false;
				});

		// delete product from cart
		$("a[delete_from_cart='true']").click(function() {
			var id = $(this).attr("prod_id") * 1;
			// alert(id);
				if (id > 0) {
					$.get("/catalog/cart/delete/", {
						prod_id : id
					}, function(data) {
						// alert("Data Loaded: " + data);
							// reload cart
							$("#cart_preview").load("/catalog/cart/preview/");
							/*
							 * if(data=='ok'){ $("#removed").slideDown(1000,
							 * function(){ $("#removed").fadeOut(700);
							 * 
							 * }); }
							 */

						});
				}
				return false;
			});

		// clear cart

		$("a[clear_cart='true']").click(function() {
			$.get("/catalog/cart/clear/", function(data) {
				// alert("Data Loaded: " + data);
					// reload cart
					$("#cart_preview").load("/catalog/cart/preview/");
					/*
					 * if(data=='ok'){ $("#clear").slideDown(1000, function(){
					 * $("#clear").fadeOut(700);
					 * 
					 * }); }
					 */

				}

			);
			return false;
		});

	});
