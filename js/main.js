$(document).ready(function() {
    $('#give_me_coupon').on('click', function () {
        if($('form#get_coupom_for_me input[name="form[number]"]').val()=='') {
            $('.fast_buy_form span.alert').text('Введите номер договора!');
        } else {
            $.ajax({
                    method: 'post',
                    url: '/coupon',
                    data: $('form#get_coupom_for_me').serialize(),
                    datatype: 'json',
                    success: function (response) {
                        console.log(response);
                        if(response==0) {
                            $('.fast_buy_form span.alert').text('Посчитайте выражение верно!')
                        } else if(response==-1){
                            $('.fast_buy_form span.alert').text('Не более 1 купона на один договор в течении 15 минут!')
                        } else {
                            $('.fast_buy_form div.contacts_form').hide();
                            $('.fast_buy_form h1').html('Номер Вашего скидочного купона: ' + response);

                            $('.fast_buy_form h1').append('<a href="#" id="btnPrint"><img src="/print-128.png" style="height:14px;margin-left: 5px;"></a>');

                        }
                    }
                })
                .done(function() {

                });
        }

    });

    $('.get_coupon').click(function(){
        $('.fast_buy_form').fadeIn(1500);
        return false;
    });

    $('.toggle-menu-btn').click(function () {
        $(this).toggleClass('active');
        $('.menu_top').slideToggle(450);
    });

    $('.toggle-search-btn').click(function () {
        $(this).toggleClass('active');
        $('.search_form').toggleClass('opened');
    });

    $('.mob_menu').click(function () {
        $(this).toggleClass('active');
        $('.mob_menu_inner').slideToggle(450);
    });

    $('.mob_menu_close').click(function () {
        $('.mob_menu_inner').slideToggle(450);
    });

    $('.mob_menu_close_1').click(function () {
        $('.menu_top').slideToggle(450);
    });

    $('.menu_li').each(function () {
        var self = $(this);
        if (self.find('.submenu').length>0){
            self.addClass("more_menu");
        }
    });

    $('.h_mob_search').click(function () {
        $(this).toggleClass('active');
        $('.h_search').slideToggle(450);
    });

    $('.gallery_new').slick({
        slidesToShow: 6
    });

    $('.gallery_main').slick({
        //slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 6000,
        dots: true,
        fade: true,
        speed:1600
    });

    // hide #back-top first
    $("#back-top").hide();

    // fade in #back-top
    //$(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#back-top').fadeIn();
        } else {
            $('#back-top').fadeOut();
        }
    });

    // scroll body to 0px on click
    $('#back-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    $('.see_more_table').click(function () {
        $(".product_table").toggleClass("see_all");
    });

    $('.calc_select select').selectbox();

    var isiPad = navigator.userAgent.match(/iPad/i) != null;
    if(isiPad){
        $('body').addClass('ipad');
    }

    var numbTr =$(".product_table").height();
    if (numbTr < 498) {
        $('.see_more_table').hide();
    } else {
        $('.menuBt').show();
    }

    $('.listing_slider_cell').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 6,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    infinite: true
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true
                }
            },
            {
                breakpoint: 840,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 460,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    update_cart_count();
    //update_comp_count();

    $(".zakaz_button").on('click', function(){
        if(document.all != null)
        {
            var YOffset = document.body.scrollTop;
            var width = document.body.clientWidth;
            var height = document.body.clientHeight;
        }
        else
        {
            var YOffset = window.pageYOffset;
            var width = window.innerWidth;
            var height = window.innerHeight;
        }
//        $('.product_order').css({top: (height - 200)/2 + YOffset});
        $('.product_order').fadeIn(1500);
    });

    $('.product_order_button_2').click(function () {
        $('.product_order').fadeOut(1500);
    })
    $('.close').click(function () {
        $('.product_order').fadeOut(1500);
    })


    $('.fast_buy').click(function(){
        if(document.all != null)
        {
            var YOffset = document.body.scrollTop;
            var width = document.body.clientWidth;
            var height = document.body.clientHeight;
        }
        else
        {
            var YOffset = window.pageYOffset;
            var width = window.innerWidth;
            var height = window.innerHeight;

        }
        var image = $('.big_img').attr('src');
        $('.count_products').html('1');
        $('.fast_buy_img').attr('src', image);
        var price = $('.tovar_price').html();
        $('.fast_buy_total_price').html(price);
//        $('.fast_buy_form').css({top: (height - 610)/2 + YOffset, left: (width - 718)/2});
        $('.fast_buy_form').fadeIn(1500);
    });

    $('.fast_buy_plus').click(function(){
        var value = $('.count_products').html();
        var price_for = $('.fast_buy_total_price').html();
        var price_for = price_for.replace(/\s+/g," ");
        if ( price_for == ' Изучение спроса ') {
            return false;
        }
        value++;
        $('.count_products').html(value);
        var price = $('.tovar_price').html();
        price = price.replace(/<!--(.*?)-->/, "");
        var numbers = parseInt(price.replace(/\D+/g,""));
        var total_price = numbers*value+'';
        total_price = total_price.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        $('.fast_buy_total_price').html(total_price + " руб.");
    });

    $('.fast_buy_minus').click(function(){
        var value = $('.count_products').html();
        var price_for = $('.fast_buy_total_price').html();
        var price_for = price_for.replace(/\s+/g," ");
        if ( price_for == ' Изучение спроса ') {
            return false;
        }
        if (value != 1) {
            value--;
        }
        $('.count_products').html(value);
        var price = $('.tovar_price').html();
        price = price.replace(/<!--(.*?)-->/, "");
        var numbers = parseInt(price.replace(/\D+/g,""));
        var total_price = numbers*value+'';
        total_price = total_price.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        $('.fast_buy_total_price').html(total_price + " руб.");
    });

    $('.fast_buy_close').click(function(){
        $('.fast_buy_form').fadeOut(1500);
    });

    $("#send_fast_zakaz").click(function(){
        var name, city, street, house, phone, mail, captcha;
        var result;
        var error = [];
        var checking;
        $('.error_message').html('');
        name = $('#form_name').val();
        city = $('#form_city').val();
        street = $('#form_street').val();
        house = $('#form_house').val();
        phone = $('#form_phone').val();
        mail = $('#form_email').val();
        captcha = $('#form-captcha').val();
        checking = $('#send_fast_zakaz').attr('checking');
        //result = ('.ans_ss').html();
        //alert(result);
        if (name == '') {
            error.push('Не заполнено имя');
        }
        if (phone == '') {
            error.push('Не заполнен телефон');
        }
        //if (captcha != (cpt_result/2)){
        //    error.push('Неправильно посчитано выражение');
        //}
        if (captcha == '') {
            error.push('Не заполнена капча');
        }
        if (error.length > 0) {
            $.each(error, function(){
                $('.error_message').append(this + '<br>');
            });
            return false;
        }
        var id_unit = $('.zakaz_button').attr('id_unit');
        $('#id_unit_form').val(id_unit);
        var $id_transformers = $('select#transformers option:selected').val();
        $('#transformer_form').val($id_transformers);
        var price = $('.fast_buy_total_price').html();
        $('#total_price_form').val(price);
        var count = $('.count_products').html();
        $('#count_products_form').val(count);
        if (checking == 1) {
            var msg   = $('#zakaz').serialize();
            $('#send_fast_zakaz').attr('checking', 0);
            $.ajax({
                type: 'POST',
                url: '/cart/act/fastbuy',
                data: msg,
                success: function(data) {

                    var arr_url = location.href.split('/');
                    window.location = arr_url[0]+"//"+arr_url[2]+"/cart/fast_complete";

                },
                error:  function(xhr, str){
                    $('#send_fast_zakaz').attr('checking', 1);
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
        }
        return false;
    });

    $(".open_zv").on('click', function () {
        $(".form_zvonok").toggle();
    });

    $("#call_button").click(function(){
        var process = $(this).attr('process');
        if(process == 0){
            var btn = $(this);
            btn.attr('process','1');
            var fio = $('#fio').val();
            var tel = $('#tel').val();
            var comment = $('#comment').val();
            var cpt = $('#answer').val();
            var answer = $('#zcaptcha').val();
            $.get("/callback", { fio: fio,  tel:tel, comment:comment, cpt:cpt, answer:answer},
                function(data){
                    if(data=='ok'){
                        $('#error_message').html('Спасибо, Ваше сообщение отправлено. В ближайшее время с Вами свяжется один из наших менежеров.');
                        $('#call_button').css('display','none');
                    } else {
                        $('#error_message').html('Произошла ошибка. Попробуйте позже.');
                        btn.attr('process','0');
                    }
                }
            );
        }
    });

    $("#close_popup").on('click', function(){
        $(".popup").slideUp('slow');
    });

    // каталог товаров
    // добавление товара в корзину
    $(".zakaz_button").on('click', function(){
        //var id_product = $(this).attr('id_product');
        var id_unit = $(this).attr('id_unit');
        var division = $(this).attr('division');
        var id_transformers = $(this).attr('id_transformer');
        $.get("/cart", {
                act: 'add',
                //id_product: id_product,
                id_unit: id_unit,
                division: division,
                id_transformers: id_transformers
            },
            function(data){
                if(data=='ok'){
                    //var popupY = $(document).scrollTop() + Math.round($(window).height()/2) - Math.round($(".popup").height()/2);
                    //$(".popup").css('top', popupY);
                    //$(".popup").slideDown("slow");
                    update_cart_count();
                    //console.log(data);
                } else {
                    alert('Ошибка','error');
                }
            });
        return false;
    });

    // изменение количества товара в корзине
    $("input[id_unit]").on('change', function (){
        var id_unit = $(this).attr('id_unit');
        var count = $(this).val();
        $.get("/cart", {
                act: 'change',
                id_unit: id_unit,
                count: count
            },
            function(data){
                if(data=='ok'){
                    recalc_cart();
                    update_cart_count();
                } else {
                    alert("Ошибка!");
                }
            });
    });

    // удаление товара из корзины
    $("a[delete]").on('click', function(){
        var id_unit = $(this).attr('delete');
        var row_id = $(this).attr('row');
        var a = $(this);
        $.get("/cart", {
                act: 'remove',
                id_unit: id_unit
            },
            function(data){
                if(data > 0){
                    $("#"+row_id).remove();
                    recalc_cart();
                    update_cart_count();
                } else {
                    location.href='/cart';
                }
            });
        return false;
    });

    $("#recalc").on('click', function(){
        recalc_cart();
        return false;
    });

    function formatPrice(p) {
        p = p.toFixed(0);
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(p)) {
            p = p.replace(rgx, '$1' + ' ' + '$2');
        }
        return p;
    }

    function update_cart_count(){
        $.get("/cart", {
                act: 'recalc'
            },
            function(data){
                if( data != 'error' && data > 0){
                    $(".basket_sub").html('');
                    $(".basket_sub").append('<a href="/cart" title="Корзина"><span>Корзина :</span> <i>'+data+'</i></a>');
                }
                else {
                    $(".basket_sub").html('');
                    $(".basket_sub").append('<a href="#" title="Корзина"><span>Корзина : пусто</span></a>');
                }
            });
        return false;
    }

    function update_comp_count(){
        $.get("/comparison", {
                act: 'recalc'
            },
            function(data){
                if( data.length <= 2 ){
                    $(".count_comp").text(data);
                }else{
                    $(".count_comp").text('не определено');
                }
            }
        );
        return false;
    }

    function recalc_cart(){
        var total = 0;
        $(".product_row").each(function(i){
            var price = $(this).find(".pr_price").val();
            var count = $(this).find(".pr_count").val();
            var pr_total = count * price;
            total = total + pr_total;
        });
        $("#total_price").text(formatPrice(total));
        $("#rassr_price").val(total);
        calculate ();
        checkprice();
        window.location.reload();
    }

    // добавление товара для сравнения
    $(".compar_button").on('click', function(){
        var id_product = $(this).attr('id_product');
        var division = $(this).attr('division');
        var width = screen.width;
        $.get("/comparison", {
                act: 'add',
                id_product: id_product,
                division: division,
                width: width
            },
            function(data){
                if(data=='ok'){
                    showAlertWindow('Товар добавлен для сравнения','accept');
                    update_comp_count();
                } else if(data=='exist') {
                    showAlertWindow('Товар уже присутствует в списке','message');
                } else if(data=='5'){
                    showAlertWindow('Количество товаров для сравнения не более 5','error');
                } else if(data=='3'){
                    showAlertWindow('Количество товаров для сравнения не более 3','error');
                }else{
                    showAlertWindow('Ошибка добавления','error');
                }
                //alert(data);
            });
        return false;
    });

    // добавление товара для сравнения с выбранной конфигурацией
    $(".compar_button_flash").on('click', function(){
        var id_product = $(this).attr('id_product');
        var division = $(this).attr('division');
        //------------
        var config = getConfiguration();
        $.post("/cart", {
                act: 'saveconfig',
                conf: config,
                id_product: id_product,
                division: division
            },
            function(data){}
        );
        //--------------
        var width = screen.width;
        $.get("/comparison", {
                act: 'add',
                id_product: id_product,
                division: division,
                width: width
            },
            function(data){
                if(data=='ok'){
                    showAlertWindow('Товар добавлен для сравнения','accept');
                    update_comp_count();
                } else if(data=='exist') {
                    showAlertWindow('Товар уже присутствует в списке','message');
                } else if(data=='5'){
                    showAlertWindow('Количество товаров для сравнения не более 5','error');
                } else if(data=='3'){
                    showAlertWindow('Количество товаров для сравнения не более 3','error');
                }else{
                    showAlertWindow('Ошибка добавления','error');
                }
                //alert(data);
            });
        return false;
    });

    $(".open_er").on('click', function(){
        $(".send_error_message").css('display','none');
    });

    function sendmsg(){
        if(window.getSelection)
            txt = window.getSelection().toString();
        else if(document.getSelection)
            txt = document.getSelection();
        else if(document.selection)
            txt = document.selection.createRange().text;
        var popupY = $(document).scrollTop() + Math.round($(window).height()/2) - Math.round($(".send_error_message").height()/2);
        $(".send_error_message").css('top', popupY);
        $(".send_error_message").css('display','block');
        locat = document.location;
        $("#selected_text").text('<!!!>'+txt+'<!!!>');
        $("#selected_url").text('url: '+locat);
        $("#textarea").html('<textarea rows="5" cols="" id="comment_selected" name="comment_selected" style="border: 1px solid #C5B895; width: 220px;"></textarea>');
    }

    $("#er_button").on('click', function(){
        var error = $("#selected_text").text();
        var url = document.location+'';
        var comment = $('#comment_selected').val();
        $.get("/callback", { act: 'error', error: error, url: url, comment:comment}, function(data){
            if(data=='ok'){ $(".send_error_message").css('display','none'); }
        });
    });

    // --- Автозаполнение ---
    function liFormat (row, i, num) {
        if(row[2] == 'Изучение спроса') {
            price = '<span>'+row[2]+'</span>';
        }
        else { price = 'Цена от: <span>'+row[2]+' руб.</span>';}
        var result = '<div class="product_option"><div class="product_option_left"><a href="/cat/'+row[4]+'/'+row[3]+'" id="product_href"><center><img height="60px" src="/pics/catalog/product/img/'+row[1]+'" /></center></a></div><div class="product_option_right"><div class="product_option_title"><a href="/cat/'+row[4]+'/'+row[3]+'">'+row[0]+'</a></div><div class="product_option_price">'+price+'</div></div><div class="clear"></div></div>';
        return result;
    }

    function selectItem(li) {

        var title = $('.input_big').val();
        //alert(title);
    }

    // --- Автозаполнение2 ---
    $("#search").autocomplete("/search/ajaxsearch/id/231", {
        delay:10,
        minChars:2,
        matchSubset:1,
        autoFill:false,
        matchContains:1,
        cacheLength:10,
        selectFirst:false,
        formatItem:liFormat,
        maxItemsToShow:7,
        onItemSelect:selectItem
    });

    var width;
    $('.tips').each(function(){
        width = $(this).children('.division_title_middle').width()+64+118+1;
        $(this).css({width: width, height: 40});
    });

    $('.tips').hover(function(){
        $(this).children('.division_title_middle').children('.arrow_box').show();
    });

    $('.division_title_middle').hover(function(){
        $(this).children('.arrow_box').show();
    });

    $('.division_title_left').hover(function(){
        $(this).next().children('.arrow_box').show();
    });

    $('.division_title_right').hover(function(){
        $(this).prev().children('.arrow_box').show();
    });

    $('.tip_a').hover(function(){
        $(this).parent().parent().children('.arrow_box').show();
    });

    $('.tip_p').hover(function(){
        $(this).parent().children('.arrow_box').show();
    });

    $('.tips').mouseout(function(){
        $(this).children('.division_title_middle').children('.arrow_box').hide();
    });

    $(".city_title").each(function(){
        $(this).next().next().children('a').attr('target','_blank');
    });

    $(".subscription_close").click(function(){
        $(".subscription_content").toggle(500);
    });

    $("#subscriptionForm").on('submit',function(){           
        if(1){            
            var mail = $('#mailSubscript').val();        
            $.get("/podpiska", {mail:mail},

                function(data){
                    if(data=='ok'){
                        $(".subscription_content_inner p").text("Спасибо! Вы подписались на новости, вам выслано письмо с подтверждением на e-mail.");
                    } else {
                        if(data=='no'){
                             $(".subscription_content_inner p").text("Вы уже подписаны на новости");
                        }else{
                             $(".subscription_content_inner p").text("E-mail введён не верно");
                        }

                    }

                    $(".subscription_content").toggle(500);

                }
            );
        }
        return false;
    });
//    перенесенные вызовы

    $('.div_carousel').jcarousel();
    $('select').selectbox();

    $().ready(function() {
        $('.kwicks').kwicks({
            size: 193,
            maxSize: 256,
            spacing : 0,
            behavior: 'menu'
        });

        $(".main_division_one_picture").hover(function () {
            $(this).stop(true,true).animate({
                marginTop: -10+"px"
            },200);

        },function () {
            $(this).animate({
                marginTop: 0+"px"
            },200);

        });

    });

    $('#mycarousel').jcarousel({
        wrap: 'circular'
    });
    $("a.zoom").fancybox({
        'titleShow' : true,
        'titlePosition' : 'inside',
        'transitionIn'	: 'elastic',
        'transitionOut'	: 'elastic',
        'centerOnScroll' : true
    });
    slide(1);
    $('.squares').fadeIn(2500);

});
