<?php
set_time_limit(0);
define('ROOT_DIR', (dirname(( __FILE__ ))) . DIRECTORY_SEPARATOR);
define('CACHE_DIR', ROOT_DIR . 'cache_db' . DIRECTORY_SEPARATOR);
//define('CACHE_IMG_DIR', ROOT_DIR . 'cached_images' . DIRECTORY_SEPARATOR);

function emptyDir($path) {
	if ($handle = opendir($path)) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != "..") {
				if(is_file($path."/".$file)) {
					@unlink($path."/".$file); 
				} else {
					emptyDir($path."/".$file);
					@rmdir($path."/".$file);
				}
			}
		}
	}
}

emptyDir(CACHE_DIR);
//emptyDir(CACHE_IMG_DIR);

?>
