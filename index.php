<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('Error_Reporting', 'E_ALL & ~E_NOTICE');
include_once 'start.inc.php';
define('ROOT_DIR', (dirname(( __FILE__ ))) . DIRECTORY_SEPARATOR);
require_once(ROOT_DIR . 'application/system/Loader.php');
Loader::load('application/system/Bootstrap');
Bootstrap::start();
include_once 'end.inc.php';
