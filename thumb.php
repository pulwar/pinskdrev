<?
define('ROOT_DIR', dirname(__FILE__) . DIRECTORY_SEPARATOR);

if (isset($_REQUEST['img']) && is_file(dirname(__FILE__) . $_REQUEST['img'])) {
	$new_width = $_REQUEST['w'];
	$new_height = $_REQUEST['h'];
	
	if ($new_width <= 0 || $new_height <= 0) {
		exit();
	}
	
	$img_src = dirname(__FILE__) . $_REQUEST['img'];
	
	$cashe_path = ROOT_DIR . "cached_images/";
	
	$cached_filename = md5($img_src . filemtime($img_src) . $new_width . $new_height) . '.jpg';
	$path_to_cache_file = $cashe_path . $new_width . "x" . $new_height;
	$cache = $path_to_cache_file . '/' . $cached_filename;
	
	if (!file_exists($cache)) {
		if (!is_dir($path_to_cache_file)) {
			@mkdir($path_to_cache_file);
		}
		//$text = ROOT_DIR . 'text2.png';
		$text = ROOT_DIR . 'text4.png';
		$text_source = imagecreatefrompng($text);
		$text_size = getimagesize($text);
		
		$img_size = getimagesize($img_src);
		$format = strtolower(substr($img_size['mime'], strpos($img_size['mime'], '/') + 1));
		$icfunc = "imagecreatefrom" . $format;
		$scfunc = "image" . $format;
		$img_source = $icfunc($img_src);
		
		$x_ratio = $new_width / $img_size[0];
		$y_ratio = $new_height / $img_size[1];
		
		$ratio = min($x_ratio, $y_ratio);
		$use_x_ratio = ($x_ratio == $ratio);
		
		$new_width = $use_x_ratio ? $new_width : floor($img_size[0] * $ratio);
		$new_height = !$use_x_ratio ? $new_height : floor($img_size[1] * $ratio);
		
		$x_text = ($new_width - $text_size[0]) / 2;
		$y_text = ($new_height - $text_size[1]) / 2;
		
		$idest = imagecreatetruecolor($new_width, $new_height);
		imagefill($idest, 0, 0, $rgb = 0xFFFFFF);
		imagecopyresampled($idest, $img_source, 0, 0, 0, 0, $new_width, $new_height, $img_size[0], $img_size[1]);
		imagecopy($idest, $text_source, $x_text - 15, $y_text, 0, 0, $text_size[0], $text_size[1]);
		$scfunc($idest, $cache);
		
		imagedestroy($img_source);
		imagedestroy($text_source);
	}
	
	date_default_timezone_set('Europe/Minsk');
	ob_clean();
	header('Content-type: image/jpeg');
	header('Content-Length: ' . filesize($cache));
	header('Accept-Ranges: bytes');
	header('ETag: ' . sha1_file($cache));
	header('Last-Modified: ' . date(DATE_RSS), filemtime($img_src));
	header('Expires: ' . date(DATE_RSS, time() + 604800));
	header('Cache-Control: max-age=604800, public');
	echo file_get_contents($cache);
}

