ALTER TABLE site_catalog_product_units
ADD
`uniq_id_tr` varchar(255) DEFAULT NULL;
ALTER TABLE site_catalog_product_units
ADD
  `completing_code_tr` varchar(50) DEFAULT NULL;
ALTER TABLE site_catalog_product_units
ADD
  `in_stock_into_store_tr` int(11) DEFAULT NULL;
ALTER TABLE site_catalog_product_units
ADD
  `in_stock_to_factory_tr` int(11) DEFAULT NULL;