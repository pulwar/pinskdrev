<?php
class Mail {

	/**
     * Отправка сообщения с переданными параметрами
     * на указанный email
     *
     * @param string $email
     * @param string $msg
     * @param string $from
     * @param string $subject
     * @param string $recipient
     */
	public static function send($emails, $msg, $from, $subject, $recipient = null, $filePath = '') {
		//echo 'stop'; die();
		if(!is_array($emails)){
			$emails = array($emails);
		}

		$pathArray = explode('/', $filePath);
		$fileName =  end($pathArray);

		foreach ($emails as $email) {
			$config = array(
					'ssl' => 'tls',
					'port' => 25 
			);
			$transport = new Zend_Mail_Transport_Smtp("127.0.0.1", $config);
			$mail = new Zend_Mail('UTF-8');
			$mail->setBodyHtml($msg, 'UTF-8');
			$mail->setFrom($from, $from);
			$mail->addTo($email, $email);
			$mail->setSubject($subject);
			//$mail->send($transport);
			//добавляем файл
			if($fileName != ''){
				$content = file_get_contents($filePath);
				$attachment = new Zend_Mime_Part($content);
				//$attachment->type = 'application/pdf';
				$attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
				$attachment->encoding = Zend_Mime::ENCODING_BASE64;
				$attachment->filename = $fileName;

				$mail->addAttachment($attachment);  	
			}
			$mail->send();
		}
	}
}
