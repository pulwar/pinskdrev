<?

abstract class DefaultController extends Zend_Controller_Action
{
    public function preDispatch(){
        $this->initView();
        $db = Zend_Registry::get('db');
        $this->view->profiler = $db->getProfiler()->getEnabled() ? $db->getProfiler() : null;
    }

    public function postDispatch(){
        $this->setCanonical();
    }

    public function setCanonical() {
        try {
            $className = get_class($this);
            $page = $this->view->page;
            $params = $this->_getAllParams();
            $canonical = '';
            $site = 'http://pinskdrev.msk.ru';
            //Zend_Debug::dump($params);
            switch ($className) {
                case 'pages_IndexController':
                    $page_url = Pages::getInstance()->getUrlById($params['id']);
                    $canonical = '/'.$page_url;
                break;
                case 'Catalog_IndexController':
                    if (isset($params['product'])) {
                        if (!preg_match("/[^0-9]/",$params['product'])) {
                            $product_id = $params['product'];
                        }
                        else {
                            $product_id = Catalog_Product::getInstance()->getIdByUrl($params['product']);
                        }
                        $division = Catalog_Division::getInstance()->getDivisionUrlByProductId($product_id);
                        $product = Catalog_Product::getInstance()->getUrlById($product_id);
                        $canonical = '/cat/'.$division.'/'.$product;
                    } elseif (isset($params['action']) && ($params['action'] == 'akcii' || $params['action'] == 'actions')) {
                        $page_url = Pages::getInstance()->getUrlById($params['id']);
                        $canonical = '/'.$page_url;
                    } elseif (isset($params['action']) && ($params['action'] == 'history')) {
                        $canonical = '/history';
                    }
                    else {
			//Zend_Debug::dump($params);
                        if (isset($params['cat']) && !preg_match("/[^0-9]/",$params['cat'])) {
                            $division_id = $params['cat'];
			    $div_canonical = Catalog_Division::getInstance()->getDivisionValues($division_id);
			    if (isset($div_canonical->seo_canonical)) {
                            $div_canonical = $div_canonical->seo_canonical;}
                            if ($div_canonical == null) {
                                $division_url = Catalog_Division::getInstance()->getUrlById($division_id);
                                $canonical = '/cat/'.$division_url;
                            }
                            else {
                                $canonical = $div_canonical;
                            }
                        }
                        else {
                            if (isset($params['cat'])) {
                            $div_id = Catalog_Division::getInstance()->getIdByUrl($params['cat']);
                                $div_canonical = Catalog_Division::getInstance()->getDivisionValues($div_id);
                                $div_canonical = $div_canonical->seo_canonical;
                                if ($div_canonical == null) {
                                    $canonical = '/cat/'.$params['cat'];
                                }
                                else {
                                    $canonical = $div_canonical;
                                }
                            }
                        }
			if ($params['module'] == 'catalog' && $params['id'] == 973 && !isset($params['cat'])) {
			    $canonical = '/cat';
			}
                    }
                    break;
                case 'News_NewsController':
                    if (isset($params['item'])) {
                        $canonical = '/novosti/item/'.$params['item'];
                    }
                    else {
                        $canonical = '/novosti';
                    }
                    break;
                case 'Reviews_ReviewsController':
                    if (isset($params['item'])) {
                        $canonical = '/obzori/item/'.$params['item'];
                    }
                    else {
                        $canonical = '/obzori';
                    }
                    break;
                case 'ContactsTemplates_IndexController':
                    if (isset($params['module']) && $params['module'] == 'contactstemplates') {
                        $canonical = '/gde_kupit';
                    }
                    break;
                case 'Forms_FormsController':
                    $page_url = Pages::getInstance()->getUrlById($params['id']);
                    $canonical = '/'.$page_url;
                    break;
                case 'Makers_IndexController':
                    if (isset($params['item'])) {
                        $canonical = '/mebel_ot_proizvoditelya/item/'.$params['item'];
                    }
                    else {
                        $canonical = '/mebel_ot_proizvoditelya';
                    }
                    break;
                case 'Catalog_TransformerController':
                    if (isset($params['item'])) {
                        $canonical = '/mehanizmy_transformacii/item/'.$params['item'];
                    }
                    else {
                        $canonical = '/mehanizmy_transformacii';
                    }
                    break;
                case 'Search_IndexController':
                    if (isset($params['action']) && $params['action'] == 'moresearch')  {
                        $canonical = '/search';
                    }
                    break;
            }
            if ($canonical == '') {
                $canonical = null;
            }
            $this->view->placeholder('canonical')->set($site.$canonical);
        } catch ( Exception $e ) {
            $this->view->canonical = null;
        }
    }
}
