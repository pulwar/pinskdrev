<?php
class GruzTowns {
	protected static $_id_cache = "gruz_town";
	protected static $_url = "http://www.pecom.ru/ru/calc/towns.php";

	public static function getTowns() {
		$filename = DIR_PUBLIC . "gruz_town.json";

		try {
			$cache = Configurator::getInstance()->getCache();
			$towns = $cache->load(self::$_id_cache);
			if (!$towns) {
				$httpClient = new Zend_Http_Client(self::$_url);
				$response = $httpClient->request(Zend_Http_Client::GET);
				$towns = $response->getBody();
				$towns_array = Zend_Json::decode($towns);
				if (empty($towns_array)) {
					require_once 'Zend/Http/Client/Exception.php';
					throw new Zend_Http_Client_Exception('Not JSON string!');
				}
				$cache->save($towns, self::$_id_cache);
				// Write to reserve-file
				$handle = fopen($filename, "w");
				fwrite($handle, $towns);
				fclose($handle);
			} else {
				$towns_array = Zend_Json::decode($towns);
			}
			unset($cache);
			$result = array();
			foreach ($towns_array as $regionKey => $regionData)
				foreach ($regionData as $townKey => $townName)
					$result[$townKey] = $townName . ' (' . $regionKey . ')';
			asort($result);
			return $result;
		} catch (Exception $e) {
			$handle = fopen($filename, "r");
			$contents = fread($handle, filesize($filename));
			fclose($handle);
			$towns_array = Zend_Json::decode($contents);
			unset($cache);
			$result = array();
			foreach ($towns_array as $regionKey => $regionData)
				foreach ($regionData as $townKey => $townName)
					$result[$townKey] = $townName . ' (' . $regionKey . ')';
			asort($result);
			return $result;
		}
	}
}
