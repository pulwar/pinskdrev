<?php

class Captcha
{
	/**
	 * Получение арифметического выражения
	 * и запись ррезультата в сессию
	 *
	 * @return string
	 */
	public function getOperation(){
		require_once (DIR_PEAR . 'Text' . DS . 'CAPTCHA' . DS . 'Numeral.php');
		$numcap = new Text_CAPTCHA_Numeral;
		//Zend_Debug::dump($numcap->getAnswer());
		$_SESSION['answer'] = $numcap->getAnswer();		
		return $numcap->getOperation();
	}

	public function getOperationCallback(){
		require_once (DIR_PEAR . 'Text' . DS . 'CAPTCHA' . DS . 'Numeral.php');
		Zend_Debug::dump(1);exit();
		$numcap = new Text_CAPTCHA_Numeral;
		//Zend_Debug::dump($numcap->getAnswer());
		$_SESSION['answer_callback'] = $numcap->getAnswer();		
		return $numcap->getOperationCallback();
	}
	
	/**
	 * Проверка результата
	 *
	 * @param int $answer
	 * @return boolean
	 */
	public function isValidate($answer){
		// Zend_Debug::dump($_SESSION['answer']);exit();	
		if (isset($_SESSION['answer']) && $answer == $_SESSION['answer']){
			return true;
		} 
		else{
			return false;
		}
	}
}
