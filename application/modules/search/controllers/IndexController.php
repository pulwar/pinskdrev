<?php
class Search_IndexController extends DefaultController {
	/**
	 *
	 * @var zend_layout
	 */
	private $layout = null;
	
	/**
	 *
	 * @var string
	 */
	private $lang = null;
	private $_count = null;
	private $_offset = null;
	private $_current_page = null;

	public function init() {
		
		/*$this->initView()
		$view = Zend_Layout::getMvcInstance()->getView();*/
		$this->view->addHelperPath(Zend_Registry::get('helpersPaths'), 'View_Helper');
		$this->layout = $this->view->layout();
		$this->lang = $this->_getParam('lang', 'ru');
		$this->layout->lang = $this->lang;
		
		if ($this->_hasParam('reindex')) {
			$this->_forward('reindex');
		}
		$id = $this->_getParam('id');
		$this->view->placeholder('id_page')->set($id);
		$page = Pages::getInstance()->find($id)->current();
		
		if (!is_null($page)) {
			if ($page->published == 0) {
				$this->_redirect('/404');
			}
			$options = PagesOptions::getInstance()->getPageOptions($id);
			$this->view->placeholder('title')->set($options->title);
			$this->view->placeholder('keywords')->set($options->keywords);
			$this->view->placeholder('descriptions')->set($options->descriptions);
			$this->view->placeholder('h1')->set($options->h1);
			$this->layout->page = $page;
			$this->view->current_page = $this->_current_page = $this->_getParam('page', 1);
			$this->_onpage = $this->view->onpage = $this->_getParam('onpage', 12);
			$this->_offset = ($this->_current_page - 1) * $this->_onpage;
		}
		$act = $this->_getParam('act');
		if ($act == 'material')
			$this->_forward('material');
		if ($act == 'transform')
			$this->_forward('transform');
		if ($act == 'getcount')
			$this->_forward('getcount');
		if ($act == 'sleep')
			$this->_forward('sleep');
	}

	public function reindexAction() {
		$numDocs = News::getInstance()->reindex();
		$numDocs += Pages::getInstance()->reindex();
		$numDocs += Catalog_Division::getInstance()->reindex();
		$numDocs += Catalog_Product::getInstance()->reindex();
		echo $numDocs;
		exit();
	}

	public function moresearchAction() {		
		$this->layout->setLayout("front/default");
		$this->view->material_options = Catalog_Product_Material::getInstance()->getItemsAsOptions();
		$this->view->mebeltype_options = Catalog_Product_Mebeltype::getInstance()->getItemsAsOptions();
		$this->view->makers_options = Makers::getInstance()->getActiveItemsAsOptions();
		$this->view->material_options[0] = $this->view->mebeltype_options[0] = $this->view->makers_options[0] = 'Все';
		$this->view->catalog_path = SiteDivisionsType::getInstance()->getPagePathBySystemName('division');
		$this->view->maker_path = SiteDivisionsType::getInstance()->getPagePathBySystemName('makers');
		if ($this->_hasParam('name')) {
			$title = $this->_getParam('name');
			if ($title == 'Поиск' || $title == '') {
				$title = null;
			}
			if($this->_hasParam('mobileSearch')){				
				$type = 0; 
				$material = null;
				$color = null;
				$width = null;
				$price_from = null;
				$price_to = null;
				$default_values = null;
				$color_type = 0;				
			} else {			
				$type = $this->_getParam('type');
				if ($type == '') {
					$type = null;
				}
				$color_type = Catalog_Product_Mebeltype::getInstance()->getType($type);
				$material = $this->_getParam('materials');
				if ($material == '') {
					$material = null;
				}
				$color = $this->_getParam('colors');
				if ($color == '' || $color == 0) {
					$color = null;
				}
				$width = $this->_getParam('width');
				if ($width == '' || $width == 'Ширина до') {
					$width = null;
				}
				$price_from = $this->_getParam('price_from');
				if ($price_from == '' || $price_from == 'Цена от') {
					$price_from = null;
				}
				$price_to = $this->_getParam('price_to');
				if ($price_to == '' || $price_to == 'Цена до') {
					$price_to = null;
				}
				$default_values = array();
				
				$action = $this->_getParam('akciya');
				if (isset($action)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('akciya');
				}
				
				$new = $this->_getParam('is_new');
				if (isset($new)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('is_new');
				}
				$present = $this->_getParam('is_present');
				if (isset($present)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('is_present');
				}
				
				$econom = $this->_getParam('econom');
				if (isset($econom)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('econom');
				}
				
				$box_for_cloth = $this->_getParam('box_for_cloth');
				if (isset($box_for_cloth)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('box_for_cloth');
				}
				
				$transformation = $this->_getParam('transformation');
				if (isset($transformation)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('transformation');
				}
				
				$prihojaya = $this->_getParam('prihojaya');
				if (isset($prihojaya)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('prihojaya');
				}
				
				$for_childs = $this->_getParam('for_childs');
				if (isset($for_childs)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('for_childs');
				}
				
				$for_guests = $this->_getParam('for_guests');
				if (isset($for_guests)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('for_guests');
				}
				
				$in_bedroom = $this->_getParam('in_bedroom');
				if (isset($in_bedroom)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('in_bedroom');
				}
				
				$for_cabin = $this->_getParam('for_cabin');
				if (isset($for_cabin)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('for_cabin');
				}
				
				$for_kithen = $this->_getParam('for_kithen');
				if (isset($for_kithen)) {
					$default_values[] = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName('for_kithen');
				}
				
				if (count($default_values) == 0) {
					$default_values = null;
				} else {
					//$default_values=implode(',',$default_values);
				}
			}			
			$this->view->products = Catalog_Product::getInstance()->moresearch($title, $type, $material, $color, $width, $price_from, $price_to, $default_values, $color_type);
		}
	}

	public function indexAction() {
		$this->layout->setLayout("front/default");
		$this->view->material_options = Catalog_Product_Material::getInstance()->getItemsAsOptions();
		$this->view->mebeltype_options = Catalog_Product_Mebeltype::getInstance()->getItemsAsOptions();
		$this->view->makers_options = Makers::getInstance()->getItemsAsOptions();
		$this->view->material_options[0] = $this->view->mebeltype_options[0] = $this->view->makers_options[0] = 'Все';
		$hits = array();
		$search = $this->_getParam('keywords');
		if ($search) {
			$charset = $this->detect_cyr_charset($search);
			if ('w' == $charset || 'm' == $charset) {
				$search = mb_convert_encoding($search, 'utf-8', 'windows-1251');
			}
			$this->view->placeholder('title')->set($this->view->placeholder('title') . ": $search");
			$this->view->placeholder('keywords')->set($this->view->placeholder('keywords') . ": $search");
			$this->view->placeholder('descriptions')->set($this->view->placeholder('descriptions') . ": $search");
			
			$this->view->layout()->search = $search;
			$search = Zend_Search_Lucene_Search_QueryParser::parse(strip_tags($search), Ext_Search_Lucene::ENCODING);
			
			/**
			 * 
			 * поиск по товарам
			 */
			
			$result = Ext_Search_Lucene::open(Ext_Search_Lucene::CATALOG_PRODUCTS);
			$result = $result->find($search);
			$hits = array_merge($hits, $result);
			
			/**
			 * поиск по разделам каталога
			 */
			$result = Ext_Search_Lucene::open(Ext_Search_Lucene::CATALOG_DIVISIONS);
			$result = $result->find($search);
			$hits = array_merge($hits, $result);
			
			// поиск по новостям			
			$news_index = Ext_Search_Lucene::open(Ext_Search_Lucene::NEWS);
			$result = $news_index->find($search);
			$hits = array_merge($hits, $result);
			
			// поиск по страницам
			$result = Ext_Search_Lucene::open(Ext_Search_Lucene::PAGES);
			$result = $result->find($search);
			$hits = array_merge($hits, $result);
			
			if (!sizeof($hits)) {
				$this->view->err = $message = "найдено результатов 0";
				//echo $message;
			} else {
				$this->view->total = count($hits);
				$count = $this->_count;
				$offset = ($this->_current_page - 1) * $this->_count;
				$hits = array_slice($hits, $offset, $count);
				$this->view->hits = $hits;
			}
		}
	}

	public function materialAction() {
		$this->layout->disableLayout();
		$mebel_type_id = $this->_getParam('id_type', 0);
		$materials_rowset = Catalog_Product_Material::getInstance()->getByType($mebel_type_id);
		//var_dump($materials_rowset);
		if ($materials_rowset) {
			print_r(json_encode($materials_rowset->toArray()));
			exit();
		}
		echo json_encode('err');
		exit();
	}

	public function transformAction() {
		$this->layout->disableLayout();
		$mebel_type_id = $this->_getParam('id_type', 0);
		$transformers_rowset = Catalog_Product_Transformer::getInstance()->getByType($mebel_type_id);
		//var_dump($transformers_rowset);
		if ($transformers_rowset) {
			print_r(json_encode($transformers_rowset->toArray()));
			exit();
		}
		echo json_encode('err');
		exit();
	}

	public function sleepAction() {
		$this->layout->disableLayout();
		$mebel_type_id = $this->_getParam('id_type', 0);
		$sleep = Catalog_Product::getInstance()->checkSleep($mebel_type_id);
		//var_dump($materials_rowset);
		

		if ($sleep) {
			echo json_encode('1');
			exit();
		} else {
			echo json_encode('err');
			exit();
		}
	}

	private function detect_cyr_charset($str) {
		define('LOWERCASE', 3);
		define('UPPERCASE', 1);
		$charsets = Array(
				'k' => 0,
				'w' => 0,
				'd' => 0,
				'i' => 0,
				'm' => 0 
		);
		for ($i = 0, $length = strlen($str); $i < $length; $i++) {
			$char = ord($str[$i]);
			//non-russian characters
			if ($char < 128 || $char > 256)
				continue;
				
				//CP866
			if (($char > 159 && $char < 176) || ($char > 223 && $char < 242))
				$charsets['d'] += LOWERCASE;
			if (($char > 127 && $char < 160))
				$charsets['d'] += UPPERCASE;
				
				//KOI8-R
			if (($char > 191 && $char < 223))
				$charsets['k'] += LOWERCASE;
			if (($char > 222 && $char < 256))
				$charsets['k'] += UPPERCASE;
				
				//WIN-1251
			if ($char > 223 && $char < 256)
				$charsets['w'] += LOWERCASE;
			if ($char > 191 && $char < 224)
				$charsets['w'] += UPPERCASE;
				
				//MAC
			if ($char > 221 && $char < 255)
				$charsets['m'] += LOWERCASE;
			if ($char > 127 && $char < 160)
				$charsets['m'] += UPPERCASE;
				
				//ISO-8859-5
			if ($char > 207 && $char < 240)
				$charsets['i'] += LOWERCASE;
			if ($char > 175 && $char < 208)
				$charsets['i'] += UPPERCASE;
		}
		arsort($charsets);
		return key($charsets);
	}

	public function ajaxsearchAction() {
		$id = $this->_getParam('id', 0);
		$search = $this->_getParam('q', '');
		$act = $this->_getParam('act', '');
		if ($id) {
			$where[] = "title LIKE '%$search%'";
			$where[] = "active = 1";
			$res = Catalog_Product::getInstance()->fetchAll($where);
			foreach ($res as $data) {
				$pr_url = Catalog_Product::getInstance()->getUrlById($data->id);
				$div_url = Catalog_Division::getInstance()->getDivisionUrlByProductId($data->id);
				$unit = Catalog_Product_Units::getInstance()->getMainByProduct($data->id);
				if ($unit->price > 0) {
					$price = $unit->price;
				} else {
					if ($unit->price_with_tr > 0) {
						$price = $unit->price_with_tr;
					} else {
						$price = 'Изучение спроса';
					}
				}
				print $data->title . "|" . $unit->img . "|" . $price . "|" . $pr_url . "|" . $div_url . "\n";
			}
		}
		exit();
	}

	public function getcountAction() {
		$this->layout->disableLayout();
		$id_type = $this->_getParam('id_type');
		$id_material = $this->_getParam('id_material');
		$id_color = $this->_getParam('id_color');
		$width = $this->_getParam('width_before');
		$price_low = $this->_getParam('price_low');
		$price_to = $this->_getParam('price_to');
		//checkbox
		$akciya = $this->_getParam('akciya');
		$is_new = $this->_getParam('is_new');
		$is_present = $this->_getParam('is_present');
		$econom = $this->_getParam('econom');
		$for_guests = $this->_getParam('for_guests');
		$in_bedroom = $this->_getParam('in_bedroom');
		$for_cabin = $this->_getParam('for_cabin');
		$color_type = Catalog_Product_Mebeltype::getInstance()->getType($id_type);
		if ($id_type == 0) {
			$id_type = null;
		}
		if ($id_material == 0) {
			$id_material = null;
		}
		if ($id_color == 0) {
			$id_color = null;
		}
		if ($width == 'Ширина до') {
			$width = null;
		}
		if ($price_low == 'Цена от') {
			$price_low = null;
		}
		if ($price_to == 'Цена до') {
			$price_to = null;
		}		
		
		$products = Catalog_Product::getInstance()->searchcountproducts($id_type, $id_material, $id_color, $width, $price_low, $price_to, $color_type, $akciya, $is_new, $is_present, $econom, $for_guests, $in_bedroom, $for_cabin);
		if ($id_type > 0) {
			$materials = array(
					0 
			);
			foreach ($products as $product) {
				$materials[] = $product['material_ids'];
			}
			$materials = array_unique($materials);
			$materials = implode(',', $materials);
			$material_select = Catalog_Product_Material::getInstance()->getItemsAsOptionsForSearch($materials);
		} else {
			$material_select = '<option>Все материалы</option>';
		}
		if ($id_material > 0) {
			$colors = array(
					0 
			);
			if ($color_type == 1) {
				foreach ($products as $product) {
					$colors[] = $product['color_ids'];
				}
				$colors = array_unique($colors);
				$colors = implode(',', $colors);
				
				$colors_select = Catalog_Product_Color::getInstance()->getItemsAsOptionsForSearch($colors);
			}
			if ($color_type == 2) {
				//$colors_select = '<option>Все цвета</option>';
				foreach ($products as $product) {
					$colors[] = $product['fabrics_ids'];
				}
				$colors = array_unique($colors);
				$colors = implode(' ', $colors);
				$colors = str_replace(' ', ',', $colors);
				$colors = explode(',', $colors);
				$colors = array_unique($colors);
				$colors = implode(',', $colors);
				//Zend_Debug::dump($colors);exit();
				$colors_select = Catalog_Fabrics_Groups::getInstance()->getItemsAsOptionsForSearch($colors);
			}
		} else {
			$colors_select = '<option>Цвет</option>';
		}
		
		$count = count($products);
		$test = array(
				'material' => $material_select,
				'color' => $colors_select,
				'count' => $count 
		);
		echo json_encode($test);
		exit();
	}
}
