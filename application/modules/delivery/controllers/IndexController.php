<?php
class delivery_IndexController extends Zend_Controller_Action {
    /**
     * Шаблон страницы
     * @var zend_layout
     */
    private $layout = null;

    /**
     * Язык страницы
     * @var string
     */
    private $lang = null;

    /**
     * Объект страницы
     * @var Pages_row
     */
    private  $_page = null;

    public function init() {
		$this->view->addHelperPath(Zend_Registry::get('helpersPaths'), 'View_Helper');
		$this->layout = $this->view->layout();
		$this->lang = $this->_getParam('lang', 'ru');
		$this->layout->lang = $this->lang;
		if ($this->getRequest()->getActionName() == 'productunit') {
			$this->layout->setLayout("layout");
			return;
		}
		
		if ($this->getRequest()->getActionName() == 'prices') {
			$this->layout->setLayout("layout");
			return;
		}

		if ($this->getRequest()->getActionName() == 'pricesold') {
			$this->layout->setLayout("layout");
			return;
		}

		if ($this->getRequest()->getActionName() == 'material') {
			$this->layout->setLayout("layout");
			return;
		}
		
		if ($this->getRequest()->getActionName() == 'delivery') {
			$this->layout->setLayout("layout");
			return;
		}
		
		$this->view->maker_path = SiteDivisionsType::getInstance()->getPagePathBySystemName('makers');
		$id = $this->_getParam('id');
		$page = Pages_Cache::getInstance()->getPage($this->_getParam('id'));
		if (is_null($page) || $page->published == '0') {
			//$this->_redirect( '/404' );
		}
		$this->_page = $page;
		
		$this->layout->setLayout("front/default");
		$this->layout->current_type = 'pages';
		
		$this->layout->page = $this->_page;
		
		if ($this->_hasParam('product')) {
			$this->_forward('product');
		}
		
		$this->_onpage = $this->view->onpage = $this->_getParam('onpage', 999);
		$this->_sort = $this->view->sort = $this->_getParam('sort', 0);
		
		$this->_filter = new Zend_Session_Namespace('paginationFulSession');
		if (! isset($this->_filter->maker))
			$this->_filter->maker = 0;
		if (! isset($this->_filter->type))
			$this->_filter->type = 0;
		if (! isset($this->_filter->category))
			$this->_filter->category = 0;
		
		$this->_maker = $this->_hasParam('maker') ? $this->_getParam('maker', 0) : $this->_filter->maker;
		$this->view->maker = $this->_filter->maker = $this->_maker;
		
		$this->_type = $this->_hasParam('type') ? $this->_getParam('type', 0) : $this->_filter->type;
		$this->view->type = $this->_type;
		
		$this->_category = $this->_hasParam('category') ? $this->_getParam('category', 0) : $this->_filter->category;
		$this->view->category = $this->_category;
		
		$this->_current_page = $this->view->current_page = $this->_getParam('page', 1);
		
		$this->_offset = ($this->_current_page - 1) * $this->_onpage;
		//$this->view->onpage = $this->_onpage;
		$coll = $this->view->collection = $this->_getParam('coll');
		switch ($coll) {
			case 1 :
				$this->_collection = 'оскар';
				break;
			case 2 :
				$this->_collection = 'венеция';
				break;
			case 3 :
				$this->_collection = 'милана';
				break;
			case 4 :
				$this->_collection = 'верди';
				break;
			case 5 :
				$this->_collection = 'верди люкс';
				break;
			default :
				$this->_collection = $this->view->collection = '';
		}
                if($page){
                    $this->view->content = $page->content;
                }
		$this->view->options = $options = PagesOptions_Cache::getInstance()->getPageOptions($id);
		//Zend_Debug::dump($options); exit;
		$this->view->placeholder('title')->set('Расчет');
		$this->view->placeholder('keywords')->set($options->keywords);
		$this->view->placeholder('descriptions')->set($options->descriptions);
		$this->view->placeholder('h1')->set($options->h1);
		$this->view->placeholder('id_page')->set($id);
		if(isset($this->_page->id)){
                    $this->layout->id_object = $this->_page->id;
                }
                if($page){
                    if ($page->show_childs == 1) {
                            $this->layout->page_childs = Pages_Cache::getInstance()->getChildrenAndURLs($page->id);
                    }
                }
	}
    public function indexAction() {    	
		print_r('hu');
    }


}
