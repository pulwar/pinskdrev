﻿<?php

class Forms_FormsController extends DefaultController {

    public function init() {
        $this->initView();
        $this->layout = $this->view->layout();
        $this->lang = $this->_getParam('lang', 'ru');

        $this->layout->setLayout("front/default");
        $id = $this->_getParam('id');
        $page = Pages::getInstance()->getPage($this->_getParam('id'));
        if(!is_null($page)) {
            if ($page->published == '0') {
                $this->_redirect('/404');
            }

            $this->layout->page = $page;
            $this->layout->lang = $page->version;

            $this->view->addScriptPath(DIR_LAYOUTS) ;
            $this->view->addHelperPath(Zend_Registry::get('helpersPaths'), 'View_Helper') ;

            $this->view->options = $options = PagesOptions::getInstance()->getPageOptions($id);
            $this->view->placeholder('title')->set($options->title);
            $this->view->placeholder('keywords')->set($options->keywords);
            $this->view->placeholder('descriptions')->set($options->descriptions);
            $this->view->placeholder('id_page')->set($id);
            $this->view->placeholder('object_id')->set($id);
            $this->view->placeholder('h1')->set($page->name);
            $this->layout->current_type = 'pages';
            $this->view->page = $page;
            $this->layout->id_object = $page->id;
        }

        $act = $this->_request->getParam('act');
        if($act == 'error'){
            $this->_forward('errormessage');
        }
    }

    public function couponAction() {
        Loader::loadCommon('Captcha');
        $captcha = new Captcha();
        if ($this->_request->isPost()) {
            $form = $this->_getParam('form', array());
            if($captcha->isValidate($form['captcha'])) {
                //проеряем есть ли купон в течении 15 последних минут по введенному номеру договора
                $previus = Coupon::getInstance()->getCollection($form['number']);
                if(count($previus)>0) {
                    $result = -1;
                } else {
                    //создаем новый купон
                    $form['code'] = $this->generateRandomString();
                    $coupon = Coupon::getInstance()->addCoupon($form);
                    $result = $form['code'];

                    $data['number'] = $form['number'];
                    $data['capcha'] = $form['code'];
                    if($form['name']){
                        $data['name'] = $form['name'];
                    }
                    if($form['email']){
                        $data['email'] = $form['email'];
                        //отправка письма на почту
                        $emails[] = $form['email'];
                        $recipient = 'New message';
                        $template = FeedbackTemplates::getInstance()->getBySystemName('coupon');

                        $subject = $template['name'];
                        $template = $template['content'];

                        foreach ($form as $key=>$param) {
                            $template = str_replace('{'.$key.'}', $param, $template);
                        }
                        $template = preg_replace('/{.+}/Usi', '', $template);
                        $body = $template;
                        Loader::loadCommon('Mail');
                        $from = 'info@pinskdrev.msk.ru';

                        Mail::send($emails, $body, $from, $subject.' '.$_SERVER["HTTP_HOST"], $recipient);
                    }
                    $data['time'] = date('Y:m:d H:i:s');
                    // отправляем в GD
                    $str = http_build_query($data);
                    file_get_contents('https://script.google.com/macros/s/AKfycbxw6G_7-_GrWGWS1c--fWyOKgpb55FT7rxovXn1H6rZc_QbL7qi/exec?'.$str);

                }
            } else {
                $result = 0;
            }

        }
        echo $result; die;

    }

    public function feedbackAction() {
        Loader::loadCommon('Captcha');
        $captcha = new Captcha();
        $this->view->addHelperPath(Zend_Registry::get('helpersPaths') , 'View_Helper');
        if ($this->_request->isPost()) {
            $form = $this->_getParam('form', array());
            if ($form['fio'] == '' ||
                $form['phone'] == '') {
                $this->view->err = 1;
                $this->view->form = $form;
            } else if( !$captcha->isValidate($form['captcha'])) {
                $this->view->err = 2;
                $this->view->form = $form;
            } else {
                $users = Users::getInstance()->getUsersToSendMail();
                $emails = array();
                foreach ($users as $user) {
                    $emails[] = $user['email'];
                }
                
                $emails[] = 'box.pinskdrev@gmail.com';
                $emails[] = 'pinskdrev.moscow@yandex.ru';
                $emails[] = 'box.pinskdrev@outlook.com';

                $recipient = 'New message';
                $subject = 'feedback pinskdrev.msk.ru';

                $template = FeedbackTemplates::getInstance()->getBySystemName('feedback');

                $subject = $template['name'];
                $template = $template['content'];

                foreach ($form as $key=>$param) {
                    $template = str_replace('{'.$key.'}', $param, $template);
                }
                $template = preg_replace('/{.+}/Usi', '', $template);
                $body = $template;
                Loader::loadCommon('Mail');
                $from = 'info@pinskdrev.msk.ru';

                Mail::send($emails, $body, $from, $subject.' '.$_SERVER["HTTP_HOST"], $recipient);

                $this->_request->clearParams();
                $this->view->ok = 1;
                $this->view->form = array();
            }
        }
        $this->view->captcha = $captcha->getOperation();
        $this->view->placeholder('captcha')->set($this->view->captcha);
    }


    public function optfeedbackAction() {
        Loader::loadCommon('Captcha');
        $captcha = new Captcha();
        $this->view->addHelperPath(Zend_Registry::get('helpersPaths') , 'View_Helper');
        if ($this->_request->isPost()) {
            $form = $this->_getParam('form', array());
            if ($form['fio'] == '' ||
                $form['phone'] == '' ||
                $form['captcha'] == '') {
                $this->view->err = 1;
                $this->view->form = $form;
            } else if( !$captcha->isValidate($form['captcha'])) {
                $this->view->err = 2;
                $this->view->form = $form;
            } else {
                $users = Users::getInstance()->getUsersToSendMail();
                $emails = array();
                foreach ($users as $user) {
                    $emails[] = $user['email'];
                }

                $emails[] = 'box.pinskdrev@gmail.com';
                $emails[] = 'pinskdrev.moscow@yandex.ru';
                $emails[] = 'box.pinskdrev@outlook.com';


                $recipient = 'New message';
                $subject = 'optoviki feedback pinskdrev.msk.ru';

                $template = FeedbackTemplates::getInstance()->getBySystemName('optfeedback');

                $subject = $template['name'];
                $template = $template['content'];

                foreach ($form as $key=>$param) {
                    $template = str_replace('{'.$key.'}', $param, $template);
                }
                $template = preg_replace('/{.+}/Usi', '', $template);
                $body = $template;
                Loader::loadCommon('Mail');
                $from = 'info@pinskdrev.msk.ru';

                Mail::send($emails, $body, $from, $subject.' '.$_SERVER["HTTP_HOST"], $recipient);

                $this->_request->clearParams();
                $this->view->ok = 1;
                $this->view->form = array();
            }
        }
        $this->view->captcha = $captcha->getOperation();
        $this->view->placeholder('captcha')->set($this->view->captcha);
    }


    public function rassrAction() {
        if ($this->_request->isPost()) {
            $form = $this->_getParam('form', array());
            $platezh = round($form['rassr_total']*(1-$form['rassr_vznos']/10)/$form['rassr_srok']);
            $form['platezh'] = $platezh;
            $users = Users::getInstance()->getUsersToSendMail();
            $emails = array();
            foreach ($users as $user) {
                $emails[] = $user['email'];
            }

            $emails[] = 'box.pinskdrev@gmail.com';
            $emails[] = 'pinskdrev.moscow@yandex.ru';
            $emails[] = 'box.pinskdrev@outlook.com';


            $recipient = 'New message';
            $subject = 'Заказ рассрочки';

            $template = FeedbackTemplates::getInstance()->getBySystemName('rassrochka');

            //$subject = $template['name'];
            $template = $template['content'];

            foreach ($form as $key=>$param) {
                $template = str_replace('{'.$key.'}', $param, $template);
            }
            $template = preg_replace('/{.+}/Usi', '', $template);
            $body = $template;
            Loader::loadCommon('Mail');
            $from = 'info@pinskdrev.msk.ru';

            Mail::send($emails, $body, $from, $subject.' '.$_SERVER["HTTP_HOST"], $recipient);

            $this->_request->clearParams();
            $this->view->ok = 1;
            $this->view->form = array();
        }
    }

    public function callbackAction() {
        $form = array();
        $form['fio'] = $this->_getParam('fio', '');
        $form['tel'] = $this->_getParam('tel', '');
        $form['comment'] = $this->_getParam('comment', '');
        $form['cpt'] = $this->_getParam('cpt', '');
        $form['answer'] = $this->_getParam('answer','');
        //Zend_Debug::dump($form['answer']);exit();
        Loader::loadCommon('Captcha');
        $captcha = new Captcha();
        //Zend_Debug::dump($captcha);exit();
        if($form['fio']!='' && $form['tel']!='' && ($form['answer'] == ($form['cpt']/2))){
            $users = Users::getInstance()->getUsersToSendMail();
            $emails = array();
            foreach ($users as $user) {
                $emails[] = $user['email'];
            }


            $emails[] = 'box.pinskdrev@gmail.com';
            $emails[] = 'pinskdrev.moscow@yandex.ru';
            $emails[] = 'box.pinskdrev@outlook.com';


            $recipient = 'New message';
            $subject = 'zakaz obratnogo zvonka pinskdrev.msk.ru';

            $template = FeedbackTemplates::getInstance()->getBySystemName('callback');

            //$subject = $template['name'];
            $template = $template['content'];

            foreach ($form as $key=>$param) {
                $template = str_replace('{'.$key.'}', $param, $template);
            }
            $template = preg_replace('/{.+}/Usi', '', $template);
            $body = $template;
            Loader::loadCommon('Mail');
            $from = 'info@pinskdrev.msk.ru';

            Mail::send($emails, $body, $from, $subject, $recipient);
            echo 'ok';
            exit;
        }
        exit;
    }

    public function errormessageAction() {
        $form = array();
        $form['error'] = $this->_getParam('error', '');
        $form['url'] = $this->_getParam('url', '');
        $form['comment'] = $this->_getParam('comment', '');
        if($form['error']!='' && $form['url']!=''){
            $users = Users::getInstance()->getUsersToSendMail();
            $emails = array();
            foreach ($users as $user) {
                //$emails[] = $user['email'];
            }
            $recipient = 'New message';
            $subject = 'Обнаружена ошибка на '.$_SERVER["HTTP_HOST"];

            $template = FeedbackTemplates::getInstance()->getBySystemName('error');

            //$subject = $template['name'];
            $template = $template['content'];

            foreach ($form as $key=>$param) {
                $template = str_replace('{'.$key.'}', $param, $template);
            }
            $template = preg_replace('/{.+}/Usi', '', $template);
            $body = $template;
            Loader::loadCommon('Mail');
            $from = 'info@pinskdrev.msk.ru';
            $emails[] = 'info@awagro.by';
            Mail::send($emails, $body, $from, $subject, $recipient);
            echo 'ok';
            exit;
        }
        exit;
    }
    public function podpiskaAction() {        
        $form = array();
        $form['mail'] = $this->_getParam('mail', '');
        //var_export($form['mail']); die();
        if($form['mail']!=''){
            $validator = new Zend_Validate_EmailAddress();           
            Loader::loadCommon('Mail');            
            $info = Subscribe::getInstance()->provAddSubscription($form['mail']);
            
            if (isset($info)) {
                echo 'no';
                exit;
            }
            if (! $validator->isValid($form['mail']) ) {                
                exit;
            }else{ 
                $kod = md5(time());
                $form['kod'] = $kod;
                $form['status'] = 0;
                $form['email'] = $form['mail'];
                $emails[] = $form['mail'];
                $from = 'pinsk.msk@yandex.ru';
                $subject = 'podpiska';
                $recipient = 'Podpiska pinskdrev.msk.ru';
                $bodyUser = 'Благодарим Вас за оказанное нам доверие. Мы очень рады, что вы доверились нам и согласились получать нашу информационную рассылку.<br /><br />
                Знаете ли вы, что это значит?<br /><br />
                Это значит, что вы узнаете первым о наших распродажах, акциях и новых поступлениях товаров. А также мы постараемся держать вас в
                курсе самых последних и самых интересных тенденций в мире дизайна.<br /><br />
                Готовы с нами подружиться? Тогда вам нужно сделать последний
                шаг и перейти по ссылке http://'.$_SERVER["HTTP_HOST"].'/email/pr/'.$form['email'].'/kod/'.$kod;
                //var_export($form['email']); die();
                Mail::send($emails, $bodyUser, $from, $subject, $recipient);

               
                Subscribe::getInstance()->addSubscription($form);


            }
            $users = Users::getInstance()->getUsersToSendMail();
            $emails = array();
            foreach ($users as $user) {
                //$emails[] = $user['email'];
            }

            $recipient = 'New message Podpiska';
            $subject = 'Podpiska';

            $template = FeedbackTemplates::getInstance()->getBySystemName('podpiska');

            //$subject = $template['name'];
            $template = $template['content'];

            foreach ($form as $key=>$param) {
                $template = str_replace('{'.$key.'}', $param, $template);
            }
            $template = preg_replace('/{.+}/Usi', '', $template);
            $body = $template;
            $from = 'pinsk.msk@yandex.ru';

            Mail::send($emails, $body, $from, $subject, $recipient);
            echo 'ok';
            exit;
        }
        exit;
    }

    public function emailAction() {
        //echo 123; die();
        $email = $this->_getParam('pr', '');
        $kod = $this->_getParam('kod', '');
        if ($kod == '' || $email == ''){
            $this->_redirect('/404');
        }
        $info = Subscribe::getInstance()->provSubscription($email,$kod);
        if (isset($info) && $info->status == 1) {
            $this->view->message = 'Вы уже подписаны на рассылку';
        }elseif(isset($info) && $info->status == 0){
            $info->status = 1;
            $info->save();
            $this->view->message = 'Ваш email подтвержден!';
        }else{
            $this->view->message = 'Данный емайл не участвует в рассылке';
        }
    }

    public function gruzcalcAction() {
        $towns = GruzTowns::getTowns();
        $this->view->towns = $towns;
    }

    static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
