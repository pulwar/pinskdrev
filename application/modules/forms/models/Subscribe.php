<?php

/**
 * Printed
 *
 * @author Vitali
 * @version
 */

class Subscribe extends Zend_Db_Table {
    /**
     * The default table name
     */
    protected $_name = 'site_subscription';
    protected $_primary = array('id');
    protected $_sequence = true;

    protected static $_instance = null;

    /**
     * Singleton instance
     *
     * @return Printed
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function addSubscription($data){
        $sub['status'] = $data['status'];
        $sub['email'] = $data['email'];
        $sub['kod'] = $data['kod'];

        return $this->insert($sub);
    }

    public function deleteSubscription($email) {
        $where = array(
                $this->getAdapter ()->quoteInto ( "email = ?", $email ),
        );
        $this->delete($where);
    }

    public function provSubscription($email, $kod) {
        $where = array($this->getAdapter()->quoteInto("email = ?", $email), $this->getAdapter()->quoteInto("kod = ?", $kod));
        return $this->fetchRow($where);
    }

    public function provAddSubscription($email) {
        $where = array($this->getAdapter()->quoteInto("email = ?", $email));
        return $this->fetchRow($where);
    }

    public function editSubscription($data,$id) {

        $new = $this->find($id)->current();
        $new->setFromArray(array_intersect_key($data, $new->toArray()));
        $new->save();
    }
}