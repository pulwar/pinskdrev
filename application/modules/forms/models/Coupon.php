<?php

/**
 * Printed
 *
 * @author Vitali
 * @version
 */

class Coupon extends Zend_Db_Table {
    /**
     * The default table name
     */
    protected $_name = 'site_coupons';
    protected $_primary = array('id');
    protected $_sequence = true;

    protected static $_instance = null;

    /**
     * Singleton instance
     *
     * @return Printed
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function addCoupon($data){
        $sub['number'] = $data['number'];
        $sub['code'] = $data['code'];
        if($data['email'])
            $sub['email'] = $data['email'];

        return $this->insert($sub);
    }

    public function getCollection($number) {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array(
            'c' => $this->_name
        ));
        $select->where("c.number = ?", $number);
        $select->where("c.added >= ?", 'NOW() - INTERVAL 15 MINUTE');
        return $this->fetchAll($select);
    }
}