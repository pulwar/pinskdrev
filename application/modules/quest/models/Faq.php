<?php

/*



CREATE TABLE `site_tags` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `weight` int(10) default '0',
  `priority` int(5) default '0',
  `active` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) 

*/


class Faq extends Zend_Db_Table {

    protected $_name = 'site_faq';
    protected $_primary = array('id');	
	protected $_sequence = true;
    protected static $_instance = null;

    /**
     * Singleton instance
     *
     * @return Faq
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
    public function reindex(){
		$index = New Ext_Search_Lucene('faq', true);
		//$pages_rowset = $this->fetchAll('published=1')
		$count = 10 ;
		$offset = 0 ;
		//print_r($this->getMainTagId(227)); exit;
		while( ( $rowset = $this->fetchAll( 'active=1', null, $count, $offset ) ) && ( 0 < $rowset->count() ) ) {
			while( $rowset->valid() ) {
				$row = $rowset->current() ;
				//
				// Prepare document
				//
				$doc = new Ext_Search_Lucene_Document();
				$doc->setUrl('voprosy#'.$row->id);
                                $vowels=array("<p>","</p>");
				$doc->setTitle(str_replace($vowels, '', $row->question));
                                $doc->setContent(strip_tags($row->question.$row->answer));
				$doc->setId($row->id);

		        $index->addDocument( $doc ) ;

				$rowset->next() ;
			}
			$offset += $count ;
		}

		$index->commit() ;
		return $index->numDocs();

	}
   
	
   

}