<?php
class Quest_IndexController extends Zend_Controller_Action {
	/**
	 * 
	 * @var zend_layout
	 */
	private $layout = null;
	
	/**
	 * 
	 * @var string
	 */
	private $lang = null;
	
	private $_count = null;
	
	private $_offset = null;
	
	private $_current_page = null;
	
	public function init() {
		/*$this->initView()
		$view = Zend_Layout::getMvcInstance()->getView();*/
		
		$this->view->addHelperPath(Zend_Registry::get('helpersPaths'), 'View_Helper');
		$this->layout = $this->view->layout();
		$this->layout->setLayout("front/default");
		$this->lang = $this->_getParam('lang', 'ru');
		$this->layout->lang = $this->lang;
		
		if ($this->_hasParam('reindex')) {
			$this->_forward('reindex');
		}
		$id = $this->_getParam('id');
		$this->view->placeholder('id_page')->set($id);
		$page = Pages::getInstance()->find($id)->current();
		
		if (! is_null($page)) {
			if ($page->published == 0) {
				$this->_redirect('/404');
			}
			$options = PagesOptions::getInstance()->getPageOptions($id);
			$this->view->placeholder('title')->set($options->title);
			$this->view->placeholder('keywords')->set($options->keywords);
			$this->view->placeholder('descriptions')->set($options->descriptions);
			$this->view->placeholder('h1')->set($options->h1);
			
			$this->layout->current_type = 'pages';
			$this->layout->id_page = $page->id;
			$this->layout->id = $id;
			$this->layout->page = $page;
			$this->view->content = $page->content;
			$this->view->current_page = $this->_current_page = $this->_getParam('page', 1);
			$this->view->onpage = $this->_count = $this->_getParam('onpage', 12);
		}
	}
	
	public function indexAction() {
		$this->view->total = $items = Faq::getInstance()->fetchAll('active=1')->count();
		$items = Faq::getInstance()->fetchAll('active=1', 'number ASC', $this->_count, ($this->_current_page - 1) * $this->_count);
		$this->view->items = $items;
		
		$this->layout->port_page = 1;
		$this->view->month = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
		Loader::loadCommon('Captcha');
		$captcha = new Captcha();
		$this->view->addHelperPath(Zend_Registry::get('helpersPaths'), 'View_Helper');
		if ($this->_request->isPost()) {
			$form = $this->_getParam('form', array());
			if ($form['fio'] == '' || $form['email'] == '' || $form['text'] == '' || $form['captcha'] == '') {
				$this->view->err = 1;
				$this->view->form = $form;
			} else if (! $captcha->isValidate($form['captcha'])) {
				$this->view->err = 2;
				$this->view->form = $form;
			} else {
				$users = Users::getInstance()->getUsersToSendMail();
				$emails = '';
				foreach ($users as $user) {
					$emails[] = $user['email'];
				}
				$recipient = 'New message';
				$subject = 'Вопрос';
				
				$template = FeedbackTemplates::getInstance()->getBySystemName('feedback');
				
				$subject = $template['name'];
				$template = $template['content'];
				
				foreach ($form as $key => $param) {
					$template = str_replace('{' . $key . '}', $param, $template);
				}
				$template = preg_replace('/{.+}/Usi', '', $template);
				$body = $template;
				Loader::loadCommon('Mail');
				$from = 'Site';
				
				Mail::send($emails, $body, $from, $subject . ' ' . $_SERVER["HTTP_HOST"], $recipient);
				
				$this->_request->clearParams();
				$this->view->ok = 1;
				$this->view->form = array();
			}
		}
		$this->view->captcha = $captcha->getOperation();
	}
}
