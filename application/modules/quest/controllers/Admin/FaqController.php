<?php

/**
 * Admin_WorksController
 * 
 * @author Grover
 * @version 1.0
 */

class Quest_Admin_FaqController extends MainAdminController  {
	
	/**
 	 * @var string
 	 */
	private $_curModule = null;	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 20;
	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_lang = null;
	
	private $_page = null;
	/**
	 * offset
	 *
	 * @var int
	 */	
	private $_offset = null;	
	
	public function init(){
            $this->view->addHelperPath(Zend_Registry::get('helpersPaths') , 'View_Helper') ;
		//$this->initView();
		$this->layout = $this->view->layout();
		$this->layout->title = "Вопрос-ответ";
		$this->view->caption = 'Список вопросов';
		$lang = $this->_getParam('lang','ru');		
		
		$this->view->currentModule = $this->_curModule = SP.$this->getRequest()->getModuleName().SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);	
		$this->_offset =($this->_page-1)*$this->_onpage;	
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
                
		$this->layout->action_title = "Список вопросов";
		$this->view->currentModule = $this->_curModule;		
		$where = null;
		$this->view->total = count(Faq::getInstance()->fetchAll($where));		
		//$this->view->all = $peoples =  Catalog_Params::getInstance()->fetchAll($where, 'priority DESC', (int)$this->_onpage, (int)$this->_offset);
		$this->view->all = $peoples =  Faq::getInstance()->fetchAll($where, 'number DESC', (int)$this->_onpage, (int)$this->_offset);		
		
	}
	
	public function editAction(){
		$id = $this->_getParam('id', '');
		if ($id){
			$item = Faq::getInstance()->find($id)->current();
			$this->layout->action_title = "Редактировать элемент";
                        
		} else{
			$item = Faq::getInstance()->createRow();
			$this->layout->action_title = "Создать новый элемент";
                        
		}
			
		if ($this->_request->isPost()){						
			$data = $this->trimFilter($this->_getParam('edit'));			
			if ($data['question']!=''){

                            if ($data['created_at']!='') {
                                $adate = preg_match('/([\d]{2}).+([\d]{2}).+([\d]{4})/is', $data['created_at'], $matches);
                                $data['created_at'] = $matches[3].'-'.$matches[2].'-'.$matches[1];
                            } else {
                                $data['created_at'] = date('Y-m-d H:i:s');

                            }
				$item->setFromArray($data);
				$id =  $item->save();				
				$this->view->ok=1;
				
			} else{
				$this->view->err=1;				
			}
			
		}
                $fck_q = $this->getFck('edit[question]', '90%', '200');
		$fck_a = $this->getFck('edit[answer]', '90%', '200');
		$this->view->fck_q = $fck_q;
		$this->view->fck_a = $fck_a;
		
		$this->view->item = $item;
	}
	/**
	 * изменение активности элемента
	 *
	 */
	public function activeAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			$page = Faq::getInstance()->find($id)->current();			
			if (!is_null($page)){
				$page->active =  abs($page->active-1);
				$page->save();
				/*if ($this->_request->isXmlHttpRequest()){
					echo $page->active; exit;
				}*/
			}
						
		}
		$this->_redirect($this->_curModule);
	}

	
	public function deleteAction(){
		$id = $this->_getParam('id');
		if ($id ){
			$item = Faq::getInstance()->find($id)->current();
			$item->delete();			
			$this->_redirect($this->_curModule);
		}
	}
}