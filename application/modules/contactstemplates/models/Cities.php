<?php

class Cities extends Zend_Db_Table {

    /**
     * The default table name
     */
    protected $_name = 'contacts_single';
    /**
     * The default primary key.
     *
     * @var array
     */
    protected $_primary = array ('id' );

    /**
     * Whether to use Autoincrement primary key.
     *
     * @var boolean
     */
    protected $_sequence = true;


    /**
     * Singleton instance.
     *
     * @var Tags
     */
    protected static $_instance = null;





    /**
     * Singleton instance
     *
     * @return Cities
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self( );
        }

        return self::$_instance;
    }

    public function getAll($id) {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('o'=>$this->_name));
        $select->where('id_citie = ?', $id);
        return $this->fetchAll($select);
    }

    public function getAllActive($id) {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('o'=>$this->_name));
        $select->where('id_citie = ?', $id);
        $select->where('active = ?', 1);
        $select->order('priority DESC');
        return $this->fetchAll($select);
    }

}
