<?php

class Contacts extends Zend_Db_Table {

    /**
     * The default table name
     */
    protected $_name = 'contacts_cities';
    /**
     * The default primary key.
     *
     * @var array
     */
    protected $_primary = array ('id' );

    /**
     * Whether to use Autoincrement primary key.
     *
     * @var boolean
     */
    protected $_sequence = true;


    /**
     * Singleton instance.
     *
     * @var Tags
     */
    protected static $_instance = null;





    /**
     * Singleton instance
     *
     * @return Contacts
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self( );
        }

        return self::$_instance;
    }

        public function getAll() {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('o'=>$this->_name));
        return $this->fetchAll($select);
    }

    public function getAllActive() {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('o'=>$this->_name));
        $select->where('active = ?', 1);
        $select->order(array('priority DESC','name'));
        return $this->fetchAll($select);
    }

}
