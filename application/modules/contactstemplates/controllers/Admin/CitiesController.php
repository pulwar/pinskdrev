<?php

class ContactsTemplates_Admin_CitiesController extends MainAdminController {

/**
 * @var string
 */
    private $_curModule = null;
    /**
     * items per page
     *
     * @var int
     */
    private $_onpage = 20;

    /**
     * items per page
     *
     * @var int
     */
    private $_lang = null;

    private $_page = null;
    /**
     * offset
     *
     * @var int
     */
    private $_offset = null;

    public function init() {
    //$this->initView();
        $this->layout = $this->view->layout();
        $this->layout->title = "Контакты";
        $this->view->caption = '';
        $lang = $this->_getParam('lang','ru');

        $this->view->currentModule = $this->_curModule = SP.$this->getRequest()->getModuleName().SP.$lang.SP.$this->getRequest()->getControllerName();
        $this->_page = $this->_getParam('page', 1);
        $this->_offset =($this->_page-1)*$this->_onpage;
        $this->view->current_page = $this->_page;
        $this->view->onpage = $this->_onpage;
    }


    public function indexAction() {
        $id = $this->_getParam('id');
        $this->view->id = $id;
        $this->view->contacts = Cities::getInstance()->getAll($id);
        //$this->view->divisions = Catalog_Division::getInstance()->getAllMainDivisions();
    }

    public function addAction(){
        $id = $this->_getParam('id', '');
        $citie = $this->_getParam('citie');
        $this->view->citie = $citie;
        if ($id){
            $item = Cities::getInstance()->find($id)->current();
            $this->layout->action_title = "Редактировать элемент";
        } else{
            $item = Cities::getInstance()->createRow();
            $this->layout->action_title = "Создать элемент";
        }

        if ($this->_request->isPost()){
            $data = $this->trimFilter($this->_getParam('edit'));
            if ($data['name']!=''){
                $item->setFromArray($data);
                //Zend_Debug::dump($item);exit();
                $id =  $item->save();
                $img_name = $_FILES['img']['name'];
                $img_source = $_FILES['img']['tmp_name'];
                if ($img_name != '' && $img_source != '') {
                    $ext = @end(explode('.', $img_name));
                    $small_img = DIR_PUBLIC . 'pics/catalog/contacts/' . $id . '_img.' . $ext;
                    if (copy($img_source, $small_img)) {
                        $item->image = $id . '_img.' . $ext;
                        $item->save();
                    }
                    }
                    $this->_redirect($this->_curModule.'/edit/id/'.$id.'/citie/'.$data['id_citie']);

            } else{
                $this->view->err=1;
            }

        }
        $this->view->item = $item;
        $fck1 = $this->getFck('edit[adress]', '100%', '200');
        $this->view->fck_adress = $fck1;
        $fck2 = $this->getFck('edit[phones]', '100%', '200');
        $this->view->fck_phones = $fck2;
	$fck3 = $this->getFck('edit[site]', '100%', '200');
        $this->view->fck_site = $fck3;
    }

    public function editAction(){
        $id = $this->_getParam('id', '');
        $citie = $this->_getParam('citie');
        $this->view->citie = $citie;
        if ($id){
            $item = Cities::getInstance()->find($id)->current();
            $this->layout->action_title = "Редактировать элемент";
        } else{
            //$this->_redirect($this->_curModule.'/index/citie/'.$citie);
        }

        if ($this->_request->isPost()){
            $data = $this->trimFilter($this->_getParam('edit'));
            if ($data['name']!=''){
                $item->setFromArray($data);
                $id =  $item->save();
                $img_name = $_FILES['img']['name'];
                $img_source = $_FILES['img']['tmp_name'];
                $delete_img = $this->_getParam('delete_img');
                if ($img_name != '' && $img_source != '' && ! $delete_img) {
                    $ext = @end(explode('.', $img_name));
                    $small_img = DIR_PUBLIC . 'pics/catalog/contacts/' . $id . '_img.' . $ext;
                    if (copy($img_source, $small_img)) {
                        $item->image = $id . '_img.' . $ext;
                        $item->save();
                    }

                } else if ($delete_img) {
                    @unlink(DIR_PUBLIC . 'pics/catalog/contacts/' . $item->image);
                    $item->image = '';
                    $item->save();
                }
                $this->_redirect('/contactstemplates/ru/admin_cities/edit/id/'.$id.'/citie/'.$data['id_citie']);
                $this->view->ok=1;
            } else{
                $this->view->err=1;
            }

        }
        $this->view->item = $item;
        $fck1 = $this->getFck('edit[adress]', '100%', '200');
        $this->view->fck_adress = $fck1;
        $fck2 = $this->getFck('edit[phones]', '100%', '200');
        $this->view->fck_phones = $fck2;
	$fck3 = $this->getFck('edit[site]', '100%', '200');
        $this->view->fck_site = $fck3;
    }


    public function activeAction(){
        if($this->_hasParam('id')){
            $id = (int)$this->getRequest()->getParam('id');
            $page = Cities::getInstance()->find($id)->current();
            if (!is_null($page)){
                $page->active =  abs($page->active-1);
                $page->save();
            }

        }
        $this->_redirect($this->_curModule);
    }


    public function deleteAction(){
        $id = $this->_getParam('id');
        if ($id ){
            $item = Cities::getInstance()->find($id)->current();
            $item->delete();
        }
        $this->_redirect($this->_curModule);
    }


}
