<?php

class ContactsTemplates_Admin_ContactsController extends MainAdminController {

/**
 * @var string
 */
    private $_curModule = null;
    /**
     * items per page
     *
     * @var int
     */
    private $_onpage = 20;

    /**
     * items per page
     *
     * @var int
     */
    private $_lang = null;

    private $_page = null;
    /**
     * offset
     *
     * @var int
     */
    private $_offset = null;

    public function init() {
    //$this->initView();
        $this->layout = $this->view->layout();
        $this->layout->title = "Контакты/Города";
        $this->view->caption = '';
        $lang = $this->_getParam('lang','ru');

        $this->view->currentModule = $this->_curModule = SP.$this->getRequest()->getModuleName().SP.$lang.SP.$this->getRequest()->getControllerName();
        $this->_page = $this->_getParam('page', 1);
        $this->_offset =($this->_page-1)*$this->_onpage;
        $this->view->current_page = $this->_page;
        $this->view->onpage = $this->_onpage;
    }


    public function indexAction() {
        $this->view->cities = Contacts::getInstance()->getAll();
        //$this->view->divisions = Catalog_Division::getInstance()->getAllMainDivisions();
    }

    public function addAction(){
        $id = $this->_getParam('id', '');
        if ($id){
            $item = Contacts::getInstance()->find($id)->current();
            $this->layout->action_title = "Редактировать элемент";
        } else{
            $item = Contacts::getInstance()->createRow();
            $this->layout->action_title = "Создать элемент";
        }

        if ($this->_request->isPost()){
            $data = $this->trimFilter($this->_getParam('edit'));
            if ($data['name']!=''){
                $item->setFromArray($data);
                $id =  $item->save();
                $this->_redirect($this->_curModule.'/edit/id/'.$id);

            } else{
                $this->view->err=1;
            }

        }
        $this->view->item = $item;
    }

    public function editAction(){
        $id = $this->_getParam('id', '');
        if ($id){
            $item = Contacts::getInstance()->find($id)->current();
            $this->layout->action_title = "Редактировать элемент";
        } else{
            $this->_redirect($this->_curModule);
        }

        if ($this->_request->isPost()){
            $data = $this->trimFilter($this->_getParam('edit'));
            if ($data['name']!=''){
                $item->setFromArray($data);
                $id =  $item->save();
                $this->view->ok=1;

            } else{
                $this->view->err=1;
            }

        }
        $this->view->item = $item;
    }


    public function activeAction(){
        if($this->_hasParam('id')){
            $id = (int)$this->getRequest()->getParam('id');
            $page = Contacts::getInstance()->find($id)->current();
            if (!is_null($page)){
                $page->active =  abs($page->active-1);
                $page->save();
            }

        }
        $this->_redirect($this->_curModule);
    }


    public function deleteAction(){
        $id = $this->_getParam('id');
        if ($id ){
            $item = Contacts::getInstance()->find($id)->current();
            $item->delete();
        }
        $this->_redirect($this->_curModule);
    }


}