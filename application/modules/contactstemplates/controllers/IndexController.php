<?php
/**
 * Created by JetBrains PhpStorm.
 * User: desolate
 * Date: 4/17/13
 * Time: 5:01 AM
 * To change this template use File | Settings | File Templates.
 */

class ContactsTemplates_IndexController extends DefaultController {
    /**
     * Шаблон страницы
     * @var zend_layout
     */
    private $layout = null;

    /**
     * Язык страницы
     * @var string
     */
    private $lang = null;

    /**
     * Объект страницы
     * @var Pages_row
     */
    private $_page = null;

    private $_onpage = 999;

    private $_current_page = null;

    private $_offset = null;

    public function init() {
        $this->view->addHelperPath( Zend_Registry::get( 'helpersPaths' ), 'View_Helper' );
        $id = $this->_getParam( 'id' );
        $page = Pages::getInstance()->getPage( $this->_getParam( 'id' ) );
        $this->_page = $page;
        $this->layout = $this->view->layout();
        $this->layout->setLayout( "front/default" );
        $this->layout->current_type = 'pages';
        $this->layout->lang = $this->lang;
        $this->layout->page = $this->_page;
        $this->lang = $this->_getParam( 'lang', 'ru' );
        $this->_onpage = $this->view->onpage = $this->_getParam( 'onpage', 999 );
        $this->_current_page = $this->view->current_page = $this->_getParam( 'page', 1 );
        $this->_offset = ($this->_current_page - 1) * $this->_onpage;
        $this->view->content = $page->content;
        $this->view->options = $options = PagesOptions::getInstance()->getPageOptions( $id );
        $this->view->placeholder( 'title' )->set( $options->title );
        $this->view->placeholder( 'keywords' )->set( $options->keywords );
        $this->view->placeholder( 'descriptions' )->set( $options->descriptions );
        $this->view->placeholder( 'h1' )->set( $options->h1 );
        $this->view->placeholder( 'id_page' )->set( $id );

        $this->layout->id_object = $this->_page->id;

        if ($page->show_childs == 1) {
            $this->layout->page_childs = Pages::getInstance()->getChildrenAndURLs( $page->id );
        }
    }

    public function indexAction() {
        $this->view->contacts = Contacts::getInstance()->getAllActive();
    }



}
