<?php


class Makers extends Zend_Db_Table {

    protected $_name = 'site_makers';
    protected $_primary = array('id');	
	protected $_sequence = true;
    protected static $_instance = null;
    /**
     * 
     * @var Zend_Cache_Frontend_Class
     */
    protected $_cache = null;


    /**
	 * Class to use for rows.
	 *
	 * @var string
	 */
	protected $_rowClass = "Makers_Row" ;


    /**
     * Singleton instance
     *
     * @return Makers
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
    
    /**
     * получение всех элементов в виде массива для formSelect
     * @return array
     */
    public function getItemsAsOptions() {
    	$select = $this->select()
    		->order('priority DESC');
    	$items = $this->fetchAll($select);
    	$options = array(0=> 'Нет');
    	if ($items->count()){
    		foreach ($items as $item){
    			$options[$item->id] = $item->title;
    		}
    	}	
    	return $options;
    }
	public function getActiveItemsAsOptions() {
    	$select = $this->select()
			->where('active = 1')
    		->order('priority DESC');
    	$items = $this->fetchAll($select);
    	$options = array(0=> 'Нет');
    	if ($items->count()){
    		foreach ($items as $item){
    			$options[$item->id] = $item->title;
    		}
    	}	
    	return $options;
    }

    public function getMakersByDivId($id_division) {
        $maker_ids = Catalog_Product::getInstance()->getMakersIdsByDivId($id_division);
        
        $ids=array();
        $items=array();

        $ids=$maker_ids->toArray();
        if($maker_ids->count()){
        $select = $this->select()
                ->order('priority DESC')
                ->where("`id` IN (?)", $ids);
        $items = $this->fetchAll($select);
        }
        $options = array(0=> 'Все');
    	if (count($items)){
    		foreach ($items as $item){
    			$options[$item->id] = $item->title;
    		}
    	}
        return $options;
    }

    public function getPublicMakers($count=null, $offset=null){
        $select = $this->select();
        $select->from($this->_name);
        $select->where('active =?', 1);
        $select->order('priority DESC');
        $select->limit($count, $offset);
        return $this->fetchAll($select);
    }
	
	public function getMakersForMenu($page_path){
        $select = $this->select();
        $select->from(array('sm' => $this->_name), array("title AS name", "CONCAT('".$page_path."','/item/',sm.id) AS path"));
        $select->where('active = ?', 1);
		$select->where('show_in_menu = ?', 1);
        $select->order('priority DESC');
        return $this->fetchAll($select);
    }
	
    public function getCountPublicMakers(){
        $select = $this->select();
		$select->from($this->_name, 'COUNT(*) as count');
		$select->where('active = ?', 1);
		return $this->getAdapter()->fetchOne($select);
    }

    public function getPublicItem($id){
        $select = $this->select();
        $select->from($this->_name);
        $select->where('id =?', $id);
        $select->where('active =?', 1);

        return $this->fetchRow($select);
    }
}