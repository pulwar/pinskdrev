<?php

class Makers_Row extends Zend_Db_Table_Row {
	

	/**
	 * gets tovar from this division
	 * return Catalog_Product_Rowset
	 */
	public function getProducts($count = null, $offset = null) {
            
            return Catalog_Product::getInstance()->getPublicProductsByMakerId($this->id, $count, 'RAND()');
	}
	

	/**
	 * Allows post-update logic to be applied to row.
	 * Subclasses may override this method.
	 *
	 * @return void
	 */
	protected function _postUpdate() {
		//Ext_Search_Lucene::deleteItemFromIndex( $this->id, Ext_Search_Lucene::CATALOG_DIVISIONS );
		//$this->addItemToSearchIndex();
		//Catalog_Division::getInstance()->setParentIds();
	
	}
	
	/**
	 * Allows post-insert logic to be applied to row.
	 * Subclasses may override this method.
	 *
	 * @return void
	 */
	protected function _postInsert() {
		//$this->addItemToSearchIndex();
		//Catalog_Division::getInstance()->setParentIds();
	}
	
	/**
	 * добавление элемента в индекс
	 * @return void
	 *
	 */
	protected function addItemToSearchIndex() {
		$index = Ext_Search_Lucene::open( Ext_Search_Lucene::CATALOG_DIVISIONS );
		$doc = new Ext_Search_Lucene_Document( );
		$doc->setUrl( SiteDivisionsType::getInstance()->getPagePathBySystemName( 'division' ) . '/division/' . $this->id );
		$doc->setTitle( $this->name );
		$doc->setContent( strip_tags( $this->description ) );
		$doc->setId( $this->id );
		$index->addDocument( $doc );
	}

}