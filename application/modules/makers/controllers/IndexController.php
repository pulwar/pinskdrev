<?php
class Makers_IndexController extends DefaultController {
	/**
	 * Шаблон страницы
	 * @var zend_layout
	 */
	private $layout = null;
	
	/**
	 * Язык страницы
	 * @var string
	 */
	private $lang = null;
	
	/**
	 * Объект страницы
	 * @var Pages_row
	 */
	private $_page = null;
	
	private $_onpage = 6;
	
	private $_current_page = null;
	
	private $_offset = null;
	
	public function init() {
		$this->view->addHelperPath( Zend_Registry::get( 'helpersPaths' ), 'View_Helper' );
		$this->view->path = SiteDivisionsType::getInstance()->getPagePathBySystemName('makers');
		$this->view->catalog_path = SiteDivisionsType::getInstance()->getPagePathBySystemName('division');
		$id = $this->_getParam( 'id' );
		$page = Pages::getInstance()->getPage( $this->_getParam( 'id' ) );
		
		if (is_null( $page ) || $page->published == '0') {
			//$this->_redirect( '/404' );
		}
		$this->_page = $page;
		$this->layout = $this->view->layout();
		$this->layout->setLayout( "front/default" );
		$this->layout->current_type = 'pages';
		$this->layout->lang = $this->lang;
		$this->layout->page = $this->_page;
		$this->lang = $this->_getParam( 'lang', 'ru' );
		if ($this->_hasParam( 'item' )) {
			$this->_forward( 'maker' );
		}
		$this->_onpage = $this->view->onpage = $this->_getParam( 'onpage', 6 );
              	$this->_current_page = $this->view->current_page = $this->_getParam( 'page', 1 );
		$this->_offset = ($this->_current_page - 1) * $this->_onpage;
		//$this->view->onpage = $this->_onpage;
		
		$this->view->content = $page->content;
		$this->view->options = $options = PagesOptions::getInstance()->getPageOptions( $id );
		$this->view->placeholder( 'title' )->set( $options->title );
		$this->view->placeholder( 'keywords' )->set( $options->keywords );
		$this->view->placeholder( 'descriptions' )->set( $options->descriptions );
		$this->view->placeholder( 'h1' )->set( $options->h1 );
		$this->view->placeholder( 'id_page' )->set( $id );
		
		$this->layout->id_object = $this->_page->id;
		
		if ($page->show_childs == 1) {
			$this->layout->page_childs = Pages::getInstance()->getChildrenAndURLs( $page->id );
		}
	}
	
	/**
	 * раздел каталога
	 */
	public function indexAction() {
		$this->view->makers = Makers::getInstance()->getPublicMakers($this->_onpage, $this->_offset);
        $this->view->total = Makers::getInstance()->getCountPublicMakers();
	}
	
	/**
	 * раздел каталога
	 */
	public function makerAction() {
		$id_maker = $this->_getParam( 'item' );
                
		$maker = Makers::getInstance()->getPublicItem( $id_maker );
                
		if ($maker != null) {
			$this->view->placeholder( 'title' )->set( $maker->seo_title );
			$this->view->placeholder( 'keywords' )->set( $maker->seo_keywords );
			$this->view->placeholder( 'descriptions' )->set( $maker->seo_description );
			$this->view->placeholder( 'h1' )->set( $maker->title );
			//$bread_items = Pages::getInstance()-> $division->getBread( $this->_page->path );
			$bread_items [] = array ('title' => $maker->title);
			$this->view->layout()->bread_items = $bread_items;
		} else {
			$this->_redirect( '/404' );
		}
		
				
		$this->view->maker = $maker;
                
	
	}

       
}
