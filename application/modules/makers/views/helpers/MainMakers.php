<?php

/**
 * Layout Helper
 * вывод хлебных крошек
 * 
 *
 */
class View_Helper_mainMakers extends Zend_View_Helper_Abstract
{
	
	/**
	 * @var Zend_View
	 */
	protected $_view = null ;
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {		
		$this->_view = Zend_Registry::get('view');
	}
	
	/**
	 * Блок с ножом на главной странице
	 *
	 * @return phtml
	 */
    public function mainMakers($count)
    {
    	$this->init();
        $this->_view->makers = Makers::getInstance()->fetchAll('active=1 AND main=1','priority DESC',$count);
        $this->_view->path = SiteDivisionsType::getInstance()->getPagePathBySystemName('makers');
        return $this->_view->render('MainMakers.phtml') ;
    }
}
