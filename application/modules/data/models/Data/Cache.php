<?php
/** 
 * @author SVKPRO
 * 
 * 
 */
class Data_Cache extends Zend_Cache {
	
	const CACHE_DIR =  'data';
	const CACHE_TAG = 'data';
	
	/**
	 * 
	 * @var Zend_Cache_Frontend_Class
	 */
	protected static $_cache = null;
	
	/**
     * Singleton instance
     *
     * @return Zend_Cache_Frontend_Class
     */
    public static function getInstance() {
        if (null === self::$_cache) {
        	$backendName = 'File';
			$frontendName = 'Class';
			
       		$cache_dir = DIR_DB_CACHE . self::CACHE_DIR;
			if (!is_dir($cache_dir)){
				mkdir($cache_dir, 0777);
			}

			$frontendOptions = array(			
			    'cached_entity' 	=> Data::getInstance(),
				'cache_id_prefix'	=>'data_',
				'caching'			=> true,  			
			);

			$backendOptions = array ('cache_dir' => $cache_dir, 'cache_file_umask' => 0777);		
			$cache = parent::factory($frontendName, $backendName, $frontendOptions, $backendOptions);
			$cache->setTagsArray(array(self::CACHE_TAG));
            self::$_cache = $cache;
        }

        return self::$_cache;
    }
	
	
	
	/**
	 * removing of cache
	 */
	public static function clean(){
		$cache = self::getInstance();
		$cache->clean(Zend_Cache::CLEANING_MODE_ALL);
	}
}

?>