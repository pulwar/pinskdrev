<?php
/**
 * Goods
 */
class Data_Goods extends Zend_Db_Table {
	/**
     * The default table name.
     *
     * @var string
     */
	protected $_name = 'site_catalog_product_units';
	
	/**
     * The default primary key.
     *
     * @var array
     */
	protected $_primary = array(
			'id' 
	);
	
	/**
     * Whether to use Autoincrement primary key.
     *
     * @var boolean
     */
	protected $_sequence = true;
	

	/**
     * Singleton instance.
     *
     * @var Goods
     */
	protected static $_instance = null;
	
	/**
     * Singleton instance
     *
     * @return Goods
     */
	public static function getInstance() {
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
     * Addition of product
     *
     * @param array $data
     */
	public function addProduct($data) {
		return $this->insert($data);
	}

	
	public function clearAvailability() {
		$where[] = $this->getAdapter()->quoteInto("id!= ?", 0);		
		$data = array('in_stock_into_store' => 0,'in_stock_to_factory' => 0,'in_stock_into_store_tr' => 0,'in_stock_to_factory_tr' => 0);
		return $this->update($data, $where);
	}

	public function updateData($data) {
		$where[] = $this->getAdapter()->quoteInto('uniq_id = ?', $data['uniq_id']);
		return $this->update($data, $where);
	}

	public function updateDataTr($data) {
		$where[] = $this->getAdapter()->quoteInto('uniq_id_tr = ?', $data['uniq_id_tr']);
		return $this->update($data, $where);
	}

	public function updatePlusData($data) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array('i' => $this->_name), array(
				'i.in_stock_into_store' 
		));
		$select->where("i.uniq_id = ?", $data['uniq_id']);		
		//$result = $this->getAdapter()->fetchOne($select);
        $result = $this->fetchAll($select);
        if($result){  
            // echo 'yes';
            // echo "<br>";  	
			$where[] = $this->getAdapter()->quoteInto("uniq_id= ?", $data['uniq_id']);		
			$data2 = array("in_stock_into_store" => $data['in_stock_into_store']+$result[0]['in_stock_into_store']);
			return $this->update($data2, $where);
		} else {
			// echo 'no';
   //          echo "<br>";
			return 0;
		}

	}

	public function updatePlusDataTr($data) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array('i' => $this->_name), array(
				'i.in_stock_into_store_tr' 
		));
		$select->where("i.uniq_id_tr = ?", $data['uniq_id_tr']);		
		//$result = $this->getAdapter()->fetchOne($select);
		$result = $this->fetchAll($select);
        if($result){        	
			$where[] = $this->getAdapter()->quoteInto("uniq_id_tr= ?", $data['uniq_id_tr']);		
			$data2 = array("in_stock_into_store_tr" => $data['in_stock_into_store_tr']+$result[0]['in_stock_into_store_tr']);
			return $this->update($data2, $where);
		} else {
			return 0;
		}
	}
}
