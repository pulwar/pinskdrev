<?php

/**
 * Layout Helper
 *
 */
class View_Helper_getDataContent extends Zend_View_Helper_Abstract
{
   
/**
	 * @var Zend_View
	 */
	protected $_view = null ;
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		
		//$this->_view = Zend_Registry::get('view');
	}
	
	public function getDataContent($system_name, $order = null){
		$cache_blocks = Data_Cache::getInstance();
		return $cache_blocks->getContentBySystemName($system_name, $order);
		
	}	
}