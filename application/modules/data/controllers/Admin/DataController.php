<?php

/**
 * Admin_DataController
 * 
 * @author Grover
 * @version 1.0
 */

class Data_Admin_DataController extends MainAdminController  {
	
	/**
 	 * @var string
 	 */
	private $_curModule = null;	

	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_page = null;

	/**
	 * priceFiles
	 *
	 * @var array
	 */	
	private $_priceFiles = null;
	
	public function init(){
		$this->layout = $this->view->layout();
		$this->layout->title = "Данные";
		$this->view->caption = 'Синхронизация данных';
		$lang = $this->_getParam('lang','ru');

		$this->view->currentModule = $this->_curModule = SP.$this->getRequest()->getModuleName().SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);
		$this->view->current_page = $this->_page;
		$this->_priceFiles = scandir("files/sync/prices");
		unset($this->_priceFiles[0]);
		unset($this->_priceFiles[1]);
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		$this->layout->action_title = "Синхронизация данных";
		$this->view->currentModule = $this->_curModule;
		$this->view->priceFiles = $this->_priceFiles;
	}

	/**
	 * This action for updating of data
	 */
	public function updateDataAction(){			
		$condition = $this->_getParam('condition');
		$file = $this->_getParam('file')!= NULL ? $this->_getParam('file') : 'SCLAD' . date('dmY').'.xls';
		$fileM = $this->_getParam('file')!= NULL ? $this->_getParam('file') : 'SCLADM' . date('dmY').'.xls';
		$filePathM = "files/sync/" . $condition . "/" . $fileM;
		$filePath = "files/sync/" . $condition . "/" . $file;
		if(file_exists($filePath)){
			$objPHPExcel = PHPExcel_IOFactory::load($filePath);
			$goods = Data_Goods::getInstance();
			$data = array();

			//определяем начало документа
			if($condition=='availability') {
				$startRow = 1;
				//обнуляем наличие
				$goods->clearAvailability();
			} else {
				$startRow = 2;
			}
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();

				for ($row = $startRow; $row <= $highestRow; $row++)
				{
					$nomenclature = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					if($nomenclature == 'K'){
						$productCode = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
						$facingCode = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
						$paintCode = trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());
						$uniqueCode = $productCode . $facingCode . $paintCode;

					}
					elseif($nomenclature == 'M'){
						$modelCode = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
						$fabricName = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());
						$fabricName = preg_replace('/\+/','', $fabricName);
						$fabricNameGroup = trim($worksheet->getCellByColumnAndRow(6, $row)->getValue());
						$completingCode = trim($worksheet->getCellByColumnAndRow(7, $row)->getValue());
						$completingCode = preg_replace('/\+/','', $completingCode);
						$uniqueCode = $modelCode . $fabricName . $fabricNameGroup . $completingCode;
					}
					if($condition == 'availability'){
						$data[] = array(
							"uniq_id" => $uniqueCode,
							"in_stock_into_store" => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
							"in_stock_to_factory" => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
						);
						$dataTr[] = array(
							"uniq_id_tr" => $uniqueCode,
							"in_stock_into_store_tr" => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
							"in_stock_to_factory_tr" => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
						);

					}
					elseif($condition == 'prices'){
						$data[] = array(
							"uniq_id" => $uniqueCode,
							"price" => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
						);
						$dataTr[] = array(
							"uniq_id_tr" => $uniqueCode,
							"price_with_tr" => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
						);
					}
				}
			}

			$counter = count($data);

			$updatedRows = 0;
			for($index = 0; $index < $counter; $index++){
				$updatedRow = $goods->updateData($data[$index]);
				if($updatedRow){
					$updatedRows++;
				}
			}
			
			$counterTr = count($dataTr);

			for($index = 0; $index < $counterTr; $index++){
				$updatedRow = $goods->updateDataTr($dataTr[$index]);
				if($updatedRow){
					$updatedRows++;
				}
			}

			// читаем другой документ. Который с буквой М ------------------------------------------------------------------------
			if(file_exists($filePathM)){
				$objPHPExcel = PHPExcel_IOFactory::load($filePathM);
				$goods = Data_Goods::getInstance();
				$data = array();

				//определяем начало документа
				if($condition=='availability') {
					$startRow = 1;
				} else {
					$startRow = 2;
				}
				foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
				{
					$highestRow = $worksheet->getHighestRow();

					for ($row = $startRow; $row <= $highestRow; $row++)
					{
						$nomenclature = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
						if($nomenclature == 'K'){
							$productCode = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
							$facingCode = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
							$paintCode = trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());
							$uniqueCode = $productCode . $facingCode . $paintCode;

						}
						elseif($nomenclature == 'M'){
							$modelCode = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
							$fabricName = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());
							$fabricName = preg_replace('/\+/','', $fabricName);
							$fabricNameGroup = trim($worksheet->getCellByColumnAndRow(6, $row)->getValue());
							$completingCode = trim($worksheet->getCellByColumnAndRow(7, $row)->getValue());
							$completingCode = preg_replace('/\+/','', $completingCode);
							$uniqueCode = $modelCode . $fabricName . $fabricNameGroup . $completingCode;
						}
						if($condition == 'availability'){
							$data[] = array(
								"uniq_id" => $uniqueCode,
								"in_stock_into_store" => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
								//"in_stock_to_factory" => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
							);
							$dataTr[] = array(
								"uniq_id_tr" => $uniqueCode,
								"in_stock_into_store_tr" => $worksheet->getCellByColumnAndRow(9, $row)->getValue(),
								//"in_stock_to_factory_tr" => $worksheet->getCellByColumnAndRow(10, $row)->getValue(),
							);

						}
						elseif($condition == 'prices'){
							$data[] = array(
								"uniq_id" => $uniqueCode,
								"price" => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
							);
							$dataTr[] = array(
								"uniq_id_tr" => $uniqueCode,
								"price_with_tr" => $worksheet->getCellByColumnAndRow(11, $row)->getValue(),
							);
						}
					}
				}

				$counter = count($data);

				//$updatedRows = 0;
				for($index = 0; $index < $counter; $index++){
					$updatedRow = $goods->updatePlusData($data[$index]);
					//echo 123; die();
				// $counterTr = cou
					if($updatedRow){
						$updatedRows++;
					}

				}
				//echo 123; die();
				$counterTr = count($dataTr);

				for($index = 0; $index < $counterTr; $index++){
					$updatedRow = $goods->updatePlusDataTr($dataTr[$index]);
					if($updatedRow){
						$updatedRows++;
					}
				}
			}
			// конец второго документа-----------------------------------------

			//обновить наличие (галочки)
			$this->checkProduct();

			echo json_encode(array(
					'success' => true,
					'message' => $updatedRows ? "Обновлено: " . $updatedRows . "стр." : "Обновлено: 0 стр. Возможно вы использовали документ с актуальными данными для обновления!")
			);die;
		}
		else{
			echo json_encode(array('success' => false, 'message' =>'Не могу найти необходимый файл!'));die;
		}
	}

	private function checkProduct() {		
		$products = Catalog_Product::getInstance()->getAll();
		$count = 0;
		foreach ($products as $key => $product) {
			$productId = $product['id'];
			$units = Catalog_Product_Units::getInstance()->getActiveByProductId($productId);
			$inStock = 0;
			foreach ($units as $key2 => $unit) {
				if($unit['in_stock_to_factory'] > 0 || $unit['in_stock_into_store'] > 0 || $unit['in_stock_to_factory_tr'] > 0 || $unit['in_stock_into_store_tr'] > 0 ) {
					$inStock = 1;
				}
				//обновляем наличие
				if($unit['in_stock_into_store'] + $unit['in_stock_into_store_tr'] > 0) {
					if($unit['in_stock_into_store'] + $unit['in_stock_into_store_tr']!=$unit['id_availability']) {						
						//echo 'equals:'. $unit['in_stock_into_store'].' - '.$unit['id_availability'];
						Catalog_Product_Units::getInstance()->changeAvailability($unit['id'],$unit['in_stock_into_store'] + $unit['in_stock_into_store_tr']);
						//echo "save";
						//echo "<br>";
						$count++;
					}
				} else if($unit['id_availability']>0){
					Catalog_Product_Units::getInstance()->changeAvailability($unit['id'],0);
					$count++;
				}
			}
			Catalog_Product_Default_Values::getInstance()->updateValues($productId, array('103' => $inStock));
			
			
		}
		//echo($count);
	}
}