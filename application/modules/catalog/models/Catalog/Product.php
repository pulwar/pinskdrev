<?php
/**
 * Catalog_Product
 */
class Catalog_Product extends Zend_Db_Table {
	/**
     * The default table name.
     *
     * @var string
     */
	protected $_name = 'site_catalog_product';
	
	/**
     * The default primary key.
     *
     * @var array
     */
	protected $_primary = array(
			'id' 
	);
	
	/**
     * Whether to use Autoincrement primary key.
     *
     * @var boolean
     */
	protected $_sequence = true; // Ис;льзование таблицы с автоинкрементным ключом
	

	/**
     * Singleton instance.
     *
     * @var Catalog_Product
     */
	protected static $_instance = null;
	
	/**
     * Dependent tables.
     *
     * @var array
     */
	protected $_dependentTables = array(
			'Catalog_Product_Units' 
	);
	
	/**
     * Reference map.
     *
     * @var array
     */
	protected $_referenceMap = array();
	
	/**
     * Class to use for rows.
     * Class to use for rows.
     *
     * @var string
     */
	protected $_rowClass = "Catalog_Product_Row";
	
	/**
     * Class to use for row sets.
     *
     * @var string
     */
	protected $_rowsetClass = "Catalog_Product_Rowset";

	/**
     * Singleton instance
     *
     * @return Catalog_Product
     */
	public static function getInstance() {
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
     * backend
     * получение товаров раздела
     * @param int  $id_div
     * @param int $count
     * @param int $offset
     * @return Catalog_Product_Rowset
     */
	public function getProductsByDivId($id_div = null, $count = null, $offset = null) {
		$select = $this->select()->from(array(
				'p' => $this->_name 
		));
		if ($id_div) {
			$select->setIntegrityCheck(false);
			$select->joinInner(array(
					'd' => 'site_catalog_product2divisions' 
			), 'd.id_product=p.id AND d.id_division=' . (int) $id_div, array(
					'cross_id' => 'd.id',
					'priority' 
			));
		}
		$select->order("d.priority DESC");
		$select->order("p.priority DESC");
		$select->limit($count, $offset);
		return $this->fetchAll($select);
	}

	/**
     * frontend
     * @param int $id_div
     * @param int $count
     * @param int $offset
     * @param int $maker_id - id производителя для товара
     * @param int $default_id - id свойства из site_catalog_product_default_values
     * @return Catalog_Product_Rowset
     */
	public function getPublicProductsByDivId($id_div, $count = null, $offset = null, $maker_id = null, $default_id = null, $sort = null, $type_id = null, $collection = null) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		if ($maker_id) {
			$select->joinInner(array(
					'm' => 'site_makers' 
			), 'm.id=p.id_maker AND m.id=' . (int) $maker_id, array(
					'logo_maker' => 'logo',
					'title_maker' => 'title' 
			));
		} else {
			$select->joinLeft(array(
					'm' => 'site_makers' 
			), 'p.id_maker = m.id', array(
					'logo_maker' => 'logo',
					'title_maker' => 'title' 
			));
		}
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), 'd.id_product=p.id AND d.id_division=' . (int) $id_div, array());
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.id = d.main_element', array(
				'img',
				'price',
				'price_with_tr',
				'price_old' 
		));
		$select->joinLeft(array(
				't' => 'site_catalog_product_material' 
		), 'i.id_material = t.id', 'title AS material');
		//if($maker_id){
		//	$select->joinInner(array('mak'=>'site_makers'), 'mak.id=p.id_maker AND mak.id='.(int)$maker_id, array());
		//}
		if ($type_id) {
			$select->where('p.id_mebeltype = ?', (int) $type_id);
		}
		if ($default_id) {
			$select->joinInner(array(
					'dv' => 'site_catalog_product_default_values' 
			), 'dv.id_default=' . (int) $default_id . ' AND dv.id_product=p.id AND dv.value=1', array());
		}
		if ($sort) {
			switch ($sort) {
				case 1 :
					$select->order("i.price ASC");
					break;
				case 2 :
					$select->order("p.title ASC");
					break;
			}
		}
		//$select->where("p.id_division = ?", $id_div);
		$select->where("p.active = ?", 1);
		$select->order("p.priority DESC");
		$select->order("d.priority DESC");
		if ($collection) {
			$select->where("p.title LIKE '%$collection%'");
		} else {
			$select->limit($count, $offset);
		}
		//Zend_Debug::dump($select->assemble()); exit();
		return $this->fetchAll($select);
	}
    public function getPackages() {
        $packages = Catalog_Product_Package::getInstance()->getPackages($this->id);
        if (!$packages || $packages->count() == 0){
            $packages = Catalog_Product_Package::getInstance()->getPackagesForComplect($this->id);
        }
        return $packages;
    }
	/**
     * опубликованный товар
     * @param int $id
     * @return Catalog_Product_Row
     */
	public function getPublicItem($id) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.main=1', array(
				'unit_id' => 'id',
				'nomenclature' => 'nomenclature',
				'img',
				'img_big',
				'price',
				'price_description' 
		));
		$select->joinLeft(array(
				'm' => 'site_makers' 
		), 'm.id = p.id_maker', array(
				'logo_maker' => 'logo',
				'maker' => 'title' 
		));
		$select->joinLeft(array(
				'mt' => 'site_catalog_product_material' 
		), 'i.id_material = mt.id', 'title AS material');
		$select->joinLeft(array(
				'av' => 'site_catalog_product_availability' 
		), 'i.id_availability = av.id', 'title AS availability');
		$select->joinLeft(array(
				'mktm' => 'site_catalog_product_maketerm' 
		), 'i.id_maketerm = mktm.id', 'title AS maketerm');
		$select->joinLeft(array(
				't' => 'site_catalog_product_transformer' 
		), 'i.id_transformer = t.id AND t.active=1', array(
				'transformer' => 'title',
				'transformer_url' => 'url' 
		));
		$select->joinLeft(array(
				'c' => 'site_catalog_product_colors' 
		), 'i.id_color = c.id', 'title AS color');
		$select->where("p.id = ?", $id);
		$select->where("p.active = ?", 1);
		//Zend_Debug::dump($select->assemble()); exit();
		return $this->fetchRow($select);
	}

	/**
     * количество опубликованных товаров в разделе
     * @param int $id_division
     * @return int
     */
	public function getCountPublicInDiv($id_division, $maker_id = null, $category = null, $type_id = null, $collection = null) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		), 'COUNT(*) AS count');
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), "d.id_product=p.id AND d.id_division=" . (int) $id_division, array());
		if ($maker_id) {
			$select->where('p.id_maker = ?', (int) $maker_id);
		}
		if ($type_id) {
			$select->where('p.id_mebeltype = ?', (int) $type_id);
		}
		if ($category) {
			$select->joinInner(array(
					'dv' => 'site_catalog_product_default_values' 
			), 'dv.id_default=' . (int) $category . ' AND dv.id_product=p.id AND dv.value=1', array());
		}
		if ($collection) {
			$select->where("p.title LIKE '%$collection%'");
		}
		$select->where("p.active = ?", 1);
		return $this->getAdapter()->fetchOne($select);
	}

	/**
     * изменение активности
     * @param int $id
     */
	public function changeActivity($id) {
		$row = $this->find($id)->current();
		$row->active = abs($row->active - 1);
		return $row->save();
	}

	/**
     * изменение приоритета для нескольких товаров
     * @param unknown_type $params
     */
	public function setPriority($params, $prices = array()) {
		$ids = array_keys($params);
		$rowset = $this->find($ids);
		foreach ($rowset as $row) {
			$row->priority = $params[$row->id];
			$row->save();
		}
	}

	public function setDefaultOptions($options = array()) {
		$ids = array_keys($options);
		$rowset = $this->find($ids);
		Loader::loadPublicModel("Catalog_Product_Default_Values");
		$defValuesInstance = Catalog_Product_Default_Values::getInstance();
		foreach ($rowset as $row) {
			$defValuesInstance->updateValues($row->id, $options[$row->id]);
		}
	}

	/**
     * действия над товарами
     * @param string $action
     * @param array $ids
     */
	public function processProducts($action, $ids, $div_id) {
		$rowset = $this->find($ids);
		foreach ($rowset as $row) {
			switch ($action) {
				case 'disable' :
					$row->active = 0;
					$row->save();
					break;
				case 'enable' :
					$row->active = 1;
					$row->save();
					break;
				case 'delete' :
					$row->delete();
					break;
				case 'move' :
					$div_row = Catalog_Product2Divisions::getInstance()->fetchRow('id_product=' . (int) $row->id . ' AND is_primary=1');
					$div_row->id_division = $div_id;
					$div_row->save();
					break;
				case 'virtual' :
					$sh_row = Catalog_Product2Divisions::getInstance()->fetchRow('id_product=' . (int) $row->id . ' AND id_division=' . (int) $div_id);
					if (is_null($sh_row)) {
						$shadow_row = Catalog_Product2Divisions::getInstance()->createRow();
						$shadow_row->id_product = $row->id;
						$shadow_row->id_division = $div_id;
						$shadow_row->save();
					}
					break;
			}
		}
	}

	/**
     * Добавление элемента
     *
     * @param array $data
     *
     */
	public function addProduct($data) {
		if (!isset($data['created_at'])) {
			$data['created_at'] = new Zend_Db_Expr('NOW()');
		}
		$data['prior'] = $this->getMaxPriorInDivision($data['id_division']) + 1;
		return $this->insert($data);
	}

	/**
     * проверить есть в разделе с переданным id  товары или нет
     *
     * @param int $id_division
     * @return	boolean
     */
	public function issetProductInDiv($id) {
		$where = $this->getAdapter()->quoteInto('id_division = ?', $id);
		$res = $this->fetchRow($where);
		if ($res != null) {
			return true;
		}
		return false;
	}

	public function getProductByPrior($prior, $div_id) {
		$where = array(
				$this->getAdapter()->quoteInto('priority = ?', $prior),
				$this->getAdapter()->quoteInto('id_division = ?', $div_id) 
		);
		$res = $this->fetchRow($where);
		if ($res == null) {
			return false;
		} else {
			return true;
		}
	}

	/**
     * получение стоимости товара
     * @param int $id_product
     * @return int
     */
	public function getPrice($id_product) {
		$select = $this->select();
		$select->where("id = ?", (int) $id_product);
		$row = $this->fetchRow($select);
		if ($row != null) {
			return $row->price;
		}
		return '';
	}

	public function search($search, $defaults, $count = null, $offset = null) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		if ($search) {
			//Zend_Debug::dump($search); exit();
			if (isset($search['id_division']) && (int) $search['id_division'] != false) {
				$select->joinInner(array(
						'd' => 'site_catalog_product2divisions' 
				), "d.id_product=p.id AND d.id_division=" . (int) $search['id_division'], array());
				//$select->where('id_division = ?',$search['id_division'] );
			}
			if (isset($search['title']) && trim($search['title']) != '') {
				$select->where("p.title LIKE '%{$search['title']}%'");
			}
			if (isset($search['price_from']) && trim($search['price_from']) != '') {
				$select->where('price >= ?', $search['price_from']);
			}
			if (isset($search['price_to']) && trim($search['price_to']) != '') {
				$select->where('price <= ?', $search['price_to']);
			}
			if (isset($search['active']) && $search['active'] == '1') {
				$select->where('p.active = ?', 1);
			}
		}
		if (sizeof($defaults)) {
			$defaults_ids = implode(',', $defaults);
			//echo $defaults_ids;exit;
			$select->joinInner(array(
					'dv' => 'site_catalog_product_default_values' 
			), 'dv.id_product = p.id AND dv.id_default IN (' . $defaults_ids . ') AND dv.value = 1', array());
		}
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'i.id_product = p.id AND i.main=1', array(
				'price',
				'unit_id' => 'id' 
		));
		$select->group('p.id');
		$select->limit($count, $offset);
		$select->order('priority DESC');
		$select->order('title ASC');
		return $this->fetchAll($select);
	}

	public function collectionsearch($title) {
		$select = $this->select();
		$select->from(array(
				'p' => $this->_name 
		));
		$select->setIntegrityCheck(false);
		$select->where("p.active = ?", 1);
		$select->where("(REPLACE(p.title, '\"', '') LIKE '%{$title}%')");
		return $this->fetchAll($select);
	}

	public function moresearch($title = null, $type = null, $material = null, $color = null, $width = null, $price_from = null, $price_to = null, $default_options = null, $color_type = null) {
		$select = $this->select();
		$select->from(array('p' => $this->_name), array(
				'id',
				'url',
				'title',
				'title_short'
		));
		$select->setIntegrityCheck(false);
		$select->where("p.active = ?", 1);
		if ($title) {
			$select->where("(REPLACE(p.title, '\"', '') LIKE '%{$title}%')");
		}
		if (isset($type) && $type) {
			$select->where("id_mebeltype = ?", $type);
		}
		if (isset($material) && $material) {
			$select->where("p.material_ids LIKE '%{$material}%'");
		}
		if (isset($color_type) && $color_type == 1) {
			if (isset($color) && $color) {
				$select->where("p.color_ids LIKE '%{$color}%'");
			}
		} else {
			if (isset($color) && $color) {
				$select->where("p.fabrics_ids LIKE '%{$color}%'");
			}
		}
		if (isset($width) && $width) {
			$select->where("pr_width <= ?", $width);
		}
		if (isset($price_from) && $price_from) {
			$select->where("price >= ?", $price_from);
		}
		if (isset($price_to) && $price_to) {
			$select->where("price <= ?", $price_to);
		}
		if (isset($color_type) && ($color_type == 1) && ($color != null)) {
			$select->joinLeft(array(
					'i' => 'site_catalog_product_units' 
			), 'p.id=i.id_product AND i.color_ids =' . $color, array(
					'img',
					'price',
					'price_with_tr' 
			));
		} elseif (isset($color_type) && ($color_type == 2) && ($color != null)) {
			$select->joinLeft(array(
					'i' => 'site_catalog_product_units' 
			), 'p.id=i.id_product AND i.fabrics_ids =' . $color, array(
					'img',
					'price',
					'price_with_tr' 
			));
		} else {
			$select->joinLeft(array(
					'i' => 'site_catalog_product_units' 
			), 'p.id=i.id_product AND i.main=1', array(
					'img',
					'price',
					'price_with_tr' 
			));
		}
		if (isset($default_options)) {
			$i = 1;
			foreach ($default_options as $option) {
				$select->joinInner(array(
						'dv' . $i . '' => 'site_catalog_product_default_values' 
				), 'p.id = dv' . $i . '.id_product AND dv' . $i . '.value = 1 AND dv' . $i . '.id_default = ' . $option . '', array());
				$i++;
			}
		}
		$select->joinInner(array(
				'p2d' => 'site_catalog_product2divisions' 
		), 'p.id=p2d.id_product AND p2d.is_primary=1', array(
				'division' => 'id_division'
		));
		$select->joinInner(array(
				'd' => 'site_catalog_division'
		), 'd.id=p2d.id_division', array(
				'division_url' => 'url',				
		));
	
		//определяем element_id для комплекта
		$select->joinLeft(array('p2d3'=>'site_catalog_product2divisions'),'p2d3.id_product = p.id AND p2d3.id_division = d.id',array('element_id'=>'p2d3.main_element'));
		//определяем id комплекта
		//$select->joinLeft(array('c'=>'site_catalog_product_complect'),'c.id_unit = p2d3.main_element',array('complect_id'=>'id'));		
		//добавляем сортировку
		//$select->order('complect_id DESC');
		
		$select->order('p.priority DESC');
		$select->order('price');
		$select->limit(300);
		return $this->fetchAll($select);
	}

	public function searchcountproducts($type = null, $material = null, $color = null, $width = null, $price_from = null, $price_to = null, $color_type = null, $akciya = null, $is_new = null, $is_present = null, $econom = null, $for_guests = null, $in_bedroom = null, $for_cabin = null) {
		$select = $this->select();
		$select->from(array(
				'p' => $this->_name 
		));
		$select->setIntegrityCheck(false);
		$select->joinInner(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.main=1', array(
				'price' => 'price' 
		));		
		if((isset($is_new) && $is_new=='true') || (isset($akciya) && $akciya=='true') || (isset($is_present) && $is_present=='true') || (isset($econom) && $econom=='true') || (isset($for_guests) && $for_guests=='true') || (isset($in_bedroom) && $in_bedroom=='true') || (isset($for_cabin) && $for_cabin=='true')) {
			$select->joinInner(array(
					'dv' => 'site_catalog_product_default_values' 
			), 'dv.id_product=p.id', array());
		}
		$select->where("p.active = ?", 1);
		if (isset($type) && $type) {
			$select->where("p.id_mebeltype = ?", $type);
		}
		if (isset($material) && $material) {
			$select->where("p.material_ids LIKE '%{$material}%'");
		}
		if (isset($color_type) && $color_type == 1) {
			if (isset($color) && $color) {
				$select->where("p.color_ids LIKE '%{$color}%'");
			}
		} else {
			if (isset($color) && $color) {
				$select->where("p.fabrics_ids LIKE '%{$color}%'");
			}
		}
		if (isset($width) && $width) {
			$select->where("p.pr_width <= ?", $width);
		}
		
		if (isset($price_from) && $price_from) {
			$select->where("i.price >= ?", (int) $price_from);
		}
		if (isset($price_to) && $price_to) {
			$select->where("i.price <= ?", (int) $price_to);
		}
		if (isset($akciya) && $akciya=='true') {
			$select->where("dv.id_default = 94 AND dv.value = 1");
		}
		if (isset($is_new) && $is_new=='true') {
			$select->where("dv.id_default = 95 AND dv.value = 1");
		}
		if (isset($is_present) && $is_present=='true') {
			$select->where("dv.id_default = 103 AND dv.value = 1");
		}
		if (isset($econom) && $econom=='true') {
			$select->where("dv.id_default = 107 AND dv.value = 1");
		}
		if (isset($for_guests) && $for_guests=='true') {
			$select->where("dv.id_default = 112 AND dv.value = 1");
		}
		if (isset($in_bedroom) && $in_bedroom=='true') {
			$select->where("dv.id_default = 113 AND dv.value = 1");
		}
		if (isset($for_cabin) && $for_cabin=='true') {
			$select->where("dv.id_default = 114 AND dv.value = 1");
		}
		//echo $select->assemble(); exit();
		return $this->fetchAll($select);
	}

	public function countmoresearch($more_search, $count = null, $offset = null) {
		$select = $this->select();
		$select->from(array(
				"p" => $this->_name 
		), 'COUNT(*) as count');
		$select->where("p.active = ?", 1);
		if ($more_search['name']) {
			$select->where("p.title LIKE '%{$more_search['name']}%'");
		}
		if (isset($more_search['id_maker']) && $more_search['id_maker']) {
			$select->where("id_maker = ?", $more_search['id_maker']);
		}
		if (isset($more_search['id_mebeltype']) && $more_search['id_mebeltype']) {
			$select->where("id_mebeltype = ?", $more_search['id_mebeltype']);
		}
		if (isset($more_search['id_material']) && $more_search['id_material']) {
			$select->where("id_material = ?", $more_search['id_material']);
		}
		if (isset($more_search['id_transformer']) && $more_search['id_transformer']) {
			$select->where("id_transformer = ?", $more_search['id_transformer']);
		}
		switch ($more_search['size_search_type']) {
			case 1 :
				$comp = '=';
				break;
			case 2 :
				$comp = '>=';
				break;
			case 3 :
				$comp = '<=';
				break;
		}
		if (isset($more_search['height']) && $more_search['height']) {
			$select->where("pr_height $comp ?", $more_search['height']);
		}
		if (isset($more_search['width']) && $more_search['width']) {
			$select->where("pr_width $comp ?", $more_search['width']);
		}
		if (isset($more_search['length']) && $more_search['length']) {
			$select->where("pr_length $comp ?", $more_search['length']);
		}
		if (isset($more_search['size_search_type_sl']) && $more_search['size_search_type_sl']) {
			switch ($more_search['size_search_type_sl']) {
				case 1 :
					$comp = '=';
					break;
				case 2 :
					$comp = '>=';
					break;
				case 3 :
					$comp = '<=';
					break;
			}
			if ($more_search['sl_length']) {
				$select->where("sl_length $comp ?", $more_search['sl_length']);
			}
			if ($more_search['sl_width']) {
				$select->where("sl_width $comp ?", $more_search['sl_width']);
			}
		}
		if (isset($more_search['price_from']) && $more_search['price_from']) {
			$select->where("price >= ?", $more_search['price_from']);
		}
		if (isset($more_search['price_to']) && $more_search['price_to']) {
			$select->where("price <= ?", $more_search['price_to']);
		}
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.main=1', array());
		return $this->getAdapter()->fetchOne($select);
	}

	public function reindex() {
		$index = new Ext_Search_Lucene(Ext_Search_Lucene::CATALOG_PRODUCTS, true);
		$count = 10;
		$offset = 0;
		//$path = SiteDivisionsType::getInstance()->getPagePathBySystemName('division');
		while (($rowset = $this->fetchAll(null, null, $count, $offset)) && (0 < $rowset->count())) {
			while ($rowset->valid()) {
				$row = $rowset->current();
				$doc = new Ext_Search_Lucene_Document();
				$doc->setUrl('/product/' . $row->id);
				$doc->setTitle($row->title);
				$doc->setId($row->id);
				$doc->setContent(strip_tags($row->description));
				$index->addDocument($doc);
				$rowset->next();
			}
			$offset += $count;
		}
		$index->commit();
		return $index->numDocs();
	}

	public function getPopular() {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product', 'img');
		$select->where("p.popular = ?", 1);
		return $this->fetchAll($select);
	}

	/**
     * при удалении производителя отметка в товаре сбрасывается
     * @param int $id_maker
     */
	public function clearMaker($id_maker) {
		if ((int) $id_maker) {
			$where[] = $this->getAdapter()->quoteInto('id_maker = ?', (int) $id_maker);
			$data = array(
					'id_maker' => 0 
			);
			return $this->update($data, $where);
		}
	}

	public function clearMebeltype($id_mebeltype) {
		if ((int) $id_mebeltype) {
			$where[] = $this->getAdapter()->quoteInto('id_mebeltype = ?', (int) $id_mebeltype);
			$data = array(
					'id_mebeltype' => 0 
			);
			return $this->update($data, $where);
		}
	}

	public function getRelated($id = 0, $count = null) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinInner(array(
				'r' => 'site_catalog_product_related' 
		), 'r.id_related_product=p.id', array());
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.active=1 AND i.main=1', array(
				'img',
				'img_big',
				'price',
				'price_with_tr' 
		));
		$select->joinLeft(array(
				'd' => 'site_catalog_product2divisions' 
		), 'p.id=d.id_product AND d.is_primary=1', array(
				'division' => 'id_division' 
		));
		$select->group('p.id');
		$select->where("r.id_product = ?", $id);
		$select->where("p.active = ?", 1);
		$select->order("i.main DESC");
		$select->order("RAND()");
		return $this->fetchAll($select);
	}

	public function getCollection($id = 0, $count = null) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinInner(array(
				'r' => 'site_catalog_product_collections' 
		), 'r.id_collection_product=p.id', array());
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.active=1 AND i.main=1', array(
				'img',
				'img_big',
				'price',
				'price_with_tr' 
		));
		$select->joinLeft(array(
				'd' => 'site_catalog_product2divisions' 
		), 'p.id=d.id_product AND d.is_primary=1', array(
				'division' => 'id_division' 
		));
		$select->group('p.id');
		$select->where("r.id_product = ?", $id);
		$select->where("p.active = ?", 1);
		$select->order("i.main DESC");
		$select->order("RAND()");
		return $this->fetchAll($select);
	}

	public function getElite($sis_name, $count = null) {
		$id = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName($sis_name);
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinInner(array(
				'dv' => 'site_catalog_product_default_values' 
		), 'dv.id_default=' . $id . ' AND dv.id_product=p.id AND dv.value=1', array());
		$select->joinLeft(array(
				'd' => 'site_catalog_product2divisions' 
		), 'p.id=d.id_product AND d.is_primary=1', array(
				'division' => 'id_division' 
		));
		$select->joinInner(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.main=1', array(
				'price',
				'price_with_tr' 
		));
		$select->order("RAND()");
		$select->where("p.active = ?", 1);
		$select->limit($count);
		return $this->fetchAll($select);
	}

	public function getMainDivisionProducts($sis_name, $div_id, $count = null) {
		$id = Catalog_Product_Default::getInstance()->getDefaultIdBySystemName($sis_name);
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinInner(array(
				'scd' => 'site_catalog_division' 
		), 'scd.id=' . $div_id, array());
		$select->joinInner(array(
				'dv' => 'site_catalog_product_default_values' 
		), 'dv.id_default=' . $id . ' AND dv.id_product=p.id AND dv.value=1', array());
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), 'p.id=d.id_product AND d.id_division = ' . $div_id, array(
				'division' => 'id_division' 
		));
		$select->joinInner(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.main=1', 'price');
		$select->order("RAND()");
		$select->limit($count);
		$select->where("p.active = ?", 1);
		$select->where("scd.active = ? ", 1);
		return $this->fetchAll($select);
	}

	public function getPublicProductsByMakerId($maker_id, $count = null, $order = null, $offset = null) {
		//echo "maker_id=$maker_id count=$count order=$order offset=$offset";exit;
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinInner(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.main=1', 'img');
		$select->joinLeft(array(
				'd' => 'site_catalog_product2divisions' 
		), 'p.id=d.id_product AND d.is_primary=1', array(
				'division' => 'id_division' 
		));
		$select->where("p.active = ?", 1);
		$select->where("p.id_maker = ?", $maker_id);
		$select->order($order);
		$select->limit($count, $offset);
		return $this->fetchAll($select);
	}

	public function getMakersIdsByDivId($id_division) {
		$select = $this->select();
		$select->from(array(
				'p' => $this->_name 
		), array(
				'p.id_maker' 
		));
		$select->setIntegrityCheck(false);
		$select->distinct(true);
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), "d.id_product=p.id AND d.id_division=" . (int) $id_division, array());
		$select->where("p.active = ?", 1);
		$select->where("p.id_maker > ?", 0);
		return $this->fetchAll($select);
	}

	public function getTypesIdsByDivId($id_division) {
		$select = $this->select();
		$select->from(array(
				'p' => $this->_name 
		), array(
				'p.id_mebeltype' 
		));
		$select->setIntegrityCheck(false);
		$select->distinct(true);
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), "d.id_product=p.id AND d.id_division=" . (int) $id_division, array());
		$select->where("p.active = ?", 1);
		$select->where("p.id_mebeltype > ?", 0);
		return $this->fetchAll($select);
	}

	public function checkUrl($url, $id = null) {
		$where = "url='$url'";
		if ($id != null) {
			$where .= " AND id!=" . (int) $id;
		}
		$row = $this->fetchRow($where);
		if (!is_null($row)) {
			return false;
		} else {
			return true;
		}
	}

	public function getIdByUrl($url) {
		$where = "`url` = '$url'";
		$item = $this->fetchRow($where);
		if (count($item)) {
			return $item->id;
		}
		return null;
	}

	public function getAllProductsInDivision($division) {
		//$ids = explode(',', $division->child_ids);
		$ids = Catalog_Division::getInstance()->getChildsIds($division->id, $division->level + 1);
		$ids[] = $division->id;
		$divids = implode(',', $ids);
		//echo $divids;exit;
		$select = $this->select();
		$select->from(array(
				'p' => $this->_name 
		));
		$select->setIntegrityCheck(false);
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), "d.id_product=p.id AND d.id_division IN (" . $divids . ")", array());
		return $this->fetchAll($select);
	}

	public function getAllActive($id) {
		$select = $this->select();
		$select->from(array(
				'p' => $this->_name 
		));
		$select->setIntegrityCheck(false);
		$select->where('active=1');
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), "d.id_product=p.id AND d.id_division = $id", array(
				"COALESCE(d.id_division,'') AS division" 
		));
		return $this->fetchAll($select);
	}

	public function getPublicProductsAkcii($sort = null, $count = null, $offset = null) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.main=1', 'img');
		$select->joinLeft(array(
				'm' => 'site_makers' 
		), 'p.id_maker = m.id', array(
				'logo_maker' => 'logo',
				'title_maker' => 'title' 
		));
		$select->joinLeft(array(
				't' => 'site_catalog_product_material' 
		), 'i.id_material = t.id', 'title AS material');
		$select->joinLeft(array(
				'd' => 'site_catalog_product2divisions' 
		), 'd.id_product=p.id AND d.is_primary=1', array(
				'division' => 'id_division' 
		));
		$select->joinInner(array(
				'dv' => 'site_catalog_product_default_values' 
		), 'dv.id_default=94 AND dv.id_product=p.id AND dv.value=1', array());
		if ($sort != null) {
			$select->where("p.id_mebeltype IN ($sort)");
		}
		$select->where("p.active = ?", 1);
		$select->order("p.priority DESC");
		//$select->order("d.priority DESC");
		$select->limit($count, $offset);
		return $this->fetchAll($select);
	}

	public function checkSleep($id_type) {
		$select = $this->select();
		$select->from($this->_name, 'COUNT(*) as count');
		$select->where("active = ?", 1);
		$select->where("id_mebeltype = ?", $id_type);
		$select->where("sl_length != ?", 0);
		$select->where("sl_width != ?", 0);
		return $this->getAdapter()->fetchOne($select);
	}

	public function getTovarById($id_tovar) {
		$select = $this->select();
		$select->where('id = ?', $id_tovar);
		return $this->fetchRow($select);
	}

	public function saveProduct($tovar_id, $site, $seo_title, $seo_h1, $seo_description, $seo_keywords, $fck_intro, $fck_description) {
		$this->getAdapter()->query("
          UPDATE $this->_name
		  SET
		    intro_$site = $fck_intro,
		    description_$site = $fck_description,
		    seo_title_$site = $seo_title,
		    seo_h1_$site = $seo_h1,
		    seo_description_$site = $seo_description,
		    seo_keywords_$site = $seo_keywords
		  WHERE
		    id = $tovar_id
		  ");
	}

	public function getPublicProductsByDivisionsIds($products) {
		$id_div = implode(',', $products);
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), "d.id_product=p.id AND d.id_division IN (" . $id_div . ")", array());
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.id=d.main_element', array(
				'img',
				'price',
				'price_old',
				'price_with_tr' 
		));
		$select->where("p.active = ?", 1);
		$select->order("RAND()");
		$select->limit(12);
		//Zend_Debug::dump($this->fetchAll($select));
		return $this->fetchAll($select);
	}

	public function getAllPublic() {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.main=1', array(
				'img',
				'price',
				'price_with_tr' 
		));
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), "d.id_product=p.id AND d.is_primary = 1", array(
				'id_division' 
		));
		$select->where("p.active = ?", 1);
		$select->where("p.export_yandex = ?", 1);
		return $this->fetchAll($select);
	}

	public function getAllProducts() {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.main=1', array(
				'img',
				'price',
				'price_with_tr' 
		));
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), "d.id_product=p.id AND d.is_primary = 1", array(
				'id_division' 
		));
		$select->where("p.active = ?", 1);
		return $this->fetchAll($select);
	}

	public function getAll() {
		$select = $this->select();
		$select->setIntegrityCheck(false);		
		$select->from(array('p' => $this->_name), array(
				'p.id'				
		));	
		return $this->fetchAll($select);
	}

	public function getAllProductsForExcel($collection, $type) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.main=1', array(
				'img',
				'price',
				'price_with_tr' 
		));
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), "d.id_product=p.id AND d.is_primary = 1", array(
				'id_division' 
		));
		$select->joinInner(array(
				'c' => 'site_collections' 
		), "c.id=p.collection_id", array(
				'collection_name' => 'title' 
		));
		$select->joinInner(array(
				't' => 'site_catalog_product_mebeltype' 
		), "t.id=p.id_mebeltype", array(
				'mebeltype' => 'title' 
		));
		if ($collection != 0) {
			$select->where("p.collection_id = ?", $collection);
		}
		if ($type != 0) {
			$select->where("p.id_mebeltype = ?", $type);
		}
		$select->where("p.active = ?", 1);
		$select->order("c.priority DESC");
		$select->order("p.product_type DESC");
		return $this->fetchAll($select);
	}

	public function getActions($ids) {
		if (count($ids) > 1) {
			$action_ids = implode(',', $ids);
		} else {
			$action_ids = $ids;
		}
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		));
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.main=1', array(
				'img',
				'price',
				'price_with_tr' 
		));
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), "d.id_product=p.id AND d.is_primary = 1", array());
		$select->where("p.active = ?", 1);
		if (count($ids) > 1) {
			$select->where("p.action_types IN ($action_ids)");
		} else {
			$select->where("p.action_types = ?", $action_ids);
		}
		return $this->fetchAll($select);
	}

	public function getUrlById($id) {
		$db = $this->getAdapter();
		$sql = "
                SELECT url
                FROM site_catalog_product
                WHERE id = '$id'
                ";
		$result = $db->fetchOne($sql);
		return $result;
	}

	public function getActionIfExistenceIsWell($id) {
		$db = $this->getAdapter();
		$sql = "
                SELECT value
                FROM site_catalog_product_default_values
                WHERE id_default = 94 AND id_product = '$id' 
                ";
		$result = $db->fetchOne($sql);
		return $result;
	}

	public function getPublicProductsByDivisionsIdsCount($products) {
		$id_div = implode(',', $products);
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array(
				'p' => $this->_name 
		), 'COUNT(*) AS count_total');
		$select->joinInner(array(
				'd' => 'site_catalog_product2divisions' 
		), "d.id_product=p.id AND d.id_division IN (" . $id_div . ")", array());
		$select->joinLeft(array(
				'i' => 'site_catalog_product_units' 
		), 'p.id=i.id_product AND i.id=d.main_element', array(
				'img',
				'price',
				'price_with_tr' 
		));
		$select->where("p.active = ?", 1);
		return $this->fetchAll($select);
	}
}
