<?php

class Catalog_Fabrics_Items extends Zend_Db_Table {
	
	protected $_name = 'site_catalog_fabrics_items';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом
	protected static $_instance = null;
	
	protected $_dependentTables = array(
		'Catalog_Fabrics_Items2Product'
		
	);
	
	protected $_referenceMap    = array(
        'groups' => array(
            'columns'           => 'id_group',
            'refTableClass'     => 'Catalog_Fabrics_Groups',
            'refColumns'        => 'id',
	 		'onDelete'          => self::CASCADE,
        )
        
    );
	
	/**
	 * Class to use for row	
	 *
	 * @var string
	 */
	protected $_rowClass = "Catalog_Fabrics_Items_Row" ;
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Fabrics_Items
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public function getSelectedItemsByGroup($id_product, $id_group, $price, $price_mehanizm){
           
		$db = $this->getAdapter();
		$sql = "
			SELECT $this->_name.*, site_catalog_fabrics_items2product.related, site_catalog_fabrics_items2product.main,
			$price AS price, $price_mehanizm AS price_mehanizm
			FROM $this->_name
			INNER JOIN site_catalog_fabrics_items2product ON (site_catalog_fabrics_items2product.id_item=$this->_name.id)
			WHERE 
				$this->_name.id_group=".(int)$id_group." AND 
				site_catalog_fabrics_items2product.id_product=".(int)$id_product." AND
				$this->_name.active=1
			ORDER BY priority DESC, name ASC	
				";
				
		$result = $db->fetchAll($sql);
		return $result;
				
	}
	
}









