<?php

class Catalog_Fabrics_Groups2Product extends Zend_Db_Table {
	
	protected $_name = 'site_catalog_fabrics_groups2product';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом
	protected static $_instance = null;
	

	
	 protected $_referenceMap    = array(
        'groups' => array(
            'columns'           => 'id_group',
            'refTableClass'     => 'Catalog_Fabrics_Groups',
            'refColumns'        => 'id',
	 		'onDelete'          => self::CASCADE,
        ),
        'tovar'=> array(
            'columns'           => 'id_product',
            'refTableClass'     => 'Catalog_Product',
            'refColumns'        => 'id',
	 		'onDelete'          => self::CASCADE,
        ),
    );
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Fabrics_Groups2Product
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	/**
	 * получение цены группы текстур для товара
	 * @param int  $id_product
	 * @param int $id_group
	 */
	public function getPriceByTovarGroup($id_product, $id_group){
		return $this->fetchRow("id_product = '$id_product' AND id_group = '$id_group'");
		
	}

        public function getPricesByProductId($id){
            $select = $this->select();
            $select->from(array('t'=>$this->_name));
            $select->setIntegrityCheck(false);
            $select->joinInner(
			array('g'=>'site_catalog_fabrics_groups'),
			't.id_group=g.id AND is_decor = 0',
			array('id_group'=>'id','name'=>'name','priority'=>'priority')
            );
            $select->where('t.id_product = ?', $id );
            $select->order('g.priority DESC');
            return $this->fetchAll($select);
        }
	
	
}