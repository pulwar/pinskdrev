<?php

class Catalog_Product_Complect extends Zend_Db_Table {
	
	protected $_name = 'site_catalog_product_complect';
	protected $_primary = array('id');
	protected $_sequence = true; // Ис;льзование таблицы с автоинкрементным ключом
	protected static $_instance = null;
	
	protected $_dependentTables = array();
	
	protected $_referenceMap = array(
		'rel_units' => array(
            'columns'           => array('id_complect_unit'),
            'refTableClass'     => 'Catalog_Product_Units',
            'refColumns'        => array('id'),
            'onDelete'          => self::CASCADE
            
        ),
		'units' => array(
            'columns'           => array('id_unit'),
            'refTableClass'     => 'Catalog_Product_Units',
            'refColumns'        => array('id'),
            'onDelete'          => self::CASCADE
        )
	);

    /**
	 * Singleton instance
	 *
	 * @return Catalog_Product_Complect
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public function deleteByUnitId($id){
		$where = "id_unit = '$id' OR id_complect_unit = '$id'";
		$this->delete($where);
	}

	public function getUnitComplect($id){
		$select = $this->select();
		$select->from(array('c'=>$this->_name));
		$select->setIntegrityCheck(false);
		$select->joinLeft(
			array('i'=>'site_catalog_product_units'),
			'i.id = c.id_complect_unit',
			array('title','price')
		);
		$select->joinLeft(
			array('p'=>'site_catalog_product'),
			'p.id = i.id_product',
			array('title_product'=>'title')
		);
		$select->where('c.id_unit = ?', $id);
		$select->order('c.priority DESC');
		//Zend_Debug::dump($select->assemble()); exit();
		return $this->fetchAll($select);
	}

	public function ProcessComplect($count, $priority, $transform, $main_unit){
		if(count($count) && count($priority)){
			$ids = array_keys($count);
			$rowset = $this->find($ids);
			foreach ($rowset as $row){
				$row->priority = $priority[$row->id];
				$row->count = $count[$row->id];
                $row->with_transform = $transform[$row->id];
                $row->main_unit = $main_unit[$row->id];
				$row->save();
			}
		}
	}

	public function getComplect($id){
		$select = $this->select();
		$select->from(array('c'=>$this->_name));
		$select->setIntegrityCheck(false);
		$select->joinLeft(
			array('i'=>'site_catalog_product_units'),
			'i.id = c.id_complect_unit',
			array('img'=>'img','price','price_with_tr')
		);
		$select->joinLeft(
			array('p'=>'site_catalog_product'),
			'p.id = i.id_product',
			array('id AS id_product','title','url','intro','pr_width','pr_height','pr_length')
		);
		$select->joinLeft(
			array('p2d'=>'site_catalog_product2divisions'),
			'p2d.id_product = i.id_product AND p2d.is_primary = 1',
			array()
		);
		$select->joinLeft(
			array('d'=>'site_catalog_division'),
			'd.id = p2d.id_division',
			array('div_url'=>'url')
		);
		$select->where('c.id_unit = ?', $id);
		//$select->order('i.main DESC');
		$select->order('c.priority DESC');
		$select->group('p.id');
		return $this->fetchAll($select);
	}

	public function copyComplect($id, $new_id){
		$select = $this->select();
		$select->from($this->_name);
		$select->where('id_unit = ?', $id);
		$rowset = $this->fetchAll($select);
		foreach($rowset as $item){
			$item = $item->toArray();
			unset($item['id']);
			$item['id_unit'] = $new_id;
			$this->insert($item);
		}
	}
}
