<?php
class Catalog_Product_Row extends Zend_Db_Table_Row {
	
	private $_default_options = array();
	
	public function getPrice() {
		return number_format($this->price, 2, '.', '');
	}
	
	public function getImgMain() {
		return Catalog_Product_Units::getInstance()->fetchRow('active=1 AND id_product=' . (int)$this->id, array('main DESC', 'priority DESC'));
	}
	
	public function getImages() {
		return Catalog_Product_Units::getInstance()->fetchAll('active=1 AND id_product=' . (int)$this->id, array('main DESC', 'priority DESC'));
	}
	
	public function getImageNovinka() {
		$img = Catalog_Product_Units::getInstance()->fetchRow('active=1 AND id_product=' . (int)$this->id, array('is_new DESC', 'priority DESC', 'main DESC'));
		if ($img) {
			return $img->img;
		} else {
			return '';
		}
	}
        public function getPackages() {
            $packages = Catalog_Product_Package::getInstance()->getPackages($this->id);
            if (!$packages || $packages->count() == 0){
                $packages = Catalog_Product_Package::getInstance()->getPackagesForComplect($this->id);
            }
        return $packages;
    }
	public function getImageAkcia() {
		$img = Catalog_Product_Units::getInstance()->fetchRow('active=1 AND id_product=' . (int)$this->id, array('is_action DESC', 'priority DESC', 'main DESC'));
		if ($img) {
			return $img->img;
		} else {
			return '';
		}
	}
	
	public function getImagePopular() {
		$img = Catalog_Product_Units::getInstance()->fetchRow('active=1 AND id_product=' . (int)$this->id, array('is_popular DESC', 'priority DESC', 'main DESC'));
		if ($img) {
			return $img->img;
		} else {
			return '';
		}
	}
	
	public function getOptions() {
		return Catalog_Product_Options_Enabled::getCache()->getAllOptionsValues($this->id);
	}
	
	/**
	 * получение параметров вместе со значениями
	 */
	public function getDefaultOptions() {
		$options = Catalog_Product_Default::getInstance()->getDefaultToProduct($this->id);
		if ($options) {
			foreach ($options as $key => $option) {
				$this->_default_options[$option->system_name] = $option;
			}
		}
		return $this->_default_options;
	}
	
	/**
	 * получение значения опции по system_name
	 * @param string $system_name
	 */
	public function getDefValue($system_name) {
		if (! $this->_default_options) {
			$this->_default_options = $this->getDefaultOptions();
		}
		if (isset($this->_default_options[$system_name])) {
			$option = $this->_default_options[$system_name];
			return $option->value;
		}
		return '';
	}
	
	public function divisionUrl($div_id = null) {
		if (! $div_id && $this->division) {
			$div_id = $this->division;
		} else {
			$where = "id_product = " . $this->id;
			$order = "is_primary DESC";
			$item = Catalog_Product2Divisions::getInstance()->fetchRow($where, $order);
			$div_id = $item->id_division;
		}
		return Catalog_Division::getInstance()->getUrlById($div_id);
	}
	
	public function getDivisionUrl() {
		return Catalog_Division::getInstance()->getDivisionUrlByProductId($this->id);
	}
	
	/**
	 * каскадное удаление
	 * удаляет все зависемости
	 */
	public function delete() {
		Catalog_Product_Options_Prices::getInstance()->deletePricesByProduct($this->id);
		Catalog_Product_Options_Values::getInstance()->deleteByProduct($this->id);
		Catalog_Product_Options_Enabled::getInstance()->deleteByProduct($this->id);
		Catalog_Product_Units::getInstance()->deleteByProduct($this->id);
		Catalog_Product_Default_Values::getInstance()->deleteValues($this->id);
		// Ext_Search_Lucene::deleteItemFromIndex ( $this->id, Ext_Search_Lucene::CATALOG_PRODUCTS );
		return parent::delete();
	}
	
	/**
	 * Allows post-update logic to be applied to row.
	 * Subclasses may override this method.
	 *
	 * @return void
	 */
	protected function _postUpdate() {
		//Ext_Search_Lucene::deleteItemFromIndex($this->id, Ext_Search_Lucene::CATALOG_PRODUCTS);
	//if ($this->active == 1) {
	//	$this->addItemToSearchIndex();
	//}
	}
	
	/**
	 * Allows post-insert logic to be applied to row.
	 * Subclasses may override this method.
	 *
	 * @return void
	 */
	protected function _postInsert() {
		//if ($this->active == 1) {
	//	$this->addItemToSearchIndex();
	//}
	}
	
	/**
	 * добавление элемента в индекс
	 * @return void
	 *
	 */
	protected function addItemToSearchIndex() {
		$index = Ext_Search_Lucene::open(Ext_Search_Lucene::CATALOG_PRODUCTS);
		$doc = new Ext_Search_Lucene_Document();
		$doc->setUrl('/cat/' . $this->divisionUrl($this->getDivMain()) . '/' . $this->url);
		$doc->setTitle($this->title);
		$doc->setContent(strip_tags($this->description));
		$doc->setId($this->id);
		$index->addDocument($doc);
	}
	
	public function getDivMain() {
		$row = Catalog_Product2Divisions::getInstance()->fetchRow('id_product=' . (int)$this->id . ' AND is_primary = 1');
		if ($row != null)
			return $row->id_division;
		else
			return null;
	}
}