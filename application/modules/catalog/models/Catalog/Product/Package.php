<?php

class Catalog_Product_Package extends Zend_Db_Table {
	
	protected $_name = 'site_catalog_product_package';
	protected $_primary = array('id');
	protected $_sequence = true;
	protected static $_instance = null;
	/**
	 * 
	 * @var Zend_Cache_Frontend_Class
	 */
	protected $_cache = null;
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Product_Package
	 */
	public static function getInstance() {
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		
		return self::$_instance;
	}


	public function getPackageByProduct($id) {
		$select = $this->select();
		$select->where('id_tovar = ?', $id);
		return $this->fetchAll($select);
	}

    public function getPackages($tovar_id) {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('p' => $this->_name), array("p.id", "p.id_tovar", "p.depth", "p.width_p", "p.height", "p.weight", "p.name", "(1) as cnt"));
        $select->joinInner(array('i'=>'site_catalog_product'), 'i.id = p.id_tovar', array());
        $select->where('p.id_tovar = ?', $tovar_id);
        $select->order('p.priority DESC');
        return $this->fetchAll($select);
    }

    public function getPackagesForComplect($tovar_id) {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('p'=>$this->_name), array("id", "id_tovar", "depth", "width_p", "height", "weight", "name"));
        $select->joinInner(array('i'=>'site_catalog_product'), 'i.id = p.id_tovar', array());
        $select->joinInner(array('u'=>'site_catalog_product_units'), 'i.id = u.id_product', array());
        $select->joinInner(array('c'=>'site_catalog_product_complect'), 'c.id_complect_unit=u.id', array("c.count AS cnt"));
        $select->joinInner(array('u2'=>'site_catalog_product_units'), 'c.id_unit = u2.id', array());
        $select->where("u2.id_product = ?", $tovar_id);
        $select->where("i.active = 1");
        $select->group("p.id");
        $select->order(array("c.priority DESC", "p.priority DESC", "id_tovar", "name"));
        return $this->fetchAll($select);
    }
}
