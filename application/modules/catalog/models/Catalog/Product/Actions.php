<?php

class Catalog_Product_Actions extends Zend_Db_Table {
	
	protected $_name = 'site_divisions_type';
	protected $_primary = array('id');
	protected $_sequence = true;
	protected static $_instance = null;
	/**
	 * 
	 * @var Zend_Cache_Frontend_Class
	 */
	protected $_cache = null;
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Product_Actions
	 */
	public static function getInstance() {
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		
		return self::$_instance;
	}
	
	public function getAllActions() {
        $db = $this->getAdapter();
        $sql =  '
                SELECT *
                FROM site_divisions_type
                WHERE action = 1
                ';
        $result = $db->fetchAll($sql);
        return $result;
    }
}