<?php


class Catalog_Product_Color extends Zend_Db_Table {

    protected $_name = 'site_catalog_product_colors';
    protected $_primary = array('id');	
	protected $_sequence = true;
    protected static $_instance = null;
    /**
     * 
     * @var Zend_Cache_Frontend_Class
     */
    protected $_cache = null;

    /**
     * Singleton instance
     *
     * @return Catalog_Product_Color
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    /**
     * получение всех элементов в виде массива для formSelect
     * @return array
     */
    public function getAllColors() {
    	$select = $this->select()
    		->order('priority DESC');
    	$items = $this->fetchAll($select);
    	return $items;
    }
   
    public function getColorsByIds($ids){
        $select = $this->select();
        $select->from($this->_name);
        $select->where("`id` IN ($ids)");
        $select->order('priority DESC');
        //echo $select->__toString();exit;
        return $this->fetchAll($select);
    }

    public function getColorsAsArray(){
        $select = $this->select()
    		->order('priority DESC');
    	$items = $this->fetchAll($select);
		$options = array();
    	if ($items->count()){
    		foreach ($items as $item){
    			$options[$item->id] = $item->img;
    		}
    	}
    	return $options;
    }
	
	/**
	 * получение всех элементов в виде массива для formSelect
	 * @return array
	 */
	public function getItemsAsOptions() {
		$select = $this->select()->order('priority DESC');
		$items = $this->fetchAll($select);
		$options = array(0 => 'Нет');
		if ($items->count()) {
			foreach ($items as $item) {
				$options[$item->id] = $item->title;
			}
		}
		return $options;
	}

    public function getByProduct($ids) {
        $select = $this->select()
            ->order('priority DESC');
        $items = $this->fetchAll($select);
        $all = explode(',', $ids);
        $options = array();
        if ($items->count()){
            foreach ($items as $item){
                if(in_array($item->id, $all)) {
                    $options[] = array ($item->id, $item->title, $item->intro, $item->img) ;
                }
            }
        }
        return $options;
    }

public function getItemsAsOptionsForSearch($ids) {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('p' => $this->_name));
        $select->where("p.id IN ($ids)");
        $items = $this->fetchAll($select);
        $html = '<option value="0">Цвет</option>';
        if ($items->count()){
            foreach ($items as $item){
                $html .='<option data-image="/pics/color/'.$item->img.'" value="'.$item->id.'">'.$item->title.'</option>';
            }
            //$html .= '</select></div>';
        }
        return $html;
    }

	public function getNameById($id) {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('p' => $this->_name));
        $select->where("p.id = ?", $id);
        $item = $this->fetchRow($select);
        return $item->title;
    }
}
