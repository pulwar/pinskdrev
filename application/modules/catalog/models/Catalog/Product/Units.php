<?php

class Catalog_Product_Units extends Zend_Db_Table {
	
	protected $_name = 'site_catalog_product_units';
	protected $_primary = array('id');
	protected $_sequence = true; // Ис;льзование таблицы с автоинкрементным ключом
	protected static $_instance = null;
	
	/**
	 * Class to use for row	
	 *
	 * @var string
	 */
	protected $_rowClass = "Catalog_Product_Units_Row";
	
	protected $_dependentTables = array();
	
	protected $_referenceMap = array('tovar' => array('columns' => 'id_product', 'refTableClass' => 'Catalog_Product', 'refColumns' => 'id', 'onDelete' => self::CASCADE));
	
	/**
	 * действия с элементами
	 * @var unknown_type
	 */
	private $_options = array('disable' => ' Заблокировать', 'enable' => ' Активировать', 'delete' => ' Удалить');
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Product_Units
	 */
	public static function getInstance() {
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	/**
	 * получение опций
	 */
	public function getOptions() {
		return $this->_options;
	}
	
	/**
	 * получать фотографии товара
	 * @param int  $id_product
	 * @param int $count
	 * @param int $offset
	 * @return Zend_Db_Table_Rowset
	 */
	public function getImagesByProductId($id_product, $count = null, $offset = null) {
		$select = $this->select();
		$select->where("id_product = ?", (int)$id_product);
		$select->order('priority DESC');
		$select->limit($count, $offset);
		return $this->fetchAll($select);
	}
	
	public function getActiveByProductId($id_product, $count = null, $offset = null) {
		$select = $this->select();
		$select->where("id_product = ?", (int)$id_product);
		$select->where("active = ?", 1);
		$select->order('priority DESC');
		$select->limit($count, $offset);
		return $this->fetchAll($select);
	}
	
	/**
	 * изменение активности
	 * @param int $id
	 */
	public function changeActivity($id) {
		$row = $this->find($id)->current();
		if ($row != null) {
			$row->active = abs($row->active - 1);
			return $row->save();
		}
	}

	public function changeAvailability($id,$val) {
		$row = $this->find($id)->current();
		if ($row != null) {
			$row->id_availability = $val;
			return $row->save();
		}
	}

	    public function getAll() {
		$select = $this->select();
		$select->where("active = ?", 1);
		return $this->fetchAll($select);
	    }
	
	public function setPrices($prices) {
		$ids = array_keys($prices);
		foreach ($ids as $product_id) {
			$data = array('price' => $prices[$product_id]);
			$where = array($this->getAdapter()->quoteInto('id_product = ?', (int)$product_id));
			$this->update($data, $where);
		}
	}
	
	/**
	 * изменение приоритета для нескольких элементов 
	 * @param unknown_type $params
	 */
	public function setProperties($priority, $titles, $prices, $prices_with_tr, $is_new=array(), $is_action=array(), $is_popular=array()) {
		if (is_array($priority)) {
			$ids = array_keys($priority);
			$rowset = $this->find($ids);
			foreach ($rowset as $row) {
				$row->priority = $priority[$row->id];
				$row->title = isset($titles[$row->id]) ? $titles[$row->id] : '';
				$row->price = $prices[$row->id];
				$row->price_with_tr = $prices_with_tr[$row->id];
				if (isset($is_new[$row->id])) $row->is_new = $is_new[$row->id];
				if (isset($is_action[$row->id])) $row->is_action = $is_action[$row->id];
				if (isset($is_popular[$row->id])) $row->is_popular = $is_popular[$row->id];
				$row->save();
			}
		}
	}
	
	/**
	 * действия над элементами
	 * @param string $action
	 * @param array $ids
	 */
	public function processImages($action, $ids) {
		$rowset = $this->find($ids);
		foreach ($rowset as $row) {
			switch ($action) {
				case 'disable' :
					$row->active = 0;
					$row->save();
					break;
				case 'enable' :
					$row->active = 1;
					$row->save();
					break;
				case 'delete' :
					$row->delete();
					break;
			}
		}
	}
	
	/**
	 * Загрузка изображений 
	 */
	public function upload_Images($id_product) {
		foreach ($_FILES as $key => $file) {
			if ($file['error'] === 0 && strpos($file['type'], 'image') !== false && strpos($key, 'image') !== false) {
				$img_name = $file['name'];
				$img_source = $file['tmp_name'];
				if (isset($img_name) && $img_name != '' && $img_source != '') {
					$ext = @end(explode('.', $img_name));
					$name = date('Ymd_h_i_s');
					$img_big = DIR_PUBLIC . 'pics/catalog/product/' . $name . '_img.' . $ext;
					if (copy($img_source, $img_big)) {
						$row = $this->fetchNew();
						$row->img = $name . '_img.' . $ext;
						$row->title = isset($_POST['title_' . $key]) ? $_POST['title_' . $key] : '';
						$row->id_product = $id_product;
						$row->active = 1;
						$row->save();
					}
				}
			}
		}
	}
	
	/**
	 * получение главной картинки к товару
	 * @param int $id_product
	 */
	public function getMainByProduct($id_product) {
		$select = $this->select();
		$select->where("id_product = ?", $id_product);
		$select->where("main = ?", 1);
		$select->where("active = ?", 1);
		return $this->fetchRow($select);
	}
	
	public function getById($id) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array('i' => $this->_name));
		$select->joinLeft(array('mt' => 'site_catalog_product_material'), 'i.id_material = mt.id', 'title AS material');
		$select->joinLeft(array('av' => 'site_catalog_product_availability'), 'i.id_availability = av.id', 'title AS availability');
		$select->joinLeft(array('mktm' => 'site_catalog_product_maketerm'), 'i.id_maketerm = mktm.id', 'title AS maketerm');
		$select->joinLeft(array('t' => 'site_catalog_product_transformer'), 'i.id_transformer = t.id AND t.active=1', array('transformer' => 'title', 'transformer_url' => 'url'));
		$select->joinLeft(array('c' => 'site_catalog_product_colors'), 'i.id_color = c.id', 'title AS color');
		$select->where("i.id = ?", $id);
		$select->where("i.active = ?", 1);
		return $this->fetchRow($select);
	}

	public function getByParentId($parent_id) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array('i' => $this->_name));
		$select->joinLeft(array('mt' => 'site_catalog_product_material'), 'i.id_material = mt.id', 'title AS material');
		$select->joinLeft(array('av' => 'site_catalog_product_availability'), 'i.id_availability = av.id', 'title AS availability');
		$select->joinLeft(array('mktm' => 'site_catalog_product_maketerm'), 'i.id_maketerm = mktm.id', 'title AS maketerm');
		$select->joinLeft(array('t' => 'site_catalog_product_transformer'), 'i.id_transformer = t.id AND t.active=1', array('transformer' => 'title', 'transformer_url' => 'url'));
		$select->joinLeft(array('c' => 'site_catalog_product_colors'), 'i.id_color = c.id', 'title AS color');
		$select->where("i.parent_id = ?", $parent_id);
		$select->where("i.active = ?", 1);
		return $this->fetchRow($select);
	}

    public function getPrice($id,$transformer) {
        $db = $this->getAdapter();
        if ($transformer != 0) {
            $field = 'price_with_tr';
        }
        else {
            $field = 'price';
        }
        $sql = "SELECT $field FROM site_catalog_product_units WHERE id='$id'";
        $select= $db->fetchOne($sql);
        //Zend_Debug::dump($select);exit();
        return $select;
    }

    public function getoldPrice($id,$transformer) {
        $db = $this->getAdapter();
        if ($transformer != 0) {
            $field = 'price_old';
        }
        else {
            $field = 'price_old';
        }
        $sql = "SELECT $field FROM site_catalog_product_units WHERE id='$id'";
        $select= $db->fetchOne($sql);
        //Zend_Debug::dump($select);exit();
        return $select;
    }

    public function getAvailability($id) {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('i' => $this->_name));
        $select->joinLeft(array('pa' => 'site_catalog_product_availability'), 'i.id_availability = pa.id', 'title AS avaibl');
        $select->joinLeft(array('mt' => 'site_catalog_product_maketerm'), 'i.id_maketerm = mt.id', 'title AS terms');
        $select->where("i.id = ?", $id);
        $select->where("i.active = ?", 1);
        return $this->fetchRow($select);
    }

    public function getAvailabilityCount($id,$transformer) {   
        $select = $this->select();
        $select->setIntegrityCheck(false);
        if($transformer != 0){
	        $select->from(array('i' => $this->_name), array(
					'id',
					'in_stock_into_store_tr as id_availability'
			));
		} else {
			$select->from(array('i' => $this->_name), array(
					'id',					
					'in_stock_into_store as id_availability'
			));
		}
        $select->joinLeft(array('pa' => 'site_catalog_product_availability'), 'i.id_availability = pa.id', 'title AS avaibl');
        $select->joinLeft(array('mt' => 'site_catalog_product_maketerm'), 'i.id_maketerm = mt.id', 'title AS terms');
        $select->where("i.id = ?", $id);
        $select->where("i.active = ?", 1);
        return $this->fetchRow($select);          
    }

    public function getDelivery($id) {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('i' => $this->_name));
        $select->where("i.id = ?", $id);
        $select->where("i.active = ?", 1);
        return $this->fetchRow($select);
    }

    public function getByTovar($id, $mat, $type) {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('i' => $this->_name));
        $select->joinLeft(array('mt' => 'site_catalog_product_material'), 'i.id_material = mt.id', 'title AS material');
        $select->joinLeft(array('av' => 'site_catalog_product_availability'), 'i.id_availability = av.id', 'title AS availability');
        $select->joinLeft(array('mktm' => 'site_catalog_product_maketerm'), 'i.id_maketerm = mktm.id', 'title AS maketerm');
        $select->joinLeft(array('t' => 'site_catalog_product_transformer'), 'i.id_transformer = t.id AND t.active=1', array('transformer' => 'title', 'transformer_url' => 'url'));
        $select->joinLeft(array('c' => 'site_catalog_product_colors'), 'i.id_color = c.id', 'title AS color');
        $select->where("i.id_product = ?", $id);
        $select->where("i.$type = ?", $mat);
        $select->where("i.active = ?", 1);
        //Zend_Debug::dump($id);Zend_Debug::dump($mat);Zend_Debug::dump($type);
        return $this->fetchRow($select);
    }
	
	/**
	 * удаление главной картинки товара
	 * @param unknown_type $id_product
	 */
	public function deleteMainByProduct($id_product) {
		$select = $this->select();
		$select->where("id_product = ?", $id_product);
		$select->where("main = ?", 1);
		$row = $this->fetchRow($select);
		if ($row != null) {
			$row->delete();
		}
	}
	
	public function deleteByProduct($id_product) {
		$select = $this->select();
		$select->where("id_product = ?", (int)$id_product);
		$rowset = $this->fetchAll($select);
		if ($rowset->count()) {
			foreach ($rowset as $row) {
				$row->delete();
			}
		}
	}
	
	public function checkParams($row) {
		$params = array('main', 'is_new', 'is_action', 'is_popular');
		foreach ($params as $param) {
			if ($row->$param == 1) {
				$where[] = $this->getAdapter()->quoteInto("$param = ?", 1);
				$where[] = $this->getAdapter()->quoteInto("id != ?", $row->id);
				$where[] = $this->getAdapter()->quoteInto("id_product = ?", $row->id_product);
				$data = array($param => 0);
				$this->update($data, $where);
			}
			$where = array();
		}
	}
	
	public function checkMain($row) {
		$where[] = $this->getAdapter()->quoteInto("main= ?", 1);
		$where[] = $this->getAdapter()->quoteInto("id != ?", $row->id);
		$where[] = $this->getAdapter()->quoteInto("id_product = ?", $row->id_product);
		$data = array('main' => 0);
		$this->update($data, $where);
	}
	
	public function clearMaterial($id_material) {
		if ((int)$id_material) {
			$where[] = $this->getAdapter()->quoteInto('id_material = ?', (int)$id_material);
			$data = array('id_material' => 0);
			return $this->update($data, $where);
		}
	}
	
	public function clearMaketerm($id_maketerm) {
		if ((int)$id_maker) {
			$where[] = $this->getAdapter()->quoteInto('id_maketerm = ?', (int)$id_maketerm);
			$data = array('id_maketerm' => 0);
			return $this->update($data, $where);
		}
	}

	public function getUnitsByProductToSelect($id, $division_id, $main_div) {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array('i' => $this->_name));
		$select->where("i.id_product = ?", $id);
		$select->where("i.active = ?", 1);
		$result = $this->fetchAll($select);
        return $result;
    }
	
	public function clearColor($id_color) {
		if ((int)$id_maker) {
			$where[] = $this->getAdapter()->quoteInto('id_color = ?', (int)$id_color);
			$data = array('id_color' => 0);
			return $this->update($data, $where);
		}
	}

	public function addImgWithWatermark($name) {
        foreach(glob('pics/catalog/product/big/*') as $images) {
            $img_new = substr($images,25);
            if ($img_new == $name) {
                $text = ROOT_DIR . 'text3.png';
                $logo = ROOT_DIR . 'logo.png';

                $text_source = imagecreatefrompng ( $text );
                $text_size = getimagesize ( $text );

                $logo_source = imagecreatefrompng ( $logo );
                $logo_size = getimagesize ( $logo );

                $img_src = $images;
                $img_size = getimagesize ( $img_src );
                $format = strtolower ( substr ( $img_size ['mime'], strpos ( $img_size ['mime'], '/' ) + 1 ) );
                $icfunc = "imagecreatefrom" . $format;
                $scfunc = "image" . $format;
                $img_source = $icfunc ( $img_src );
                $x_text = ($img_size[0] - $text_size[0])/2;
                $y_text = ($img_size[1] - $text_size[1])/2;
                $x_logo = ($img_size[0] - $logo_size[0]);
                $y_logo = ($img_size[1] - $logo_size[1]);
                imagecopy($img_source, $text_source, $x_text - 15, $y_text, 0, 0, $text_size[0], $text_size[1]);
                imagecopy($img_source, $logo_source, $x_logo, $y_logo, 0, 0, $logo_size[0], $logo_size[1]);
                $images=substr($images,21);
                $path = 'cached_images/';
                $images = $path.$images;
                $scfunc ( $img_source, $images, 100 );
            }
        }
    }

    public function getByColor($id_product, $color) {
        $select = $this->select();
        $select->where('id_product=?', $id_product);
        $select->Where('fabrics_ids=?', $color);
        $select->orWhere('color_ids=?', $color);
        $row = $this->fetchRow($select);
        return $row->id;
    }
}
