<?php


class Catalog_Product_Mebeltype extends Zend_Db_Table {

    protected $_name = 'site_catalog_product_mebeltype';
    protected $_primary = array('id');	
	protected $_sequence = true;
    protected static $_instance = null;
    /**
     * 
     * @var Zend_Cache_Frontend_Class
     */
    protected $_cache = null;

    /**
     * Singleton instance
     *
     * @return Makers
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
    
    /**
     * получение всех элементов в виде массива для formSelect
     * @return array
     */
    public function getItemsAsOptions() {
    	$select = $this->select()
    		->order('priority DESC');
    	$items = $this->fetchAll($select);
    	$options = array(0=> 'Нет');
    	if ($items->count()){
    		foreach ($items as $item){
    			$options[$item->id] = $item->title;
    		}
    	}
        
    	return $options;
    }
   
    public function getTypesByDivId($id_division) {
        $types_ids = Catalog_Product::getInstance()->getTypesIdsByDivId($id_division);

        $ids=array();
        $items=array();

        $ids=$types_ids->toArray();
        if($types_ids->count()){
        $select = $this->select()
                ->order('priority DESC')
                ->where("`id` IN (?)", $ids);
        $items = $this->fetchAll($select);
        }
        $options = array(0=> 'Все');
    	if (count($items)){
    		foreach ($items as $item){
    			$options[$item->id] = $item->title;
    		}
    	}
        return $options;
    }
   
	public function getAllActive() {
        $select = $this->select();
        return $this->fetchAll($select);
    }

	public function getType($id) {
        $select = $this->select()->where('id = ?', $id);
        $row = $this->fetchRow($select);
        if ($row)
        	return $row->type;
        else
        	return 0;
    }
}