<?php


class Catalog_Product_Material extends Zend_Db_Table {

    protected $_name = 'site_catalog_product_material';
    protected $_primary = array('id');	
	protected $_sequence = true;
    protected static $_instance = null;
    /**
     * 
     * @var Zend_Cache_Frontend_Class
     */
    protected $_cache = null;

    /**
     * Singleton instance
     *
     * @return Catalog_Product_Material
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
    
    /**
     * получение всех элементов в виде массива для formSelect
     * @return array
     */
    public function getItemsAsOptions() {
    	$select = $this->select()
    		->order('priority DESC');
    	$items = $this->fetchAll($select);
    	$options = array(0=> 'Нет');
    	if ($items->count()){
    		foreach ($items as $item){
    			$options[$item->id] = $item->title;
    		}
    	}	
    	return $options;
    }
   
    public function getByType($id_type){
        $select = $this->select();
        $select->from(array('m'=>$this->_name),array('m.id'));
        $select->setIntegrityCheck(false);
        $select->distinct(true);
        $select->joinInner(
                array('p'=>'site_catalog_product'),
                "p.id_material=m.id AND p.active=1",
                array());
        $select->where("p.id_mebeltype=?",(int)$id_type);

		$result = $this->fetchAll($select);

         if (count($result)){
			$ids = array();
			foreach ($result as $id){
				$ids[] = $id['id'];
			}
			$ids = implode(',', $ids);
			$materials_rowset = $this->fetchAll("id IN($ids)");
			if ($materials_rowset->count()){
				return $materials_rowset;
			}
		}
		return '';
    }

    public function getByProduct($ids) {
        $select = $this->select()
            ->order('priority DESC');
        $items = $this->fetchAll($select);
        $all = explode(',', $ids);
        $options = array();
        if ($items->count()){
            foreach ($items as $item){
                if(in_array($item->id, $all)) {
                $options[] = array ($item->id, $item->title, $item->intro) ;
                }
            }
        }
        //Zend_Debug::dump($options);exit();
        return $options;
    }

    public function getByUnit($ids) {
        $select = $this->select()
            ->where('material_ids = ?', $ids);
        $item = $this->fetchAll($select);
        //Zend_Debug::dump($options);exit();
        return $item;
    }


    public function getAllActive() {
        $select = $this->select();
        return $this->fetchAll($select);
    }

    public function getDescr($id) {
        $select = $this->select()
            ->where('id = ?', $id);
        return $this->fetchRow($select);
    }

    public function getItemsAsOptionsForSearch($ids) {
        $select = $this->select();
        $select->setIntegrityCheck(false);
        $select->from(array('p' => $this->_name));
        $select->where("p.id IN ($ids)");
        $items = $this->fetchAll($select);
        $html = '<option value="0">Все материалы</option>';
        if ($items->count()){
            foreach ($items as $item){
                $html .='<option value="'.$item->id.'">'.$item->title.'</option>';
            }
            //$html .='</select></div>';
        }
        return $html;
    }
}
