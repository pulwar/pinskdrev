<?php

class Catalog_Product_Maketerm extends Zend_Db_Table {
	
	protected $_name = 'site_catalog_product_maketerm';
	protected $_primary = array('id');
	protected $_sequence = true;
	protected static $_instance = null;
	/**
	 * 
	 * @var Zend_Cache_Frontend_Class
	 */
	protected $_cache = null;
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Product_Maketerm
	 */
	public static function getInstance() {
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		
		return self::$_instance;
	}
	
	/**
	 * получение всех элементов в виде массива для formSelect
	 * @return array
	 */
	public function getItemsAsOptions() {
		$select = $this->select()->order('priority DESC');
		$items = $this->fetchAll($select);
		//$options = array(0 => 'Нет');
		if ($items->count()) {
			foreach ($items as $item) {
				$options[$item->id] = $item->title;
			}
		}
		return $options;
	}
}