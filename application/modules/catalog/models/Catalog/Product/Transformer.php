<?php

class Catalog_Product_Transformer extends Zend_Db_Table {
	
	protected $_name = 'site_catalog_product_transformer';
	protected $_primary = array('id');
	protected $_sequence = true;
	protected static $_instance = null;
	/**
	 * 
	 * @var Zend_Cache_Frontend_Class
	 */
	protected $_cache = null;
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Product_Transformer
	 */
	public static function getInstance() {
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	/**
	 * получение всех элементов в виде массива для formSelect
	 * @return array
	 */
	public function getItemsAsOptions() {
		$select = $this->select()->order('priority DESC');
		$items = $this->fetchAll($select);
		$options = array(0 => 'Нет');
		if ($items->count()) {
			foreach ($items as $item) {
				$options[$item->id] = $item->title;
			}
		}
		
		return $options;
	}
	
	public function getByType($id_type) {
		$select = $this->select();
		$select->from(array('t' => $this->_name), array('t.id'));
		$select->setIntegrityCheck(false);
		$select->distinct(true);
		$select->joinInner(array('p' => 'site_catalog_product'), "p.id_transformer=t.id AND p.active=1", array());
		$select->where("p.id_mebeltype=?", (int)$id_type);
		
		$result = $this->fetchAll($select);
		
		if (count($result)) {
			$ids = array();
			foreach ($result as $id) {
				$ids[] = $id['id'];
			}
			$ids = implode(',', $ids);
			$transformer_rowset = $this->fetchAll("id IN($ids)");
			if ($transformer_rowset->count()) {
				return $transformer_rowset;
			}
		}
		return '';
	}
	
	/**
	 * получение опубликованного элемента по url
	 * @param string $url
	 * @return Zend_Db_Table_Row
	 */
	public function getPubItem($url) {
		$select = $this->select()->where('active = ?', 1)->where('url = ?', $url);
		return $this->fetchRow($select);
	}


    public function getByProduct($ids) {
        $select = $this->select()
            ->order('priority DESC');
        $items = $this->fetchAll($select);
        $all = explode(',', $ids);
        $options = array();
        if ($items->count()){
            foreach ($items as $item){
                if(in_array($item->id, $all)) {
                    $options[] = array ($item->id, $item->title, $item->intro) ;                }
            }
        }
        return $options;
    }
}