<?php

class Catalog_Product_Collections extends Zend_Db_Table {
	
	protected $_name = 'site_catalog_product_collections';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом
	protected static $_instance = null;
	
	/**
	 * Class to use for row	
	 *
	 * @var string
	 */
	protected $_dependentTables = array(

	);
	protected $_referenceMap = array(
            'rel_products' => array(
            'columns'           => array('id_collection_product'),
            'refTableClass'     => 'Catalog_Product',
            'refColumns'        => array('id'),
            'onDelete'          => self::CASCADE
            
        ),
            'products' => array(
            'columns'           => array('id_product'),
            'refTableClass'     => 'Catalog_Product',
            'refColumns'        => array('id'),
            'onDelete'          => self::CASCADE
        )
	) ;


           /**
	 * Singleton instance
	 *
	 * @return Catalog_Tovar_Related
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public function deleteByProductId($id){
            $where = "`id_product` = '$id' OR `id_collection_product` = '$id'";
            $this->delete($where);
        }
}
