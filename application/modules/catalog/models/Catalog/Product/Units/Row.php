<?php
class Catalog_Product_Units_Row extends Zend_Db_Table_Row {
	
	public function toArray() {
		return parent::toArray();
	}
	
	/**
	 * delete
	 * @see application/library/Zend/Db/Table/Row/Zend_Db_Table_Row_Abstract#delete()
	 */
	public function delete() {
		@unlink(DIR_PUBLIC . "pics/catalog/product/img/" . $this->img);
		@unlink(DIR_PUBLIC . "pics/catalog/product/big/" . $this->img_big);
		return parent::delete();
	}
}