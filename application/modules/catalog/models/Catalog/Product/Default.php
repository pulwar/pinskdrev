<?php
/**
 * Catalog_Product_Default
 */
class Catalog_Product_Default extends Zend_Db_Table {
	/**
	 * The default table name.
	 *
	 * @var string
	 */
	protected $_name = 'site_catalog_product_default';
	
	/**
	 * The default primary key.
	 *
	 * @var array
	 */
	protected $_primary = array('id');
	
	/**
	 * Whether to use Autoincrement primary key.
	 *
	 * @var boolean
	 */
	protected $_sequence = true; // Ис;льзование таблицы с автоинкрементным ключом
	
	/**
	 * Singleton instance.
	 *
	 * @var Catalog_Product_Default
	 */
	protected static $_instance = null;
	
	private $_types = array(
		'input'		=> 'Текстовое поле',		
		'textarea' 	=> 'Многострочное текстовое поле',
		'checkbox' 	=> 'Флажок',
		'fck'		=> 'Html редактор',	
		'fck_small'	=> 'Html редактор мини'	
	);
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Product_Default
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	/**
	 * @return array 
	 */
	public function getTypes(){
		return $this->_types;
	}
	
	public function getDefaultToProduct($id_product=null){
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(
			array('default'=>$this->_name), 
			array(
				'default.*',
				// для новых товаров
				// если значения нет, подставляется дефолтное
				'IF(values.id_product>0, values.value, default.default_value) AS value'
			)
		);	
		$select->distinct(true);
		$select->joinLeft(
			array('values'=>'site_catalog_product_default_values'),
			"values.id_default=default.id AND values.id_product='".(int)$id_product."'", 
			array('values.id_product' )
		);
		$select->order("default.priority DESC");
		return $this->fetchAll($select);
	}

    public function getCheck() {
        $select = $this->select();
        $select->where('main_check = ?', 1);
	$select->where('active = ?', 1);
        return $this->fetchAll($select);
    }
	
	public function getDefaultIdBySystemName($sis_name){
		$row = $this->fetchRow("`system_name`='$sis_name'");
		if($row){
			return $row->id;
		}else {
			return null;
		}
	}
	
	public function getBySystemNames($names){
		if (!is_array($names))
			$names = array($names);
		$where = $this->getAdapter()->quoteInto('system_name IN (?)', $names);
		return $this->fetchAll($where);
	}
}
