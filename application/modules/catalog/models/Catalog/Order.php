<?php
/**
 * Catalog_Order
 *
 */
class Catalog_Order extends Zend_Db_Table {
	
	
	
	
	/**
	 * The default table name.
	 *
	 * @var string
	 */
	protected $_name = 'site_catalog_order';
	
	/**
	 * The default primary key.
	 *
	 * @var array
	 */
	protected $_primary = array('id');
	
	/**
	 * Whether to use Autoincrement primary key.
	 *
	 * @var boolean
	 */
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом
	
	/**
	 * Singleton instance.
	 *
	 * @var St_Model_Layout_Pages
	 */
	protected static $_instance = null;
	
	/**
	 * Dependent tables.
	 *
	 * @var array
	 */
	
	
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Order
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

        public function getConfigById($id){
            $select = $this->select();
            $select->where('id = ?', $id);
            $row = $this->fetchRow($select);
            if($row){
                return $row->config;
            }
        }




}