<?php

/**
 * Catalog_Division 
 */
class Catalog_Division extends Zend_Db_Table {
	
	/**
	 * The default table name.
	 *
	 * @var string
	 */
	protected $_name = 'site_catalog_division';
	
	/**
	 * The default primary key.
	 *
	 * @var array
	 */
	protected $_primary = array(
			'id' 
	);
	
	/**
	 * Whether to use Autoincrement primary key.
	 *
	 * @var boolean
	 */
	protected $_sequence = true; // Ис;льзование таблицы с автоинкрементным ключом
	

	/**
	 * Singleton instance.
	 *
	 * @var Catalog_Division
	 */
	protected static $_instance = null;
	
	/**
	 * Dependent tables.
	 *
	 * @var array
	 */
	protected $_dependentTables = array(
			'Catalog_Product2Divisions' 
	);
	
	/**
	 * Reference map.
	 *
	 * @var array
	 */
	protected $_referenceMap = array();
	
	/**
	 * Class to use for rows.
	 *
	 * @var string
	 */
	protected $_rowClass = "Catalog_Division_Row";
	
	/**
	 * Class to use for row sets.
	 *
	 * @var string
	 */
	protected $_rowsetClass = "Catalog_Division_Rowset";
	
	/**
	 * Необходима для рекурсивного сохранения
	 * дерева
	 *
	 * @var array
	 */
	private $_tree = null;
	/**
	 * необходима для построения меню каталога
	 *
	 * @var array 
	 */
	private $_div = null;
	private $_page_path = null;
	private $_divisions_array = array(
			0 => 'Нет' 
	);
	protected $_cache = null;

	/**
	 * Singleton instance
	 *
	 * @return catalog_division
	 */
	public static function getInstance() {
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * 
	 * @param $id
	 * @return Catalog_Division_Row
	 */
	public function getItem($id) {
		return Catalog_Division_Cache::getInstance()->find($id)->current();
	}

	/**
	 * Построение дерева для Меню каталога
	 *
	 * 
	 * @param int $parentId
	 * @param int $level
	 * @param int $id_page
	 * @return html
	 */
	public function getCatalogMenu($parentId = 0, $level = 0, $id_current = null, $show_ids) {
		/*if (! $this->_page_path) {
			$this->_page_path = SiteDivisionsType::getInstance ()->getPagePathBySystemName ( 'division' );
		}*/
		$select = $this->select();
		$select->where("level = ?", (int) $level);
		$select->where("parent_id = ?", (int) $parentId);
		$select->where("active = ?", 1);
		$select->where("leftmenu = ?", 1);
		$select->order('sortId');
		$nodes = $this->fetchAll($select);
		$html = '';
		if ($nodes->count()) {
			//$show = in_array($parentId, $show_ids)?  "class='show'" : '';
			if ($level > 1)
				$displ = 'style="display:none"';
			else
				$displ = '';
			$html = "<ul class=\"dropdown\" $displ>";
			foreach ($nodes as $data) {
				//$current = $id_current==$data->id ? "class='active'" : '';
				if ($this->getCountOfChildren($data->id) > 0) {
					
					$html .= "
						<li>
							<a href=\"/cat/$data->url\"><span>$data->name</span></a>";
					$html .= $this->getCatalogMenu($data->id, $data->level + 1, $id_current, $show_ids);
					$html .= "</li>";
				} else {
					$html .= "
						<li>
							<a href=\"/cat/$data->url\"><span>$data->name</span></a>";
					$html .= "</li>";
				}
			}
			$html .= '</ul>';
			return $html;
		}
	}

	public function getWebMenu($parentId = 0, $level = 0, $id_current = null, $show_ids) {
		/*if (! $this->_page_path) {
			$this->_page_path = SiteDivisionsType::getInstance ()->getPagePathBySystemName ( 'division' );
		}*/
		$select = $this->select();
		$select->where("level = ?", (int) $level);
		$select->where("parent_id = ?", (int) $parentId);
		$select->where("active = ?", 1);
		$select->where("leftmenu = ?", 1);
		$select->order('sortId');
		$nodes = $this->fetchAll($select);
		$html = '';
		if ($nodes->count()) {
			//$show = in_array($parentId, $show_ids)?  "class='show'" : '';
			if ($level > 1)
				$displ = 'style="display:none"';
			else
				$displ = '';
			$html = "<ul class=\"dropdown\" $displ>";
			foreach ($nodes as $data) {
				//$current = $id_current==$data->id ? "class='active'" : '';
				if ($this->getCountOfChildren($data->id) > 0) {
					
					$html .= "
						<li>
							<a href=\"/cat/$data->url\"><span>$data->name</span></a>";
					$html .= $this->getCatalogMenu($data->id, $data->level + 1, $id_current, $show_ids);
					$html .= "</li>";
				} else {
					$html .= "
						<li>
							<a href=\"/cat/$data->url\"><span>$data->name</span></a>";
					$html .= "</li>";
				}
			}
			$html .= '</ul>';
			return $html;
		}
	}

	public function getCatalogMenuMap($parentId = 0, $level = 0, $id_current = null, $show_ids) {
		/*if (! $this->_page_path) {
			$this->_page_path = SiteDivisionsType::getInstance ()->getPagePathBySystemName ( 'division' );
		}*/
		$select = $this->select();
		$select->where("level = ?", (int) $level);
		$select->where("parent_id = ?", (int) $parentId);
		$select->where("active = ?", 1);
		$select->order('sortId');
		$nodes = $this->fetchAll($select);
		$html = '';
		if ($nodes->count()) {
			//$show = in_array($parentId, $show_ids)?  "class='show'" : '';
			$html = "<ul>";
			foreach ($nodes as $data) {
				//$current = $id_current==$data->id ? "class='active'" : '';
				if ($this->getCountOfChildren($data->id) > 0) {
					
					$html .= "
						<li>
							<a href=\"/cat/$data->url\">$data->name</a>";
					$html .= $this->getCatalogMenuMap($data->id, $data->level + 1, $id_current, $show_ids);
					$html .= "</li>";
				} else {
					$html .= "
						<li>
							<a href=\"/cat/$data->url\">$data->name</a>";
					$html .= "</li>";
				}
			}
			$html .= '</ul>';
			return $html;
		}
	}

	/**
	 * Построение дерева для jqueryTree
	 *
	 * @param string $version
	 * @param int $parentId
	 * @param int $level
	 * @return html
	 */
	public function getTree($parentId = 0, $level = 0, $version = null) {
		$return = array();
		$nodes = array();
		$where = array(
				$this->getAdapter()->quoteInto('level = ?', $level),
				$this->getAdapter()->quoteInto('parent_id = ?', $parentId) 
		);
		$nodes = $this->fetchAll($where, 'sortId');
		$html = '';
		if ($nodes->count()) {
			$html = '<ul>';
			foreach ($nodes as $data) {
				$isset_products = (int) $data->issetProductInDiv();
				$isset_childs = (int) $data->issetChilds();
				if ($this->getCountOfChildren($data->id) > 0) {
					$html .= "
						<li id=\"$data->id\" products=\"$isset_products\" childs=\"$isset_childs\">
							
							<a href=\"#\" active=\"$data->active\" ><ins>&nbsp;</ins>$data->name</a>";
					//							if ($isset_products){
					//								$html.= " <small class='products_count'>товары</small>";
					//							}
					$html .= $this->getTree($data->id, $data->level + 1, $version);
					$html .= "</li>";
				} else {
					$html .= "
						<li id=\"$data->id\" products=\"$isset_products\" childs=\"$isset_childs\" >
							
							<a href=\"#\" active=\"$data->active\"  ><ins>&nbsp;</ins>$data->name</a>";
					//					if ($isset_products){
					//						$html.= " <small class='products_count'>товары</small>";
					//					}
					$html .= "</li>";
				}
			}
			$html .= '</ul>';
			return $html;
		}
	}

	/**
	 * Сохранение результата dnd 
	 * в зависимости от типа возвращаемого ExtJS
	 * результата
	 *
	 * @param int $id
	 * @param int $parentId
	 * @param string $point
	 */
	public function replace($id, $parentId, $point) {
		/*if ($point == 'above')
			$this->replaceAbove ( $id, $parentId, $point ); elseif ($point == 'below')
			$this->replaceBelow ( $id, $parentId, $point ); elseif ($point == 'append')
			$this->replaceAppend ( $id, $parentId, $point );*/
		if ($point == 'before')
			$this->replaceAbove($id, $parentId, $point);
		elseif ($point == 'after')
			$this->replaceBelow($id, $parentId, $point);
		elseif ($point == 'inside')
			$this->replaceAppend($id, $parentId, $point);
		
		$this->addLevel($id);
	}

	/**
	 * рекурсивное обновление уровня вложенности при переносе
	 *
	 * @param unknown_type $id
	 */
	private function addLevel($id) {
		$db = $this->getAdapter();
		
		$all = $this->fetchAll("parent_id = $id");
		$parentRow = $this->fetchRow("id = $id");
		
		$this->update(array(
				'level' => $parentRow->level + 1 
		), "parent_id = $id");
		
		foreach ($all as $value) {
			$this->addLevel($value->id);
		}
	}

	/**
	 * Проверка действительности события dnd
	 *
	 * @param int $id
	 * @param int $parentId
	 * @return boolean
	 */
	public function isReplace($id, $parentId) {
		$node = $this->getItem($id);
		return $node->parentId == $parentId ? false : true;
	}

	/**
	 * Получение раздела по ID
	 *
	 * @param int $id
	 * 
	 */
	public function getItemById($id) {
		return $this->getItem($id);
	}

	/**
	 * Получение родительских элементов каталога (parent_id = 0)
	 *
	 * @param int $parent_id
	 * 
	 */
	public function getMainDivisions($parent_id = 0) {
		$where = $this->getAdapter()->quoteInto('parent_id = ?', $parent_id);
		return $this->fetchRow($where);
	}

	public function getParentDivision($parent_id = 0) {
		$where = $this->getAdapter()->quoteInto('parent_id = ?', $parent_id);
		return $this->fetchRow($where);
	}

	/**
	 * Перемещение страницы через dnd,
	 * при котором страница выше находится 
	 * на том же уровне
	 * 
	 * @param int $id
	 * @param int $parentId
	 * @param string $point
	 */
	private function replaceAbove($id, $parentId, $point) {
		//$page = $this->getPage ( $id );
		$parent = $this->find($parentId)->current();
		$this->getAdapter()->query("UPDATE $this->_name 
				SET parent_id = :parent_id, 
					level = :level, 
					sortid = sortid + 1 
				WHERE parent_id = :id AND 
					sortid >= :sort", array(
				'parent_id' => $parent->parent_id,
				'level' => $parent->level,
				'id' => $parent->parent_id,
				'sort' => $parent->sortid 
		));
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->update(array(
				'parent_id' => $parent->parent_id,
				'level' => $parent->level,
				'sortid' => $parent->sortid 
		), 		//, 
		//'path' => $this->generateStringPath ( $parent->parent_id, '/' ) . $page->path 
		$where);
	}

	/**
	 * Перемещение страницы через dnd,
	 * при котором страница выше является родительской
	 *
	 * @param int $id
	 * @param int $parent_id
	 * @param string $point
	 */
	private function replaceBelow($id, $parent_id, $point) {
		//$page = $this->getPage ( $id );
		$parent = $this->find($parent_id)->current();
		
		$this->getAdapter()->query("UPDATE $this->_name 
			SET parent_id = :parent_id, 
				level = :level, 
				sortid = sortid + 1 
			WHERE parent_id = :id AND 
				sortid > :sort", array(
				'parent_id' => $parent->parent_id,
				'level' => $parent->level,
				'id' => $parent->parent_id,
				'sort' => $parent->sortid 
		));
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->update(array(
				'parent_id' => $parent->parent_id,
				'level' => $parent->level,
				'sortid' => $parent->sortid + 1 
		), 		//, 
		//'path' => $this->generateStringPath ( $parent->parent_id, '/' ) . $page->path
		$where);
	}

	/**
	 * Перемещение страницы через dnd, при котором
	 * она была брошена на страницу, тем самым была добавлена
	 * в конец списка вложенных
	 *
	 * @param int $id
	 * @param int $parent_id
	 * @param string $point
	 */
	private function replaceAppend($id, $parent_id, $point) {
		$item = $this->find($id)->current();
		$parent = $this->find($parent_id)->current();
		$max_sort = $this->getMaxSort($parent_id);
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->update(array(
				'parent_id' => $parent_id,
				'level' => $parent->level + 1, //на 1 больше родительского уровня
				'sortid' => $max_sort + 1 
		), 		//на 1 больше максимального- добавляется в конец
		/*'path' => $this->generateStringPath ( $parent->id, '/' ) . $page->path*/
		$where);
	}

	/**
	 * Получение максимального sortid 
	 * родительской страницы
	 *
	 * @param int $id
	 * @param int $id_page
	 * @return int
	 * 
	 */
	public function getMaxSort($id) {
		return $this->getSort($id, 'DESC');
	}

	public function getMainDiv($id) {
		$data = $this->getItemById($id);
		while ($data->parent_id != 0) {
			$data = $this->getItemById($data->parent_id);
		}
		
		return $data->id;
	}

	/**
	 * обновление счетчика товаров для раздела
	 * @param int $id_division
	 */
	public function updateCountProducts($id_division) {
		$count = Catalog_Product::getInstance()->getCountByDivId($id_division);
		$data = array(
				'products_count' => (int) $count 
		);
		$where = $this->getAdapter()->quoteInto("id = ?", $id_division);
		$this->update($data, $where);
	}

	/**
	 * Получение sortid 
	 * родительской страницы,
	 * определенным образом сортируя(asc, desc)
	 *
	 * @param int $id
	 * @return int
	 */
	private function getSort($id, $type) {
		$select = $this->select();
		$select->order("sortid $type");
		$select->where('parent_id = ?', $id);
		$item = $this->fetchRow($select);
		
		return $item ? $item->sortid : 0;
	}

	/**
	 * Создание копии раздела вместе с поддеревом
	 *
	 * @param int $id
	 */
	public function copyDivisionWithChildren($id, $parentId = 0, $deep = 0) {
		$division = $this->getDivision($id);
		$parentId = $this->addDivision($division, $parentId, $deep);
		
		$meta = $this->getMetaDivision($id);
		$meta = $meta->toArray();
		unset($meta['id']);
		$meta['item_id'] = $parentId;
		$this->editMetaDivision($meta, $parentId);
		
		$deep++;
		
		foreach ($this->getChildren($division->id) as $key => $data) {
			$this->copyDivisionWithChildren($data, $parentId, $deep);
		}
	}

	/**
	 * Получение количествва дочерних элементов
	 *
	 * @param int $id
	 * @return int
	 */
	public function getCountOfChildren($id) {
		$select = $this->select();
		$select->from($this->_name, 'COUNT(*) as count');
		$select->where('parent_id = ?', $id);
		return $this->getAdapter()->fetchOne($select);
	}

	public function getCountOfPublicChildren($id) {
		$select = $this->select();
		$select->from($this->_name, 'COUNT(*) as count');
		$select->where('parent_id = ?', $id);
		$select->where('active = ?', 1);
		return $this->getAdapter()->fetchOne($select);
	}

	/**
	 * Получение дочерних элементов
	 *
	 * @param int $id
	 * @return array
	 */
	public function getChildren($id) {
		$where = $this->getAdapter()->quoteInto('parent_id = ?', $id);
		$nodes = Catalog_Division_Cache::getInstance()->fetchAll($where);
		$array = array();
		foreach ($nodes as $key => $data) {
			if ($data->active == 1) {
				$array[$data->name] = $data->id;
			}
		}
		return $array;
	}

	/**
	 * получение корневых разделов каталога
	 * @param int $count
	 * @param int $offset
	 * @return Catalog_Division_Rowset
	 */
	public function getRootItems($count = null, $offset = null) {
		$select = $this->select();
		$select->where("parent_id = ?", 0);
		$select->where("level = ?", 0);
		$select->where("active = ?", 1);
		$select->order("sortid");
		$select->limit($count, $offset);
		return $this->fetchAll($select);
	}

	/**
	 * Получение максимального ID
	 *
	 * @return unknown
	 */
	private function getMaxId() {
		$max = $this->fetchRow(null, 'id DESC');
		return $max->id;
	}

	/**
	 * Получение количества копий раздела
	 * для определения префикса новой копии
	 *
	 * @param string $name
	 * @return int
	 */
	private function getCountOfCopies($name) {
		$db = $this->getAdapter();
		$sql = $db->quoteInto("SELECT count(*) as c FROM $this->_name WHERE name regexp ?", $name . '_[[:digit:]]');
		$result = $db->query($sql);
		$count = $result->fetchAll();
		return $count[0]['c'];
	}

	/**
	 * переиндексация элементов для поиска
	 */
	public function reindex() {
		$index = new Ext_Search_Lucene(Ext_Search_Lucene::CATALOG_DIVISIONS, true);
		$count = 10;
		$offset = 0;
		//$path = SiteDivisionsType::getInstance ()->getPagePathBySystemName ( 'division' );
		while (($rowset = $this->fetchAll(null, null, $count, $offset)) && (0 < $rowset->count())) {
			while ($rowset->valid()) {
				$row = $rowset->current();
				$doc = new Ext_Search_Lucene_Document();
				$doc->setUrl('cat/' . $row->url);
				$doc->setTitle($row->name);
				$doc->setId($row->id);
				$doc->setContent(strip_tags($row->description));
				
				$index->addDocument($doc);
				
				$rowset->next();
			}
			$offset += $count;
		}
		
		$index->commit();
		return $index->numDocs();
	}

	public function processItems($ids, $type) {
		if ($ids != '') {
			$last = substr($ids, -1);
			if ($last == ',') {
				$ids = substr($ids, 0, -1);
			}
			$select = $this->select()->where('id IN (' . $ids . ')');
			
			$items = $this->fetchAll($select);
			if ($items->count()) {
				switch ($type) {
					case 'active' :
						foreach ($items as $item) {
							$item->active = abs($item->active - 1);
							$item->save();
						}
						return true;
						;
						break;
					case 'delete' :
						foreach ($items as $item) {
							$item->delete();
						}
						return true;
						;
						break;
					
					default :
						;
						break;
				}
			}
		}
		return false;
	}

	/**
	 * проставляет в раздел id родительских разделов.
	 */
	public function setParentIds() {
		$rowset = $this->fetchAll();
		foreach ($rowset as $row) {
			$ids = array();
			$current = $row;
			while ($parent = $current->getParent()) {
				$ids[] = $parent->id;
				$current = $parent;
			}
			if (count($ids)) {
				$row->parent_ids = implode(',', $ids);
				$row->save();
			}
		}
	}

	public function getDivisionsWithParents($parent_id = 0, $level = 0) {
		$select = $this->select();
		$select->from($this->_name, array(
				'id',
				'name',
				'parent_ids',
				'level' 
		));
		$select->where('parent_id = ?', $parent_id);
		$select->where('level = ?', $level);
		$select->order('sortid');
		//echo $select->__toString();
		$divisions = $this->fetchAll($select);
		
		if ($divisions->count()) {
			foreach ($divisions as $div) {
				$parents = $div->getParents();
				$title = '';
				if ($parents != '') {
					foreach ($parents as $parent) {
						$title .= $parent->name . '->';
					}
				}
				$title .= $div->name;
				$this->_divisions_array[$div->id] = $title;
				if ($div->issetChilds()) {
					$this->getDivisionsWithParents($div->id, $div->level + 1);
				}
			}
		}
		return $this->_divisions_array;
	}

	public function getAllChildrenIdArray($parentId = 0, $level = 0) {
		$res = Catalog_Division_Cache::getInstance()->fetchAll();
		foreach ($res as $item) {
			$result[] = $item->id;
		}
		
		return $result;
	}

	public function checkUrl($url, $id = null) {
		$where = "url='$url'";
		if ($id != null) {
			$where .= " AND id!=" . (int) $id;
		}
		$row = $this->fetchRow($where);
		if (!is_null($row)) {
			return false;
		} else {
			return true;
		}
	}

	public function getIdByUrl($url) {
		if (!$url)
			return null;
		$where = "url = '$url'";
		$item = $this->fetchRow($where);
		if ($item) {
			return $item->id;
		}
		return null;
	}

	public function getUrlById($id) {
		$item = $this->find($id)->current();
		if (count($item)) {
			return $item->url;
		}
		;
		return null;
	}

	public function getDivisionUrlByProductId($id) {
		$select = $this->select();
		$select->from(array(
				'd' => $this->_name 
		));
		$select->setIntegrityCheck(false);
		$select->joinInner(array(
				'p' => 'site_catalog_product2divisions' 
		), 'd.id=p.id_division AND p.id_product=' . $id, array());
		$select->where('d.active = ?', 1);
		$select->order('is_primary DESC');
		$row = $this->fetchRow($select);
		if ($row)
			return $row->url;
		else
			return false;
	}

	public function getChildsIds($parentId = 0, $level = 0, $ids = array()) {
		$where = array(
				$this->getAdapter()->quoteInto('level = ?', $level),
				$this->getAdapter()->quoteInto('parent_id = ?', $parentId),
				$this->getAdapter()->quoteInto('active = ?', 1) 
		);
		$nodes = $this->fetchAll($where);
		if ($nodes->count()) {
			foreach ($nodes as $data) {
				$ids[] = $data->id;
				if ($this->getCountOfChildren($data->id) > 0) {
					$ids = $ids + $this->getChildsIds($data->id, $data->level + 1, $ids);
				}
			}
		}
		return $ids;
	}

	public function getAllMainDivisions() {
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->where('level=0');
		$select->where('active=1');
		//Zend_Debug::dump($this->fetchAll($select));exit();
		return $this->fetchAll($select);
	}

	public function getSubDivisions($id) {
		$select = $this->select();
		$select->where('active=1');
		$select->where('parent_id=?', $id);
		return $this->fetchAll($select);
	}

	public function getParents($parent_id, $id) {
		$select = $this->select();
		$select->where("parent_id = ?", $parent_id);
		$select->where("active = ?", 1);
		$select->order('sortid');
		return $this->fetchAll($select);
	}

	public function getMenu($type) {
		$select = $this->select();
		$select->where('active=1');
		if ($type == 'left') {
			$menu = 'leftmenu = ?';
		}
		if ($type == 'center') {
			$menu = 'centermenu = ?';
		}
		if ($type == 'right') {
			$menu = 'rightmenu = ?';
		}
		if ($type == 'web_menu') {
			$menu = 'web_menu = ?';
		}
		$select->where($menu, 1);
		$select->order('menu_sort DESC');
		return $this->fetchAll($select);
	}

	public function getChecking($id) {
		$select = $this->select();
		$select->where('parent_id=?', $id);
		$select->where('active=1');
		$sql = $this->fetchAll($select);
		if (!$sql) {
			return 0;
		}
		return 1;
	}

	public function getDivisionValues($id) {
		$select = $this->select();
		$select->where('id=?', $id);
		return $this->fetchRow($select);
	}

	public function getDivisionIdByProductId($id) {
		$select = $this->select();
		$select->from(array(
				'd' => $this->_name 
		));
		$select->setIntegrityCheck(false);
		$select->joinInner(array(
				'p' => 'site_catalog_product2divisions' 
		), 'd.id=p.id_division AND p.id_product=' . $id, array());
		$select->where('d.active = ?', 1);
		$select->order('is_primary DESC');
		$row = $this->fetchRow($select);
		if ($row)
			return $row->id;
		else
			return false;
	}

	function getCategories($ids, $categories = null, $products_in_div = null) {
		$db = $this->getAdapter();
		if (count($ids) < 2) {
			$sql = "SELECT id FROM site_catalog_division WHERE parent_id = $ids AND active = 1";
		} else {
			$sql = "SELECT id FROM site_catalog_division WHERE parent_id IN (" . implode(',', $ids) . ") AND active = 1";
		}
		$result = $db->fetchAll($sql);
		if (count($result) > 0) {
			$categories = $result;
		}
		if (isset($products_in_div)) {
			foreach ($products_in_div as $div) {
				$products[] = $div;
			}
		}
		foreach ($result as $item) {
			$products[] = $item['id'];
		}
		if (isset($products)) {
		} else {
			$products[] = $ids;
		}
		if (count($result) > 0) {
			if (count($result) == 1) {
				foreach ($result as $item) {
					$cats = $item['id'];
				}
			} else {
				$cats = array();
				foreach ($result as $item) {
					$cats[] = $item['id'];
				}
			}
			return Catalog_Division::getInstance()->getCategories($cats, $categories, $products);
		} else {
			return Catalog_Product::getInstance()->getPublicProductsByDivisionsIds($products);
		}
	}

	public function getCategoriesCount($ids, $categories = null, $products_in_div = null) {
		$db = $this->getAdapter();
		if (count($ids) < 2) {
			$sql = "SELECT id FROM site_catalog_division WHERE parent_id = $ids AND active = 1";
		} else {
			$sql = "SELECT id FROM site_catalog_division WHERE parent_id IN (" . implode(',', $ids) . ") AND active = 1";
		}
		$result = $db->fetchAll($sql);
		if (count($result) > 0) {
			$categories = $result;
		}
		if (isset($products_in_div)) {
			foreach ($products_in_div as $div) {
				$products[] = $div;
			}
		}
		foreach ($result as $item) {
			$products[] = $item['id'];
		}
		if (isset($products)) {
		} else {
			$products[] = $ids;
		}
		if (count($result) > 0) {
			if (count($result) == 1) {
				foreach ($result as $item) {
					$cats = $item['id'];
				}
			} else {
				$cats = array();
				foreach ($result as $item) {
					$cats[] = $item['id'];
				}
			}
			return Catalog_Division::getInstance()->getCategoriesCount($cats, $categories, $products);
		} else {
			return Catalog_Product::getInstance()->getPublicProductsByDivisionsIdsCount($products);
		}
	}
}
