<?php
/**
 * Catalog_Tovar
 *
 */
class Catalog_Product2Divisions extends Zend_Db_Table {
	/**
	 * The default table name.
	 *
	 * @var string
	 */
	protected $_name = 'site_catalog_product2divisions';
	
	/**
	 * The default primary key.
	 *
	 * @var array
	 */
	protected $_primary = array('id');
	
	/**
	 * Whether to use Autoincrement primary key.
	 *
	 * @var boolean
	 */
	protected $_sequence = true; // Ис;льзование таблицы с автоинкрементным ключом
	
	/**
	 * Singleton instance.
	 *
	 * @var St_Model_Layout_Pages
	 */
	protected static $_instance = null;
	
	/**
	 * Reference map.
	 *
	 * @var array
	 */
	protected $_referenceMap = array(
		'Catalog_Division' => array(
            'columns'           => array('id_division'),
            'refTableClass'     => 'Catalog_Division',
            'refColumns'        => array('id'),
            'onDelete'          => self::CASCADE,
            'onUpdate'          => self::RESTRICT
        )
	);
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Product2Divisions
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	/**
	 * создать запись о наличии категории
	 * @param int $id_product
	 * @param int $id_division
	 * @param int $is_primary
	 */
	public function addDivision($id_product, $id_division, $is_primary = null){
		$select = $this->select()
			->where('id_product = ?', $id_product)
			->where('id_division = ?', $id_division);
		$row = $this->fetchRow($select);
		$element_id = Catalog_Product_Units::getInstance()->getMainByProduct($id_product);
		if (isset($element_id)) {
        	$element_id = $element_id->id;
		}
		else {
		$element_id = '';
		}
			$data['id_product'] = $id_product;
			$data['id_division'] = $id_division;
			$data['is_primary'] = (int)$is_primary;
            		$data['main_element'] = $element_id;
			if ($is_primary!=0){			
				$this->delete("id_product ='{$id_product}' AND is_primary='1'");
			}
			return $this->createRow($data)->save();
	}
	
	/**
	 * получение главной категории
	 * @param int $id_product
	 * @return Zend_Db_Table_Row
	 */
	public function getPrimaryDiv($id_product){		
		$select = $this->select()
			->where('id_product = ?', $id_product)
			->where('is_primary = ?', 1);
		return $this->fetchRow($select);
		
	}
	
	/**
	 * получение второстепенных категорий
	 * @param int $id_product
	 * @return Zend_Db_Table_Rowset
	 */
	public function getOtherDivs($id_product){
		$select = $this->select()
			->where('id_product = ?', (int)$id_product)
			->where('is_primary != ?', 1);
		return $this->fetchAll($select);
	}
	
	public function deleteLink($id){
		$this->delete("id=".(int)$id);
	}
	
	public function deleteDivision($id){
		$this->delete("id_division=".(int)$id);
	}
	
	/**
	 * удаление раздела
	 * @param int $id_product
	 */
	public function deleteMain($id_product){
		$this->delete("id_product ='{$id_product}' AND is_primary='1'");
	}
	
	public function issetProductInDiv($id_division){
		return $this->getCountByDivId($id_division) > 0;
	}
	
	/**
	 * количество товаров в разделе
	 * @param int $id_division
	 * @return int
	 */
	public function getCountByDivId($id_division){
		$select = $this->select();
		$select->from($this->_name, 'COUNT(*) as count');
		$select->where("id_division = ?", (int)$id_division);
		return (int) $this->getAdapter()->fetchOne($select);
	}
	
	/**
	 * изменение приоритета для нескольких товаров
	 * @param unknown_type $params
	 */
	public function setPriority($params){
		$ids = array_keys($params);
		$rowset = $this->find($ids);
		foreach ($rowset as $row){
			$row->priority = $params[$row->id];
			$row->save();
		}
	}

public function saveElements($id_product, $id_division, $id_element){
        $select = $this->select()
            ->where('id = ?', $id_division);
        $row = $this->fetchRow($select);
        $data['id'] = $row['id'];
        $data['id_product'] = $id_product;
        $data['id_division'] = $row['id_division'];
        $data['is_primary'] = $row['is_primary'];
        $data['main_element'] = $id_element;
        $data['priority'] = $row['priority'];
        $where = $this->getAdapter()->quoteInto('id = ?',$row['id']);
        return $this->update($data, $where);
    }
	
	public function updatePriority($id_product, $id_division, $priority){
		
		if ($id_division && $id_product){
			$select = $this->select()
				->where('id_product = ?', $id_product)
				->where('id_division = ?', $id_division);
			$row = $this->fetchRow($select);
			if ($row!=null){	
				$where[] = $this->getAdapter()->quoteInto('id_product = ?', $id_product);
				$where[] = $this->getAdapter()->quoteInto('id_division = ?', $id_division);
				$data['priority'] = (int)$priority;
				return $this->update($data, $where);
			}
		}
	}

	public function getElement($id_div, $id_product) {
        $select = $this->select();
        $select->where('id_product=?', $id_product);
        $select->where('id_division=?', $id_div);
        return $this->fetchRow($select);
    }

public function getMain($id_tovar, $id_division) {
        $select = $this->select();
        $select->where('id_product=?', $id_tovar);
        if ($id_division != '') {
        $select->where('id_division=?', $id_division);
        }
        $select->where('is_primary=?', 1);
        $row = $this->fetchRow($select);
        if ($row) {
            return 1;
        }
        else {
            return 0;
        }
    }
}
