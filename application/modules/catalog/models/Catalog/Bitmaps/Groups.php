<?php

class Catalog_Bitmaps_Groups extends Zend_Db_Table {
	
	protected $_name = 'site_catalog_bitmaps_groups';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом
	protected static $_instance = null;
	
	/**
	 * Class to use for row	
	 *
	 * @var string
	 */
	protected $_rowClass = "Catalog_Bitmaps_Groups_Row" ;
	
	
	protected $_dependentTables = array(
		'Catalog_Bitmaps_Groups2Product',
		'Catalog_Bitmaps_Items2Product',
		'Catalog_Bitmaps_Items'
	);
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Bitmaps_Groups
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	/**
	 * Получаем все активные группы назначенные для товара
	 *
	 * @param int $id_product
	 * @param int $id_group
	 * @return array
	 */
	public function getGroupsByTovar($id_product){
		$db = $this->getAdapter();
		$sql = "
			SELECT 
				site_catalog_bitmaps_groups.*,
				site_catalog_bitmaps_groups2product.price,
                                site_catalog_bitmaps_groups2product.price_mehanizm
			FROM site_catalog_bitmaps_groups
			INNER JOIN site_catalog_bitmaps_groups2product ON (site_catalog_bitmaps_groups2product.id_group=site_catalog_bitmaps_groups.id)
			WHERE 				
				site_catalog_bitmaps_groups.active=1 AND
				site_catalog_bitmaps_groups2product.price>0 AND
				site_catalog_bitmaps_groups2product.id_product=".(int)$id_product."
			ORDER BY site_catalog_bitmaps_groups.is_decor ASC, site_catalog_bitmaps_groups.priority DESC, site_catalog_bitmaps_groups.name ASC
				";
                //echo $sql;exit;
		$result = $db->fetchAll($sql);
		if (count($result)){
			return  $result;
		}
		return '';
	}
	
	 public function getGroupsForTovar($id){
            $select = $this->select();
            $select->from(array('g'=>$this->_name));
            $select->setIntegrityCheck(false);
            $select->joinInner(
			array('t'=>'site_catalog_bitmaps_groups2product'),
			"t.id_product = $id AND t.id_group = g.id",
			array()
            );
            $select->where('is_decor = ?', 0);
            $select->order('g.priority DESC');
            return $this->fetchAll($select);
        }

    public function getItemsAsOptions() {
        $select = $this->select()->order('priority DESC');
        $items = $this->fetchAll($select);
        $options[0] = array('0', 'Нет');
        if ($items->count()) {
            foreach ($items as $item) {
                $options[] = array ($item->id, $item->name);
            }
        }
        return $options;
    }

    public function getByProduct($ids) {
        $select = $this->select()
            ->order('priority DESC');
        $items = $this->fetchAll($select);
        $all = explode(',', $ids);
        $options = array();
        if ($items->count()){
            foreach ($items as $item){
                if(in_array($item->id, $all)) {
                    $options[] = array ($item->id, $item->name) ;
                }
            }
        }
        //Zend_Debug::dump($options);exit();
        return $options;
    }
}