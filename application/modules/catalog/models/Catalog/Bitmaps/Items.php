<?php

class Catalog_Bitmaps_Items extends Zend_Db_Table {
	
	protected $_name = 'site_catalog_bitmaps_items';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом
	protected static $_instance = null;
	
	protected $_dependentTables = array(
		'Catalog_Bitmaps_Items2Product'
		
	);
	
	protected $_referenceMap    = array(
        'groups' => array(
            'columns'           => 'id_group',
            'refTableClass'     => 'Catalog_Bitmaps_Groups',
            'refColumns'        => 'id',
	 		'onDelete'          => self::CASCADE,
        )
        
    );
	
	/**
	 * Class to use for row	
	 *
	 * @var string
	 */
	protected $_rowClass = "Catalog_Bitmaps_Items_Row" ;
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Bitmaps_Items
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public function getSelectedItemsByGroup($id_product, $id_group, $price, $price_mehanizm){
           
		$db = $this->getAdapter();
		$sql = "
			SELECT $this->_name.*, site_catalog_bitmaps_items2product.related, site_catalog_bitmaps_items2product.main,
			$price AS price, $price_mehanizm AS price_mehanizm
			FROM $this->_name
			INNER JOIN site_catalog_bitmaps_items2product ON (site_catalog_bitmaps_items2product.id_item=$this->_name.id)
			WHERE 
				$this->_name.id_group=".(int)$id_group." AND 
				site_catalog_bitmaps_items2product.id_product=".(int)$id_product." AND
				$this->_name.active=1
			ORDER BY priority DESC, name ASC	
				";
				
		$result = $db->fetchAll($sql);
		return $result;
				
	}

    public function getItemsAsOptions($id) {
        $select = $this->select()
            ->where('id_group = ?', $id)
            ->order('priority DESC');
        $items = $this->fetchAll($select);
        $options = array();
        if ($items->count()){
            foreach ($items as $item){
                    $options[] = array ($item->id, $item->name, $item->img_big) ;
                }
            }
        //Zend_Debug::dump($options);exit();
        return $options;
    }

    public function getItemsAsOptions2($id) {
        $select = $this->select()->where('id_group = ?', $id)->order('priority DESC');
        $items = $this->fetchAll($select);
        $options = array();
        if ($items->count()) {
            foreach ($items as $item) {
                $options[$item->id] = $item->name;
            }
        }
        return $options;
    }

    public function getItems($id) {
        $select = $this->select()->where('id = ?', $id);
        $result = $this->fetchRow($select);
        //Zend_Debug::dump($result);exit();
        return $result;
    }

    public function getItemsAsOptions3($id) {
        $db = $this->getAdapter();
        $sql = "
			SELECT bitmaps_ids
			FROM site_catalog_product_units
			WHERE
				id_product=".(int)$id." AND
				active=1 AND
				bitmap_id > 0
		";
        $result = $db->fetchAll($sql);
        $options = array();
        foreach ($result as $key => $value) {
            $items = Catalog_Bitmaps_Items::getInstance()->getItems($value['bitmaps_ids']);
            $options[] = array ($items->id, $items->name, $items->img_big);
        }
        return $options;
    }
	
}









