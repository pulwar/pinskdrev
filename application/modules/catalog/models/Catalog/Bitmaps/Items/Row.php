<?php
class Catalog_Bitmaps_Items_Row extends Zend_Db_Table_Row {
	/**
	 * @var Zend_View
	 */
	private  $_view = null;
	
	private $_img_path = '';
	
	public function init(){
		//$this->_view = Registry::getView();
		//$this->_img_path = Catalog_Tovar::getInstance()->getFilePath();
	}
			
	public function toArray(){
		return parent::toArray();
	}
	
	/**
	 * delete
	 * @see application/library/Zend/Db/Table/Row/Zend_Db_Table_Row_Abstract#delete()
	 */
	public function delete(){
		@unlink(DIR_PUBLIC.'pics/catalog/bitmaps/'.$this->img_small);
		@unlink(DIR_PUBLIC.'pics/catalog/bitmaps/'.$this->img_big);
		return parent::delete();
	}
	
	public function getrelated($id_product, $id_group){
            $item = Catalog_Bitmaps_Items2Product::getInstance()->fetchRow('id_product = '.$id_product.' AND id_group = '.$id_group.' AND id_item = '.$this->id);
            if($item){
                return $item->related;
            }
            return '';
        }
	
	
}