<?php

class Catalog_Bitmaps_Items2Product extends Zend_Db_Table {
	
	protected $_name = 'site_catalog_bitmaps_items2product';
	protected $_primary = array('id');
	protected $_sequence = true; // Использование таблицы с автоинкрементным ключом
	protected static $_instance = null;
	
	
	protected $_referenceMap    = array(
        'items' => array(
            'columns'           => 'id_item',
            'refTableClass'     => 'Catalog_Bitmaps_Items',
            'refColumns'        => 'id',
	 		'onDelete'          => self::CASCADE,
        ),
         'groups' => array(
            'columns'           => 'id_group',
            'refTableClass'     => 'Catalog_Bitmaps_Groups',
            'refColumns'        => 'id',
	 		'onDelete'          => self::CASCADE,
        ),
        'tovar'=> array(
            'columns'           => 'id_product',
            'refTableClass'     => 'Catalog_Product',
            'refColumns'        => 'id',
	 		'onDelete'          => self::CASCADE,
        ),
    );
	
	/**
	 * Singleton instance
	 *
	 * @return Catalog_Bitmaps_Items2Product
	 */
	public static function getInstance(){
		if (null === self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public function addNewBitmapToTovar($id_bitmap){
		$item = Catalog_Bitmaps_Items::getInstance()->find($id_bitmap)->current();
		if (!is_null($item)){
			$groups_to_tovar = Catalog_Bitmaps_Groups2Product::getInstance()->fetchAll('id_group='.(int)$item->id_group);
			if ($groups_to_tovar->count()){
				foreach ($groups_to_tovar as $elem){
					$bitmap_to_tovar = Catalog_Bitmaps_Items2Product::getInstance()->createRow();
					$bitmap_to_tovar->id_product = $elem->id_product;
					$bitmap_to_tovar->id_group = $elem->id_group;
					$bitmap_to_tovar->id_item = $id_bitmap;
					$bitmap_to_tovar->save();
				}
			}
		}
		
	}
	
}