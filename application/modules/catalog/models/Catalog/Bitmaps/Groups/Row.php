<?php
class Catalog_Bitmaps_Groups_Row extends Zend_Db_Table_Row {
	/**
	 * @var Zend_View
	 */
	private  $_view = null;
	
	private $_img_path = '';
	
	
			
	public function toArray(){
		return parent::toArray();
	}
	/**
	 * delete
	 * @see application/library/Zend/Db/Table/Row/Zend_Db_Table_Row_Abstract#delete()
	 */
	public function delete(){
		
		return parent::delete();
	}
	
	public function getPrice(){
		return number_format($this->price, 2, '.', '');
	}
	
	public function getItems(){
		return Catalog_Bitmaps_Items::getInstance()->fetchAll('id_group='.(int)$this->id, array('priority DESC', 'name ASC'));
	}
	
	
}