<?php

/**

 * 
 *
 */
class View_Helper_transformers extends Zend_View_Helper_Abstract
{
	
	/**
	 * @var Zend_View
	 */
	protected $_view = null ;
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {		
		$this->_view = Zend_Registry::get('view');
	}
	
	/**
	 * Блок с ножом на главной странице
	 *
	 * @return phtml
	 */
    public function transformers()
    {
    	$this->init();
        $this->_view->transformers = Catalog_Product_Transformer::getInstance()->fetchAll('active=1','priority DESC');
        $this->_view->path = SiteDivisionsType::getInstance()->getPagePathBySystemName('transformers');
        return $this->_view->render('Transformers.phtml') ;
    }
}
