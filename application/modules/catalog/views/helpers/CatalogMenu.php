<?php

/**
 * Layout Helper
 * вывод меню каталога товаров
 * 
 *
 */
class View_Helper_catalogMenu extends Zend_View_Helper_Abstract
{
	
	
	
	/**
	 * Блок с ножом на главной странице
	 *
	 * @return html
	 */
    public function catalogMenu($id_current = null, $show_level_id = 0, $show_level = 0)    {
        $cache_menu = Catalog_Division_Cache::getInstance();

    	$show_ids[]=$id_current;
    	$current_row = $cache_menu->find($id_current)->current();
    	if ($current_row!=null){
	    	while ($parent=$current_row->getParent()) {
	    		$show_ids[] = $parent->id;
	    		$current_row = $parent;
	    	}  
    	}
        //echo Catalog_Division::getInstance()->getCatalogMenu($show_level_id, $show_level, $id_current, $show_ids);exit;
        return $cache_menu->getCatalogMenu($show_level_id, $show_level, $id_current, $show_ids);
    }
}
