<?php
class Catalog_PalitteController extends Zend_Controller_Action {
	/**
	 * Шаблон страницы
	 * @var zend_layout
	 */
	private $layout = null;
	
	/**
	 * Язык страницы
	 * @var string
	 */
	private $lang = null;
	
	/**
	 * Объект страницы
	 * @var Pages_row
	 */
	//private $_page = null;
	
	private $_onpage = 12;
	
	private $_current_page = null;
	
	private $_offset = null;
	
	public function init() {
		$this->initView();
        $this->layout = $this->view->layout();
	}
	
	/**
	 * раздел каталога
	 */
	 public function indexAction() {
        $this->layout->disableLayout();
        $id = $this->_getParam('id', 0);
        $order = $this->_getParam('order',null);
        if($order){
            $config = Catalog_Order::getInstance()->getConfigById($order);
            echo $config;
            exit;
        }
        if ($id) {
            $tovar = Catalog_Product::getInstance()->find($id)->current();
            $groups = Catalog_Bitmaps_Groups::getInstance()->getGroupsByTovar($id);
            $wood_groups = array();
            $cloth_groups = array();
            if ($groups) {
                foreach ($groups as $key=>$group) {
//var_dump($group);
                    $items = Catalog_Bitmaps_Items::getInstance()->getSelectedItemsByGroup(
                            $id,
                            $group['id'],
                            $group['price'],
                            $group['price_mehanizm']
                    );
                    if (count($items)) {
                        $group['items'] = $items;

                        if ($group['is_decor']==0) {
                            $cloth_groups[] = $group;
                        } else {
                            $wood_groups[] = $group;
                        }
                    }

                }
            }

            $this->view->cloth_groups = $cloth_groups;
            $this->view->wood_groups = $wood_groups;
            //var_dump($groups);
            //exit;
        }

    }

       
}
