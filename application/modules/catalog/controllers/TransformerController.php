<?php
class Catalog_TransformerController extends DefaultController {
	
	private $lang = null;
	
	public $_page = null;
	
	private $_onpage = 100;
	
	private $_current_page = null;
	
	private $_offset = null;
	
	public function init() {
		$this->view->addHelperPath(Zend_Registry::get('helpersPaths'), 'View_Helper');
		$id = $this->_getParam('id');
		$page = Pages::getInstance()->getPage($this->_getParam('id'));
		$this->_page = $page;
		$this->layout = $this->view->layout();
		$this->layout->setLayout("front/default");
		$this->layout->current_type = 'pages';
		$this->layout->lang = $this->lang;
		$this->layout->page = $this->_page;
		$this->lang = $this->_getParam('lang', 'ru');
		
		if ($this->_hasParam('item')) {
			$this->_forward('item');
		}
		$this->view->content = $page->content;
		$this->view->options = $options = PagesOptions::getInstance()->getPageOptions($id);
		$this->view->placeholder('title')->set($options->title);
		$this->view->placeholder('keywords')->set($options->keywords);
		$this->view->placeholder('descriptions')->set($options->descriptions);
		$this->view->placeholder('h1')->set($options->h1);
		$this->view->placeholder('id_page')->set($id);
		
		$this->layout->id_object = $this->_page->id;
		
		$this->_onpage = $this->view->onpage = $this->_getParam('onpage', 100);
		$this->_current_page = $this->view->current_page = $this->_getParam('page', 1);
		$this->_offset = ($this->_current_page - 1) * $this->_onpage;
		parent::init();
		$this->layout->setLayout("front/default");
	}
	
	/**
	 * список элементов
	 */
	public function indexAction() {
		$items = Catalog_Product_Transformer::getInstance()->fetchAll("active = '1'", 'priority DESC');
		$this->view->items = $items;
	}
	
	/**
	 * страница элемента
	 */
	public function itemAction() {		
		$url = $this->_getParam('item');		
		if ($url) {
			$item = Catalog_Product_Transformer::getInstance()->getPubItem($url);
			if (!($item)) {
				$this->_redirect( '/404' );
			}
			if (! is_null($item)) {
				$this->view->placeholder('title')->set($item->seo_title);
				$this->view->placeholder('keywords')->set($item->seo_keywords);
				$this->view->placeholder('descriptions')->set($item->seo_description);
				$this->view->placeholder('h1')->set($item->title);
				$bread_items[] = array('title' => $item->title, 'path' => $item->url);
				$this->view->layout()->bread_items = $bread_items;
				$this->view->item = $item;
			} else {
				$this->error404();
			}
		}
	}
}
