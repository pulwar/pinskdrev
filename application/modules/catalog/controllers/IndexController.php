<?php

class Catalog_IndexController extends DefaultController {
	/**
	 * Шаблон страницы
	 * @var zend_layout
	 */
	private $layout = null;
	
	/**
	 * Язык страницы
	 * @var string
	 */
	private $lang = null;
	
	/**
	 * Объект страницы
	 * @var Pages_row
	 */
	private $_page = null;
	
	private $_onpage = '999';
	
	private $_current_page = null;
	
	private $_offset = null;
	
	/**
	 * @var Zend_Session_Namespace
	 */
	private $_filter = null;
	
	public function init() {		
		$this->view->addHelperPath(Zend_Registry::get('helpersPaths'), 'View_Helper');
		$this->layout = $this->view->layout();
		$this->lang = $this->_getParam('lang', 'ru');
		$this->layout->lang = $this->lang;
		if ($this->getRequest()->getActionName() == 'productunit') {
			$this->layout->setLayout("layout");
			return;
		}
		
		if ($this->getRequest()->getActionName() == 'prices') {
			$this->layout->setLayout("layout");
			return;
		}

		if ($this->getRequest()->getActionName() == 'pricesold') {
			$this->layout->setLayout("layout");
			return;
		}

		if ($this->getRequest()->getActionName() == 'material') {
			$this->layout->setLayout("layout");
			return;
		}
		
		if ($this->getRequest()->getActionName() == 'delivery') {
			$this->layout->setLayout("layout");
			return;
		}
		
		$this->view->maker_path = SiteDivisionsType::getInstance()->getPagePathBySystemName('makers');
		$id = $this->_getParam('id');
		$page = Pages_Cache::getInstance()->getPage($this->_getParam('id'));		
		if (is_null($page) || $page->published == '0') {
			//$this->_redirect( '/404' );
		}
		$this->_page = $page;
		
		$this->layout->setLayout("front/default");
		$this->layout->current_type = 'pages';
		
		$this->layout->page = $this->_page;
		
		if ($this->_hasParam('product')) {
			$this->_forward('product');
		}
		
		$this->_onpage = $this->view->onpage = $this->_getParam('onpage', 999);
		$this->_sort = $this->view->sort = $this->_getParam('sort', 0);
		
		$this->_filter = new Zend_Session_Namespace('paginationFulSession');
		if (! isset($this->_filter->maker))
			$this->_filter->maker = 0;
		if (! isset($this->_filter->type))
			$this->_filter->type = 0;
		if (! isset($this->_filter->category))
			$this->_filter->category = 0;
		
		$this->_maker = $this->_hasParam('maker') ? $this->_getParam('maker', 0) : $this->_filter->maker;
		$this->view->maker = $this->_filter->maker = $this->_maker;
		
		$this->_type = $this->_hasParam('type') ? $this->_getParam('type', 0) : $this->_filter->type;
		$this->view->type = $this->_type;
		
		$this->_category = $this->_hasParam('category') ? $this->_getParam('category', 0) : $this->_filter->category;
		$this->view->category = $this->_category;
		
		$this->_current_page = $this->view->current_page = $this->_getParam('page', 1);
		
		$this->_offset = ($this->_current_page - 1) * $this->_onpage;
		//$this->view->onpage = $this->_onpage;
		$coll = $this->view->collection = $this->_getParam('coll');
		switch ($coll) {
			case 1 :
				$this->_collection = 'оскар';
				break;
			case 2 :
				$this->_collection = 'венеция';
				break;
			case 3 :
				$this->_collection = 'милана';
				break;
			case 4 :
				$this->_collection = 'верди';
				break;
			case 5 :
				$this->_collection = 'верди люкс';
				break;
			default :
				$this->_collection = $this->view->collection = '';
		}
                if($page){
                    $this->view->content = $page->content;
                }
		$this->view->options = $options = PagesOptions_Cache::getInstance()->getPageOptions($id);
		//Zend_Debug::dump($options); exit;
		$this->view->placeholder('title')->set($options->title);
		$this->view->placeholder('keywords')->set($options->keywords);
		$this->view->placeholder('descriptions')->set($options->descriptions);
		$this->view->placeholder('h1')->set($options->h1);
		$this->view->placeholder('id_page')->set($id);
		if(isset($this->_page->id)){
                    $this->layout->id_object = $this->_page->id;
                }
                if($page){
                    if ($page->show_childs == 1) {
                            $this->layout->page_childs = Pages_Cache::getInstance()->getChildrenAndURLs($page->id);
                    }
                }
	}
	
	/**
	 * раздел каталога
	 */
	public function indexAction() {			
		$url = $this->_getParam('cat');

		if (!preg_match("/[^0-9]/", $url) && $url !='') {
		    $this->_redirect('/cat');
		}
		$this->view->id_div = $id_division = Catalog_Division::getInstance()->getIdByUrl($url);
		if ($id_division == null && $url != null) {
		    $this->_redirect('/404');
		}
		$division = Catalog_Division::getInstance()->getItem($id_division);
		if (isset($division)) {
			$parent_div = $division->parent_id;
		}
		
		if ($this->_hasParam('clearsort')) {
			$this->_filter->maker = 0;
			$this->_filter->type = 0;
			$this->_filter->category = 0;
			$this->_redirect('/cat/' . $url);
		}
		
		if ($division != null) {
			$this->view->placeholder('title')->set($division->seo_title);
			$this->view->placeholder('keywords')->set($division->seo_keywords);
			$this->view->placeholder('descriptions')->set($division->seo_description);
			$this->view->placeholder('h1')->set($division->seo_h1);
			$this->view->layout()->bread_items = $division->getBread($this->_page->path);
			
			if ($division->issetChilds()) {
				($division->issetProductInDiv()) ? $onpage = null : $onpage = $this->_onpage;
				$this->view->childs = $division->getPublicChilds($onpage, $this->_offset);
				$this->view->total = Catalog_Division::getInstance()->getCountOfPublicChildren($id_division);
			}
			if ($division->issetProductInDiv()) {
				$this->view->products = Catalog_Product::getInstance()->getPublicProductsByDivId($id_division, $this->_onpage, $this->_offset, $this->_maker, $this->_category, $this->_sort, $this->_type, $this->_collection);
				$this->view->divs_parents = Catalog_Division::getInstance()->getParents($parent_div, $id_division);
			
			}
			$ids = explode(',', $division->parent_ids);
			if (in_array(183, $ids)) {
				$this->view->korpus = 1;
			}
			$this->view->division = $division;
			$this->layout->id_division = $division->id;
		
		} else {
			$this->view->childs = $childs = Catalog_Division::getInstance()->getRootItems($this->_onpage, $this->_offset);
			$this->view->total = Catalog_Division::getInstance()->getCountOfPublicChildren($id_division = 0);
		}
		$this->view->id_division = $id_division;
		$this->view->division_url = $url;
	}
	
	/**
	 * раздел каталога
	 */
	public function productAction() {			
		Loader::loadCommon('Captcha');
        	$captcha = new Captcha();
		$url_cat = $this->_getParam('cat');
		$this->view->cat = $url_cat;
		$id_division = Catalog_Division_Cache::getInstance()->getIdByUrl($url_cat);
		/*if ($id_division == null) {
		    $this->_redirect('/404');
		}*/
		$division = Catalog_Division_Cache::getInstance()->getItem($id_division);
		$url_product = $this->_getParam('product');
		
		 if (!preg_match("/[^0-9]/", $url_product)) {
		    $id_product = $url_product;
		}
		else {
		    $id_product = Catalog_Product::getInstance()->getIdByUrl($url_product);
		}		
		if ($id_product == null) {
		    $this->_redirect('/404');
		}
		if ($division == null) {
			$id_div = Catalog_Division::getInstance()->getDivisionIdByProductId($id_product);
			$division = Catalog_Division_Cache::getInstance()->getItem($id_div);
		}
		$this->view->order = $this->_getParam('order');
		$product = Catalog_Product::getInstance()->getPublicItem($id_product);

		if ($product == null) {
		    $this->_redirect('/404');
		}		
                $this->view->packages = $packages =  $product->getPackages();
		$this->view->mebeltypes = $product->id_mebeltype;
		$element = Catalog_Product2Divisions::getInstance()->getElement($division->id, $id_product);
		if ($element != null) {
		$element = $element->main_element;
		}
		if ($element != 0) {
		    $element_id = $element;
		} else {
		    $element_id = $product->unit_id;
		}
		if (isset($_SERVER['HTTP_REFERER'])) {
		$referer = $_SERVER['HTTP_REFERER'];
		$referer = explode("&", $referer);
		if (isset($referer[3])) {
		    $referer = explode("=", $referer[3]);
		    if ($referer[0] == 'colors' && $referer[1] != 0) {
		        $element_color = $referer[1];
		        $element_id = Catalog_Product_Units::getInstance()->getByColor($id_product, $element_color);
		    }
		}
		}
		$unit = Catalog_Product_Units::getInstance()->getById($element_id);
		$main_prod2div = Catalog_Product2Divisions::getInstance()->getPrimaryDiv($id_product);
		
		$this->history = new Zend_Session_Namespace('user_history');
		if (! isset($this->history->products)) {
			$this->history->products = array();
		}
		
		$product = Catalog_Product::getInstance()->getPublicItem($id_product);
		
		$this->view->packages = $packages =  $product->getPackages();
		$options = $product->getDefaultOptions();

		$this->history->products[$id_product] = array('title' => $product->title, 'img' => $unit->img, 'price' => $unit->price, 'price_with_tr' => $unit->price_with_tr, 'product_url' => $product->url, 'division_url' => $division->url, 'special_offer' => $options['akciya']->value );
				
		$this->history->setExpirationSeconds(43200);
		$time_left = $_SESSION['__ZF']['user_history']['ENT'] - time();
		if ($time_left == 0) {
			Zend_Session::namespaceUnset('user_history');
		}
		$main_division = Catalog_Division_Cache::getInstance()->getItem($main_prod2div->id_division);
		$this->view->isSoft = $main_division->getTopParent()->id != 183;
		//Zend_Debug::dump($unit);exit;
		$related = Catalog_Product::getInstance()->getRelated($id_product);
		$collection = Catalog_Product::getInstance()->getCollection($id_product);
		$images = Catalog_Product_Units::getInstance()->getActiveByProductId($id_product);
		$complect = Catalog_Product_Complect::getInstance()->getComplect($element_id);
		if ($product != null) {
			$this->view->placeholder('title')->set($product->seo_title);
			$this->view->placeholder('keywords')->set($product->seo_keywords);
			$this->view->placeholder('descriptions')->set($product->seo_description);
			$this->view->placeholder('h1')->set($product->seo_h1);
			
			if ($division != null) {
				$bread_items = $division->getBread($this->_page->path);
			}
			$bread_items[] = array('title' => $product->title);
			$this->view->layout()->bread_items = $bread_items;
		} else {
			$this->_redirect('/404');
		}
                $towns = GruzTowns::getTowns();
                
                if ($towns != null && $towns) {
                    $select_towns = '<select name="town_1" id="town_1" class="groupselect">';
			/*foreach ($towns as $key => $value) {
                                            $select_towns .= '<optgroup label="'.$key.'">';
                                            foreach ($value as $town_id => $town) {
                                                $select_towns .= '<option value="'.$town_id.'">'.$town.'</option>';
                                            }
                                            $select_towns .= '</optgroup>';
                                        }*/
                    foreach ($towns as $key => $value) {
                            $select_towns .= '<option value="' . $key . '">' . $value . '</option>';
                    }
                    $select_towns .= '</select>';
                    $this->view->towns = $select_towns;
		}		
		//элементы 
		$this->view->elements = Catalog_Product_Units::getInstance()->getImagesByProductId($id_product);		
		$options = $product->getDefaultOptions();
		$this->view->options = $options;
		
		$product->description = str_replace('{table_prices}', $this->getPricesTable($product->id), $product->description);
		$this->layout->id_product = $id_product;
		$this->view->captcha = $captcha->getOperation();
		$this->view->product = $product;		
		$this->view->unit = $unit;
		$this->view->related = $related;
		$this->view->collection = $collection;
		$this->view->images = $images;
		$this->view->product_contacts = Catalog_Product_Contacts::getInstance()->getAll();
		$this->view->complect = $complect;
		$this->view->id_division = $id_division;
		$this->view->division_url = $url_cat;
		// echo "<pre>";
		// var_export($unit); die();
		if($unit->only_transform) {
			$tr = 0;
		} else {
			$tr = 1;
		}
		$aviability = Catalog_Product_Units::getInstance()->getAvailabilityCount($element_id, $tr);
		$this->view->delivery = $aviability;		
		$this->SetViewOptions($unit);
		$this->SetDefaultOptions($product);
		//Zend_Debug::dump($unit);exit();
	//$this->GetMatDesc($unit->mat);
	

	}
	public function gruzcalcAction() {
		$result = null;
		$params = $this->_request->getParam('p');
		$params_array = explode('&', $params);
		$url = "http://www.pecom.ru/bitrix/components/pecom/calc/ajax.php";
		$client = new Zend_Http_Client($url . "?" . $params);
		$response = $client->request(Zend_Http_Client::GET);
		$result = $response->getBody();
		$result_array = json_decode($result, true);
		$result = json_encode($result_array);
		echo $result;
		exit();
	}	
	public function GetMatDesc($unit) {
		//$material = Catalog_Product_Material::getInstance()->get
	}
	
	public function pricesAction() {
		if (! $this->_request->isXmlHttpRequest()) {
			exit();
		} else {
			$transformer = $this->_getParam('transformer');
			$price = Catalog_Product_Units::getInstance()->getPrice($this->_getParam('id'), $transformer);
			$this->view->unit_prices = $price;
		}
	}

	public function pricesoldAction() {
		if (! $this->_request->isXmlHttpRequest()) {
			exit();
		} else {
			$transformer = $this->_getParam('transformer');
			$price = Catalog_Product_Units::getInstance()->getoldPrice($this->_getParam('id'), $transformer);
			$old_price = '';			
			if ($price != 0 && $price != null) {
			$old_price = '<center><p class="old_price">Старая цена: <span>'.number_format($price,0,',',' ').' руб.</span></p><p class="new_price">Новая цена:</p></center></style>';
			}
			else {
			$old_price = '';
			}
			$this->view->old_price = $old_price;
		}
		
	}
	
	public function materialAction() {
		if (! $this->_request->isXmlHttpRequest()) {
			exit();
		} else {
			$material = Catalog_Product_Material::getInstance()->getDescr($this->_getParam('id'));
			$this->view->material_descr = $material;
		}
	}
	
	public function deliveryAction() {
		if (! $this->_request->isXmlHttpRequest()) {
			exit();
		} else {
			$aviability = Catalog_Product_Units::getInstance()->getAvailabilityCount($this->_getParam('id'), $this->_getParam('transformer'));
			//echo 123; die();
			$this->view->delivery = $aviability;
		}
	}
	
	public function productunitAction() {
		if (! $this->_request->isXmlHttpRequest())
			exit();
		if ($this->_getParam('mat') != '') {
			$type = 'material_ids';
			$unit = Catalog_Product_Units::getInstance()->getByTovar($this->_getParam('pr'), $this->_getParam('mat'), $type);
		} elseif ($this->_getParam('material') != '') {
			$material = Catalog_Product_Material::getInstance()->getDescr($this->_getParam('material'));
			$this->view->material_descr = $material;
		} elseif ($this->_getParam('color') != '') {
			$type = 'color_ids';
			$unit = Catalog_Product_Units::getInstance()->getByTovar($this->_getParam('pr'), $this->_getParam('color'), $type);
		} elseif ($this->_getParam('clr') != '') {
			$type = $this->_getParam('type');
			//Zend_Debug::dump($type); exit();
			if ($this->_getParam('type') == "fabric") {
				$type = 'fabrics_ids';
			}
			if ($this->_getParam('type') == "bitmap") {
				$type = 'bitmaps_ids';
			}
			$unit = Catalog_Product_Units::getInstance()->getByTovar($this->_getParam('pr'), $this->_getParam('clr'), $type);
		} elseif ($this->_getParam('decor') != '') {
			$type = 'decor_ids';
			$unit = Catalog_Product_Units::getInstance()->getByTovar($this->_getParam('pr'), $this->_getParam('decor'), $type);
		} elseif ($this->_getParam('transformers') != '') {
			$type = 'transformer_ids';
			$unit = Catalog_Product_Units::getInstance()->getByTovar($this->_getParam('pr'), $this->_getParam('transformers'), $type);
		} else {
			$unit = Catalog_Product_Units::getInstance()->getById($this->_getParam('id'));
		}
		if (! $unit)
			exit();
			//echo json_encode($unit->toArray());
		$this->view->unit = $unit;
		$url_cat = $this->_getParam('cat');
		$id_tovar = $unit->id_product;
		$id_division = Catalog_Division_Cache::getInstance()->getIdByUrl($url_cat);
		$division = Catalog_Division_Cache::getInstance()->getItem($id_division);
		$this->view->id_division = $id_division;
		$product = Catalog_Product::getInstance()->getPublicItem($unit->id_product);
		$this->view->mebeltypes = $product->id_mebeltype;
		$main_prod2div = Catalog_Product2Divisions::getInstance()->getPrimaryDiv($unit->id_product);
		$main_division = Catalog_Division_Cache::getInstance()->getItem($main_prod2div->id_division);
		$this->view->isSoft = $main_division->getTopParent()->id != 183;
		
		$this->view->product = $product;
		$this->view->options = $options = $product->getDefaultOptions();
		$complect = Catalog_Product_Complect::getInstance()->getComplect($unit->id);
		
		if ($complect->count() == 0) {
			$complect = Catalog_Product_Complect::getInstance()->getComplect($product->unit_id);
		}
		
		$this->view->complect = $complect;
		$this->SetViewOptions($unit);
		$this->SetDefaultOptions($product);
	}
	
	private function SetDefaultOptions($product) {
		if ($product->material_ids != '')
			$this->view->default_materials = Catalog_Product_Material::getInstance()->getByProduct($product->material_ids);
		if ($product->bitmaps_ids != '')
			$this->view->default_bitmaps = Catalog_Bitmaps_Items::getInstance()->getItemsAsOptions3($product->id);
		if ($product->fabrics_ids != '')
			$this->view->default_fabrics = Catalog_Fabrics_Groups::getInstance()->getByProduct($product->fabrics_ids);
		if ($product->decor_ids != '')
			$this->view->default_decors = Catalog_Product_Decor::getInstance()->getByProduct($product->decor_ids);
		if ($product->transformer_ids != '')
			$this->view->default_transformers = Catalog_Product_Transformer::getInstance()->getByProduct($product->transformer_ids);
		if ($product->color_ids != '')
			$this->view->default_colors = Catalog_Product_Color::getInstance()->getByProduct($product->color_ids);
	}
	
	private function SetViewOptions($unit) {
		if ($unit->material_ids != '')
			$this->view->materials = Catalog_Product_Material::getInstance()->getItemsAsOptions();
		if ($unit->bitmaps_ids != '')
			//$this->view->bitmaps = Catalog_Bitmaps_Groups::getInstance()->getItemsAsOptions();
			if ($unit->fabrics_ids != '')
				$this->view->fabrics = Catalog_Fabrics_Groups::getInstance()->getItemsAsOptions();
		if ($unit->decor_ids != '')
			$this->view->decors = Catalog_Product_Decor::getInstance()->getItemsAsOptions();
		if ($unit->transformer_ids != '')
			$this->view->transformers = Catalog_Product_Transformer::getInstance()->getItemsAsOptions();
		if ($unit->color_ids != '')
			$this->view->colors = Catalog_Product_Color::getInstance()->getItemsAsOptions();
		if ($unit->id_transformer > 0) {
			$page_transform = Pages_Cache::getInstance()->getPageByDivType('transformers');
			if ($page_transform)
				$this->view->options['transform_page_id'] = $page_transform->id;
		}
	}
	
	public function akciiAction() {
		$this->view->actions = Pages::getInstance()->getChildrensActions(1212);
	}
	
	public function historyAction() {
	
	}
	/*
	* $sort - айдишники типов мебели относящихся к корпусной мебели
	*/
	public function akciikorpusAction() {
		$sort = '20,21,22,23,24,25,26,27,28,29';
		$this->view->products = Catalog_Product::getInstance()->getPublicProductsAkcii($sort, $this->_onpage, $this->_offset);
		
		$this->view->total = count(Catalog_Product::getInstance()->getPublicProductsAkcii($sort)); //Catalog_Product::getInstance()->getCountPublicProductsAkcii( );
	}
	/*
	* $sort - айдишники типов мебели относящихся к мягкой мебели
	*/
	public function akciimyagkayaAction() {
		$sort = '14,15,16,17,18,19,31';
		$this->view->products = Catalog_Product::getInstance()->getPublicProductsAkcii($sort, $this->_onpage, $this->_offset);
		$this->view->total = count(Catalog_Product::getInstance()->getPublicProductsAkcii($sort)); //Catalog_Product::getInstance()->getCountPublicProductsAkcii( );
	}
	
	public function actionsAction() {		
		$page_url = $_SERVER['REQUEST_URI'];
		$page_url = substr($page_url, 1);
		$ids = Pages::getInstance()->getInstance()->getDivType($page_url);
		if($ids==false){
			$this->_redirect( '/404' );
		}		
		$this->view->products = Catalog_Product::getInstance()->getActions($ids);
	}
	
	private function getPricesTable($id) {
		$html = '';
		if ($id) {
			
			$prices = Catalog_Bitmaps_Groups2Product::getInstance()->getPricesByProductId($id);
			if ($prices->count()) {
				$html = '<table cellspacing="1" cellpadding="1" border="1" width="450"><tbody>
					<tr>
					<td style="text-align: center;"><strong>обивка</strong></td>
					<td style="text-align: center;"><strong>с механизмом</strong></td>
					<td style="text-align: center;"><strong>без механизма</strong></td>
					</tr>';
				foreach ($prices as $price) {
					$html .= '<tr>
								<td style="text-align: left;">' . $price->name . '</td><td style="text-align: center;"><span style="">';
					$html .= ($price->price_mehanizm) ? number_format($price->price_mehanizm, 0, '', ' ') . ' руб.' : '-';
					$html .= '</span></td><td style="text-align: center;">';
					$html .= ($price->price) ? number_format($price->price, 0, '', ' ') . ' руб.' : '-';
					$html .= '</td></tr>';
				}
				$html .= '</tbody></table>';
			}
		
		}
		return $html;
	}

}