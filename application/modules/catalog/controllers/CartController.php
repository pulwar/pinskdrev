<?php
class Catalog_CartController extends Zend_Controller_Action {
	/**
	 * Шаблон страницы
	 * @var zend_layout
	 */
	private $layout = null;
	
	private $_cart = null;
	
	/**
	 * Объект страницы
	 * @var Pages_row
	 */
	private $_page = null;
	
	private $_id_unit = null;

    private $_id_transform = null;
	
	public function init() {
		
		$this->_id_unit = $unit = $this->_getParam('id_unit');
		$this->_division = $this->_getParam('division', 0);
        $this->_id_transform = $trans = $this->_getParam('id_transformers');
		$this->_url_cat = "";
		if ($this->_division > 0)
			$this->_url_cat = Catalog_Division::getInstance()->getUrlById($this->_division);
		$this->_cart = $abw = new Zend_Session_Namespace('cart');
		$this->_order = new Zend_Session_Namespace('order');
		
		if (! isset($this->_cart->products)) {
			$this->_cart->products = array();
		}
		if (! isset($this->_order->products)) {
			$this->_order->products = array();
		}
		if ($this->getRequest()->getActionName() == 'print') {
			$this->layout = $this->view->layout();
			$this->layout->setLayout("front/print");
			$this->_forward('print');
			return;
		} else if ($this->getRequest()->getActionName() == 'savetxt') {
			$this->layout = $this->view->layout();
			$this->layout->setLayout("front/print");
			$this->_forward('savetxt');
			return;
		}
		$action = $this->_getParam('act');
		// process action
		switch ($action) {
			case 'saveconfig' :
				$this->_forward('saveconfig');
				break;
			case 'add' :
				$this->_forward('add');
				break;
			case 'remove' :
				$this->_forward('remove');
				break;
			case 'recalc' :
				$this->_forward('recalc');
				break;
			case 'clear' :
				$this->_forward('clear');
				break;
			case 'change' :
				$this->_forward('change');
				break;
			    case 'fastbuy' :
				$this->_forward('fastbuy');
				break;
			case 'addcustom' :
				$this->_forward('addcustom');
				break;
			default :
				$this->_forward('index');
				break;
		}
		$this->order_file = '';

		$this->view->addHelperPath(Zend_Registry::get('helpersPaths'), 'View_Helper');
		$id = $this->_getParam('id');
		$page = Pages::getInstance()->getPage($this->_getParam('id'));
		if (is_null($page) || $page->published == '0') {
			$this->_redirect('/404');
		}
		$this->_page = $page;
		$this->layout = $this->view->layout();
		$this->layout->setLayout("front/default");
		$this->layout->current_type = 'pages';
		$this->layout->page = $this->_page;
		
		$this->view->content = $page->content;
		$options = PagesOptions::getInstance()->getPageOptions($id);
		$this->view->placeholder('title')->set($options->title);
		$this->view->placeholder('keywords')->set($options->keywords);
		$this->view->placeholder('descriptions')->set($options->descriptions);
		$this->view->placeholder('h1')->set($options->h1);
	}
	
	/**
	 * просмотр корзины
	 */
	public function indexAction() {
		$this->view->catalog_path = $path = SiteDivisionsType::getInstance()->getPagePathBySystemName('division');
		
		if (is_array($this->_cart->products)) {
			$captcha = new Captcha();
			if ($this->_request->isPost()) {
				$data = $this->_getParam('form');
				if ($data['user_name'] == '' || $data['user_phone'] == '' || $data['user_email'] == '') {
					$this->view->err = 1;
					$this->view->order = $data;
				} else if ((($data['captch_res'])/2) != $data['captcha']) {
					$this->view->err = 2;
					$this->view->order = $data;
				} else {

					$order = $this->getOrder($path);
					$this->createOrderFile();

					$ordertxt = $this->getOrderTxt($path);
					if ($ordertxt == '') {
						$opera_fail = 'fail';
					}
					else {
						$opera_fail = 'no_fail';
					}
					if ($opera_fail != 'fail') {
					$users = Users::getInstance()->getUsersToSendMail();
					$emails = array();
					foreach ($users as $user) {
						$emails[] = $user['email'];
					}
					$recipient = 'New message';

					if (isset($data['payment']) && $data['payment'] == 'В рассрочку') {
						$template = FeedbackTemplates::getInstance()->getBySystemName('order_rassrochka');
						$total_price = $this->getPrice();
						$platezh = round($total_price * (1 - $data['rassr_vznos'] / 10) / $data['rassr_srok']);
						$data['itogo'] = $total_price;
						$data['platezh'] = $platezh;
						$template = FeedbackTemplates::getInstance()->getBySystemName('order_rassrochka');
						$subject = 'Заказ с сайта с рассрочкой';
					} else {
						$template = FeedbackTemplates::getInstance()->getBySystemName('order');
						$subject = 'Zakaz';
					}
					$template = $template['content'];
					
					foreach ($data as $key => $param) {
						$template = str_replace('{' . $key . '}', $param, $template);
					}
					$template = preg_replace('/{.+}/Usi', '', $template);
					$body = $order . $template; //echo $order; exit;
					$ordertxt .= "\r\n" . strip_tags($template);
					Loader::loadCommon('Mail');
					$from = 'info@pinskdrev.msk.ru';
                        $serializearray = array();
                        foreach ($this->_cart->products as $key => $products) {
                            $serializearray[$key]['name'] = $products['title'];
                            $serializearray[$key]['count'] = $products['count'];
                            $serializearray[$key]['id'] = $products['id_product'];
                            $serializearray[$key]['price'] = $products['total_price'];
                        }

					$zakaz = Catalog_Zakaz::getInstance()->insert(array('content' => $body, 'price' => $this->getPrice(), 'contenttxt' => $ordertxt, 'serialize_arr' => serialize($serializearray)));
                    			$this->getOrderTransaction($zakaz);
					
		            $emails[] = 'box.pinskdrev@gmail.com';		            
		            $emails[] = 'pinskdrev.moscow@yandex.ru';
					$emails[] = 'box.pinskdrev@outlook.com';

						//get 3 blocks for the letter
					$page = Pages::getInstance()->getPage(1256);


					Mail::send($emails, $body, $from, $subject.' N '.$zakaz.' '.$_SERVER["HTTP_HOST"], $recipient, $this->order_file);

					//отправляем уведомление
					$template = FeedbackTemplates::getInstance()->getBySystemName('confirm');
					$template = $template['content'];
					$subject = 'Уведомление о заказе';
					$params = '<p>Номер Вашего заказа <b>' . $zakaz . 'e</b></p>';
					$body = $template . $params . $order.$page->content;
					$from = 'info@pinskdrev.msk.ru';
					if ($data['user_email']  != '') {
						$email[] = $data['user_email'];
						Mail::send($email, $body, $from, $subject . ' N ' . $zakaz . 'e', $recipient);
					}
					$this->_request->clearParams();
					$this->_cart->products = array();
					$this->_order->products = array();
					$this->view->ok = 1;
					$this->view->zakaz = $zakaz;
					$session_complete = new Zend_Session_Namespace('zakaz_complete');
					$session_complete->zakaz = $zakaz;
					$session_complete->ok = 1;
					$this->_redirector = $this->_helper->getHelper('Redirector');
					$this->_redirector->setCode(302)->setGotoUrl('http://' . $_SERVER['HTTP_HOST'] . '/cart/complete');
					$this->_redirector->redirectAndExit();
					}
				}
			}
			$this->view->products = $abw = $this->_cart->products;
            //Zend_Debug::dump($abw);exit();
			$this->view->order_params = $this->_order->products;
			$this->view->captcha = $captcha->getOperation();
		}
		
		$complete = $this->_hasParam('complete');
		if ($complete) {
			$session_complete = new Zend_Session_Namespace('zakaz_complete');
			if ($session_complete && $session_complete->ok) {
				$this->view->zakaz = $session_complete->zakaz;
				$this->view->zakaz_row = Catalog_Zakaz::getInstance()->find($session_complete->zakaz)->current();
				$this->view->ok = 1;
				$page = Pages::getInstance()->getPageByParam('path', 'cart_complete');
				$options = PagesOptions::getInstance()->getPageOptions($page->id);
				$this->view->content = $page->content;
				$this->view->placeholder('title')->set($options->title);
				$this->view->placeholder('keywords')->set($options->keywords);
				$this->view->placeholder('descriptions')->set($options->descriptions);
				$this->view->placeholder('h1')->set($options->h1);
			} else {
				$this->_redirector = $this->_helper->getHelper('Redirector');
				$this->_redirector->setCode(302)->setGotoUrl('http://' . $_SERVER['HTTP_HOST'] . '/cart');
				$this->_redirector->redirectAndExit();
			}
		}

	$session_complete = new Zend_Session_Namespace('fast_zakaz_complete');
        if ($session_complete) {
            if ($session_complete && $session_complete->ok) {
                $this->view->zakaz = $session_complete->zakaz;
                $this->view->zakaz_row = Catalog_Zakaz::getInstance()->find($session_complete->zakaz)->current();
                $this->view->ok = 1;
                $page = Pages::getInstance()->getPageByParam('path', 'cart_complete');
                $options = PagesOptions::getInstance()->getPageOptions($page->id);
                $this->view->content = $page->content;
                $this->view->placeholder('title')->set($options->title);
                $this->view->placeholder('keywords')->set($options->keywords);
                $this->view->placeholder('descriptions')->set($options->descriptions);
                $this->view->placeholder('h1')->set($options->h1);
                Zend_Session::namespaceUnset('fast_zakaz_complete');
            }
        }
	}

	public function createOrderFile($id_unit = null, $count = null) {

		$last_order_number = @file_get_contents('files/orders/order_log.txt') + 1;
		$temp = $last_order_number;
		$capacity = 0;
		$zeros = '';

		while($temp) {
			$capacity ++;
			$temp = intval((int)$temp/10);
		}

		for($i = $capacity; $i < 5; $i++){
			$zeros .= 0;
		}

		$last_order_number = $zeros.$last_order_number;

		$orderLog = fopen ('files/orders/order_log.txt','w+');
		fwrite ($orderLog, $last_order_number);
		fclose ($orderLog);

		$id_unit = $this->_cart->products ? 0 : $id_unit;

		$complect = Catalog_Product_Complect::getInstance()->getUnitComplect($id_unit);

		if (count($complect) && $id_unit != null && $count != null) {

			foreach ($complect as $item) {
				$unit = Catalog_Product_Units::getInstance()->getById($item->id_complect_unit);

				if($unit->nomenclature == 'K'){
					$product_code = $unit->product_cod ?  $unit->product_cod : "0";
					$facing_code = $unit->facing_cod ?  $unit->facing_cod : "0";
					$dyeing_code = $unit->dyeing_cod ?  $unit->dyeing_cod : "0";

					$orderRaw = 'K|' .
						$product_code.
						'|' . $facing_code .
						'|' . $dyeing_code .
						'|0|0|0|0|' . $count * $item->count;
				}
				elseif($unit->nomenclature == 'M'){
					$model_code = $unit->model_code ?  $unit->model_code : "0";
					$fabric_name = $unit->fabric_name ?  $unit->fabric_name : "0";
					$fabric_name_group = $unit->fabric_name_group ?  $unit->fabric_name_group : "0";
					$completing_code = $unit->completing_code ?  $unit->completing_code : "0";

					$orderRaw = 'M|0|0|0|' .
						$model_code .
						'|' . $fabric_name .
						'|' . $fabric_name_group .
						'|' . $completing_code .
						'|' . $count * $item->count;
				}

				date_default_timezone_set('Europe/Moscow');

				$orderTxt = fopen ('files/orders/ZAKAZ_000000000000_' .date('dmY') . '_PM_' . $last_order_number . '.txt','a');
				fwrite ($orderTxt, $orderRaw. "\r\n");
				fclose ($orderTxt);

				$this->order_file = 'files/orders/ZAKAZ_000000000000_' .date('dmY') . '_PM_' . $last_order_number . '.txt';

			}
		}

		if(!count($complect) && $id_unit != null && $count != null){
			$unit = Catalog_Product_Units::getInstance()->getById($id_unit);

			if($unit->nomenclature == 'K'){
				$product_code = $unit->product_cod ?  $unit->product_cod : "0";
				$facing_code = $unit->facing_cod ?  $unit->facing_cod : "0";
				$dyeing_code = $unit->dyeing_cod ?  $unit->dyeing_cod : "0";

				$orderRaw = 'K|' .
					$product_code.
					'|' . $facing_code .
					'|' . $dyeing_code .
					'|0|0|0|0|' . $count;
			}
			elseif($unit->nomenclature == 'M'){
				$model_code = $unit->model_code ?  $unit->model_code : "0";
				$fabric_name = $unit->fabric_name ?  $unit->fabric_name : "0";
				$fabric_name_group = $unit->fabric_name_group ?  $unit->fabric_name_group : "0";
				$completing_code = $unit->completing_code ?  $unit->completing_code : "0";

				$orderRaw = 'M|0|0|0|' .
					$model_code .
					'|' . $fabric_name .
					'|' . $fabric_name_group .
					'|' . $completing_code .
					'|' . $count;
			}

			date_default_timezone_set('Europe/Moscow');

			$orderTxt = fopen ('files/orders/ZAKAZ_000000000000_' .date('dmY') . '_PM_' . $last_order_number . '.txt','a');
			fwrite ($orderTxt, $orderRaw);
			fclose ($orderTxt);

			$this->order_file = 'files/orders/ZAKAZ_000000000000_' .date('dmY') . '_PM_' . $last_order_number . '.txt';
		}
		else{

			foreach($this->_cart->products as $key => $val){

				$complect = Catalog_Product_Complect::getInstance()->getUnitComplect($key);

				if (count($complect)) {

					foreach ($complect as $item) {
						$unit = Catalog_Product_Units::getInstance()->getById($item->id_complect_unit);

						if($unit->nomenclature == 'K'){
							$product_code = $unit->product_cod ?  $unit->product_cod : "0";
							$facing_code = $unit->facing_cod ?  $unit->facing_cod : "0";
							$dyeing_code = $unit->dyeing_cod ?  $unit->dyeing_cod : "0";

							$orderRaw = 'K|' .
								$product_code.
								'|' . $facing_code .
								'|' . $dyeing_code .
								'|0|0|0|0|' . $val['count'] * $item->count;
						}
						elseif($unit->nomenclature == 'M'){
							$model_code = $unit->model_code ?  $unit->model_code : "0";
							$fabric_name = $unit->fabric_name ?  $unit->fabric_name : "0";
							$fabric_name_group = $unit->fabric_name_group ?  $unit->fabric_name_group : "0";
							$completing_code = $unit->completing_code ?  $unit->completing_code : "0";

							$orderRaw = 'M|0|0|0|' .
								$model_code .
								'|' . $fabric_name .
								'|' . $fabric_name_group .
								'|' . $completing_code .
								'|' . $val['count'] * $item->count;
						}

						date_default_timezone_set('Europe/Moscow');

						$orderTxt = fopen ('files/orders/ZAKAZ_000000000000_' .date('dmY') . '_PM_' . $last_order_number . '.txt','a');
						fwrite ($orderTxt, $orderRaw. "\r\n");
						fclose ($orderTxt);

						$this->order_file = 'files/orders/ZAKAZ_000000000000_' .date('dmY') . '_PM_' . $last_order_number . '.txt';

					}
				}
				else{
					$unit = Catalog_Product_Units::getInstance()->getById($key);

					if($unit->nomenclature == 'K'){
						$product_code = $unit->product_cod ?  $unit->product_cod : "0";
						$facing_code = $unit->facing_cod ?  $unit->facing_cod : "0";
						$dyeing_code = $unit->dyeing_cod ?  $unit->dyeing_cod : "0";

						$orderRaw = 'K|' .
							$product_code.
							'|' . $facing_code .
							'|' . $dyeing_code .
							'|0|0|0|0|' . $val['count'];
					}
					elseif($unit->nomenclature == 'M'){
						$model_code = $unit->model_code ?  $unit->model_code : "0";
						$fabric_name = $unit->fabric_name ?  $unit->fabric_name : "0";
						$fabric_name_group = $unit->fabric_name_group ?  $unit->fabric_name_group : "0";
						$completing_code = $unit->completing_code ?  $unit->completing_code : "0";

						$orderRaw = 'M|0|0|0|' .
							$model_code .
							'|' . $fabric_name .
							'|' . $fabric_name_group .
							'|' . $completing_code .
							'|' . $val['count'];
					}

					date_default_timezone_set('Europe/Moscow');

					$orderTxt = fopen ('files/orders/ZAKAZ_000000000000_' .date('dmY') . '_PM_' . $last_order_number . '.txt','a');
					fwrite ($orderTxt, $orderRaw . "\r\n");
					fclose ($orderTxt);

					$this->order_file = 'files/orders/ZAKAZ_000000000000_' .date('dmY') . '_PM_' . $last_order_number . '.txt';

				}
			}
		}
	}
	
	public function printAction() {
		$session_complete = new Zend_Session_Namespace('zakaz_complete');
		if ($session_complete && $session_complete->ok) {
			$this->view->zakaz = $session_complete->zakaz;
			//Zend_Debug::dump($session_complete->zakaz);exit();
			$this->view->zakaz_row = Catalog_Zakaz::getInstance()->find($session_complete->zakaz)->current();
			$this->view->ok = 1;
		} else {
			$this->_redirector = $this->_helper->getHelper('Redirector');
			$this->_redirector->setCode(302)->setGotoUrl('http://' . $_SERVER['HTTP_HOST'] . '/cart');
			$this->_redirector->redirectAndExit();
		}
	}
	
	public function savetxtAction() {
		$session_complete = new Zend_Session_Namespace('zakaz_complete');
		if ($session_complete && $session_complete->ok) {
			$i = 0;
			$total = 0;
			$zakaz = $session_complete->zakaz;
			$zakaz_row = Catalog_Zakaz::getInstance()->find($session_complete->zakaz)->current();
			if ($zakaz_row) {
				$text = "Номер Вашего заказа " . $zakaz . "e\r\n";
				//$text.= strip_tags($zakaz_row->content);
				$text .= $zakaz_row->contenttxt;
				
				$text = str_replace('&nbsp;', '', $text);
				//$text = htmlspecialchars_decode($text);
				$text = iconv("utf-8", "windows-1251", $text);
				header('Content-type: text/plain');
				header("Content-Disposition: attachment; filename=order" . $zakaz . ".txt");
				echo $text;
				exit();
			}
		}
		$this->_redirector = $this->_helper->getHelper('Redirector');
		$this->_redirector->setCode(302)->setGotoUrl('http://' . $_SERVER['HTTP_HOST'] . '/cart');
		$this->_redirector->redirectAndExit();
	}
	
	private function getPrice() {
		$total = 0;
		if ($this->_cart->products) {
			foreach ($this->_cart->products as $key => $products) {
				$total_price = $this->_cart->products[$key]['total_price'];
				$total += $total_price;
			}
		}
		return $total;
	}
	
	/**
	 * формирование заказа для отправки по почте
	 */
	private function getOrder($path) {
		$html = '';
		if ($this->_cart->products) {
			$link = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $path . '';
			$html = '<p style="text-align: center;">&nbsp;<strong>Заказ с сайта</strong></p>
                <table width="100%" border="1" cellspacing="0" cellpadding="0">
                    <tr><td>№</td><td>Наименование</td><td>Кол-во</td><td>Цена</td><td>Итого</td></tr>';
			$i = 0;
			$total = 0;
			foreach ($this->_cart->products as $key => $products) {
				$i ++;
				$id = $this->_cart->products[$key]['product_url'];
				$division = $this->_cart->products[$key]['division_url'];
				$title = $this->_cart->products[$key]['title'];
				$price = $this->_cart->products[$key]['price'];
				$count = $this->_cart->products[$key]['count'];
				$total_price = $this->_cart->products[$key]['total_price'];
				$order = '';
				if (isset($this->_order->products[$key]['order_id'])) {
					$order = "order/" . $this->_order->products[$key]['order_id'];
				}
				$html .= "<tr><td>$i.</td><td><a href=\"$link/$division/$id/$order\">$title</a></td><td>$count</td><td>$price</td><td>$total_price</td></tr>";
				$total += $total_price;
			}
			$html .= "</table>Итого: <strong>$total</strong> руб.<p>&nbsp;</p>";
		}
		return $html;
	}

    private function getOrderTransaction($zakaz) {
        if ($this->_cart->products) {
            $this->transaction = new Zend_Session_Namespace('transaction');
            if (! isset($this->transaction->products)) {
                $this->transaction->products = array();
            }
            foreach ($this->_cart->products as $key => $products) {
                $id_product = $this->_cart->products[$key]['id_product'];
                $id_unit = $this->_cart->products[$key]['id_unit'];
                $title = $this->_cart->products[$key]['title'];
                $price = $this->_cart->products[$key]['price'];
                $count = $this->_cart->products[$key]['count'];
                $total_price = $this->_cart->products[$key]['total_price'];
                $this->transaction->products[$id_product] = array(
                    'title' => $title,
                    'id' => $id_unit,
                    'price' => $price,
                    'count' => $count,
                    'total_price' => $total_price,
                    'order' => $zakaz
                );
            }
        }
    }
	
	/**
	 * формирование текстового формата заказа
	 */
	private function getOrderTxt($path) {
		$txt = '';
		if ($this->_cart->products) {
			$link = 'http://' . $_SERVER['HTTP_HOST'];
			$txt = "Заказ с сайта $link \r\n\r\n";
			$i = 0;
			$total = 0;
			foreach ($this->_cart->products as $key => $products) {
				$i ++;
				$id = $this->_cart->products[$key]['product_url'];
				$division = $this->_cart->products[$key]['division_url'];
				$title = $this->_cart->products[$key]['title'];
				$price = $this->_cart->products[$key]['price'];
				$count = $this->_cart->products[$key]['count'];
				$total_price = $this->_cart->products[$key]['total_price'];
				$order = '';
				if (isset($this->_order->products[$key]['order_id'])) {
					$order = "order/" . $this->_order->products[$key]['order_id'];
				}
				$txt .= "№ $i. ";
				$txt .= "Наименование: $title\r\n";
				$txt .= "Кол-во: $count\r\n";
				$txt .= "Цена (Итого): $price ($total_price)\r\n";
				$txt .= "\r\n";
				$total += $total_price;
			}
			$txt .= "\r\nИтого: $total руб.";
		}
		return $txt;
	}
	
	/**
	 * добавление товара в корзину
	 * @todo другой title для товаров с id_price
	 */
	public function addAction() {
		
		$key = $this->_id_unit;
        $transform = $this->_id_transform;
		$id_price = 0;
		
		if ($this->_id_unit && ! array_key_exists($key, $this->_cart->products)) {
			$unit = Catalog_Product_Units::getInstance()->getById($this->_id_unit);
			$price = null;
			$title = null;
			if (isset($this->_order->products[$key]['price']))
				$price = $this->_order->products[$key]['price'];
			if ($unit != null) {
                if ($transform == 0){
                    $price_u = $unit->price;
                }
                else {
                    $price_u = $unit->price_with_tr;
                }
				$product = Catalog_Product::getInstance()->getPublicItem($unit->id_product);
				$this->_cart->products[$key] = array(
					'title' => $title != null ? $unit->title . " " . $title : $unit->title, 
					'count' => 1,
                    'price' => $price != null ? $price : $price_u,
					'total_price' => $price != null ? $price : $price_u,
					'id_unit' => $this->_id_unit,
					'id_product' => $unit->id_product,
					'division' => $this->_division,
					'img' => $unit->img,
					'product_url' => $product->url, 
					'division_url' => $this->_url_cat
				);
				echo 'ok';
			}
		} elseif ($this->_id_unit && array_key_exists($key, $this->_cart->products)) {
			$count = $this->_cart->products[$key]['count'];
			$price = $this->_cart->products[$key]['price'];
			$count ++;
			$this->_cart->products[$key]['count'] = $count;
			$this->_cart->products[$key]['total_price'] = $count * $price;
			echo 'ok';
		} else {
			echo 'err';
		}
		exit();
	}
	
	public function saveconfigAction() {
		$config = $this->_getParam('conf', '');
		$price = $this->_getParam('price', '');
		$key = $this->_id_unit;
		if ($config) {
			$save_string = '<?xml version="1.0" encoding="utf-8" ?>' . "\n";
			$save_string .= $config;
			//preg_match('/<item[^>].*mc=\"priceMc\"[^>].*price=\"([\d]+).*>/', $config, $matches);
			//$price = $matches[1];
			$array['id_product'] = $this->_id_unit;
			;
			$array['added'] = date('Y-m-d');
			$array['config'] = $save_string;
			$array['price'] = (int)$price;
			$order_id = Catalog_Order::getInstance()->createRow($array)->save();
			if ($order_id) {
				$this->_order->products[$key] = array('order_id' => $order_id, 'price' => (int)$price);
				echo 'ok';
			} else {
				echo 'err';
			}
		}
		exit();
	}
	
	/**
	 * добавление товара из конструктор в корзину
	 */
	public function addcustomAction() {
		$data = $this->_getAllParams();
		if (isset($data['dish']) && array_key_exists($data['dish'], $this->_cart->products)) {
			unset($this->_cart->products[$data['dish']]);
		}
		$order = array();
		$order['type_id'] = $data['type'];
		$order['size'] = $data['sizes'];
		if (isset($data['item']) && count($data['item'])) {
			$order['items'] = $data['item'];
		}
		if (isset($data['item_radio']) && count($data['item_radio'])) {
			$order['items'] = array_merge_recursive($data['item_radio'], $order['items']);
		}
		$order['count'] = (int)$data['count'];
		$custom_id = $order['type_id'] . $order['size'];
		foreach ($order['items'] as $item) {
			$custom_id .= $item;
		}
		$price = 0;
		foreach ($order['items'] as $item) {
			$price += (int)Constructor_Prices::getInstance()->getPrice($order['type_id'], $item, $order['size']);
		}
		$price = (int)$price + (int)Constructor_Sizes::getInstance()->getSizePrice($order['size']);
		$key = "constr_" . $custom_id;
		$id_price = 0;
		if ($id_price = $this->_getParam('id_price')) {
			$key .= '_' . $id_price;
		}
		if (! array_key_exists($key, $this->_cart->products)) {
			$title = Constructor_Sizes::getInstance()->getNameById($order['size']) . ' ' . Constructor_Types::getInstance()->getNameById($order['type_id']);
			if (count($order) != null) {
				
				$this->_cart->products[$key] = array('title' => $title != null ? $title : 'Свое блюдо', 'count' => $order['count'], 'price' => $price, 'total_price' => $price * $order['count'], 'id_product' => $key, 'id_price' => $price, 'order' => $order);
				echo 'ok';
			}
		} elseif (array_key_exists($key, $this->_cart->products)) {
			$count = $this->_cart->products[$key]['count'];
			$price = $this->_cart->products[$key]['price'];
			$count ++;
			$this->_cart->products[$key]['count'] = $count;
			$this->_cart->products[$key]['total_price'] = $count * $price;
			echo 'ok';
		} else {
			echo 'err';
		}
		$this->_redirect('cart');
	}
	
	/**
	 * удаление товара из корзины
	 */
	public function removeAction() {
		if (array_key_exists($this->_id_unit, $this->_cart->products)) {
			unset($this->_cart->products[$this->_id_unit]);
			echo count($this->_cart->products);
		}
		exit();
	}
	
	/**
	 * изменение количества товаров в корзине
	 */
	public function recalcAction() {
		echo (int)count($this->_cart->products);
		exit();
	}
	
	/**
	 * очитска корзины
	 */
	public function clearAction() {
		$this->_cart->products = null;
		$this->_redirect('/' . $this->_page->path);
	}
	
	public function changeAction() {
		if ($this->_id_unit && $count = $this->_getParam('count')) {
			if (array_key_exists($this->_id_unit, $this->_cart->products)) {
				$price = $this->_cart->products[$this->_id_unit]['price'];
				$this->_cart->products[$this->_id_unit]['count'] = $count;
				$this->_cart->products[$this->_id_unit]['total_price'] = $count * $price;
				echo 'ok';
			}
		}
		exit();
	}
	
	/**
	 *
	 * @param Orders $order
	 * @param object $user
	 * @return Zend_Db_Table_Row
	 */
	private function setUserData($order, $user) {
		$order->id_user = $user->id;
		$order->discount = $user->discount;
		$order->user_name = $order->user_name == '' ? $user->first_name : $order->user_name;
		$order->user_street = $order->user_street == '' ? $user->street : $order->user_street;
		$order->user_house = $order->user_house == '' ? $user->house : $order->user_house;
		$order->user_house_block = $order->user_house_block == '' ? $user->house_block : $order->user_house_block;
		$order->user_flat = $order->user_flat == '' ? $user->flat : $order->user_flat;
		$order->user_phone = $order->user_phone == '' ? $user->mobile_phone : $order->user_phone;
		$order->user_email = $order->user_email == '' ? $user->email : $order->user_email;
		return $order;
	
	}
	
	/**
	 * устанавливает цены из базы для товаров в корзине
	 */
	private function setTruePrices() {
		$order_price = 0;
		if ($this->_cart->products) {
			foreach ($this->_cart->products as $key => $item) {
				$id_product = $item['id_product'];
				//$id_price = $item['id_price'];
				$price = 0;
				if ($id_price && $id_product) {
					$price = Catalog_Product_Options_Prices::getInstance()->getPriceById($id_price, $id_product);
				
				} elseif ($id_product) {
					$price = Catalog_Product::getInstance()->getPrice($id_product);
				}
				if ($price) {
					$this->_cart->products[$key]['price'] = $price;
					$this->_cart->products[$key]['total_price'] = $price * $item['count'];
					$order_price += $price * $item['count'];
				} elseif (substr_count($id_product, "constr_")) {
					$order_price = $this->_cart->products[$key]['total_price'];
				} else {
					unset($this->_cart->products[$key]);
				}
			}
		}
		return $order_price;
	}


    public function fastbuyAction() {
        $this->view->catalog_path = $path = SiteDivisionsType::getInstance()->getPagePathBySystemName('division');
        if ($this->_request->isPost()) {
            $data = $this->_getParam('form');
            //Zend_Debug::dump($data);exit();
            if ($data['user_name'] == '' || $data['user_phone'] == '') {
                $this->view->err = 1;
                $this->view->order = $data;
            }
			else if ((($data['captch_res'])/2) != $data['captcha']) {
                $this->view->err = 2;
                $this->view->order = $data;
            }
			else {
                $users = Users::getInstance()->getUsersToSendMail();
                $emails = array();
                foreach ($users as $user) {
                    $emails[] = $user['email'];
                }
                $recipient = 'New message';
                $template = FeedbackTemplates::getInstance()->getBySystemName('order');
                $subject = 'Zakaz 1 click';
                $template = $template['content'];

                foreach ($data as $key => $param) {
                    $template = str_replace('{' . $key . '}', $param, $template);
                }

				$order = $this->getOrderFast($data['id_unit'], $data['count_products'], $data['transformer']);
				$this->createOrderFile($data['id_unit'], $data['count_products']);
				$ordertxt = $order;
                $template = preg_replace('/{.+}/Usi', '', $template);
                $body = $order . $template; //echo $order; exit;
                $ordertxt .= "\r\n" . strip_tags($template);
                Loader::loadCommon('Mail');
                $from = 'info@pinskdrev.msk.ru';
                $price = $this->getPriceFast($data['id_unit'], $data['count_products'], $data['transformer']);
                $unit = Catalog_Product_Units::getInstance()->getById($data['id_unit']);
                //Zend_Debug::dump($unit);exit();
                $serializearray = array();
                $serializearray['name'] =$unit->title;
                $serializearray['count'] = $data['count_products'];
                $serializearray['id'] = $data['id_product'];
                $serializearray['price'] = $price;
                $zakaz = Catalog_Zakaz::getInstance()->insert(array('content' => $body, 'price' => $price, 'contenttxt' => $ordertxt, 'id_product' => $data['id_product'], 'serialize_arr' => serialize($serializearray)));
                $this->getOrderTransaction($zakaz);

				$emails[] = 'box.pinskdrev@gmail.com';	            
	            $emails[] = 'pinskdrev.moscow@yandex.ru';
				$emails[] = 'box.pinskdrev@outlook.com';

				//get 3 blocks for the letter
                $page = Pages::getInstance()->getPage(1256);

                Mail::send($emails, $body, $from, $subject.' N '.$zakaz.' '.$_SERVER["HTTP_HOST"], $recipient, $this->order_file);

                //отправляем уведомление
                $this->view->zakaz = $zakaz;
                $session_complete = new Zend_Session_Namespace('fast_zakaz_complete');
                $session_complete->zakaz = $zakaz;
                $session_complete->ok = 1;
                $template = FeedbackTemplates::getInstance()->getBySystemName('confirm');
                $template = $template['content'];
                $subject = 'Уведомление о заказе';
                $params = '<p>Номер Вашего заказа <b>' . $zakaz . 'e</b></p>';
                
                $body = $template . $params . $order.$page->content;
                $from = 'info@pinskdrev.msk.ru';
				if ($data['user_email']) {
						$email[] = trim($data['user_email']);
						Mail::send($email, $body, $from, $subject . ' N ' . $zakaz . 'e', $recipient,$this->order_file);
				}
                $this->view->ok = 1;
            }
        }
        exit();
    }


    private function getOrderFast($id_unit, $count, $transformer) {
		$unit = Catalog_Product_Units::getInstance()->getById($id_unit);
		$html = '';
        $html = '<p style="text-align: center;">&nbsp;<strong>Заказ с сайта</strong></p>
                <table width="100%" border="1" cellspacing="0" cellpadding="0">
                    <tr><td>№</td><td>Наименование</td><td>Кол-во</td><td>Цена</td><td>Итого</td></tr>';

        $link_product = Catalog_Product::getInstance()->getUrlById($unit->id_product);
        $link = 'http://' . $_SERVER['HTTP_HOST'] . '/product/' . $link_product;
        $html .= "<tr>
                <td>1</td>
                <td><a href=\"$link\">$unit->title</a></td>
                <td>$count</td>";
        if (isset($transformer) && $transformer != 0) {
            $price = $count * $unit->price_with_tr;
	    $single_price = $unit->price_with_tr;
        }
        else {
            $price = $count * $unit->price;
	    $single_price = $unit->price;
        }
        $html .= "<td>$single_price</td>
                <td>$price</td>
                </tr>";
        $html .= "</table>Итого: <strong>$price</strong> руб.<p>&nbsp;</p>";
        return $html;
    }

    private function getPriceFast($id_unit, $count, $transformer) {
        $unit = Catalog_Product_Units::getInstance()->getById($id_unit);
        if (isset($transformer) && $transformer != 0) {
            $price = $count * $unit->price_with_tr;
        }
        else {
            $price = $count * $unit->price;
        }
        return $price;
    }
}
