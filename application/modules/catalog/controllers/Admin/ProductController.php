<?php
/**
 * Catalog_Admin_ProductController
 *
 */
class Catalog_Admin_ProductController extends MainAdminController {
    /**
     * Controller name
     *
     * @var string
     */
    private  $_current_module =null;
    /**
     * division id
     *
     * @var int
     */
    private  $_id_division = null;
    /**
     * @var $_division Catalog_Division_Row
     */
    private $_division = null;
    /*
	 * текущаяя страница
    */
    private $_current_page = null;
    private $_onpage = 50;
    private $_offset = null;

    public function init() {
        $this->initView();
        $this->layout = $this->view->layout();
        $this->layout->title = "Каталог";
        $this->layout->action_title = 'Список товаров';
        $this->view->id_division = $this->_id_division = $this->_getParam('id_division');

        if ($this->_id_division) {
            $this->_division = Catalog_Division::getInstance()->find($this->_id_division)->current();
        }
        $this->view->lang = $lang = $this->_getParam('lang','ru');
        $this->view->current_page = $this->_current_page = $this->_getParam('page', 1);
        $this->view->onpage = $this->_onpage;
        $this->_offset = ceil(($this->_current_page-1)*$this->_onpage);
        $this->view->currentModule = $this->_current_module = SP.$this->getRequest()->getModuleName().SP.$lang.SP.$this->getRequest()->getControllerName();
    }

    /**
     * список товаров раздела
     */
    public function indexAction() {
        $search = new Zend_Session_Namespace('product_search');
        $search_params = $this->_getParam('search');
        $this->view->select_defaults = array();
        if ($search_params){
            $search->params = $search_params;
        } elseif (isset($search->params)){
            $search_params = $search->params;
        }
        $defaults = $this->_getParam('defaults');
        if ($defaults){
            $search->defaults = $defaults;
            $this->view->select_defaults = $defaults;
        } elseif (isset($search->defaults)){
            $defaults = $search->defaults;
            $this->view->select_defaults = $defaults;
        }
        if ($this->_getParam('clear')){
            $search->params = null;
            $search_params = null;
            $search->defaults = null;
            $defaults = null;
            unset($search->defaults );
            $this->view->select_defaults = array();

            unset($search->params);
        }

        if ($this->_getParam('recount')){
            $products = Catalog_Product::getInstance()->getAllProducts();
            foreach ($products as $product) {
                $product_id = $product['id'];
                //Zend_Debug::dump($product_id);exit();
                $units = Catalog_Product_Units::getInstance()->getActiveByProductId($product_id);
                foreach ($units as $unit) {
                    $complects = Catalog_Product_Complect::getInstance()->getComplect($unit['id']);

                    if (count($complects) > 0) {
                        $price = 0;
                        foreach ($complects as $item) {
                            if ($item->id_complect_unit) {
                                if ($item->main_unit == 1) {
                                    $price += $item->price * $item->count;
                                }
                                else {
                                    if ($item->with_transform == 1) {
                                        $price += $item->price_with_tr * $item->count;
                                    }
                                    else {
                                        $price += $item->price * $item->count;
                                    }
                                }
                            } else {
                                $price += $item->fixed_price * $item->count;
                            }
                        }
                        if ($unit->price != $price) {
                            $unit->price = $price;
                            Catalog_Product_Units::getInstance()->update(array('price' => $price), 'id = ' . $unit['id']);
                        }
                        //Zend_Debug::dump($price);
                    }

                    if (count($complects) > 0) {
                        $price_with_tr = 0;
                        foreach ($complects as $item) {
                            if ($item->id_complect_unit) {
                                if ($item->main_unit == 1) {
                                    $price_with_tr += $item->price_with_tr * $item->count;
                                }
                                else {
                                    if ($item->with_transform == 1) {
                                        $price_with_tr += $item->price_with_tr * $item->count;
                                    }
                                    else {
                                        $price_with_tr += $item->price * $item->count;
                                    }
                                }
                            } else {
                                $price_with_tr += $item->fixed_price * $item->count;
                            }
                        }
                        if ($unit->price_with_tr != $price_with_tr) {
                            $unit->price_with_tr = $price_with_tr;
                            Catalog_Product_Units::getInstance()->update(array('price_with_tr' => $price_with_tr), 'id = ' . $unit['id']);
                        }
                    }
                }
            }
        }

        if (!$search_params['id_division'] || $this->_id_division){
            $search_params['id_division'] = $this->_id_division;
        }
        //print_r($search_params['id_division']);exit;
        if ($this->_request->isPost()){
            $array_Priority = $this->_getParam('priority');
            $array_Prices = $this->_getParam('prices');
            $array_Options = $this->_getParam('options');
            if ($array_Priority){
                Catalog_Product::getInstance()->setPriority($array_Priority);
                Catalog_Product_Units::getInstance()->setPrices($array_Prices);
                Catalog_Product::getInstance()->setDefaultOptions($array_Options);
            }

            $action = $this->_getParam('operation');
            $ids = $this->_getParam('products');
            $div_move_id = $this->_getParam('copying');
            if ($action && $ids){
                Catalog_Product::getInstance()->processProducts($action, $ids, $div_move_id);
                if ($action == 'move' || $action == 'virtual') {
                    Catalog_Division_Cache::clean(Zend_Cache::CLEANING_MODE_ALL);
                }
            }
            $this->view->ok = 1;
        }

        $products = Catalog_Product::getInstance()->search($search_params, $defaults, $this->_onpage, $this->_offset);
        $this->view->total = Catalog_Product::getInstance()->search($search_params, $defaults)->count();
        $this->view->items = $products;
        if ($search_params){
            foreach ($search_params as $key=> $value){
                $this->view->assign($key, $value);
            }
        }
        $divisions = Catalog_Division::getInstance()->getDivisionsWithParents();
        $this->view->divisions = $divisions;
        $options = array(
            'disable'=>' Выключить',
            'enable'=>' Включить',
            'delete'=>' Удалить',
            'virtual'=>' Виртуальный в:',
            'move'=>' Переместить в:',
        );
        $this->view->options = $options;
        $this->view->copying = $divisions;
        $this->view->defaults = Catalog_Product_Default::getInstance()->fetchAll("`active` = '1' AND `form_type` = 'checkbox'");

        $quick_options_names = array("akciya","is_new","popular","rassrochka","show_on_main" ,"main_div_product", "special");
        $quick_options = Catalog_Product_Default::getInstance()->getBySystemNames($quick_options_names);
        $this->view->quick_options = $quick_options;
    }

    public function divisionAction(){
        if ($this->_division!=null) {
            $this->layout->action_title = "Список товаров в разделе ". $this->_division->name;
        }

        if ($this->_request->isPost()){
            $array_Priority = $this->_getParam('priority');
            Catalog_Product2Divisions::getInstance()->setPriority($array_Priority);

            $action = $this->_getParam('operation');
            $ids = $this->_getParam('products');
            if ($action && $ids){
                Catalog_Product::getInstance()->processProducts($action, $ids);
            }
        }

        $this->view->items = Catalog_Product::getInstance()->getProductsByDivId($this->_id_division, $this->_onpage, $this->_offset);
        $this->view->total = Catalog_Product::getInstance()->getProductsByDivId($this->_id_division)->count();
        $options = array(
            'disable'=>' Disable',
            'enable'=>' Enable',
            'delete'=>' Delete',
        );
        $this->view->options = $options;
    }

    /**
     * изменение активности
     */
    public function activeAction() {
        $id = $this->_getParam('id');
        Catalog_Product::getInstance()->changeActivity($id);
        $this->redirectToIndex();
    }


    public function editAction() {
        $id =  $this->getRequest()->getParam('id');
        if ($id) {
            $item = Catalog_Product::getInstance()->find($id)->current();
            //$params = Catalog_Product_Options_Enabled::getInstance()->getAllOptionsValues($id);
            //$this->view->params = $params;
            if ($this->_division!=null) {
                $this->layout->action_title = "Редактировать товар в разделе \"". $this->_division->name.'"';
            }
            $this->view->creating = false;
        }  else {
            $item = Catalog_Product::getInstance()->createRow();
            if ($this->_division!=null) {
                $this->layout->action_title = "Создать товар в разделе \"". $this->_division->name."\"";
            }
            $this->view->creating = true;
        }
        /*$this->view->division = $division = Catalog_Division::getInstance()
                ->find($item->id_division)
                ->current();*/
        if ($this->_request->isPost()) {
            $ok = 1;
            $data = $this->_getAllParams();
            $data['url'] = preg_replace( '/[^\w_-]/', '', $data['url'] );
            if ($data['title']!='' && $data['url']!='' && $data['primary_id']!=0) {
                if (Catalog_Product::getInstance()->checkUrl( $data['url'], $id )) {
                    $item->setFromArray(array_intersect_key($data,$item->toArray()));
                    $item->material_ids = '';
                    if(isset($data['materials'])) $item->material_ids = implode(',',$data['materials']);
                    $item->fabrics_ids = '';
                    if(isset($data['fabrics'])) $item->fabrics_ids = implode(',',$data['fabrics']);
                    $item->bitmaps_ids = '';
                    if(isset($data['bitmaps'])) $item->bitmaps_ids = implode(',',$data['bitmaps']);
                    $item->decor_ids = '';
                    if(isset($data['decors'])) $item->decor_ids = implode(',',$data['decors']);
                    $item->transformer_ids = '';
                    if(isset($data['transformers'])) $item->transformer_ids = implode(',',$data['transformers']);
                    $item->color_ids = '';
                    if(isset($data['colors'])) $item->color_ids = implode(',',$data['colors']);
                    $item->action_types = '';
                    if(isset($data['action_types'])) $item->action_types = implode(',',$data['action_types']);
                    //$id = $item->save();
                    //if(isset($data['colors'])) $item->color_ids = implode(',',$data['colors']);
                    //if(isset($data['made'])) $item->made_ids = implode(',',$data['made']);
                    $id = $item->save();

                    Catalog_Product_Default_Values::getInstance()->updateValues($id, $this->_getParam('option', array()) );
                }else{
                    $this->view->message="Товар с таким url уже существует";
                    $ok = 0;
                }
            } else {
                $this->view->message="Ошибка! Заполните поля помеченные звездочкой";
                $ok = 0;
            }
            if (!$ok) $item->setFromArray( array_intersect_key( $data, $item->toArray() ) );

            //Catalog_Division::getInstance()->updateCountProducts($this->_id_division);
            $this->view->ok = $ok;
            if ($ok && $primary_id = $this->_getParam('primary_id')){
                Catalog_Product2Divisions::getInstance()->addDivision($id, $primary_id, $is_primary = 1);

            } elseif ($ok){
                Catalog_Product2Divisions::getInstance()->deleteMain($id);
            }
            if ($ok && $other_div = $this->_getParam('other_div')){
                Catalog_Product2Divisions::getInstance()->addDivision($id, $other_div);
            }
        }
        if (!isset($img_row) && $id) {
            $img_row = Catalog_Product_Units::getInstance()->getMainByProduct($id);
        }
        if (isset($img_row) && $img_row!=null) {
            $this->view->img = $img_row->img;
        }
        if ($this->_hasParam('del_div')){
            Catalog_Product2Divisions::getInstance()->deleteLink((int)$this->_getParam('del_div'));
            $this->_redirect($this->view->currentModule .SP. "edit" .SP. "id" .SP. $id .SP. "id_division" .SP. $this->_id_division);
        }
        $this->view->divisions = Catalog_Division::getInstance()->getDivisionsWithParents();
        $primary_div = Catalog_Product2Divisions::getInstance()->getPrimaryDiv( ( int ) $id );
        if ($primary_div != null) {
            $this->view->primary_id = $primary_div->id_division;
        } elseif (!$id) {
            $this->view->primary_id = $this->_id_division;
        }
        $other_divs = Catalog_Product2Divisions::getInstance()->getOtherDivs($id);
        if ($other_divs->count()){
            $this->view->other_divs = $other_divs;
        }
        $fck1 = $this->getFck('intro', '90%', '150','Basic');
        $this->view->fck_intro = $fck1;
        $fck2 = $this->getFck('description', '90%', '300');
        $this->view->fck_content = $fck2;
        $this->view->item = $item;
        $default_options = Catalog_Product_Default::getInstance()->getDefaultToProduct($id);
        $this->view->mebeltype_options = Catalog_Product_Mebeltype::getInstance()->getItemsAsOptions();
        $this->view->makers_options = Makers::getInstance()->getItemsAsOptions();
        $this->view->collections_options = Collections::getInstance()->getItemsAsOptions();
        $this->view->def_options = $default_options;
        $this->view->color = Catalog_Product_Color::getInstance()->getAllColors();
        $this->view->material_options = Catalog_Product_Material::getInstance()->getItemsAsOptions();
        $this->view->transformer_options = Catalog_Product_Transformer::getInstance()->getItemsAsOptions();
        $this->view->availability_options = Catalog_Product_Availability::getInstance()->getItemsAsOptions();
        $this->view->maketerm_options = Catalog_Product_Maketerm::getInstance()->getItemsAsOptions();
        $this->view->decor_options = Catalog_Product_Decor::getInstance()->getItemsAsOptions();
        $this->view->bitmaps_options = Catalog_Bitmaps_Groups::getInstance()->getItemsAsOptions();
        $this->view->fabrics_options = Catalog_Fabrics_Groups::getInstance()->getItemsAsOptions();
        $this->view->color_options = Catalog_Product_Color::getInstance()->getItemsAsOptions();
        $this->view->action_types = Catalog_Product_Actions::getInstance()->getAllActions();

    }

    /**
     * удаление товара
     */
    public function deleteAction() {

        //     Удаление флэшки
        $product_id = $this->_getParam('id_flash');
        if($product_id) {

            $product = Catalog_Product::getInstance()->find($product_id)->current();

            if ($product==null) {
                $this->redirectToIndex();
            }

            unlink(DIR_PUBLIC.'files/catalog/tovar/swf/' . $product->swf_file);
            unlink(DIR_PUBLIC.'files/catalog/tovar/data/' . $product->swf_data_file);

            $data['swf_file'] = '';
            $data['swf_data_file'] = '';
            //$data['default_swf_config'] = '';
            $product->setFromArray($data);
            $product->save();

            //$this->_redirect('/admin/' . $this->view->lang . '/catalog_tovar/flash/id/' . $tovar_id . '/id_page/' . $this->_id_page);
            $this->_redirect($this->_current_module.'/flash/id/'.$product_id);
            return true;
        }


        $id = $this->_getParam('id');
        if ($id) {
            $item = Catalog_Product::getInstance()->find($id)->current();
            if ($item!=null) {
                Catalog_Product_Default_Values::getInstance()->deleteValues($id);
                $item->delete();
            }
        }
        Catalog_Division::getInstance()->updateCountProducts($this->_id_division);
        $this->redirectToIndex();
    }

    /**
     * @todo нуждается в рефакторинге
     *
     */
    public function copyAction() {

        $id = $this->_getParam('id', 0);
        $item = Catalog_Product::getInstance()->find($id)->current();
        if($item) {

            $item = $item->toArray();

            unset($item['id']);

            $item['title'].='_copy';
            $item['is_new']=0;
            $item['popular']=0;
            $item['url'].='_copy';
            $item['swf_file']='';
            $item['swf_data_file']='';
            $item['active']=0;
            $new_row = Catalog_Product::getInstance()->createRow()->setFromArray($item);
            $new_id = $new_row->save();
            Catalog_Product_Default_Values::getInstance()->addOptionsForNewProduct($new_id);
            Catalog_Product_Complect::getInstance()->copyComplect($id, $new_id);
            $division = Catalog_Product2Divisions::getInstance()->fetchRow("`id_product` = '$id'",'is_primary DESC');
            if($division){
                $data = $division->toArray();
                unset($data['id']);
                $data['is_primary'] = 1;
                $data['id_product'] = $new_id;
                Catalog_Product2Divisions::getInstance()->insert($data);
            }
        }
        $this->redirectToIndex();
    }

    public function categoriesAction(){
        $products = Catalog_Product::getInstance()->fetchAll();
        foreach ($products as $product){
            Catalog_Product2Divisions::getInstance()->addDivision($product->id, $product->id_division, 1);
        }
        exit;
    }


    /**
     * редирект на список товаров
     */
    private function redirectToIndex() {
        $this->_redirect($this->_current_module.'/index/id_division/'.$this->_id_division);
    }

    public function relatedAction() {
        $this->layout->action_title = "Выбрать схожие товары";
        $this->view->id = $id = $this->_getParam('id',0);
        $this->view->id_page = $id_page = $this->_getParam('id_page',0);
        $this->view->tovar = $tovar = Catalog_Product::getInstance()->find($id)->current();
        //$this->view->division = Catalog_Division::getInstance()->find($tovar->id_division )->current();
        if ($this->_request->isPost()) {
            $rel_id = $this->_getParam('rel_id');
            $related_tovar = Catalog_Product::getInstance()->find($rel_id)->current();
            $rel_row = Catalog_Product_Related::getInstance()->fetchRow('id_product='.(int)$id.' AND id_related_product='.(int)$rel_id);
            if ($rel_id && !is_null($related_tovar) && is_null($rel_row)) {
                $related_row = Catalog_Product_Related::getInstance()->createRow();
                $related_row->id_product = $id;
                $related_row->id_related_product = $rel_id;

                $related_row->save();


                $check_rel = Catalog_Product_Related::getInstance()->fetchRow('id_product = '.$rel_id.' AND id_related_product = '.$id);
                if($check_rel==null){
                    $related_row = Catalog_Product_Related::getInstance()->createRow();
                    $related_row->id_product = $rel_id;
                    $related_row->id_related_product = $id;
                    $related_row->save();
                }

            }
            $this->view->ok = 'Данные сохранены';

        }
        $related = Catalog_Product_Related::getInstance()->fetchAll('id_product='.(int)$id);

        $rel_ids = array();
        $res = array();
        if (count($related)) {
            foreach ($related as $item) {
                $rel_ids[] = $item->id_related_product;
            }
            $rel_string = implode(',', $rel_ids);
            $where[] = "id IN ($rel_string)";
            $res = Catalog_Product:: getInstance()->fetchAll($where);

        }

        $this->view->related = $res;
    }

    public function collectionAction() {
        $this->layout->action_title = "Выбрать товары из коллекции";
        $this->view->id = $id = $this->_getParam('id',0);
        $this->view->id_page = $id_page = $this->_getParam('id_page',0);
        $this->view->tovar = $tovar = Catalog_Product::getInstance()->find($id)->current();
        //$this->view->division = Catalog_Division::getInstance()->find($tovar->id_division )->current();
        if ($this->_request->isPost()) {
            $check = $this->_getParam('check');
            if ($check == 2) {
                $text = $this->_getParam('search_all_text');
                $collection_ids = Catalog_Product::getInstance()->collectionsearch($text);
                foreach($collection_ids as $coll) {
                    $id_coll = $coll->id;
                    if($id_coll != $id) {
                        $rel_row = Catalog_Product_Collections::getInstance()->fetchRow('id_product='.$id.' AND id_collection_product='.$id_coll);
                        if(is_null($rel_row)) {
                            $related_row = Catalog_Product_Collections::getInstance()->createRow();
                            $related_row->id_product = $id;
                            $related_row->id_collection_product = $id_coll;
                            $related_row->save();
                        }
                    }
                }
                //exit();
                //Zend_Debug::dump($collection_ids);exit();
            } else {
                $rel_id = $this->_getParam('rel_id');
                $related_tovar = Catalog_Product::getInstance()->find($rel_id)->current();

                $rel_row = Catalog_Product_Collections::getInstance()->fetchRow('id_product='.(int)$id.' AND id_collection_product='.(int)$rel_id);
                if ($rel_id && !is_null($related_tovar) && is_null($rel_row)) {

                    $related_row = Catalog_Product_Collections::getInstance()->createRow();

                    $related_row->id_product = $id;
                    $related_row->id_collection_product = $rel_id;

                    $related_row->save();


                    $check_rel = Catalog_Product_Collections::getInstance()->fetchRow('id_product = '.$rel_id.' AND id_collection_product = '.$id);
                    if($check_rel==null){
                        $related_row = Catalog_Product_Collections::getInstance()->createRow();
                        $related_row->id_product = $rel_id;
                        $related_row->id_collection_product = $id;
                        $related_row->save();
                    }

                }
                $this->view->ok = 'Данные сохранены';

            }
        }
        $related = Catalog_Product_Collections::getInstance()->fetchAll('id_product='.(int)$id);

        $rel_ids = array();
        $res = array();
        if (count($related)) {
            foreach ($related as $item) {
                $rel_ids[] = $item->id_collection_product;
            }
            $rel_string = implode(',', $rel_ids);
            $where[] = "id IN ($rel_string)";
            $res = Catalog_Product:: getInstance()->fetchAll($where);

        }

        $this->view->related = $res;
    }

    public function ajaxsearchAction(){
        //$this->layout()->disable;
        $id = $this->_getParam('id', 0);
        $search = $this->_getParam('q');
        //echo $search;
        if ($id && $search) {
            $related = Catalog_Product_Related::getInstance()->fetchAll('id_product=' . (int)$id);
            $rel_ids = array($id);
            if (count($related)) {
                foreach ($related as $item) {
                    $rel_ids[] = $item->id_related_product;
                }
            }
            $rel_string = implode(',', $rel_ids);
            $where[] = "id NOT IN ($rel_string)";
            $where[] = "title LIKE '%$search%'";
            $where[] = "active = 1";
            $res = Catalog_Product::getInstance()->fetchAll($where);
            //print_r($res); exit;
            foreach ($res as $data) {
                print $data->title . "|" . $data->id . "\n";
            }

        }
        exit();
    }

    public function ajaxcolsearchAction(){
        //$this->layout()->disable;
        $id = $this->_getParam('id', 0);
        $search = $this->_getParam('q');
        //echo $search;
        if ($id && $search) {
            $related = Catalog_Product_Collections::getInstance()->fetchAll('id_product=' . (int)$id);
            $rel_ids = array($id);
            if (count($related)) {
                foreach ($related as $item) {
                    $rel_ids[] = $item->id_collection_product;
                }
            }
            $rel_string = implode(',', $rel_ids);
            $where[] = "id NOT IN ($rel_string)";
            $where[] = "title LIKE '%$search%'";
            $where[] = "active = 1";
            $res = Catalog_Product::getInstance()->fetchAll($where);
            //print_r($res); exit;
            foreach ($res as $data) {
                print $data->title . "|" . $data->id . "\n";
            }

        }
        exit();
    }

    public function reldeleteAction() {
        $id = $this->_getParam('id',0);
        $id_page = $this->_getParam('id_page',0);
        $rel_id = $this->_getParam('id_rel', 0);
        $row = Catalog_Product_Related::getInstance()->fetchRow('id_product='.(int)$id.' AND id_related_product='.(int)$rel_id);
        if (!is_null($row)) {
            $row->delete();
        }
        $this->_redirect('/'.$this->_current_module.'/related/id/'.$id);
    }

    public function coldeleteAction() {
        $id = $this->_getParam('id',0);
        $id_page = $this->_getParam('id_page',0);
        $rel_id = $this->_getParam('id_rel', 0);
        $row = Catalog_Product_Collections::getInstance()->fetchRow('id_product='.(int)$id.' AND id_collection_product='.(int)$rel_id);
        if (!is_null($row)) {
            $row->delete();
        }
        $this->_redirect('/'.$this->_current_module.'/collection/id/'.$id);
    }

    public function bitmapsAction() {
        $this->view->id = $id = $this->_getParam('id',0);
        $this->view->id_page = $id_page = $this->_getParam('id_page',0);
        $model_groups2product = Catalog_Bitmaps_Groups2Product::getInstance();
        $model_items2product = Catalog_Bitmaps_Items2Product::getInstance();

        if ($id) {
            $this->view->tovar = $tovar = Catalog_Product::getInstance()->find($id)->current();
            $this->view->division = Catalog_Division::getInstance()->find($tovar->id_division)->current();
            $this->layout->action_title = "Текстуры товара <b>&quot;".$tovar->title."&quot;</b>";
        }

        if ($this->_request->isPost()) {
            //print_r($this->_getAllParams());
            $tovar_groups = $this->_getParam('groups', array());
            $tovar_groups_prices = $this->_getParam('group_prices', array());
            $tovar_groups_prices_mehanizm = $this->_getParam('group_prices_mehanizm', array());//+++++++++
            $tovar_textures = $this->_getParam('textures', array());
            $tovar_related = $this->_getParam('related', array());
            if (count($tovar_groups)) {
                $model_groups2product->delete('id_product='.(int)$id);
                $model_items2product->delete('id_product='.(int)$id);
                foreach ($tovar_groups as $tovar_group_id) {
                    $groups2product_row = $model_groups2product->createRow();
                    $groups2product_row->id_product = (int)$id;
                    $groups2product_row->id_group = (int)$tovar_group_id;
                    $groups2product_row->price = isset($tovar_groups_prices[$tovar_group_id]) ? str_replace(',', '.', $tovar_groups_prices[$tovar_group_id]) : 0;
                    $groups2product_row->price_mehanizm = isset($tovar_groups_prices_mehanizm[$tovar_group_id]) ? str_replace(',', '.', $tovar_groups_prices_mehanizm[$tovar_group_id]) : 0;//+++++++++
                    $groups2product_row->save();
                    unset($groups2product_row);
                    $items = Catalog_Bitmaps_Items::getInstance()->fetchAll('active = 1 AND id_group = '.$tovar_group_id);

                    //if (isset($tovar_textures[$tovar_group_id])) {
                    foreach ($items as $texture_item) {
                        if ($texture_item->id) {

                            $items2product_row = $model_items2product->createRow();
                            $items2product_row->id_product = (int)$id;
                            $items2product_row->id_group = (int)$tovar_group_id;
                            $items2product_row->id_item = (int)$texture_item->id;
                            if(isset($tovar_textures[$tovar_group_id]) && in_array($texture_item->id, $tovar_textures[$tovar_group_id])){
                                $items2product_row->main = 1;
                            }
                            $items2product_row->related = $tovar_related[(int)$tovar_group_id][(int)$texture_item->id];

                            $items2product_row->save();
                            unset($items2product_row);
                        }

                    }
                    //}
                }
            } else {
                $model_groups2product->delete('id_product='.(int)$id);
                $model_items2product->delete('id_product='.(int)$id);
            }

            if (file_exists(DIR_PUBLIC.'files/catalog/tovar/data/'.$id.'_data.xml')) {
                $default_data = file_get_contents(DIR_PUBLIC.'files/catalog/tovar/data/'.$id.'_data.xml');
                //echo 	$default_data;
                $priceMc = preg_match('/<item[^>].*mc=\"priceMc\"[^>].*bitmaps\/([\d]{1,})_big[^>].*price=\"([\d]{1,})\sруб\"\/>/',$default_data, $match);
                //print_r($match);
                if (isset($match[1]) && is_numeric($match[1])) {
                    $bitmap_item = Catalog_Bitmaps_Items::getInstance()->find($match[1])->current();
                    //var_dump($bitmap_item);
                    if($bitmap_item){
                        $price_row = Catalog_Bitmaps_Groups2Product::getInstance()->getPriceByTovarGroup($id, $bitmap_item->id_group);
                        if ($price_row!=null) {
                            $new_price = str_replace($match[2],$price_row->price,$match[0] );
                            $default_data = preg_replace('/(<item[^>].*mc=\"priceMc\"[^>].*price=\"(\d+)\sруб\"\/>)/', $new_price, $default_data);
                            file_put_contents(DIR_PUBLIC.'files/catalog/tovar/data/'.$id.'_data.xml',$default_data);
                            echo $default_data;
                            /*echo $price_row->price;
                            $priceMc = preg_match('/<item[^>].*mc=\"priceMc\"[^>].*price=\"(\d+)\sруб\"\/>/',$default_data, $match);
                            print_r($match);*/
                        }
                    }
                }
            }

            $this->view->ok='Данные сохранены';
        }
        $tovar_groups = $model_groups2product->fetchAll('id_product='.(int)$id);
        $selected_groups = array();
        $selected_groups_mehanizm = array();//+++++++++
        if (count($tovar_groups)) {
            foreach ($tovar_groups as $tovar_group) {
                $selected_groups[$tovar_group->id_group]= $tovar_group->price;
                $selected_groups_mehanizm[$tovar_group->id_group]= $tovar_group->price_mehanizm;//+++++++++++
            }
        }
        $tovar_textures = $model_items2product->fetchAll('id_product='.(int)$id);
        $selected_textures = array();
        $related_textures = array();
        if (count($tovar_textures)) {
            foreach ($tovar_textures as $tovar_texture) {
                if($tovar_texture->main){
                    $selected_textures[]= $tovar_texture->id_item;
                }
                $related_textures[$tovar_texture->id_item] = $tovar_texture->related;
            }
        }
        $this->view->selected_groups = $selected_groups;
        $this->view->selected_groups_mehanizm = $selected_groups_mehanizm;//+++++++++++++++
        $this->view->selected_textures = $selected_textures;
        $this->view->related_textures = $related_textures;
        $groups = Catalog_Bitmaps_Groups::getInstance()->fetchAll(null,'priority DESC' );
        $this->view->groups = $groups;
    }

    public function flashAction() {
        $this->view->id = $id = $this->_getParam('id',0);
        $this->view->id_page = $id_page = $this->_getParam('id_page',0);
        $this->view->tovar = $tovar = Catalog_Product::getInstance()->find($id)->current();
        $this->view->division = Catalog_Division::getInstance()->find($tovar->id_division)->current();
        $this->view->lang = $this->_getParam('lang', 'ru');
        if ($this->_request->isPost()) {

            $conf = $this->_getParam('conf', '');
            if ($conf) {
                $save_string = '<?xml version="1.0" encoding="utf-8" ?>'."\n";
                $save_string.=$conf;
                if(file_put_contents(DIR_PUBLIC.'files/catalog/tovar/data/'.$tovar->swf_data_file, $save_string)) {

                    echo 'saved';
                    exit;
                }

                echo 'err';
                exit;


            }

            $swf_file = $_FILES['swf_file']['tmp_name'];
            $swf_file_name = $_FILES['swf_file']['name'];
            $ext = @end(explode('.', $swf_file_name));
            if ($swf_file) {
                if (copy($swf_file, DIR_PUBLIC.'files/catalog/tovar/swf/'.$tovar->id.'_swf.'.$ext)) {
                    $tovar->swf_file = $tovar->id.'_swf.'.$ext;
                    $tovar->save();

                }
            }

            $swf_data = $_FILES['swf_data']['tmp_name'];
            $swf_data_name = $_FILES['swf_data']['name'];
            $ext = @end(explode('.', $swf_data_name));
            if ($swf_data) {
                if (copy($swf_data, DIR_PUBLIC.'files/catalog/tovar/data/'.$tovar->id.'_data.'.$ext)) {
                    $tovar->swf_data_file = $tovar->id.'_data.'.$ext;
                    $tovar->save();

                }
            }
        }


        if ($tovar->swf_file != '' || $tovar->swf_file != null) {
            $this->view->flash_exists = true;
        }
    }

    public function relitemsAction(){
        $id_product = $this->view->id = $this->_getParam('id_product',0);
        $id_group = $this->_getParam('id_group',0);
        $id_item = $this->_getParam('id_item',0);

        if ($this->_request->isPost()) {
            $textures = $this->_getParam('textures');
            if(count($textures)){
                $related = implode(',',$textures);
            }else{
                $related = '';
            }
            $item = Catalog_Bitmaps_Items2Product::getInstance()->fetchRow('id_product = '.(int)$id_product.' AND id_item = '.(int)$id_item);
            if($item){
                $item->related = $related;
                $item->save();

            }

            $this->view->ok='Данные сохранены';

        }

        if($id_product && $id_group && $id_item){
            $item = Catalog_Bitmaps_Items::getInstance()->find($id_item)->current();
            $product = Catalog_Product::getInstance()->find($id_product)->current();
            if($item){
                $groups = $this->view->groups = Catalog_Bitmaps_Groups::getInstance()->getGroupsForTovar($id_product);
                $relitems = Catalog_Bitmaps_Items2Product::getInstance()->fetchRow('id_product = '.(int)$id_product.' AND id_item = '.(int)$id_item);
                if($relitems && $relitems->related != ''){
                    $this->view->selected_textures = explode(',',$relitems->related);
                }else{
                    $this->view->selected_textures = array();
                }
                $this->view->id_item = $item->id;
                $this->layout->action_title = 'Сформировать связи для текстуры '.$item->name.' товара '.$product->title;
            }

            //var_dump($groups);exit;
        }
    }


    public function saveconfigAction() {
        $tovar_id = $this->_getParam('tovar');
        //$tovar = Catalog_Tovar::getInstance()->find($tovar_id)->current();
        $config = $this->_getParam('conf', '');
        if ($config && $tovar_id) {
            $save_string = '<?xml version="1.0" encoding="utf-8" ?>'."\n";
            $save_string.=$config;
            preg_match('/<item[^>].*mc=\"priceMc\"[^>].*price=\"([\d]+).*>/', $config, $matches);
            $price = $matches[1];
            $array['id_tovar'] = $tovar_id;
            $array['added'] = date('Y-m-d');
            $array['config'] = $save_string;
            $array['price'] = (int)$price;
            $order_id = Catalog_Order::getInstance()->createRow($array)->save();
            if ($order_id) {
                $session = new Zend_Session_Namespace('order');
                $session->configs[]=array('tovar_id'=>$tovar_id, 'order_id'=> $order_id, 'price'=>(int)$price);

                echo $order_id;
            } else  echo 'err';

        }
        exit;
    }

    public function loadconfigAction() {
        $order_id = $this->_getParam('order', 0);
        if ($order_id) {
            $order_row = Catalog_Order::getInstance()->find($order_id)->current();
            echo $order_row->config;
            exit;
        }

    }

    public function savedivisionsAction() {
        $id_product = $this->_getParam('id_product');
        $id_division = $this->_getParam('id_division');
        $id_element = $this->_getParam('id_element');
        Catalog_Product2Divisions::getInstance()->saveElements($id_product,$id_division,$id_element);
        echo 'ok';
        exit();
    }

    public function palitteAction() {
        $this->layout->disableLayout();
        $id = $this->_getParam('id', 0);
        if ($id) {
            $tovar = Catalog_Product::getInstance()->find($id)->current();
            $groups = Catalog_Bitmaps_Groups::getInstance()->getGroupsByTovar($id);
            $wood_groups = array();
            $cloth_groups = array();
            if ($groups) {
                foreach ($groups as $key=>$group) {
//var_dump($group);
                    $items = Catalog_Bitmaps_Items::getInstance()->getSelectedItemsByGroup(
                        $id,
                        $group['id'],
                        $group['price'],
                        $group['price_mehanizm']
                    );
                    if (count($items)) {
                        $group['items'] = $items;

                        if ($group['is_decor']==0) {
                            $cloth_groups[] = $group;
                        } else {
                            $wood_groups[] = $group;
                        }
                    }

                }
            }

            $this->view->cloth_groups = $cloth_groups;
            $this->view->wood_groups = $wood_groups;
            //var_dump($groups);
            //exit;
        }

    }

}
