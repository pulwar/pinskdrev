<?php
/**
 * Admin_Catalog_Bitmaps_ItemsController
 * 
 * @author Grover
 * @version 1.0
 */

class Catalog_Admin_Bitmaps_ItemsController extends MainAdminController  {
	
	/**
 	 * @var string
 	 */
	private $_curModule = null;	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 20;
	
	private $_page = null;
	/**
	 * offset
	 *
	 * @var int
	 */	
	private $_offset = null;	
	
	private $_group_id = null;
	
	private $_owner = null;
	
	public function init(){
		$this->initView();
		$this->view->strictVars(false);
		$this->view->addScriptPath(Zend_Registry::get('scriptsPaths')) ;
        $this->view->addHelperPath(Zend_Registry::get('helpersPaths')) ;
		$this->view->caption = 'Текстуры';
		$this->view->group_id = $this->_group_id = $this->_getParam('group_id');
		if ($this->_group_id){
			$this->_owner = Catalog_Bitmaps_Groups::getInstance()->find($this->_group_id)->current();
		}	
		$group = Catalog_Bitmaps_Groups::getInstance()->find($this->_group_id)->current();	
		$this->view->child_name = $group->name;
		$this->view->lang = $lang = $this->_getParam('lang','ru');
		//var_dump($this->view->group_id);
		$this->view->currentModule = $this->_curModule = SP.'catalog'.SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);	
		$this->_offset =($this->_page-1)*$this->_onpage;	
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	
		
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		$this->view->layout()->action_title = 'Cписок текстур';
		$this->view->name = 'Текстуры';
		$this->view->currentModule = $this->_curModule;		
		$where = null;
		if ($this->_group_id){
			$where = "id_group=".(int)$this->_group_id;
		}
		$this->view->total = count(Catalog_Bitmaps_Items::getInstance()->fetchAll($where));
		$this->view->all = $peoples =  Catalog_Bitmaps_Items::getInstance()->fetchAll($where, array('priority DESC', 'name ASC'), (int)$this->_onpage, (int)$this->_offset);
		
		
		
	
	}
	
	public function editAction(){
		$id = $this->_getParam('id', '');
		$is_new = 0;
		if ($id){
			$item = Catalog_Bitmaps_Items::getInstance()->find($id)->current();
		} else{
			$item = Catalog_Bitmaps_Items::getInstance()->createRow();
			$is_new = 1;
		}
		if ($this->_group_id){
			$groups = Catalog_Bitmaps_Groups::getInstance()->fetchAll(null, 'priority DESC');
			$groups_options = array();
			foreach ($groups as $group){
				$groups_options[$group->id]=$group->name;
			}
			$this->view->groups_options = $groups_options;
			$item->id_group = $this->_group_id;
				
		}
		if ($this->_request->isPost()){						
			$data = $this->trimFilter($this->_getParam('edit'));
                        
			if ($data['name']!=''){
				$item->setFromArray($data);
				$id =  $item->save();
				$img_name = $_FILES['img_big']['name'];
				$img_source = $_FILES['img_big']['tmp_name'];
				$delete_img = $this->_getParam('delete_img_big');				
				if ($img_name!='' && $img_source!='' && !$delete_img){
					$ext = @end(explode('.', $img_name));					
					$big_img = DIR_PUBLIC.'pics/catalog/bitmaps/'.$id.'_big.'.$ext;
					if(copy($img_source,$big_img )){
						$item->img_big = $id.'_big.'.$ext;
						$item->save();
					}					
				} else if ($delete_img){					
					@unlink(DIR_PUBLIC.'pics/catalog/bitmaps/'.$item->img_big);
					$item->img_big='';
					$item->save();
				}	
				
				/*$img_name = $_FILES['img_small']['name'];
				$img_source = $_FILES['img_small']['tmp_name'];
				$delete_img_small = $this->_getParam('delete_img_small');				
				if ($img_name!='' && $img_source!='' && !$delete_img){
					$ext = @end(explode('.', $img_name));					
					$big_small = DIR_PUBLIC.'pics/catalog/bitmaps/'.$id.'_small.'.$ext;
					if(copy($img_source,$big_small )){
						$item->img_small = $id.'_small.'.$ext;
						$item->save();
					}
					
				} else if ($delete_img_small){
					@unlink(DIR_PUBLIC.'pics/catalog/bitmaps/'.$item->img_small);					
					$item->img_small='';					
					$item->save();
				}	*/

				if ($is_new){
					Catalog_Bitmaps_Items2Product::getInstance()->addNewBitmapToTovar($id);
				}
				$this->view->ok=1;
				
			} else{
				$this->view->err=1;				
			}
			
		}
		
		$this->view->item = $item;
		if ($item->id){			
			$this->view->name = 'Редактировать';
		} else {
			$this->view->name = 'Добавить';
		}	
		
	}

	
	public function deleteAction(){
		$id = $this->_getParam('id');
		$id_group = $this->_getParam('group_id');
		if ($id ){
			$item = Catalog_Bitmaps_Items::getInstance()->find($id)->current();		
			if (!is_null($item)){	
				$item->delete();	
			}		
			$this->_redirect($this->_curModule.'/index/group_id/'.$id_group.'/page/'.$this->_page);
		}
	}
	
	public function activeAction(){
	$id = $this->_getParam('id');
	
		if ($id ){
			$item = Catalog_Bitmaps_Items::getInstance()->find($id)->current();			
			if ($item->active){
				$item->active=0;			
			} else {
				$item->active=1;
			}
			$item->save();
			$this->_redirect($this->_curModule.'/index/group_id/'.$this->_group_id.'/page/'.$this->_page);
		}
	}
}