<?php
/**
 * Admin_Catalog_Bitmaps_GroupsController
 *
 * @author Grover
 * @version 1.0
 */

class Catalog_Admin_Bitmaps_GroupsController extends MainAdminController {

    /**
     * @var string
     */
    private $_curModule = null;
    /**
     * items per page
     *
     * @var int
     */
    private $_onpage = 40;

    private $_page = null;
    /**
     * offset
     *
     * @var int
     */
    private $_offset = null;



    public function init() {        
        $this->initView();
        $this->view->strictVars(false);
        $this->view->addScriptPath(Zend_Registry::get('scriptsPaths')) ;
        $this->view->addHelperPath(Zend_Registry::get('helpersPaths')) ;
        $this->view->caption = 'Группы Текстур';
        $this->view->lang = $lang = $this->_getParam('lang','ru');
        $this->view->currentModule = $this->_curModule = SP.'catalog'.SP.$lang.SP.$this->getRequest()->getControllerName();
        $this->_page = $this->_getParam('page', 1);
        $this->_offset =($this->_page-1)*$this->_onpage;
        $this->view->current_page = $this->_page;
        $this->view->onpage = $this->_onpage;

        if ($this->_hasParam('add')) {
            $this->_forward('edit');
        }
        
    }

    /**
     * The default action - show the home page
     */
    public function indexAction() {
        $this->view->layout()->action_title = "Текстуры";
        $this->view->layout()->title = "Группы текстур";
        $this->view->child_name = 'Группы Текстур';
        $this->view->currentModule = $this->_curModule;
        $this->view->total = count(Catalog_Bitmaps_Groups::getInstance()->fetchAll(null));
        $this->view->all = Catalog_Bitmaps_Groups::getInstance()->fetchAll(null, array('priority DESC', 'name ASC'), (int)$this->_onpage, (int)$this->_offset);;
    }

    public function editAction() {
        $this->view->layout()->description = "Текстуры";
        $this->view->layout()->title = "Редактировать текстуру";
        $id = $this->_getParam('id', '');
        if ($id) {
            $item = Catalog_Bitmaps_Groups::getInstance()->find($id)->current();
        } else {
            $item = Catalog_Bitmaps_Groups::getInstance()->createRow();
        }

        if ($this->_request->isPost()) {
            $data = $this->trimFilter($this->_getParam('edit'));
            if ($data['name']!='') {
                $item->setFromArray($data);
                $id =  $item->save();
                $this->view->ok=1;

            } else {
                $this->view->err=1;
            }

        }

        $this->view->item = $item;
        if ($item->id) {
            $this->view->child_name = 'Редактировать';
        } else {
            $this->view->child_name = 'Добавить';
        }
    }


    public function deleteAction() {
        $id = $this->_getParam('id');
        if ($id ) {
            $item = Catalog_Bitmaps_Groups::getInstance()->find($id)->current();
            if (!is_null($item)) {
                $item->delete();
            }
            $this->_redirect($this->_curModule.'/index/page/'.$this->_page);
        }
    }

    public function activeAction() {
        $id = $this->_getParam('id');

        if ($id ) {
            $item = Catalog_Bitmaps_Groups::getInstance()->find($id)->current();
            if ($item->active) {
                $item->active=0;
            } else {
                $item->active=1;
            }
            $item->save();
            $this->_redirect($this->_curModule.'/index/page/'.$this->_page);
        }
    }
}