<?php
/**
 * Admin_Catalog_Fabrics_GroupsController
 *
 * @author 123
 * @version 1.0
 */

class Catalog_Admin_Fabrics_GroupsController extends MainAdminController {

    /**
     * @var string
     */
    private $_curModule = null;
    /**
     * items per page
     *
     * @var int
     */
    private $_onpage = 40;

    private $_page = null;
    /**
     * offset
     *
     * @var int
     */
    private $_offset = null;



    public function init() {        
        $this->initView();
        $this->view->strictVars(false);
        $this->view->addScriptPath(Zend_Registry::get('scriptsPaths')) ;
        $this->view->addHelperPath(Zend_Registry::get('helpersPaths')) ;
        $this->view->caption = 'Группы Текстур';
        $this->view->lang = $lang = $this->_getParam('lang','ru');
        $this->view->currentModule = $this->_curModule = SP.'catalog'.SP.$lang.SP.$this->getRequest()->getControllerName();
        $this->_page = $this->_getParam('page', 1);
        $this->_offset =($this->_page-1)*$this->_onpage;
        $this->view->current_page = $this->_page;
        $this->view->onpage = $this->_onpage;

        if ($this->_hasParam('add')) {
            $this->_forward('edit');
        }
        
    }

    /**
     * The default action - show the home page
     */
    public function indexAction() {
        $this->view->layout()->action_title = "Гаммы";
        $this->view->layout()->title = "Группы гамм";
        $this->view->child_name = 'Группы гамм';
        $this->view->currentModule = $this->_curModule;
        $this->view->total = count(Catalog_Fabrics_Groups::getInstance()->fetchAll(null));
        $this->view->all = Catalog_Fabrics_Groups::getInstance()->fetchAll(null, array('priority DESC', 'name ASC'), (int)$this->_onpage, (int)$this->_offset);;
    }

    public function editAction() {
        $this->view->layout()->description = "Гаммы";
        $this->view->layout()->title = "Редактировать гамму";
        $id = $this->_getParam('id', '');
        if ($id) {
            $item = Catalog_Fabrics_Groups::getInstance()->find($id)->current();
        } else {
            $item = Catalog_Fabrics_Groups::getInstance()->createRow();
        }

        if ($this->_request->isPost()) {
            $data = $this->trimFilter($this->_getParam('edit'));
            if ($data['name']!='') {
                $item->setFromArray($data);
                //Zend_Debug::dump($data);exit();
                $id =  $item->save();
                $img_name = $_FILES['img_big']['name'];
                $img_source = $_FILES['img_big']['tmp_name'];
                $delete_img = $this->_getParam('delete_img_big');
                if ($img_name!='' && $img_source!='' && !$delete_img){
                    $ext = @end(explode('.', $img_name));
                    $big_img = DIR_PUBLIC.'pics/catalog/fabrics_gammas/'.$id.'_big.'.$ext;
                    if(copy($img_source,$big_img )){
                        $item->img_big = $id.'_big.'.$ext;
                        $item->save();
                    }
                } else if ($delete_img){
                    @unlink(DIR_PUBLIC.'pics/catalog/fabrics_gammas/'.$item->img_big);
                    $item->img_big='';
                    $item->save();
                }
                $this->view->ok=1;

            } else {
                $this->view->err=1;
            }

        }

        $this->view->item = $item;
        if ($item->id) {
            $this->view->child_name = 'Редактировать';
        } else {
            $this->view->child_name = 'Добавить';
        }
    }


    public function deleteAction() {
        $id = $this->_getParam('id');
        if ($id ) {
            $item = Catalog_Fabrics_Groups::getInstance()->find($id)->current();
            if (!is_null($item)) {
                $item->delete();
            }
            $this->_redirect($this->_curModule.'/index/page/'.$this->_page);
        }
    }

    public function activeAction() {
        $id = $this->_getParam('id');

        if ($id ) {
            $item = Catalog_Fabrics_Groups::getInstance()->find($id)->current();
            if ($item->active) {
                $item->active=0;
            } else {
                $item->active=1;
            }
            $item->save();
            $this->_redirect($this->_curModule.'/index/page/'.$this->_page);
        }
    }
}