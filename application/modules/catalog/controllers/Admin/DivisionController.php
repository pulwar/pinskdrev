<?php
class Catalog_Admin_DivisionController extends MainAdminController {
    /**
     * Controller name
     *
     * @var string
     */
    private $_current_module = null;

    public function init() {
        $this->initView();
        $this->layout = $this->view->layout();
        $this->layout->title = "Каталог";
        $this->layout->action_title = 'Дерево разделов';
        $lang = $this->_getParam('lang', 'ru');
        $this->view->currentModule = $this->_current_module = SP . $this->getRequest()->getModuleName() . SP . $lang . SP . $this->getRequest()->getControllerName();
    }

    public function indexAction() {
        $this->view->lang = $lang = $this->getParam('lang', 'ru');
        $tree = Catalog_Division::getInstance()->getTree();
        $this->view->html_tree = $tree;
    }

    public function editAction() {

        $id = $this->_getParam('id');

        if ($id) {
            $item = Catalog_Division::getInstance()->find($id)->current();
            $this->layout->action_title = 'Редактировать раздел';
            $this->view->creating = false;
        } else {
            $item = Catalog_Division::getInstance()->createRow();
            $this->layout->action_title = 'Создать раздел';
            $this->view->creating = true;
        }
        $parent_id = $this->_getParam('parent_id');

        //$errors = array();
        $ok = 1;

        if ($this->_request->isPost()) {
            $data = $this->_getAllParams();
            $data['url'] = preg_replace('/[^\w_-]/', '', $data['url']);
            if (Catalog_Division::getInstance()->checkUrl($data['url'], $id) && $data['url'] != '') {
                unset($data['id']);
                if ($parent_id) {
                    $parent = Catalog_Division::getInstance()->find($parent_id)->current();

                    if ($parent != null) {
                        $data['level'] = $parent->level + 1;
                        $data['parent_id'] = $parent_id;
                    }
                }

                if ($data['name'] != '') {
                    if (! $id) {
                        $sort = Catalog_Division::getInstance()->getMaxSort((int)$parent_id);
                        $data['sortid'] = (int)$sort + 1;

                    }
                    if (!isset($data['id_page'])) $data['id_page'] = 0;
                    $item->setFromArray(array_intersect_key($data, $item->toArray()));
                    $id = $item->save();
                    Catalog_Division_Cache::getInstance()->remove($id);
                    Catalog_Division_Cache::getInstance()->clean(Zend_Cache::CLEANING_MODE_OLD);
                    $img_name = $_FILES['img']['name'];
                    $img_source = $_FILES['img']['tmp_name'];
                    $delete_img = $this->_getParam('delete_img');
                    if ($img_name != '' && $img_source != '' && ! $delete_img) {
                        $ext = @end(explode('.', $img_name));
                        $small_img = DIR_PUBLIC . 'pics/catalog/division/' . $id . '_img.' . $ext;
                        if (copy($img_source, $small_img)) { //if($this->img_resize($img_source,$small_img, 130, 115 )){
                            $item->img = $id . '_img.' . $ext;
                            $item->save();
                        }

                    } else if ($delete_img) {
                        @unlink(DIR_PUBLIC . 'pics/catalog/division/' . $item->img);
                        $item->img = '';
                        $item->save();
                    }
                    Catalog_Division::getInstance()->setParentIds();
                    $this->view->ok = $ok;
                } else {
                    $ok = 0;
                    $error = 'Незаполнено поле название';
                }
            } else {
                $ok = 0;
                $error = 'Раздел с таким url уже существует.';
            }
        }
        if (! $ok)
            $item->setFromArray(array_intersect_key($data, $item->toArray()));
        $fck1 = $this->getFck('intro', '90%', '150', 'Basic');
        $this->view->intro = $fck1;
        $fck2 = $this->getFck('description', '90%', '300');
        $this->view->fck_content = $fck2;
        $fck_content_bottom = $this->getFck('content_bottom', '90%', '300');
        $this->view->fck_content_bottom = $fck_content_bottom;
        $this->view->item = $item;
        if ($ok == 0)
            $this->view->err = $error;
    }

    public function deleteAction() {
        if ($this->_hasParam('id')) {
            $id = (int)$this->getRequest()->getParam('id');
            $division_row = Catalog_Division::getInstance()->find($id)->current();
            if (null != $division_row) {
                $division_row->delete();
                echo 'ok';
                Catalog_Division::getInstance()->setParentIds();
            }
        }
        if ($this->_request->isXmlHttpRequest()) {
            exit();
        }
        $this->_redirect($this->_current_module);
    }

    public function processAction() {
        $type = $this->_getParam('type');
        $ids = $this->_getParam('ids');
        if ($type && $ids) {
            $ok = Catalog_Division::getInstance()->processItems($ids, $type);
        }

        if ($ok) {
            echo 'ok';
            Catalog_Division::getInstance()->setParentIds();
        } else {
            echo 'err';
        }

        exit();
    }

    public function copyAction() {
        //		print (var_dump($this->_request->getParams()));exit;
        if ($this->_hasParam('id')) {
            Catalog_Division::getInstance()->copyDivisionWithChildren($this->_getParam('id'));
        }

        //		exit;
        //		$lang = $this->_hasParam('lang') ? $this->_getParam('lang') : 'ru';
        $this->_redirect('/admin/ru/Catalog_Division/index/id_page/191/');
    }

    public function moveAction() {
        if ($this->_hasParam('node_id') && $this->_hasParam('target_id') && $this->_hasParam('type')) {
            $node = (int)$this->_getParam('node_id');
            $target = (int)$this->_getParam('target_id');
            $point = (string)$this->_getParam('type');
            $model = Catalog_Division::getInstance();
            $model->replace($node, $target, $point);
            echo 'ok';
            Catalog_Division::getInstance()->setParentIds();
        } else {
            echo 'err';
        }
        exit();
    }


    public function exportAction() {
        $this->layout->action_title = 'Экспорт / Импорт товаров';
        $id = (int)$this->getParam('id');
        $this->view->collections_options = Collections::getInstance()->getItemsAsOptions();
        $this->view->mebeltype_options = Catalog_Product_Mebeltype::getInstance()->getItemsAsOptions();
        $division = Catalog_Division::getInstance()->find($id)->current();
        if ($division != null || $id == 0) {
            if ($this->_request->isPost()) {
                $operation = $this->_getParam('operation');
                if ($operation == 'export') {
                    $collection = $this->_getParam('collection_id');
                    $type = $this->_getParam('id_mebeltype');
                    $products = Catalog_Product::getInstance()->getAllProductsForExcel($collection, $type);
                    if ($products->count()) {
                        $objPHPExcel = new PHPExcel();
                        error_reporting(E_ALL);
                        $objPHPExcel->getProperties()->setCreator("MF");
                        $objPHPExcel->getProperties()->setLastModifiedBy("MF");
                        $objPHPExcel->getProperties()->setTitle("Products prices");
                        $objPHPExcel->getProperties()->setSubject("Products prices");
                        $objPHPExcel->getProperties()->setDescription("Products prices");


                        $objPHPExcel->setActiveSheetIndex(0);
                        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'id');
                        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'collection');
                        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'category');
                        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'type');
                        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'title');
                        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'price');
                        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'price_with_transform');
                        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

                        $i=1;
                        $z=0;
                        $check = 0;
                        foreach ($products as $product) {
                            $check = 0;
                            $z++;
                            $units = Catalog_Product_Units::getInstance()->getActiveByProductId($product->id);
                            foreach ($units as $unit) {
                                $complect = Catalog_Product_Complect::getInstance()->getUnitComplect($unit->id);
                                if (count($complect)> 0) {
                                    if ($check == 0) {
                                        $z++;
                                        $check = 1;
                                    }
                                    continue;
                                }
                                if ($unit->color_ids != '') {
                                    $color = Catalog_Product_Color::getInstance()->getNameById($unit->color_ids);
                                }
                                else {
                                    $color = Catalog_Fabrics_Groups::getInstance()->getNameById($unit->fabrics_ids);
                                }
                                $title=$unit->title.' ('.$color.')';
                                $product->product_type == 1 ? $product_type = 'Корпусная мебель' : $product_type = 'Мягкая мебель';

                                $i++;

                                $objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, $unit->id);
                                $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $product->collection_name);
                                $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $product_type);
                                $objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $product->mebeltype);
                                $objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $title);
                                $objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $unit->price);
                                $objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, $unit->price_with_tr);
                                if ($z%2==0){
                                    $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getFill()
                                        ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                            'startcolor' => array('rgb' => 'FFFF00')
                                        ));
                                } else {
                                    $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getFill()
                                        ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                            'startcolor' => array('rgb' => 'E0E0E0')
                                        ));
                                }
                            }
                        }

                        $objPHPExcel->getActiveSheet()->setTitle('Prices');

                        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
                        $objWriter->save('files/export/export.xlsx');

                        $this->view->ok = 2;

                        //$xlsx = file_get_contents('files/export/export.xlsx');
                        //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        //header('Content-Disposition: attachment;filename="My Excel File.xlsx"');
                        //echo $xlsx;
                    }
                } elseif ($operation == 'import') {
                    //Zend_Debug::dump($_FILES);exit;
                    $file_source = $_FILES['import_file']['tmp_name'];
                    chmod("files/export/import.xlsx", 0777);
                    if (copy($file_source, 'files/export/import.xlsx')) {

                        $objPHPExcel = new PHPExcel();
                        $inputFileType = PHPExcel_IOFactory::identify('files/export/import.xlsx');
                        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                        $objReader->setReadDataOnly(true);
                        $objPHPExcel = $objReader->load('files/export/import.xlsx');
                        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
                        $highestRow = $objWorksheet->getHighestRow();
                        for ($row = 2; $row <= $highestRow;++$row)
                        {
                            $id_unit = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $unit_price = $objWorksheet->getCellByColumnAndRow(5, $row)->getValue();
                            $unit_price_with_tr = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
                            $unit = Catalog_Product_Units::getInstance()->find($id_unit)->current();
                            if ($unit != null) {
                                $unit->price = $unit_price;
                                $unit->price_with_tr = $unit_price_with_tr;
                                $unit->save();
                            }
                        }
                    }

                    $this->view->ok = 1;
                }

            }
        }
    }

}
