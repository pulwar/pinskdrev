<?php

class Catalog_Admin_Product_CheckController extends MainAdminController {
	
	/**
	 * @var string
	 */
	private $_curModule = null;
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 50;
	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_lang = null;
	
	private $_page = null;
	/**
	 * offset
	 *
	 * @var int
	 */
	private $_offset = null;
	
	public function init() {
		//$this->initView();
		$this->layout = $this->view->layout();
		$this->layout->title = "Проверка каталога";
		$this->view->caption = 'Список несоответствий';
		$lang = $this->_getParam('lang', 'ru');
		
		$this->view->currentModule = $this->_curModule = SP . $this->getRequest()->getModuleName() . SP . $lang . SP . $this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);
		$this->_offset = ($this->_page - 1) * $this->_onpage;
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		$this->layout->action_title = "Список элементов";
		$this->view->currentModule = $this->_curModule;
		$where = null;
		$this->view->total = $abw = count(Catalog_Product_Actions::getInstance()->getAllActions());
		$this->view->all = $abw = Catalog_Product_Units::getInstance()->getAll();
	}

}