<?php

/**
 * Catalog_Admin_Product_ImagesController
 *
 * @author Vitali
 * @version
 */

class Catalog_Admin_Product_ImagesController extends MainAdminController {

    /**
     * @var string
     */
    private $_curModule = null;
    /**
     * items per page
     *
     * @var int
     */
    private $_onpage = 20;

    private $_page = null;
    /**
     * offset
     *
     * @var int
     */
    private $_offset = null;

    private $_owner = null;

    private $_id_product = null;

    public function init() {
        $this->initView();
        $this->layout = $this->view->layout();
        $this->view->lang = $lang = $this->getParam('lang','ru');
	$this->view->id_division = $lang = $this->getParam('id_division',0);
        $id_product = $this->_getParam('id_product',0);
        if ($id_product) {
            $this->_id_product = $id_product;
            $this->view->owner =$this->_owner =  Catalog_Product::getInstance()->find($id_product)->current();
            $this->view->id_product = $id_product;
        } else {
        	$this->_redirect("/catalog/$lang/admin_division");
        }
    
        $this->layout->title = "Каталог, Фотогафии";
       
        $this->view->currentModule = $this->_curModule = '/'.$this->getRequest()->getModuleName().'/'.$lang.'/'.$this->getRequest()->getControllerName();
        $this->_page = $this->_getParam('page', 1);
        $this->_offset =($this->_page-1)*$this->_onpage;
        $this->view->current_page = $this->_page;
        $this->view->onpage = $this->_onpage;
         
    }

    /**
     * The default action - show the home page
     */
    public function indexAction() {    	
        $this->layout->action_title = "Список фотографий";
        if (!is_null($this->_owner)){
        	$this->layout->action_title.= " товара \"".$this->_owner->title.'"';
        }	
        
    	if ($this->_request->isPost()){
			$array_Priority = $this->_getParam('priority');
			$array_titles = $this->_getParam('title');
			Catalog_Product_Images::getInstance()->setProperties($array_Priority, $array_titles);			
			$action = $this->_getParam('operation');
			$ids = $this->_getParam('images');
			if ($action && $ids){
				Catalog_Product_Images::getInstance()->processImages($action, $ids);			
			}					
			Catalog_Product_Images::getInstance()->upload_Images($this->_id_product);
		}
		
        $id_product = $this->_getParam('id_product');        
        $this->view->total = Catalog_Product_Images::getInstance()->getImagesByProductId($id_product)->count();
        $this->view->all = Catalog_Product_Images::getInstance()->getImagesByProductId($id_product, $this->_onpage, $this->_offset);
      	 
        $this->view->options = Catalog_Product_Images::getInstance()->getOptions();
    }
    
    public function editAction() {
		$id = $this->_getParam( 'id' );
		if ($id) {
			$item = Catalog_Product_Images::getInstance()->find( $id )->current();
			$this->layout->action_title = 'Редактировать фото';
		} else {
			$item = Catalog_Product_Images::getInstance()->fetchNew();
			$this->layout->action_title = 'Создать фото';
		}
		if (! is_null( $this->_owner )) {
			$this->layout->action_title .= " товара \"" . $this->_owner->title . '"';
		}
		
		if ($this->_request->isPost()) {
			$data = $this->_getAllParams();
			$item->setFromArray( array_intersect_key( $data, $item->toArray() ) );
			$id = $item->save();
			$img_name = $_FILES ['img'] ['name'];
			$img_source = $_FILES ['img'] ['tmp_name'];
			$delete_img = $this->_getParam( 'delete_img' );
			if ($img_name != '' && $img_source != '' && ! $delete_img) {
				$ext = @end( explode( '.', $img_name ) );
				$name = $id . '_img.' . $ext;
				$img_big = DIR_PUBLIC . 'pics/catalog/product/img/' . $name;
				if (copy( $img_source, $img_big )) {
					$item->img = $name;
					$item->save();
				}
			
			} else if ($delete_img) {
				@unlink( DIR_PUBLIC . 'pics/catalog/product/img/' . $item->img );
				$item->img = '';
				$item->save();
			}
			$img_name = $_FILES ['img_big'] ['name'];
			$img_source = $_FILES ['img_big'] ['tmp_name'];
			$delete_img = $this->_getParam( 'delete_img_big' );
			if ($img_name != '' && $img_source != '' && ! $delete_img) {
				$ext = @end( explode( '.', $img_name ) );
				$name = $id . '_img.' . $ext;
				$img_big = DIR_PUBLIC . 'pics/catalog/product/big/' . $name;
				if (copy( $img_source, $img_big )) {
					$item->img_big = $name;
					$item->save();
				}
			
			} else if ($delete_img) {
				@unlink( DIR_PUBLIC . 'pics/catalog/product/big/' . $item->img_big );
				$item->img_big = '';
				$item->save();
			}
			Catalog_Product_Images::getInstance()->checkParams($item);
		}
		
		$this->view->item = $item;
	}

  

    public function deleteAction() {
        $id = $this->_getParam('id',0);
        $item = Catalog_Product_Images::getInstance()->find($id)->current();
        if (!is_null($item)) {
            $item->delete();
        }
        $id_product = $this->_getParam('id_product', 0);
        $this->_redirect($this->_curModule.'/index/id_product/'.$id_product);
    }

    public function activateAction() {
        $id = $this->_getParam('id',0);
        $item = Catalog_Product_Images::getInstance()->changeActivity($id);        
        $id_product = $this->_getParam('id_product', 0);
        $this->_redirect($this->_curModule.'/index/id_product/'.$id_product);
    }

    public function mainAction() {
        $id = $this->_getParam('id',0);
        $item = Catalog_Product_Images::getInstance()->find($id)->current();
        if (!is_null($item)) {
            $item->main = abs($item->main-1);
            $item->save();  
            Catalog_Product_Images::getInstance()->checkMain($item);
        }
        $id_product = $this->_getParam('id_product', 0);
        $this->_redirect($this->_curModule.'/index/id_product/'.$id_product);
    }
    
	
    
    
}