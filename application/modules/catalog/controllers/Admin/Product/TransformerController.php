<?php

/**
 * Catalog_Admin_Product_TransformerController
 *
 * 
 * @version
 */

class Catalog_Admin_Product_TransformerController extends MainAdminController {


	/**
 	 * @var string
 	 */
	private $_curModule = null;
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 50;

	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_lang = null;

	private $_page = null;
	/**
	 * offset
	 *
	 * @var int
	 */
	private $_offset = null;

	public function init(){
		//$this->initView();
		$this->layout = $this->view->layout();
		$this->layout->title = "Типы механизмов трансформации";
		$this->view->caption = 'Список механизмов';
		$lang = $this->_getParam('lang','ru');

		$this->view->currentModule = $this->_curModule = SP.$this->getRequest()->getModuleName().SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);
		$this->_offset =($this->_page-1)*$this->_onpage;
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	}

	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		$this->layout->action_title = "Список элементов";
		$this->view->currentModule = $this->_curModule;
		$where = null;
		$this->view->total = count(Catalog_Product_Transformer::getInstance()->fetchAll($where));
		//$this->view->all = $peoples =  Catalog_Params::getInstance()->fetchAll($where, 'priority DESC', (int)$this->_onpage, (int)$this->_offset);
		$this->view->all = $peoples =  Catalog_Product_Transformer::getInstance()->fetchAll($where, 'priority DESC', (int)$this->_onpage, (int)$this->_offset);

	}

	public function editAction(){
		$id = $this->_getParam('id', '');
		if ($id){
			$item = Catalog_Product_Transformer::getInstance()->find($id)->current();
			$this->layout->action_title = "Редактировать элемент";
		} else{
			$item = Catalog_Product_Transformer::getInstance()->fetchNew();
			$this->layout->action_title = "Создать элемент";
		}

		if ($this->_request->isPost()){
			$data = $this->trimFilter($this->_getAllParams());
			if ($data['title']!='' && $data['url']!='' ){
				$item->setFromArray(array_intersect_key($data, $item->toArray()));
				$id =  $item->save();
                $img_name = $_FILES['img']['name'];
                $img_source = $_FILES['img']['tmp_name'];
                $delete_img = $this->_getParam('delete_img');
                if ($img_name!='' && $img_source!='' && !$delete_img){
                    $ext = @end(explode('.', $img_name));
                    $big_img = DIR_PUBLIC.'pics/catalog/transformers/'.$id.'_big.'.$ext;
                    if(copy($img_source,$big_img )){
                        $item->img = $id.'_big.'.$ext;
                        $item->save();
                    }
                } else if ($delete_img){
                    @unlink(DIR_PUBLIC.'pics/catalog/transformers/'.$item->img);
                    $item->img='';
                    $item->save();
                }

				$this->view->ok=1;

			} else{
				$this->view->err=1;
				$item->setFromArray(array_intersect_key($data, $item->toArray()));
			}

		}

		$this->view->item = $item;
	}




	
	public function deleteAction(){
		$id = $this->_getParam('id');
		if ($id ){
			$item = Catalog_Product_Transformer::getInstance()->find($id)->current();
			if ($item!=null){
				$item->delete();
				$data['update']=date ( "Y-m-d H:i:s" );
                Catalog_Product_Transformer::getInstance()->update($data);
				Catalog_Product::getInstance()->clearTransformer($id);
			}
		}
		$this->_redirect($this->_curModule);
	}
	
	/**
	 * изменение активности элемента
	 *
	 */
	public function activeAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			$item = Catalog_Product_Transformer::getInstance()->find($id)->current();			
			if (!is_null($item)){
				$item->active =  abs($item->active-1);
				$item->save();
			}
						
		}
		$this->_redirect($this->_curModule);
	}
}