<?php
/**
 * Catalog_Admin_Product_DecorController
 */
class Catalog_Admin_Product_DecorController extends MainAdminController {
	/**
	 * @var string
	 */
	private $_curModule = null;
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 50;
	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_lang = null;
	
	private $_page = null;
	/**
	 * offset
	 *
	 * @var int
	 */
	private $_offset = null;
	
	public function init() {
		//$this->initView();
		$this->layout = $this->view->layout();
		$this->layout->title = "Варианты декоративных накладок товара";
		$this->view->caption = 'Опции';
		$lang = $this->_getParam('lang', 'ru');
		
		$this->view->currentModule = $this->_curModule = SP . $this->getRequest()->getModuleName() . SP . $lang . SP . $this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);
		$this->_offset = ($this->_page - 1) * $this->_onpage;
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		$this->layout->action_title = "Список элементов";
		$this->view->currentModule = $this->_curModule;
		$where = null;
		$this->view->total = count(Catalog_Product_Decor::getInstance()->fetchAll($where));
		//$this->view->all = $peoples =  Catalog_Params::getInstance()->fetchAll($where, 'priority DESC', (int)$this->_onpage, (int)$this->_offset);
		$this->view->all = Catalog_Product_Decor::getInstance()->fetchAll($where, 'priority DESC', (int)$this->_onpage, (int)$this->_offset);
	}
	
	public function editAction() {
		$id = $this->_getParam('id', '');
		if ($id) {
			$item = Catalog_Product_Decor::getInstance()->find($id)->current();
			$this->layout->action_title = "Редактировать элемент";
		} else {
			$item = Catalog_Product_Decor::getInstance()->fetchNew();
			$this->layout->action_title = "Создать элемент";
		}
		if ($this->_request->isPost()) {
			$data = $this->trimFilter($this->_getAllParams());
			if ($data['title'] != '') {
				$item->setFromArray(array_intersect_key($data, $item->toArray()));
				$id = $item->save();
				$img_name = $_FILES['img']['name'];
                                $img_source = $_FILES['img']['tmp_name'];
                                $delete_img = $this->_getParam('delete_img');
                                if ($img_name!='' && $img_source!='' && !$delete_img) {
                                    $ext = @end(explode('.', $img_name));
                                    $name = $id.'_decor.'.$ext;
                                    $img_big = DIR_PUBLIC.'pics/decor/'.$name;
                                    if (copy($img_source, $img_big)) {
                                        $item->img = $name;
                                        $item->save();
                                    }

                                } else if ($delete_img) {
                                    @unlink(DIR_PUBLIC.'pics/decor/'.$item->img);
                                    $item->img = '';
                                    $item->save();
                                }
				$this->view->ok = 1;
			} else {
				$this->view->err = 1;
				$item->setFromArray(array_intersect_key($data, $item->toArray()));
			}
		}
		$this->view->item = $item;
	}
	
	public function deleteAction() {
		$id = $this->_getParam('id');
		if ($id) {
			$item = Catalog_Product_Decor::getInstance()->find($id)->current();
			if ($item != null) {
				$item->delete();
			}
		}
		$this->_redirect($this->_curModule);
	}
}
