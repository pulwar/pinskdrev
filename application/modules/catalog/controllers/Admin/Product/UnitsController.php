<?php
/**
 * Catalog_Admin_Product_UnitsController
 */
class Catalog_Admin_Product_UnitsController extends MainAdminController {
	
	/**
	 * @var string
	 */
	private $_curModule = null;
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 20;
	
	private $_page = null;
	
	/**
	 * offset
	 *
	 * @var int
	 */
	private $_offset = null;
	
	private $_owner = null;
	
	private $_id_product = null;
	
	public function init() {
        if ($this->getRequest()->isXmlHttpRequest())
            return;
        $this->initView();
		$this->layout = $this->view->layout();
		$this->view->lang = $lang = $this->getParam('lang', 'ru');
        if ($this->getRequest()->getActionName() == 'bitmaps') {
            $this->layout->setLayout("layout");
            return;
        }
	if ($this->getRequest()->getActionName() == 'recount') {
            $this->layout->setLayout("layout");
            return;
        }
		$this->view->id_division = $this->getParam('id_division', 0);
		$id_product = $this->_getParam('id_product', 0);
		if ($id_product) {
			$this->_id_product = $id_product;
			$this->view->owner = $this->_owner = Catalog_Product::getInstance()->find($id_product)->current();
			$this->view->id_product = $id_product;
		} else {
			$this->_redirect("/catalog/$lang/admin_division");
		}
		$this->layout->title = "Каталог, Элементы";
		$this->view->currentModule = $this->_curModule = '/' . $this->getRequest()->getModuleName() . '/' . $lang . '/' . $this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);
		$this->_offset = ($this->_page - 1) * $this->_onpage;
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		$this->layout->action_title = "Список элементов";
		if (! is_null($this->_owner)) {
			$this->layout->action_title .= " товара \"" . $this->_owner->title . '"';
		}
		if ($this->_request->isPost()) {
			$array_priority = $this->_getParam('priority');
			$array_prices = $this->_getParam('price');
			$array_prices_with_tr = $this->_getParam('price_with_tr');
			$array_titles = $this->_getParam('title');
			$array_is_new = $this->_getParam('is_new');
			$array_is_action = $this->_getParam('is_action');
			$array_is_popular = $this->_getParam('is_popular');
			Catalog_Product_Units::getInstance()->setProperties($array_priority, $array_titles, $array_prices, $array_prices_with_tr, $array_is_new, $array_is_action, $array_is_popular);
			$action = $this->_getParam('operation');
			$ids = $this->_getParam('images');
			if ($action && $ids) {
				Catalog_Product_Units::getInstance()->processImages($action, $ids);
			}
			Catalog_Product_Units::getInstance()->upload_Images($this->_id_product);
		}
		
		$id_product = $this->_getParam('id_product');
		$this->view->total = Catalog_Product_Units::getInstance()->getImagesByProductId($id_product)->count();
		$this->view->all = Catalog_Product_Units::getInstance()->getImagesByProductId($id_product, $this->_onpage, $this->_offset);
		$this->view->options = Catalog_Product_Units::getInstance()->getOptions();
	}
	
	public function editAction() {
		$id = $this->_getParam('id');
		if ($id) {
			$item = Catalog_Product_Units::getInstance()->find($id)->current();
			$this->layout->action_title = 'Редактировать элемент';
		} else {
			$item = Catalog_Product_Units::getInstance()->fetchNew();
			$this->layout->action_title = 'Создать элемент';
		}
		if (! is_null($this->_owner)) {
			$this->layout->action_title .= " товара \"" . $this->_owner->title . '"';
		}
		if ($this->_request->isPost()) {
			$data = $this->_getAllParams();
			$item->setFromArray(array_intersect_key($data, $item->toArray()));
			$item->material_ids = '';
			if(isset($data['materials'])) $item->material_ids = implode(',',$data['materials']);
		    $item->bitmap_id = '';
		    if(isset($data['bitmap_id'])) $item->bitmap_id = $data['bitmap_id'];
		    $item->fabrics_ids = '';
		    if(isset($data['fabrics'])) $item->fabrics_ids = implode(',',$data['fabrics']);
		    $item->bitmaps_ids = '';
		    if(isset($data['bitmaps'])) $item->bitmaps_ids = implode(',',$data['bitmaps']);
			$item->decor_ids = '';
			if(isset($data['decors'])) $item->decor_ids = implode(',',$data['decors']);
			$item->transformer_ids = '';
			if(isset($data['transformers'])) $item->transformer_ids = implode(',',$data['transformers']);
			$item->color_ids = '';
			if(isset($data['colors'])) $item->color_ids = implode(',',$data['colors']);
			$id = $item->save();
			//меняем уникальный код
			$testProduct = Catalog_Product::getInstance()->getTovarById($this->_owner->id);
			
			if($testProduct->product_type == 1) {
				if($item->product_cod && $item->facing_cod && $item->dyeing_cod) {
					$item->uniq_id = $item->product_cod  . $item->facing_cod . $item->dyeing_cod;
					$item->uniq_id = preg_replace('/\+/','', $item->uniq_id);	
				} else {
					$item->uniq_id = '';
				}
			}else if($testProduct->product_type == 2) {
				if ($item->model_cod && $item->fabric_name && $item->fabric_name_group && $item->completing_code) {
					$item->uniq_id = $item->model_cod . $item->fabric_name . $item->fabric_name_group . $item->completing_code;
					$item->uniq_id = preg_replace('/\+/','', $item->uniq_id);
				} else {
					$item->uniq_id = '';
				}
				if ($item->model_cod && $item->fabric_name && $item->fabric_name_group && $item->completing_code_tr) {
					$item->uniq_id_tr = $item->model_cod . $item->fabric_name . $item->fabric_name_group . $item->completing_code_tr;
					$item->uniq_id_tr = preg_replace('/\+/','', $item->uniq_id_tr);
				} else {
					$item->uniq_id_tr = '';
				}
			}	
			$item->save();		
			$img_name = $_FILES['img']['name'];
			$img_source = $_FILES['img']['tmp_name'];
			$delete_img = $this->_getParam('delete_img');
			if ($img_name != '' && $img_source != '' && ! $delete_img) {
				$ext = @end(explode('.', $img_name));
				$name = $id . '_img.' . $ext;
				$img_big = DIR_PUBLIC . 'pics/catalog/product/img/' . $name;
				if (copy($img_source, $img_big)) {
					$item->img = $name;
					$item->save();
				}
			} else if ($delete_img) {
				@unlink(DIR_PUBLIC . 'pics/catalog/product/img/' . $item->img);
				$item->img = '';
				$item->save();
			}
			$img_name = $_FILES['img_big']['name'];
			$img_source = $_FILES['img_big']['tmp_name'];
			$delete_img = $this->_getParam('delete_img_big');
			if ($img_name != '' && $img_source != '' && ! $delete_img) {
				$ext = 'jpg';
				$name = $id . '_img.' . $ext;
				$img_big = DIR_PUBLIC . 'pics/catalog/product/big/' . $name;
				if (copy($img_source, $img_big)) {
					$item->img_big = $name;
					$item->save();
                    			//Catalog_Product_Units::getInstance()->addImgWithWatermark($name);
				}
			} else if ($delete_img) {
				@unlink(DIR_PUBLIC . 'pics/catalog/product/big/' . $item->img_big);
				@unlink(DIR_PUBLIC . 'cached_images/big/' . $item->img_big);
				$item->img_big = '';
				$item->save();
			}
			Catalog_Product_Units::getInstance()->checkParams($item);
		}
		$this->view->item = $item;
		$this->view->product = Catalog_Product::getInstance()->getTovarById($this->_owner->id);
		$this->view->material_options = Catalog_Product_Material::getInstance()->getItemsAsOptions();
		$this->view->transformer_options = Catalog_Product_Transformer::getInstance()->getItemsAsOptions();
		$this->view->availability_options = Catalog_Product_Availability::getInstance()->getItemsAsOptions();
		$this->view->maketerm_options = Catalog_Product_Maketerm::getInstance()->getItemsAsOptions();
        $this->view->decor_options = Catalog_Product_Decor::getInstance()->getItemsAsOptions();
        $this->view->bitmaps = Catalog_Bitmaps_Groups::getInstance()->getItemsAsOptions();
        if ($item['bitmap_id'] > 0) {
        $this->view->bitmaps_options = Catalog_Bitmaps_Items::getInstance()->getInstance()->getItemsAsOptions2($item['bitmap_id']);
        }
        $this->view->fabrics_options = Catalog_Fabrics_Groups::getInstance()->getItemsAsOptions();
        $this->view->color_options = Catalog_Product_Color::getInstance()->getItemsAsOptions();
	}

    public function bitmapsAction() {
        $this->_helper->layout()->disableLayout();
        if (!$this->_request->isXmlHttpRequest()) {
            exit();
        }
        else {
            $bitmap = $this->_getParam('id');
            $bitmaps = Catalog_Bitmaps_Items::getInstance()->getInstance()->getItemsAsOptions($bitmap);
            $this->view->bitmaps = $bitmaps;
        }
    }
	
	public function deleteAction() {
		$id = $this->_getParam('id', 0);
		$item = Catalog_Product_Units::getInstance()->find($id)->current();
		if (! is_null($item)) {
			$item->delete();
		}
		$id_product = $this->_getParam('id_product', 0);
		$this->_redirect($this->_curModule . '/index/id_product/' . $id_product);
	}
	
	public function activateAction() {
		$id = $this->_getParam('id', 0);
		$item = Catalog_Product_Units::getInstance()->changeActivity($id);
		$id_product = $this->_getParam('id_product', 0);
		$this->_redirect($this->_curModule . '/index/id_product/' . $id_product);
	}
	
	public function mainAction() {
		$id = $this->_getParam('id', 0);
		$item = Catalog_Product_Units::getInstance()->find($id)->current();
		if (! is_null($item)) {
			$item->main = abs($item->main - 1);
			$item->save();
			Catalog_Product_Units::getInstance()->checkMain($item);
		}
		$id_product = $this->_getParam('id_product', 0);
		$this->_redirect($this->_curModule . '/index/id_product/' . $id_product);
	}
	
	public function ajaxsearchAction() {
		$id = $this->_getParam('id', 0);
		$search = $this->_getParam('q');
		$act = $this->_getParam('act', '');
		//echo $search;
		if ($id && $search) {
			//print_r($act);exit;
			$related = array();
			$rel_ids = array($id);
			if ($act == 'related') {
				$related = Catalog_Product_Related::getInstance()->fetchAll('id_product=' . (int)$id);
				if (count($related)) {
					foreach ($related as $item) {
						$rel_ids[] = $item->id_related_product;
					}
				}
			} elseif ($act == 'complect') {
				$related = Catalog_Product_Complect::getInstance()->fetchAll('id_unit=' . (int)$id);
				if (count($related)) {
					foreach ($related as $item) {
						$rel_ids[] = $item->id_complect_unit;
					}
				}
			}
			$rel_string = implode(',', $rel_ids);
			$where[] = "id NOT IN ($rel_string)";
			$where[] = "title LIKE '%$search%'";
			$res = Catalog_Product_Units::getInstance()->fetchAll($where);
			foreach ($res as $data) {
				print $data->title . "|" . $data->id . "\n";
			}
		}
		exit();
	}


	public function ajaxproductAction() {

        $id = $this->_getParam('id', 0);
        $search = $this->_getParam('q');
        $act = $this->_getParam('act', '');
        if ($id && $search) {
            $related = array();
            $rel_ids = array($id);
            if ($act == 'related') {
                $related = Catalog_Product_Related::getInstance()->fetchAll('id_product=' . (int)$id);
                if (count($related)) {
                    foreach ($related as $item) {
                        $rel_ids[] = $item->id_related_product;
                    }
                }
            }
            $rel_string = implode(',', $rel_ids);
            $where[] = "id NOT IN ($rel_string)";
            $where[] = "title LIKE '%$search%'";
            $where[] = "main = 1";
            $res = Catalog_Product_Units::getInstance()->fetchAll($where);
            foreach ($res as $data) {
                $pr_url = Catalog_Product::getInstance()->getUrlById($data->id_product);
                $div_url = Catalog_Division::getInstance()->getDivisionUrlByProductId($data->id_product);
                print $data->title . "|" . $data->img . "|" . $data->price . "|" . $pr_url. "|" .$div_url. "\n";
            }
        }
        exit();
    }
	
	public function complectAction() {
		$this->layout->action_title = "Сформировать комплект";
		$this->view->id = $id = $this->_getParam('id', 0);
		$this->view->unit = $unit = Catalog_Product_Units::getInstance()->find($id)->current();
		if ($unit) {
			$this->layout->action_title .= " элемента \"" . $unit->title . '"';
		}
		//Zend_Debug::dump($unit); exit();
		if ($this->_request->isPost()) {
			$comp_id = $this->_getParam('comp_id');
			$title = $this->_getParam('title');
			$price = $this->_getParam('price', 0);
			$count = $this->_getParam('count', 1);
			$priority = $this->_getParam('priority', 0);
			$array_count = $this->_getParam('items_count');
			$array_priority = $this->_getParam('items_priority');
			$transform = $this->_getParam('with_transform');
		    	$array_transform = $this->_getParam('items_transform');
		    	$main_unit = $this->_getParam('main_unit');
		    	$array_main_unit = $this->_getParam('items_main_unit');
		    	if ($transform == null) { $transform = 0;} else {$transform = 1;}
		    	if ($main_unit == null) { $main_unit = 0;} else {$main_unit = 1;}
			Catalog_Product_Complect::getInstance()->ProcessComplect($array_count, $array_priority, $array_transform, $array_main_unit);

			$complect_unit = Catalog_Product_Units::getInstance()->find($comp_id)->current();
			$comp_row = Catalog_Product_Complect::getInstance()->fetchRow('id_unit=' . (int)$id . ' AND id_complect_unit=' . (int)$comp_id);
			if ($comp_id && ! is_null($complect_unit) && is_null($comp_row)) {
				$complect_row = Catalog_Product_Complect::getInstance()->createRow();
				$complect_row->id_unit = $id;
				$complect_row->id_complect_unit = $comp_id;
				$complect_row->count = $count;
				$complect_row->priority = $priority;
                		$complect_row->with_transform = $transform;
                		$complect_row->main_unit = $main_unit;
				$complect_row->save();
			} elseif ($title != '') {
				$complect_row = Catalog_Product_Complect::getInstance()->createRow();
				$complect_row->id_unit = $id;
				$complect_row->id_complect_unit = 0;
				$complect_row->count = $count;
				$complect_row->priority = $priority;
				$complect_row->fixed_price = $price;
				$complect_row->fixed_title = $title;
                		$complect_row->with_transform = $transform;
                		$complect_row->main_unit = $main_unit;
				$complect_row->save();
			}
			$this->view->ok = 'Данные сохранены';
		}
		$this->view->complect = Catalog_Product_Complect::getInstance()->getUnitComplect($id);
	}
	
	public function compdeleteAction() {
		$id = $this->_getParam('id', 0);
		$comp_id = $this->_getParam('id_comp', 0);
		$product_id = $this->_getParam('id_product', 0);
		$row = Catalog_Product_Complect::getInstance()->fetchRow('id_unit=' . (int)$id . ' AND id_complect_unit=' . (int)$comp_id);
		if ($row) {
			$row->delete();
		}
		$this->_redirect('/' . $this->view->currentModule . '/complect/id/' . $id . '/id_product/' . $product_id);
	}


	public function recountAction() {
        $this->_helper->layout()->disableLayout();
        if (!$this->_request->isXmlHttpRequest()) {
            exit();
        }
        else {
            $product_id = $this->_getParam('id');
            //$bitmaps = Catalog_Bitmaps_Items::getInstance()->getInstance()->getItemsAsOptions($bitmap);
            //$this->view->bitmaps = $bitmaps;
            $units = Catalog_Product_Units::getInstance()->getActiveByProductId($product_id);
            foreach ($units as $unit) {
                $complects = Catalog_Product_Complect::getInstance()->getComplect($unit['id']);
                //Zend_Debug::dump($complects);
                if (count($complects) > 0) {
                    $price = 0;
                    foreach ($complects as $item) {
                        if ($item->id_complect_unit) {
                            if ($item->main_unit == 1) {
                                $price += $item->price * $item->count;
                            }
                            else {
                                if ($item->with_transform == 1) {
                                    $price += $item->price_with_tr * $item->count;
                                }
                                else {
                                    $price += $item->price * $item->count;
                                }
                            }
                        } else {
                            $price += $item->fixed_price * $item->count;
                        }
                    }
                    if ($unit->price != $price) {
                        $unit->price = $price;
                        Catalog_Product_Units::getInstance()->update(array('price' => $price), 'id = ' . $unit['id']);
                    }
                    //Zend_Debug::dump($price);
                }

                if (count($complects) > 0) {
                    $price_with_tr = 0;
                    foreach ($complects as $item) {
                        if ($item->id_complect_unit) {
                            if ($item->main_unit == 1) {
                                $price_with_tr += $item->price_with_tr * $item->count;
                            }
                            else {
                                if ($item->with_transform == 1) {
                                    $price_with_tr += $item->price_with_tr * $item->count;
                                }
                                else {
                                    $price_with_tr += $item->price * $item->count;
                                }
                            }
                        } else {
                            $price_with_tr += $item->fixed_price * $item->count;
                        }
                    }
                    if ($unit->price_with_tr != $price_with_tr) {
                        $unit->price_with_tr = $price_with_tr;
                        Catalog_Product_Units::getInstance()->update(array('price_with_tr' => $price_with_tr), 'id = ' . $unit['id']);
                    }
                    //Zend_Debug::dump($price_with_tr);
                }
            }
            $this->view->recount_price = "ok";
        }
    }
}
