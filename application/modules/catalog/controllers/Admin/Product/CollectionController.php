<?php

class Catalog_Admin_Product_CollectionController extends MainAdminController {
	
	/**
	 * @var string
	 */
	private $_curModule = null;
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 50;
	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_lang = null;
	
	private $_page = null;
	/**
	 * offset
	 *
	 * @var int
	 */
	private $_offset = null;
	
	public function init() {
		//$this->initView();
		$this->layout = $this->view->layout();
		$this->layout->title = "Добавление коллекций";
		$this->view->caption = 'Коллекции';
		$lang = $this->_getParam('lang', 'ru');
		
		$this->view->currentModule = $this->_curModule = SP . $this->getRequest()->getModuleName() . SP . $lang . SP . $this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);
		$this->_offset = ($this->_page - 1) * $this->_onpage;
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		$this->layout->action_title = "Список элементов";
		$this->view->currentModule = $this->_curModule;
        	if ($this->_request->isPost()) {
			$text = $this->_getParam('search_all_text');
		        $collection_ids = Catalog_Product::getInstance()->collectionsearch($text);
			$this->view->collection = $collection_ids;
		}
	}

	public function savecollectionsAction() {
	$id_products = $this->_getParam('id_products');
	$id_disable = $this->_getParam('id_disabled');
	$id_products = substr($id_products, 0, strlen($id_products)-1);
	$id_disable = substr($id_disable, 0, strlen($id_disable)-1);
	$product_array = explode(",", $id_products);
	$disable_array = explode(",", $id_disable);
	Zend_Debug::dump($disable_array);
	foreach ($product_array as $product) {
		foreach ($product_array as $product_next) {
		if ($product != $product_next & !in_array($product_next, $disable_array)) {
	        $rel_row = Catalog_Product_Collections::getInstance()->fetchRow('id_product='.$product.' AND id_collection_product='.$product_next);
		$related_row = Catalog_Product_Collections::getInstance()->createRow();
                $related_row->id_product = $product;
                $related_row->id_collection_product = $product_next;
                $related_row->save();
		}
		}
	}
        exit();
	}

}
