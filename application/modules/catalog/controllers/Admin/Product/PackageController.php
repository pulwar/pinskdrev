<?php
/**
 * Catalog_Admin_Product_PackageController
 */
class Catalog_Admin_Product_PackageController extends MainAdminController {
	/**
	 * @var string
	 */
	private $_curModule = null;
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 50;
	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_lang = null;
	
	private $_page = null;
	/**
	 * offset
	 *
	 * @var int
	 */
	private $_offset = null;
	
	public function init() {
		//$this->initView();
		$this->layout = $this->view->layout();
		$this->layout->title = "Доставка";
		$this->view->caption = 'Опции';
		$lang = $this->_getParam('lang', 'ru');
		
		$this->view->currentModule = $this->_curModule = SP . $this->getRequest()->getModuleName() . SP . $lang . SP . $this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);
		$this->_offset = ($this->_page - 1) * $this->_onpage;
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		$this->layout->action_title = "Список элементов";
		$this->view->currentModule = $this->_curModule;
        $id = $this->_getParam('id_product', '');
        //Zend_Debug::dump($id);exit();
        $this->view->product = $id;
        $where = 'id_tovar = '.$id;
		$this->view->total = count(Catalog_Product_Package::getInstance()->fetchAll($where));
		$this->view->all = Catalog_Product_Package::getInstance()->fetchAll($where, 'priority DESC', (int)$this->_onpage, (int)$this->_offset);
	}
	
	public function editAction() {
		$id = $this->_getParam('id', '');
        $id_product = $this->_getParam('id_product', '');
        $params = $this->_getAllParams();
        //Zend_Debug::dump($params);exit();
        if ($id) {
			$item = Catalog_Product_Package::getInstance()->find($id)->current();
			$this->layout->action_title = "Редактировать элемент";
		} else {
			$item = Catalog_Product_Package::getInstance()->fetchNew();
			$this->layout->action_title = "Создать элемент";
		}
		if ($this->_request->isPost()) {
			$data = $this->trimFilter($this->_getAllParams());
			if ($data['name'] != '') {
				$item->setFromArray(array_intersect_key($data, $item->toArray()));
				$id = $item->save();
				$this->view->ok = 1;
			} else {
				$this->view->err = 1;
				$item->setFromArray(array_intersect_key($data, $item->toArray()));
			}
		}
		$this->view->item = $item;
        $this->view->product = $id_product;
	}
	
	public function deleteAction() {
		$id = $this->_getParam('id');
		if ($id) {
			$item = Catalog_Product_Package::getInstance()->find($id)->current();
			if ($item != null) {
                $id_product = $item->id_tovar;
				$item->delete();
			}
		}
		$this->_redirect($this->_curModule.'/index/id_product/'.$id_product);
	}
}