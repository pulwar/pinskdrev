<?php
class Catalog_ComparisonController extends Zend_Controller_Action {
/**
 * Шаблон страницы
 * @var zend_layout
 */
    private $layout = null;

    private $_comp = null;

    
    private $_page = null;

    private $_id_product = null;


    public function init() {
		$this->_order = new Zend_Session_Namespace('order');
        $this->_id_product = $this->_getParam('id_product');
        $this->_division = $this->_getParam('division', 0);
        $this->_url_cat = Catalog_Division::getInstance()->getUrlById($this->_division);
        $this->_comp = new Zend_Session_Namespace('comp');
        if (!isset($this->_comp->products)) {
            $this->_comp->products = array();
        }
        $action = $this->_getParam('act');
        // process action
        switch ($action) {
            case 'add': $this->_forward('add')
                ;
                break;
            case 'remove': $this->_forward('remove')
                ;
                break;
            case 'recalc': $this->_forward('recalc')
                ;
                break;
            case 'clear': $this->_forward('clear')
                ;
                break;
            case 'change': $this->_forward('change')
                ;
                break;
            case 'addcustom':
                $this->_forward('addcustom');
                break;

            default: $this->_forward('index')
                ;
                break;
        }

        $this->view->addHelperPath( Zend_Registry::get( 'helpersPaths' ), 'View_Helper' );
        $id = $this->_getParam( 'id' );
        $page = Pages::getInstance()->getPage( $this->_getParam( 'id' ) );
        if (is_null( $page ) || $page->published == '0') {
            $this->_redirect( '/404' );
        }
        $this->_page = $page;
        $this->layout = $this->view->layout();
        $this->layout->setLayout( "front/default" );
        $this->layout->current_type = 'pages';
        $this->layout->page = $this->_page;

        $this->view->content = $page->content;
        $options = PagesOptions::getInstance()->getPageOptions( $id );
        $this->view->placeholder( 'title' )->set( $options->title );
        $this->view->placeholder( 'keywords' )->set( $options->keywords );
        $this->view->placeholder( 'descriptions' )->set( $options->descriptions );
        $this->view->placeholder( 'h1' )->set( $options->h1 );




    }

    /**
     * просмотр корзины
     */
    public function indexAction() {
        $this->view->catalog_path = $path = SiteDivisionsType::getInstance()->getPagePathBySystemName('division');
       
        if (is_array($this->_comp->products)) {
            
            $this->view->products = $this->_comp->products;
			$this->view->order_params = $this->_order->products;

            }
      }

     /**
     * формирование заказа для отправки по почте

     */
    

    /**
     * добавление товара для сравнения
     * @todo другой title для товаров с id_price
     */
    public function addAction() {
        $key = $this->_id_product;
        $width = $this->_getParam('width', 0);
        if ($this->_id_product && !array_key_exists($key ,$this->_comp->products)) {
            if(count($this->_comp->products)>=5 && $width>1024){
                echo "5";
            }elseif(count($this->_comp->products)>=3 && $width<=1024){
                echo "3";
            }else{
                $product = Catalog_Product::getInstance()->getPublicItem($this->_id_product);
                $price = null;
                $title = null;


                if ($product!=null) {
                    $this->_comp->products[$key] = array(
                        'title' 		=> $title!=null ? $product->title." ".$title : $product->title,
                        'price' 		=> $price!=null ? $price : $product->price,
                        'id_product'	=> $this->_id_product,
                        'division'          => $this->_division,
                        'product_url'       => $product->url,
                        'division_url'      => $this->_url_cat,
                        'img'               => $product->img,
						'img_big'               => $product->img_big,
                        'length'            => $product->pr_length,
                        'width'             => $product->pr_width,
                        'height'            => $product->pr_height,
                        'maker'             => $product->maker,
                        'material'          => $product->material,
                        'description'       => $product->intro
                        //'id_price'		=> $this->_getParam('id_price')

                    );
                    echo 'ok';
                }
            }
        }elseif ($this->_id_product && array_key_exists	($key ,$this->_comp->products)) {
            
            echo 'exist';
        }else {
            echo 'err';
        }
        exit;
    }


    

    /**
     * удаление товара из списка
     */
    public function removeAction() {
        if(array_key_exists($this->_id_product ,$this->_comp->products)) {
            unset($this->_comp->products[$this->_id_product]);
           // echo 'ok';
        }
        //exit;
        $this->_redirect('/'.$this->_page->path);
    }
   
    /**
     * очитска списка
     */
    public function clearAction() {
        $this->_comp->products = null;
        $this->_redirect('/'.$this->_page->path);

    }

    public function recalcAction() {
        echo (int)count($this->_comp->products);

        exit;
    }

    

}
