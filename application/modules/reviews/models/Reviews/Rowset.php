<?php

class Reviews_Rowset extends Zend_Db_Table_Rowset {

	/**
	 * @return array
	 */
	public function toArray(){
		return parent::toArray();
	}
	/**
	 * gets current row
	 *
	 * @return Reviews_Row
	 */
	public function current(){
		return parent::current();
	}
	
	
}