<?php

/**
 * Layout Helper
 *
 */
class View_Helper_Menu extends Zend_View_Helper_Abstract {
	
	/**
	 * @var Zend_View
	 */
	protected $_view = null;
	
	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		$this->_view = Zend_Registry::get('view');
	}
	
	/**
	 * Site menu
	 *
	 * @param string $type
	 * @param string $version
	 * @return phtml
	 */
	public function Menu($type, $version, $current = 0) {
		$this->init();
		
		$menu_rowset = Menu::getInstance()->getMenu($type, 2);
		$result = array();
		$infor = array();
		if ($menu_rowset != null) {
			foreach ($menu_rowset as $key => $item) {
				if ($item->level == 1) {
					$menu[$item->id]['item'] = $item;
					if ($item->div_type == 'makers'){
						$makers = Makers::getInstance()->getMakersForMenu($item->path);
						foreach($makers as $mk => $mv){
							$menu[$item->id]['childs'][] = $mv;
						}
					}
					unset($menu_rowset[$key]);
				}
			}
			foreach ($menu_rowset as $key => $second_level_item) {
				if ($second_level_item->level == 2) {
					$menu[$second_level_item->parentId]['childs'][] = $second_level_item;
					unset($menu_rowset[$key]);
				}
			}
		}
		$catalogMenu = array();
		$catalogMenu[] = Catalog_Division_Cache::getInstance()->find(123)->current();
		$this->view->catalogMenu = $catalogMenu;
		
		//Zend_Debug::dump($catalogMenu); exit;
		
		$this->_view->lang = $version;
		$this->_view->menu = $menu;
		$this->_view->current = $current;
		$this->_view->current = $current;
		return $this->_view->render($type . '.phtml');
	}
	
	private function getPage($id, $version) {
		$page = Pages::getInstance()->getPage($id);
		if ($page->version != 'ru') {
			$page->path = $page->version . '/' . $page->path;
		}
		return $page;
	}
} 