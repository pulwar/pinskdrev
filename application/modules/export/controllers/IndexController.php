<?php
class export_IndexController extends Zend_Controller_Action {
    /**
     * Шаблон страницы
     * @var zend_layout
     */
    private $layout = null;

    /**
     * Язык страницы
     * @var string
     */
    private $lang = null;

    /**
     * Объект страницы
     * @var Pages_row
     */
    private  $_page = null;


    public function indexAction() {
	//Zend_Debug::dump($_SERVER['REMOTE_ADDR']);
        if ($_SERVER['REMOTE_ADDR'] == '46.4.123.244') {
        $lang = $this->getRequest()->getParam('lang','ru');
        $curModul = SP.'export'.SP.$lang.SP.$this->getRequest()->getControllerName();
        $id = $this->getRequest()->getParam('id');
	$url_to_file = ROOT_DIR.'files/export/yandex.txt';
        $file = file($url_to_file);
        $content=unserialize($file[0]);
	$url = ROOT_DIR.'export.xml';
        $handle = fopen($url, "w");
        $creation_date = date('Y-n-d H:i');
        $server = 'http://' . $_SERVER['SERVER_NAME'];

        //косяк для xml
        $result_xml_string = '<?xml version="1.0" encoding="utf-8"?>
        <!DOCTYPE yml_catalog SYSTEM "shops.dtd">
        <yml_catalog date="' . $creation_date . '">
            <shop>
              <name>'.$content['name'].'</name>
              <company>'.$content['company'].'</company>
              <url>' . $server . '/' . '</url>

              <currencies>
                  <currency id="RUR" rate="1"/>
                  <currency id="USD" rate="CBRF"/>
              </currencies>
              <categories></categories>
              <offers></offers>
        </shop>
        </yml_catalog>
        ';

        //здесь будет вся xml
        $yml = simplexml_load_string($result_xml_string);

        //берем все корневые каталоги
        $root_items = Catalog_Division::getInstance()->fetchAll('parent_id = 0 AND level = 0 AND active = 1');

        $this->shop = $yml->shop;

        //добавляем все каталоги в xml
        foreach ($root_items as $key => $root_item) {
            $category = $this->shop->categories->addChild('category', $root_item->name);
            $category->addAttribute('id', $root_item->id);
            $this->addCatalog($root_item->id);
        }

        $i=0;
        $products = Catalog_Product::getInstance()->getAllPublic();
        if (count($products) > 0) {
            foreach ($products as $product) {
                if ((int)$product->price == 0 && (int)$product->price_with_tr == 0) {
                } else {
                    if ((int)$product->price == 0) {
                        $price = (int)$product->price_with_tr;
                    } else {
                        $price = (int)$product->price;
                    }

                    $offer = $yml->shop->offers->addChild('offer');
                    $offer->addAttribute('id', $product->id);
                    //											$offer->addAttribute('type', 'vendor.model');
                    $offer->addAttribute('available', 'false');
                    $offer->addChild('url', $server . '/product/' . $product->url);

                    $offer->addChild('price', $price);
                    $offer->addChild('currencyId', 'RUR');
                    $offer->addChild('categoryId', $product->id_division);

                    $image_path = ($product->img!='')? $server . '/pics/catalog/product/img/'.$product->img : '';
                    $offer->addChild('picture', $image_path);

                    $offer->addChild('delivery',  'true');
                    $offer->addChild('name', $content['vendor'] . ' ' . $product->title);

                    //                $offer->addChild('vendor', 'Пинскдрев');
                    //заменил описание на краткое описание
                    $product->intro = strip_tags($product->intro);

                    $table = get_html_translation_table(HTML_ENTITIES);

                    foreach ($table as $key => $val) {
                        $table_new[$val] = iconv('windows-1251', 'utf-8', $key);
                    }

                    $product->intro = strtr($product->intro, $table_new);

                    $product->intro = htmlspecialchars($product->intro);
                    $product->intro = str_replace("&#13;",'',$product->intro );
                    if ($product->intro != '') {
                        $offer->addChild('description', $product->intro);
                    }
                    $offer->addChild('sales_notes', 'Предоплата от 10 процентов');
                    $offer->addChild('manufacturer_warranty',  'true');
                    $offer->addChild('country_of_origin', $content['country']);
                    $i++;
                }
            }
        }

        $xml = $yml->asXML();

        $xml = iconv('utf-8', 'windows-1251', $xml);
        $xml = str_replace('utf-8', 'windows-1251', $xml);
        $xml = str_replace('ndash;', '-', $xml);

        file_put_contents('export.xml', $xml);
    } 
	else {exit();}
    }

    public function addCatalog($id) {
        $items = Catalog_Division::getInstance()->fetchAll('parent_id = ' . $id. ' AND active = 1');
        if (count($items)) {
            foreach($items as $item) {
                $category = $this->shop->categories->addChild('category', $item->name);
                $category->addAttribute('id', $item->id);
                $category->addAttribute('parentId', $item->parent_id);
                $this->addCatalog($item->id);
            }
        }
    }

}
