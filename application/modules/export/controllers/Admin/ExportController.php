<?php

class Export_Admin_ExportController extends MainAdminController {
    /**
     *
     * @var int
     */
    private $_id_page = null;
    /**
     * Pages_row
     * @var object
     */
    private $_owner = null;

    public function init() {
        //       $this->view->addHelperPath(DIR_HELPER , 'View_Helper') ;
//        $this->_id_page = $this->_getParam('id_page');
//        $this->view->id_page = $this->_id_page;
//        if ($this->_id_page!=null) {
//            $this->_owner = Pages::getInstance()->find($this->_id_page)->current();
//            if ($this->_owner!=null) {
//                $this->view->layout()->title = $this->_owner->name;
//            }
//        }
        $this->view->lang = $lang = $this->_getParam('lang','ru');
        $this->view->currentModul = SP.'export'.SP.$lang.SP.$this->getRequest()->getControllerName();
    }

    public function indexAction() {
        $this->view->layout()->action_title = "Список элементов";
        $this->view->export = $this->getRequest()->getParam('export');

    }




    public function editAction() {
        $id = $this->getRequest()->getParam('id');
        $this->view->layout()->action_title = "Редактировать параметры для экспорта товаров для $id";
        $lang = $this->getRequest()->getParam('lang','ru');
        $this->view->currentModul = $curModul = SP.'export'.SP.$lang.SP.$this->getRequest()->getControllerName();
        $this->view->lang = $lang;
        $file = file('files/export/'.$id.'.txt');
        $content=$file[0];
        $this->view->content = unserialize($content);

        if ($this->_request->isPost()) {
            $id = $this->getRequest()->getParam('id');
            $data = $this->getRequest()->getParam('export');
            $new_data=serialize($data);
            $fileop = fopen ('files/export/'.$id.'.txt','w+');
            fputs ($fileop, $new_data);
            fclose ($fileop);
            //var_dump($new_data);
            $this->_redirect($curModul);
            //$this->render('editm');
        }


    }
    public function editmAction() {

        $this->view->layout()->action_title = "Редактировать параметры для экспорта товаров для Mail.ru";
        $lang = $this->getRequest()->getParam('lang','ru');
        $this->view->currentModul = $curModul = SP.'export'.SP.$lang.SP.$this->getRequest()->getControllerName();
        $this->view->lang = $lang;
        $file = file('files/export/mailru.txt');
        $content=$file[0];
        $this->view->content = unserialize($content);

        if ($this->_request->isPost()) {
            $id = $this->getRequest()->getParam('id');
            $data = $this->getRequest()->getParam('export');
            $new_data=serialize($data);
            $fileop = fopen ('files/export/mailru.txt','w+');
            fputs ($fileop, $new_data);
            fclose ($fileop);
            //var_dump($new_data);
            $this->_redirect($curModul);
        }


    }
    public function edityAction() {

        $this->view->layout()->action_title = "Редактировать параметры для экспорта товаров для Yandex";
        $lang = $this->getRequest()->getParam('lang','ru');
        $this->view->currentModul = $curModul = SP.'export'.SP.$lang.SP.$this->getRequest()->getControllerName();
        $this->view->lang = $lang;
        $file = file('files/export/yandex.txt');
        $content=$file[0];
        $this->view->content = unserialize($content);

        if ($this->_request->isPost()) {
            $id = $this->getRequest()->getParam('id');
            $data = $this->getRequest()->getParam('export');
            $new_data=serialize($data);
            $fileop = fopen ('files/export/yandex.txt','w+');
            fputs ($fileop, $new_data);
            fclose ($fileop);
            //var_dump($new_data);
            $this->_redirect($curModul);
        }


    }



    public function exportAction() {
        $lang = $this->getRequest()->getParam('lang','ru');
        $curModul = SP.'export'.SP.$lang.SP.$this->getRequest()->getControllerName();
        $id = $this->getRequest()->getParam('id');
        $file = file('files/export/'.$id.'.txt');
        $content=unserialize($file[0]);
        $handle = fopen("/export.xml", "w");

        if($id=='mailru'){


            $creation_date = date('Y-n-d H:i');
            $server = 'http://' . $_SERVER['SERVER_NAME'];

            //косяк для xml
            $result_xml_string = '<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<torg_price date="' . $creation_date . '">

	<shop>
           <update_type>0</update_type>
	  <shopname>'.$content['shopname'].'</shopname>
	  <company>'.$content['company'].'</company>
	  <url>' . $server . '/' . '</url>
          <action></action>
            <action_url></action_url>
	  <currencies>
		  <currency id="RUR" rate="1"/>
		  <currency id="USD" rate=""/>
	  </currencies>
	  <categories></categories>
	  <offers></offers>
</shop>
</torg_price>
';

            //здесь будет вся xml
            $yml = simplexml_load_string($result_xml_string);

            //берем все корневые каталоги
            $root_items = Catalog_Division::getInstance()->fetchAll('parent_id = 0 AND level = 0 AND active = 1');

            $this->shop = $yml->shop;

            //добавляем все каталоги в xml
            foreach ($root_items as $key => $root_item) {
                $category = $this->shop->categories->addChild('category', $root_item->name);
                $category->addAttribute('id', $root_item->id);
                $category->addAttribute('parentId', $root_item->parent_id);
                $this->addCatalog($root_item->id);
            }


            //добавляем все товары xml
            $products = Catalog_Product::getInstance()->getAllProducts();
            if (count($products) > 0) {
                foreach ($products as $product) {
//                $price = $this->getDefaultPrice($product);
//                if ($price == 0 && (int)$product->price == 0) {
//                    continue;
//                }
//				$product_image = $product->getImgMain();
//				if (is_null($product_image)){
//						// обрабатывать только товары с картинками
//					  continue;
//				}

                    //if ($price == 0) {
                    $price = (int)$product->price;
                    //}

                    $offer = $yml->shop->offers->addChild('offer');
                    $offer->addAttribute('id', $product->id);
                    //											$offer->addAttribute('type', 'vendor.model');
                    $offer->addAttribute('type', 'good');
                    $offer->addAttribute('mpc', $content['mpc']);
                    $offer->addChild('url', $server . '/product/' . $product->url);

                    $offer->addChild('price', $price);
                    $offer->addChild('price2', $price);
                    $offer->addChild('currencyId', 'RUR');
                    $offer->addChild('categoryId', $product->division);

                    $image_path = ($product->img!='')? $server . $this->thumb('/pics/catalog/product/img/'.$product->img) : '';

                    $offer->addChild('picture', $image_path);
                    $offer->addChild('vendor', $content['vendor']);
                    $offer->addChild('name', $product->title);
                    $product->intro = strip_tags($product->intro);
                    $product->intro = htmlspecialchars($product->intro);
                    $product->intro = str_replace("&#13;",'',$product->intro );
                    $offer->addChild('description', $product->intro);
                    $offer->addChild('descmore', $server . '/product/' . $product->url.'/');
                    $offer->addChild('delivery_type', 'есть');
                    $offer->addChild('delivery_price', '+');


                    //заменил описание на краткое описание
                    //$product->intro = strip_tags($product->intro);

                    $table = get_html_translation_table(HTML_ENTITIES);

                    foreach ($table as $key => $val) {
                        $table_new[$val] = iconv('windows-1251', 'utf-8', $key);
                    }
                    $product->intro = strtr($product->intro, $table_new);
                }
            }


            $xml = $yml->asXML();
            $xml = iconv('utf-8', 'windows-1251', $xml);
            $xml = str_replace('utf-8', 'windows-1251', $xml);
            $xml = str_replace('ndash;', '-', $xml);

            file_put_contents('mailru.xml', $xml);
        }


        elseif($id=='yandex'){


            $creation_date = date('Y-n-d H:i');
            $server = 'http://' . $_SERVER['SERVER_NAME'];

            //косяк для xml
            $result_xml_string = '<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="' . $creation_date . '">
	<shop>
	  <name>'.$content['name'].'</name>
	  <company>'.$content['company'].'</company>
	  <url>' . $server . '/' . '</url>

	  <currencies>
		  <currency id="RUR" rate="1"/>
		  <currency id="USD" rate="CBRF"/>
	  </currencies>
	  <categories></categories>
	  <offers></offers>
</shop>
</yml_catalog>
';

            //здесь будет вся xml
            $yml = simplexml_load_string($result_xml_string);

            //берем все корневые каталоги
            $root_items = Catalog_Division::getInstance()->fetchAll('parent_id = 0 AND level = 0 AND active = 1');

            $this->shop = $yml->shop;

            //добавляем все каталоги в xml
            foreach ($root_items as $key => $root_item) {
                $category = $this->shop->categories->addChild('category', $root_item->name);
                $category->addAttribute('id', $root_item->id);
                $this->addCatalog($root_item->id);
            }

	    $i=0;
            $products = Catalog_Product::getInstance()->getAllPublic();
            //Zend_Debug::dump($products);exit();
            if (count($products) > 0) {
                foreach ($products as $product) {
		  if ((int)$product->price == 0 && (int)$product->price_with_tr == 0) {
		  } else {
//                $price = $this->getDefaultPrice($product);
//                if ($price == 0 && (int)$product->price == 0) {
//                    continue;
//                }
                    //$product_image = $product->getImgMain();
//				if (is_null($product_image)){
//						// обрабатывать только товары с картинками
//					  continue;
//				}

                    if ((int)$product->price == 0) {
                    $price = (int)$product->price_with_tr;
                    } else {
                        $price = (int)$product->price;
                    }

                    $offer = $yml->shop->offers->addChild('offer');
                    $offer->addAttribute('id', $product->id);
                    //											$offer->addAttribute('type', 'vendor.model');
                    $offer->addAttribute('available', 'false');
                    $offer->addChild('url', $server . '/product/' . $product->url);

                    $offer->addChild('price', $price);
                    $offer->addChild('currencyId', 'RUR');
                    $offer->addChild('categoryId', $product->id_division);

                    $image_path = ($product->img!='')? $server . '/pics/catalog/product/img/'.$product->img : '';
                    $offer->addChild('picture', $image_path);

                    $offer->addChild('delivery',  'true');
                    $offer->addChild('name', $content['vendor'] . ' ' . $product->title);

                    //                $offer->addChild('vendor', 'Пинскдрев');
                    //заменил описание на краткое описание
                    $product->intro = strip_tags($product->intro);

                    $table = get_html_translation_table(HTML_ENTITIES);

                    foreach ($table as $key => $val) {
                        $table_new[$val] = iconv('windows-1251', 'utf-8', $key);
                    }

                    $product->intro = strtr($product->intro, $table_new);

                    $product->intro = htmlspecialchars($product->intro);
                    $product->intro = str_replace("&#13;",'',$product->intro );
                    if ($product->intro != '') {
                    $offer->addChild('description', $product->intro);
                    }
		    $offer->addChild('sales_notes', 'Предоплата от 10 процентов');
                    $offer->addChild('manufacturer_warranty',  'true');
                    $offer->addChild('country_of_origin', $content['country']);
		    $i++;
                }
		}
            }

            $xml = $yml->asXML();

            $xml = iconv('utf-8', 'windows-1251', $xml);
            $xml = str_replace('utf-8', 'windows-1251', $xml);
            $xml = str_replace('ndash;', '-', $xml);

            file_put_contents('export.xml', $xml);
        }
        $this->_redirect($curModul.'/index/export/ok');
    }










    public function addCatalog($id) {
        $items = Catalog_Division::getInstance()->fetchAll('parent_id = ' . $id. ' AND active = 1');
        if (count($items)) {
            foreach($items as $item) {
                $category = $this->shop->categories->addChild('category', $item->name);
                $category->addAttribute('id', $item->id);
                $category->addAttribute('parentId', $item->parent_id);
                $this->addCatalog($item->id);
            }
        }
    }

    public function getDefaultPrice($product) {
        if ($product->swf_data_file!='') {
            $def_conf = '';
            if ($product->default_swf_config!='') {
                $def_conf = $product->default_swf_config;
            } else {
                $def_conf = file_get_contents(DIR_PUBLIC.'/files/catalog/tovar/data/'.$product->swf_data_file);
            }
            if (preg_match('/<item[^>].*mc=\"priceMc\"[^>].*price=\"([\d]+).*>/', $def_conf, $matches)) {
                $price = (int)$matches[1];
                return $price;
            }
        }
        return 0;
    }
}
