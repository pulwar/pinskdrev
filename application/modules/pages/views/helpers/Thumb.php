<?php

/**
 * Layout Helper
 * вывод хлебных крошек
 * 
 *
 */
class View_Helper_thumb extends Zend_View_Helper_Abstract {
	
	/**
	 * @var Zend_View
	 */
	protected $_view = null;

	/**
	 * Enter description here...
	 *
	 */
	public function init() {
		$this->_view = Zend_Registry::get('view');
	}

	/**
	 * изменение размеров изображения
	 * @param string $img_path
	 * @param string $w ширина 
	 * @param stirng $h высота
	 * @param int $t тип ресайза
	 */
	public function thumb($img_path, $w = 0, $h = 0, $no_protect = 1, $t = 0) {
		//define("_I_CACHING","1");               //    Chaching enabled, 1 - yes, 0 - no
		//define("_I_CACHE_PATH",ROOT_DIR."cached_images/"); //    Path to cache dir
		$cashe_path = ROOT_DIR . "cached_images/";
		if (!is_dir($cashe_path)) {
			mkdir($cashe_path, 0777);
		}
		$img = urldecode($img_path);
		$type = 0; //$t;
		$img = ROOT_DIR . $img;
		
		if (file_exists($img)) {
			$new_width = (int) $w;
			$new_height = (int) $h;
			
			$cached_filename = md5($img . filemtime($img) . $new_width . $new_height) . '.jpg';
			$path_to_cache_file = $cashe_path . $new_width . "x" . $new_height;
			$path_to_img = '/cached_images/' . $new_width . "x" . $new_height . '/' . $cached_filename;
			$cache = $path_to_cache_file . '/' . $cached_filename;
			
			//if(_I_CACHING == "1")
			
			if (file_exists($cache)) {
				return $path_to_img;
				exit();
			}
			
			$filename = $img;
			$lst = GetImageSize($filename);
			
			$image_width = $lst[0];
			$image_height = $lst[1];
			$image_format = $lst[2];
			$format = strtolower(substr($lst['mime'], strpos($lst['mime'], '/') + 1));
			$icfunc = "imagecreatefrom" . $format;
			$scfunc = "image" . $format;
			
			$x_ratio = $w / $lst[0];
			$y_ratio = $h / $lst[1];
			$ratio = min($x_ratio, $y_ratio);
			$use_x_ratio = ($x_ratio == $ratio);
			
			$old_size_x = $lst[0];
			$old_size_y = $lst[1];
			
			$source_x = 0;
			$source_y = 0;
			switch ($type) {
				case 0 :
					$image_width = $use_x_ratio ? $new_width : floor($lst[0] * $ratio);
					$image_height = !$use_x_ratio ? $new_height : floor($lst[1] * $ratio);
					break;
				case 1 :
					$image_width = $new_width;
					$image_height = $image_height;
					break;
				
				case 2 :
					$image_width = $new_width;
					$image_height = $new_height;
					break;
				case 333 : //fill image
					$cacl_width = $image_height / $new_height * $new_width;
					if ($cacl_width < $image_width) { //y-fidex size
						$source_x = round(($image_width - $cacl_width) / 2);
						$old_size_x = $cacl_width;
					} else { //x-fidex size
						$cacl_height = $image_width / $new_width * $new_height;
						$source_y = round(($image_height - $cacl_height) / 2);
						$old_size_y = $cacl_height;
					}
					;
					$image_width = $new_width;
					$image_height = $new_height;
					break;
			}
			
			if ($image_format == 1) {
				$old_image = imagecreatefromgif($filename);
			} elseif ($image_format == 2) {
				$old_image = imagecreatefromjpeg($filename);
			} elseif ($image_format == 3) {
				$old_image = imagecreatefrompng($filename);
			} else {
				return $img_path;
			}
			
			$new_image = imageCreateTrueColor($image_width, $image_height);
			$white = ImageColorAllocate($new_image, 255, 255, 255);
			ImageFill($new_image, 0, 0, /*$white*/ $rgb = 0xFFFFFF);
			
			imagecopyresampled($new_image, $old_image, 0, 0, $source_x, $source_y, $image_width, $image_height, $old_size_x, $old_size_y);
			if ($image_width > 200 && $no_protect == 0) {
				$new_image = $this->potect_image($new_image, $new_width, $new_height);
			}
			if ($image_width < 100 && $no_protect == 0) {
				$new_image = $this->potect_image3($new_image, $new_width, $new_height);
			}
			if ($image_width < 200 && $no_protect == 0 && $image_width > 100) {
				$new_image = $this->potect_image2($new_image, $new_width, $new_height);
			}
			//if(_I_CACHING == "1")
			//{
			if (!is_dir($path_to_cache_file)) {
				@mkdir($path_to_cache_file);
			}
			$scfunc($new_image, $cache);
			@chmod($cache, 0777);
			//}
			return $path_to_img;
		}
	}

	function potect_image($img_source, $lst, $hght) {
		$text = ROOT_DIR . "text.png";
		$text_size = getimagesize($text);
		$text_source = imagecreatefrompng($text);
		//$x_text = ($lst[0] - $text_size[0])/2;
		//$y_text = ($lst[1] - $text_size[1])/2;
		//print_r($lst);
		$x_text = $lst - $text_size[0];
		$y_text = ($hght - $text_size[1]) / 2;
		
		if (imagecopy($img_source, $text_source, $x_text - 15, $y_text, 0, 0, $text_size[0], $text_size[1])) {
			//$scfunc($img_source, $img_dest, 95);
			return $img_source;
		}
		return $img_source;
	}

	function potect_image2($img_source, $lst, $hght) {
		$text = ROOT_DIR . "logo2.png";
		$text_size = getimagesize($text);
		$text_source = imagecreatefrompng($text);
		//$x_text = ($lst[0] - $text_size[0])/2;
		//$y_text = ($lst[1] - $text_size[1])/2;
		//print_r($lst);
		$x_text = ($lst - $text_size[0]) / 2;
		$y_text = ($hght - $text_size[1]) / 2;
		
		if (		//imagecopy($img_source,$logo_source, $x, $y, 0, 0, $logo_size[0], $logo_size[1] ) &&
imagecopy($img_source, $text_source, $x_text - 75, $y_text, 0, 0, $text_size[0], $text_size[1])) {
			//$scfunc($img_source, $img_dest, 95);
			return $img_source;
		}
		return $img_source;
	}

	function potect_image3($img_source, $lst, $hght) {
		$text = ROOT_DIR . "logo3.png";
		$text_size = getimagesize($text);
		$text_source = imagecreatefrompng($text);
		//$x_text = ($lst[0] - $text_size[0])/2;
		//$y_text = ($lst[1] - $text_size[1])/2;
		//print_r($lst);
		$x_text = ($lst - $text_size[0]) / 2;
		$y_text = ($hght - $text_size[1]) / 2;
		
		if (		//imagecopy($img_source,$logo_source, $x, $y, 0, 0, $logo_size[0], $logo_size[1] ) &&
imagecopy($img_source, $text_source, $x_text - 115, $y_text, 0, 0, $text_size[0], $text_size[1])) {
			//$scfunc($img_source, $img_dest, 95);
			return $img_source;
		}
		return $img_source;
	}
}
