<?php

/**
 * Layout Helper
 *
 */
class View_Helper_PaginationLit extends Zend_View_Helper_Abstract {
	
	/**
	 * @var Zend_View
	 */
	protected $_view = null;
	
	/**
	 * Enter description here...
	 */
	public function init() {
		$this->_view = Zend_Registry::get ( 'view' );
	}
	
	private $_count_links = 5;
	
	public function paginationLit($url, $total, $onpage = 12, $current_page = 1, $up = 0) {
		
		if ($total) {
			
			$count_pages = ($onpage) ? ceil ( $total / $onpage ) : 1;
			$this->init ();
			$this->_view->count_pages = $count_pages;
			
			$this->_view->url = $url;
			$this->_view->current = $current_page;
			$end = $this->_count_links;
			$first = 1;
			
			if ($count_pages > $this->_count_links && $current_page < $count_pages) {
				$first = floor ( $current_page / $this->_count_links ) * $this->_count_links;
				$end = $first + $this->_count_links;
				$end = $end < $total ? $end : $total;
			
			} elseif ($current_page == $count_pages && $count_pages > $total) {
				$first = $count_pages - $this->_count_links;
				$end = $count_pages;
			}
			$this->_view->end = $end;
			$this->_view->first = $first;
			$this->_view->onpage = $onpage;
			$this->_view->first_item = $current_page * $onpage - $onpage + 1;
			$this->_view->last_item = ($current_page * $onpage <= $total && $onpage != 0) ? $current_page * $onpage : $total;
			$this->_view->total = $total;
			$this->_view->up = $up;
			
			return $this->_view->render ( 'PaginationLit.phtml' );
		}
		return '';
	}
}