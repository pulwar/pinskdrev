<?php

class PagesOptions_Row extends Zend_Db_Table_Row {
	
	public function delete() {
		PagesOptions_Cache::clean();
		return parent::delete();
	}
	
	/**
	 * Allows post-update logic to be applied to row.
	 * Subclasses may override this method.
	 *
	 * @return void
	 */
	protected function _postUpdate() {
		PagesOptions_Cache::clean();
	}
	
	/**
	 * Allows post-insert logic to be applied to row.
	 * Subclasses may override this method.
	 *
	 * @return void
	 */
	protected function _postInsert() {
		PagesOptions_Cache::clean();
	}
}