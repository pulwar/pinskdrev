<?php
class pages_IndexController extends DefaultController {
    /**
     * Шаблон страницы
     * @var zend_layout
     */
    private $layout = null;

    /**
     * Язык страницы
     * @var string
     */
    private $lang = null;

    /**
     * Объект страницы
     * @var Pages_row
     */
    private  $_page = null;


    public function init() {        
        $action = $this->getRequest()->getActionName();
        if($action != 'gruzcalc'){
        $id = $this->_getParam('id');
        $page = Pages_Cache::getInstance()->find($this->_getParam('id'))->current();

            if(is_null($page) || $page->published == '0') {
                $this->_redirect('/404');
            }
        
        $this->lang = $this->_getParam('lang', 'ru');
        $this->_page = $page;

        $this->view->content = $page->content;
        $this->view->options = $options = PagesOptions_Cache::getInstance()->getPageOptions($id);
        $this->view->placeholder('title')->set($options->title);
        $this->view->placeholder('keywords')->set($options->keywords);
        $this->view->placeholder('descriptions')->set($options->descriptions);
        $this->view->placeholder('h1')->set($options->h1);
        $this->view->placeholder('id_page')->set($id);
        $this->layout = $this->view->layout();
        if($page->id==1298) {
            $this->layout->setLayout("front/coupon");
        } else {
            $this->layout->setLayout("front/default");
        }
        $this->layout->current_type = 'pages';
        $this->layout->lang = $this->lang;
        $this->layout->page = $this->_page;
        $this->layout->id_object = $this->_page->id;
        if ($page->show_childs==1) {
            $this->layout->page_childs = Pages_Cache::getInstance()->getChildrenAndURLs($page->id);
        }
        }
    }

    /**
     * Открытие страницы с переданным id
     */
    public function pageAction() {        
    }

    /**
     *  Открытие главной страницы
     */
    public function mainAction() {
        $this->layout->setLayout("front/main");
        $this->layout->softs_main = Catalog_Product::getInstance()->getElite('show_on_main');
        $this->layout->special_main = Catalog_Product::getInstance()->getElite('special');
        $this->layout->intro = Pages::getInstance()->getMainText();
        $this->layout->main_news = News::getInstance()->getNews();
        $this->layout->main_reviews_big = Reviews::getInstance()->getReviewsBig();
        $this->layout->main_reviews = Reviews::getInstance()->getReviews();
        $this->layout->types = Catalog_Product_Mebeltype::getInstance()->getAllActive();
        $this->layout->material = Catalog_Product_Material::getInstance()->getAllActive();
        $this->layout->colors = Catalog_Product_Color::getInstance()->getAllColors();
        $this->layout->checks = Catalog_Product_Default::getInstance()->getCheck();
    }

    /**
     * Карта сайта
     */
    public function sitemapAction() {
        $this->view->sitemap = Pages::getInstance()->getSitemapWithCatalog($this->lang,1,1,973,1);
    }
    
    public function deliveryAction(){
        $this->view->towns = GruzTowns::getTowns();
    }
    public function gruzcalcAction(){
		$result = null;
		$params = $this->_request->getPost();
		$url = "http://pecom.ru/bitrix/components/pecom/calc/ajax.php";
		$client = new Zend_Http_Client($url . "?" . $params['msg']);
		$response = $client->request(Zend_Http_Client::GET);
		$result = $response->getBody();
		$result_array = json_decode($result, true);
		$result = json_encode($result_array);
		echo $result;
		exit();
    }
    public function error404Action() {
        header("HTTP/1.0 404 Not Found");
    }

    /**
     * Обратная связь
     */
    public function feedbackAction() {
        $this->layout->setLayout("front/one_col");
        $this->view->form = $this->_getParam('form', array());
    }
}
