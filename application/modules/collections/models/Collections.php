<?php


class Collections extends Zend_Db_Table {

    protected $_name = 'site_collections';
    protected $_primary = array('id');	
	protected $_sequence = true;
    protected static $_instance = null;
    /**
     * 
     * @var Zend_Cache_Frontend_Class
     */
    protected $_cache = null;


    /**
	 * Class to use for rows.
	 *
	 * @var string
	 */


    /**
     * Singleton instance
     *
     * @return Collections
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
    
    /**
     * получение всех элементов в виде массива для formSelect
     * @return array
     */
    public function getItemsAsOptions() {
    	$select = $this->select()
    		->order('priority DESC');
    	$items = $this->fetchAll($select);
    	$options = array(0=> 'Нет');
    	if ($items->count()){
    		foreach ($items as $item){
    			$options[$item->id] = $item->title;
    		}
    	}	
    	return $options;
    }
	public function getActiveItemsAsOptions() {
    	$select = $this->select()
			->where('active = 1')
    		->order('priority DESC');
    	$items = $this->fetchAll($select);
    	$options = array(0=> 'Нет');
    	if ($items->count()){
    		foreach ($items as $item){
    			$options[$item->id] = $item->title;
    		}
    	}	
    	return $options;
    }

    public function getCollectionsByDivId($id_division) {
        $maker_ids = Catalog_Product::getInstance()->getCollectionsIdsByDivId($id_division);
        
        $ids=array();
        $items=array();

        $ids=$maker_ids->toArray();
        if($maker_ids->count()){
        $select = $this->select()
                ->order('priority DESC')
                ->where("`id` IN (?)", $ids);
        $items = $this->fetchAll($select);
        }
        $options = array(0=> 'Все');
    	if (count($items)){
    		foreach ($items as $item){
    			$options[$item->id] = $item->title;
    		}
    	}
        return $options;
    }

    public function getPublicCollections($count=null, $offset=null){
        $select = $this->select();
        $select->from($this->_name);
        $select->where('active =?', 1);
        $select->order('priority DESC');
        $select->limit($count, $offset);
        return $this->fetchAll($select);
    }

	
    public function getCountPublicCollections(){
        $select = $this->select();
		$select->from($this->_name, 'COUNT(*) as count');
		$select->where('active = ?', 1);
		return $this->getAdapter()->fetchOne($select);
    }

    public function getPublicItem($id){
        $select = $this->select();
        $select->from($this->_name);
        $select->where('id =?', $id);
        $select->where('active =?', 1);

        return $this->fetchRow($select);
    }
}