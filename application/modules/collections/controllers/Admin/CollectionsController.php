<?php

/**
 * Collections_Admin_CollectionsController
 * 
 * @author Grover
 * @version 1.0
 */

class Collections_Admin_CollectionsController extends MainAdminController  {
	
	/**
 	 * @var string
 	 */
	private $_curModule = null;	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_onpage = 50;
	
	/**
	 * items per page
	 *
	 * @var int
	 */
	private $_lang = null;
	
	private $_page = null;
	/**
	 * offset
	 *
	 * @var int
	 */	
	private $_offset = null;	
	
	public function init(){
		//$this->initView();
		$this->layout = $this->view->layout();
		$this->layout->title = "Коллекции";
		$this->view->caption = 'Список коллекций';
		$lang = $this->_getParam('lang','ru');		
		
		$this->view->currentModule = $this->_curModule = SP.$this->getRequest()->getModuleName().SP.$lang.SP.$this->getRequest()->getControllerName();
		$this->_page = $this->_getParam('page', 1);	
		$this->_offset =($this->_page-1)*$this->_onpage;	
		$this->view->current_page = $this->_page;
		$this->view->onpage = $this->_onpage;
	}
	
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		$this->layout->action_title = "Список элементов";		
		$this->view->currentModule = $this->_curModule;		
		$where = null;
		$this->view->total = count(Collections::getInstance()->fetchAll($where));		
		//$this->view->all = $peoples =  Catalog_Params::getInstance()->fetchAll($where, 'priority DESC', (int)$this->_onpage, (int)$this->_offset);
		$this->view->all = $peoples =  Collections::getInstance()->fetchAll($where, 'priority DESC', (int)$this->_onpage, (int)$this->_offset);		
		
	}
	
	public function editAction(){
		$id = $this->_getParam('id', '');
		if ($id){
			$item = Collections::getInstance()->find($id)->current();
			$this->layout->action_title = "Редактировать элемент";	
		} else{
			$item = Collections::getInstance()->fetchNew();
			$this->layout->action_title = "Создать элемент";	
		}
			
		if ($this->_request->isPost()){						
			$data = $this->trimFilter($this->_getAllParams());			
			if ($data['title']!='' ){
				$item->setFromArray(array_intersect_key($data, $item->toArray()));
				$item->save();
				$this->view->ok=1;
			} else{
				$this->view->err=1;				
			}
			
		}
		$this->view->item = $item;
	}
	
	/**
	 * изменение активности элемента
	 *
	 */
	public function activeAction(){
		if($this->_hasParam('id')){
			$id = (int)$this->getRequest()->getParam('id');
			$page = Collections::getInstance()->find($id)->current();			
			if (!is_null($page)){
				$page->active =  abs($page->active-1);
				$page->save();
			}
						
		}
		$this->_redirect($this->_curModule);
	}

	
	public function deleteAction(){
		$id = $this->_getParam('id');
		if ($id ){
			$item = Collections::getInstance()->find($id)->current();
			if ($item!=null){
				$item->delete();
			}
		}
		$this->_redirect($this->_curModule);
	}
}