<?
define('ROOT_DIR', dirname(__FILE__) . DIRECTORY_SEPARATOR);

if (isset($_REQUEST['img']) && is_file(dirname(__FILE__) . $_REQUEST['img'])) {
	$img_src = dirname(__FILE__) . $_REQUEST['img'];
	
	$cashe_path = ROOT_DIR . "cached_images/";
	$cached_filename = md5($img_src . filemtime($img_src)) . '.jpg';
	$path_to_cache_file = $cashe_path . 'protect';
	$cache = $path_to_cache_file . '/' . $cached_filename;
	
	if (!file_exists($cache)) {
		if (!is_dir($path_to_cache_file)) {
			@mkdir($path_to_cache_file);
		}
		
		$text = ROOT_DIR . 'text3.png';
		$logo = ROOT_DIR . 'logo.png';
		
		$text_source = imagecreatefrompng($text);
		$text_size = getimagesize($text);
		
		$logo_source = imagecreatefrompng($logo);
		$logo_size = getimagesize($logo);
		
		$img_size = getimagesize($img_src);
		$format = strtolower(substr($img_size['mime'], strpos($img_size['mime'], '/') + 1));
		$icfunc = "imagecreatefrom" . $format;
		$scfunc = "image" . $format;
		$img_source = $icfunc($img_src);
		
		$x_text = ($img_size[0] - $text_size[0]) / 2;
		$y_text = ($img_size[1] - $text_size[1]) / 2;
		$x_logo = ($img_size[0] - $logo_size[0]);
		$y_logo = ($img_size[1] - $logo_size[1]);
		if (imagecopy($img_source, $text_source, $x_text - 15, $y_text, 0, 0, $text_size[0], $text_size[1]) && imagecopy($img_source, $logo_source, $x_logo, $y_logo, 0, 0, $logo_size[0], $logo_size[1])) {
			$scfunc($img_source, $cache);
		}
		imagedestroy($img_source);
		imagedestroy($text_source);
		imagedestroy($logo_source);
	}
	
	date_default_timezone_set('Europe/Minsk');
	ob_clean();
	header('Content-type: image/jpeg');
	header('Content-Length: ' . filesize($cache));
	header('Accept-Ranges: bytes');
	header('ETag: ' . sha1_file($cache));
	header('Last-Modified: ' . date(DATE_RSS), filemtime($img_src));
	header('Expires: ' . date(DATE_RSS, time() + 604800));
	header('Cache-Control: max-age=604800, public');
	echo file_get_contents($cache);
}
