<?
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_DIR', dirname(__FILE__) . DS);

define('LIBRARY_DIR', ROOT_DIR . 'application' . DS . 'library' . DS);
set_include_path(get_include_path() . PATH_SEPARATOR . LIBRARY_DIR);

require_once ('Zend/Session/Namespace.php');
require_once ('Zend/Session.php');
require_once ("Zend/Db/Table/Abstract.php");
require_once ('Zend/Auth/Adapter/DbTable.php');
require_once ('Zend/Auth/Storage/Session.php');
require_once ('Zend/Auth.php');
require_once ("Zend/Db/Expr.php");

$auth = Zend_Auth::getInstance();
//$auth->setStorage(new Zend_Auth_Storage_Session('frontend'));
if (! $auth->hasIdentity())
	header('Location: /404');
$user = $auth->getIdentity();
if ($user == null || ! $user->username || $user->username == "guest")
	header('Location: /404');

if (isset($_REQUEST['img']) && is_file(dirname(__FILE__) . $_REQUEST['img'])) {
	$img_src = dirname(__FILE__) . $_REQUEST['img'];
	$img_size = getimagesize($img_src);
	$new_width = isset($_REQUEST['w']) ? $_REQUEST['w'] : $img_size[0];
	$new_height = isset($_REQUEST['h']) ? $_REQUEST['h'] : $img_size[1];
	$format = strtolower(substr($img_size['mime'], strpos($img_size['mime'], '/') + 1));
	$icfunc = "imagecreatefrom" . $format;
	$scfunc = "image" . $format;
	$img_source = $icfunc ( $img_src );
	
	$x_ratio = $new_width / $img_size[0];
	$y_ratio = $new_height / $img_size[1];
		  
	$ratio = min($x_ratio, $y_ratio);
	$use_x_ratio = ($x_ratio == $ratio);
	
	$new_width = $use_x_ratio  ? $new_width  : floor($img_size[0] * $ratio);
	$new_height = !$use_x_ratio ? $new_height : floor($img_size[1] * $ratio);
	
	header('content-type: image/' . $format);
	//echo file_get_contents($img_src);

	$idest = imagecreatetruecolor($new_width, $new_height);
	imagefill($idest, 0, 0, $rgb = 0xFFFFFF);
	imagecopyresampled($idest, $img_source, 0, 0, 0, 0, $new_width, $new_height, $img_size[0], $img_size[1]);
	$scfunc($idest);
	
	imagedestroy($img_source);
	//imagedestroy($text_source);
}
