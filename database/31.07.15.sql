CREATE TABLE IF NOT EXISTS `site_catalog_product_package` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_tovar` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `priority` int(5) DEFAULT '0',
  `depth` float DEFAULT '0',
  `width_p` float DEFAULT '0',
  `height` float DEFAULT '0',
  `weight` float DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;